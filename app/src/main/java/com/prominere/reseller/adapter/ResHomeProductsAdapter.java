package com.prominere.reseller.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.MyCatalogs;
import com.prominere.reseller.activity.reseller.ProductDetailsActivity;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.ProductModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResHomeProductsAdapter  extends BaseAdapter {


    private final Activity activity;
    private final ArrayList<ProductModel> NoOfItems;
    private final ListView LvProduct;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private LinearLayout LLItem;
    private ImageView imgItem2,imgItem1,imgItem3;
    private RequestQueue mQueue;
    private TextView TvWishItem,TvProdCategory,TvProduct, TvPrice;
    private LinearLayout LLMoreImages;

    public ResHomeProductsAdapter(Activity activity, ArrayList<ProductModel> productList, ListView lvProduct) {
        this.activity = activity;
        this.NoOfItems=productList;
        this.LvProduct=lvProduct;
    }



    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.mycatalog_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
        mQueue = Volley.newRequestQueue(activity);


        final ProductModel model = NoOfItems.get(position);

        LLItem = (LinearLayout) convertView.findViewById(R.id.llItem);
        imgItem1 = (ImageView) convertView.findViewById(R.id.img1);
        imgItem2 = (ImageView) convertView.findViewById(R.id.img2);
        imgItem3 = (ImageView) convertView.findViewById(R.id.img3);
        TvWishItem = (TextView) convertView.findViewById(R.id.tvLoveSymb);   TvWishItem.setTypeface(fontAwesomeFont);
        TvProdCategory = (TextView) convertView.findViewById(R.id.tvProductCategory);
        TvProduct = (TextView) convertView.findViewById(R.id.tvProduct);
        TvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
        LLMoreImages = (LinearLayout) convertView.findViewById(R.id.llMoreImages);

        TvProdCategory.setText(model.getCategory());
        TvProduct.setText(model.getTitle());
        TvPrice.setText("₹ "+model.getDiscountprice()+".00");

        if(model.getWishliststatus().equalsIgnoreCase("0")){
            TvWishItem.setText(activity.getResources().getString(R.string.fai_lovewb));
        }else{
            TvWishItem.setText(activity.getResources().getString(R.string.fai_love));
        }

        LLItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(activity, ProductDetailsActivity.class);
                in.putExtra(AppConstants.productid,model.getId());
//                     in.putExtra(AppConstants.producttitle,model.getCategory());
                activity.startActivity(in);
            }
        });
        TvWishItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getWishliststatus().equalsIgnoreCase("0"))
                {
                    AddToWishList(model);
                }else{
                    RemoveWishList(model);
                }

            }
        });

        if(!model.getImage().isEmpty()) {

            List<String> imgsCollestion = AppHelper.getImageURL(model.getImage() );

            if(imgsCollestion.size()>3){
                LLMoreImages.setVisibility(View.VISIBLE);
            }else{
                LLMoreImages.setVisibility(View.GONE);
            }
            for(int i=0;i<imgsCollestion.size();i++){
                String img = imgsCollestion.get(i);
                Log.i("img:-->"+i," "+img);
                if(img != null && !img.isEmpty()){

                    switch (i){
                        case 0 :  Glide.with(activity).load(img).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform()).into(imgItem1); // loadImages(img,imgItem1);
                                  break;
                        case 1 :  Glide.with(activity).load(img).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform()).into(imgItem2); // loadImages(img,imgItem2);
                                  break;
                        case 2 :   Glide.with(activity).load(img).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform()).into(imgItem3); // loadImages(img,imgItem3);
                                  break;

                    }
                }
            }
        }


        return convertView;
    }

    private void AddToWishList(final ProductModel model) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// {"status":"success","message":"Product added to wishlist"}
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("AddToWishList:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                SimpleToast.ok(activity,mainObj.getString("message"));
                                TvWishItem.setText(activity.getResources().getString(R.string.fai_love));
                                model.setWishliststatus("1");
                                notifyDataSetChanged();

                            }else{
                                SimpleToast.error(activity,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.addtowishlist);
                params.put("productid",model.getId());
                params.put("userid", SessionSave.getsession(AppConstants.userid,activity));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }
    private void RemoveWishList(final ProductModel model) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// {"status":"success","message":"Product added to wishlist"}
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("AddToWishList:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                SimpleToast.ok(activity,mainObj.getString("message"));
                                TvWishItem.setText(activity.getResources().getString(R.string.fai_lovewb));
                                model.setWishliststatus("0");
                                notifyDataSetChanged();

                            }else{
                                SimpleToast.error(activity,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.removefromwishlist);
                params.put("productid",model.getId());
                params.put("userid", SessionSave.getsession(AppConstants.userid,activity));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }

    private void loadImages(String url, ImageView img) {
        if(!url.isEmpty()) {
//            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);

            AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
            builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
            final AlertDialog alertDialog = builder.show();

            Picasso.with(activity)
                    .load(url)
                    .into(img, new Callback() {
                        @Override
                        public void onSuccess() {
                            if(alertDialog.isShowing()){alertDialog.dismiss();}
//                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            if(alertDialog.isShowing()){alertDialog.dismiss();}
                        }
                    });

        }

    }

}
