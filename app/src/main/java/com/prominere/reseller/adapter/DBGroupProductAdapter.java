package com.prominere.reseller.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.CartItemsActivity;
import com.prominere.reseller.activity.reseller.ProductDetailsActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.DBGroupProductModel;
import com.prominere.reseller.util.share.AndroidShare;
import com.prominere.reseller.util.share.AndroidSharePlatform;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.prominere.reseller.helper.AppHelper.getBitmapFromURL;

public class DBGroupProductAdapter extends BaseAdapter {
    private final Activity activity;
    private final List<DBGroupProductModel> NoOfItems;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private ImageView imgItem;
    private TextView DiscPercentage,DiscPrice,productPrice, productTitle;
    private LinearLayout LLShareToOtherApp,LLShareWhatsApp,LLItem;

    private static String[] PERMISSIONS_STORAGE = { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE  };
    private static final int REQUEST_EXTERNAL_STORAGE = 2;
    private int downloadFileCount=0;
    private ArrayList<Uri> downloadFileList;

    public DBGroupProductAdapter(Activity activity, List<DBGroupProductModel> productList) {
        this.activity = activity;
        this.NoOfItems = productList;
    }


    @Override
    public int getCount() {
        return NoOfItems.size();
    }

    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.db_group_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
//        queue = Volley.newRequestQueue(activity);


        final DBGroupProductModel model = NoOfItems.get(position);

        LLItem = (LinearLayout) convertView.findViewById(R.id.llItem);
        imgItem = (ImageView) convertView.findViewById(R.id.img);
        productTitle = (TextView) convertView.findViewById(R.id.tvProduct);
        productPrice = (TextView) convertView.findViewById(R.id.tvPrice);
        DiscPrice = (TextView) convertView.findViewById(R.id.tvDiscountPrice);
        DiscPercentage = (TextView) convertView.findViewById(R.id.tvDiscPercentage);
        LLShareWhatsApp = (LinearLayout) convertView.findViewById(R.id.llShareWhatsApp);
        LLShareToOtherApp = (LinearLayout) convertView.findViewById(R.id.llShareToOtherApp);

        productTitle.setText(model.getTitle());
        productPrice.setText("Deal price: ₹" + model.getDiscountprice()+".00");
        DiscPrice.setText("₹"+model.getPrice()+".00");    DiscPrice.setPaintFlags(DiscPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        Double discPrice = Double.parseDouble(model.getPrice()) - Double.parseDouble(model.getDiscountprice());

        Double discperc = (discPrice/Double.parseDouble(model.getPrice()))*100;
        DecimalFormat df = new DecimalFormat("#.##");
        String dx=df.format(discperc);
        discperc=Double.valueOf(dx);
        DiscPercentage.setText(""+discperc+"% Off");

        LLItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLItem);
                    Intent in = new Intent(activity, ProductDetailsActivity.class);
                    in.putExtra(AppConstants.productid, model.getId());
                    activity.startActivity(in);
                    activity.finish();
            }
        });
        LLShareWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLShareWhatsApp);


                if (AppHelper.isStoragePermissionGranted(activity)) {
                    downloadFileCount = 0;
                    downloadFileList = new ArrayList<Uri>();
                    List<String> imgsCollestion = AppHelper.getImageURL(model.getImage());
//                    for(int i=0;i<imgsCollestion.size();i++) {
//                        String img = imgsCollestion.get(i);
//                        Log.i("img:-->" + i, " " + img);
                    if (imgsCollestion.get(0) != null && !imgsCollestion.get(0).isEmpty() ) {
                        downloadImages(imgsCollestion.get(0),model,2);



                        }
//                     }
                    } else {
                        ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
                    }
            }
        });
        LLShareToOtherApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLShareToOtherApp);
                if (AppHelper.isStoragePermissionGranted(activity)) {
                    downloadFileCount = 0;
                    downloadFileList = new ArrayList<Uri>();
                    List<String> imgsCollestion = AppHelper.getImageURL(model.getImage());
                    if (imgsCollestion.get(0) != null && !imgsCollestion.get(0).isEmpty()) {
                        downloadImages(imgsCollestion.get(0),model,1);
                      }
                 }else {
                        ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
                }
            }
        });


        if (!model.getImage().isEmpty()) {
//            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);
            List<String> imgUrls = AppHelper.getImageURL(model.getImage());
            String sImg = imgUrls.get(0);

            Glide.with(activity).load( sImg ).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(imgItem);
//            Picasso.with(activity)
//                    .load(model.getImage())
//                    .into(imgItem, new Callback() {
//                        @Override
//                        public void onSuccess() {
////                            progressbar.setVisibility(View.GONE);
//                        }
//
//                        @Override
//                        public void onError() {
//
//                        }
//                    });

        }


        return convertView;
    }

    private void ShareImageToApps(final Bitmap bm,final DBGroupProductModel model,final int cond) {
        try {
            Log.i("onBitmapLoaded:-->","Loaded");
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/Reeseller");

            if (!myDir.exists()) {
                myDir.mkdirs();
            }

            String name = new Date().toString() + ".jpg";
//                                  String name =  "1.JPG";
            Log.i("name:-->",""+name);
            myDir = new File(myDir, name);
            FileOutputStream out = new FileOutputStream(myDir);
            bm.compress(Bitmap.CompressFormat.JPEG, 90, out);

//                                  SimpleToast.ok(activity,"Downloaded successfully");

            out.flush();
            out.close();
//            activity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {

                    String imgBitmapPath = MediaStore.Images.Media.insertImage(activity.getContentResolver(),bm,"title",null);
                    Uri imgBitmapUri = Uri.parse(imgBitmapPath);
                    downloadFileList.add(imgBitmapUri);




                    AndroidShare.Builder shareBuilder = new AndroidShare
                            .Builder(activity)

                            .setContent(model.getTitle())
                            .setTitle(" Title: "+model.getTitle());
//                            shareBuilder.setImageUris(downloadFileList);
                    shareBuilder.setImageUri(imgBitmapUri);
                    if(cond == 2){
                        shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
                    }
                    shareBuilder.build().share();

//                }
//            });


        } catch(Exception e){
            // some action
        }


    }

    private void downloadImages(String ImageUrl, final DBGroupProductModel model, final int cond) {

        Log.i("Image URL:--> ",""+ImageUrl);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();


//        new AsyncGettingBitmapFromUrl(ImageUrl,model,cond).execute();


        Glide.with(activity)
                .asBitmap()
                .load(ImageUrl)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull final Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                        try {
                            Log.i("onBitmapLoaded:-->","onResourceReady");
                            String root = Environment.getExternalStorageDirectory().toString();
                            File myDir = new File(root + "/Reeseller");

                            if (!myDir.exists()) {
                                myDir.mkdirs();
                            }

                            String name = new Date().toString() + ".jpg";
//                                  String name =  "1.JPG";
                            Log.i("name:-->",""+name);
                            myDir = new File(myDir, name);
                            FileOutputStream out = new FileOutputStream(myDir);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);

//                                  SimpleToast.ok(activity,"Downloaded successfully");

                            out.flush();
                            out.close();
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    String imgBitmapPath = MediaStore.Images.Media.insertImage(activity.getContentResolver(),bitmap,"title",null);
                                    Uri imgBitmapUri = Uri.parse(imgBitmapPath);
                                    downloadFileList.add(imgBitmapUri);

                                    List<String> imgsCollestion = AppHelper.getImageURL(model.getImage());
                                    Log.i("fCount:-->",""+downloadFileCount+"  "+imgsCollestion.size() );




                                    if(cond == 2){
                                        AndroidShare.Builder shareBuilder = new AndroidShare
                                                .Builder(activity)
                                                .setContent(model.getTitle())
                                                .setTitle(" Title: "+model.getTitle());
//                            shareBuilder.setImageUris(downloadFileList);
                                        shareBuilder.setImageUri(imgBitmapUri);
                                        shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
                                        shareBuilder.build().share();

                                        if(alertDialog.isShowing()){  alertDialog.dismiss();  }

                                    }else{

                                        // Use package name which we want to check
                                        boolean isAppInstalled = AppHelper.checkAppInstalledOrNot(activity,"com.instagram.android");

                                        if(isAppInstalled) {
                                            //This intent will help you to launch if the package is already installed

                                            openBottomShareView(model.getTitle(),imgBitmapUri);
                                            Log.i("CheckINST:","Application is already installed.");
                                        } else {
                                            AndroidShare.Builder shareBuilder = new AndroidShare
                                                    .Builder(activity)
                                                    .setContent(model.getTitle())
                                                    .setTitle(" Title: "+model.getTitle());
//                            shareBuilder.setImageUris(downloadFileList);
                                            shareBuilder.setImageUri(imgBitmapUri);
                                            shareBuilder.build().share();
                                            Log.i("CheckINST:","Application is not currently installed.");
                                        }

                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                if(alertDialog.isShowing()){  alertDialog.dismiss();  }
                                            }
                                        },3000);
                                    }






                                }
                            });


                        } catch(Exception e){ }

                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                        Log.i("onBitmapLoaded:-->","onLoadCleared");
                    }
                });


//        Picasso.with(activity)
//                .load(ImageUrl)
//                .into(new Target() {
//                          @Override
//                          public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from)
//                          {
//
//                              try {
//                                  Log.i("onBitmapLoaded:-->","Loaded");
//                                  String root = Environment.getExternalStorageDirectory().toString();
//                                  File myDir = new File(root + "/Reeseller");
//
//                                  if (!myDir.exists()) {
//                                      myDir.mkdirs();
//                                  }
//
//                                  String name = new Date().toString() + ".jpg";
////                                  String name =  "1.JPG";
//                                  Log.i("name:-->",""+name);
//                                  myDir = new File(myDir, name);
//                                  FileOutputStream out = new FileOutputStream(myDir);
//                                  bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
//
////                                  SimpleToast.ok(activity,"Downloaded successfully");
//
//                                  out.flush();
//                                  out.close();
//                    activity.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            String imgBitmapPath = MediaStore.Images.Media.insertImage(activity.getContentResolver(),bitmap,"title",null);
//                            Uri imgBitmapUri = Uri.parse(imgBitmapPath);
//                            downloadFileList.add(imgBitmapUri);
//
//                            List<String> imgsCollestion = AppHelper.getImageURL(model.getImage());
//                            Log.i("fCount:-->",""+downloadFileCount+"  "+imgsCollestion.size() );
//
//
//
//                            AndroidShare.Builder shareBuilder = new AndroidShare
//                                    .Builder(activity)
//
//                                    .setContent(model.getTitle())
//                                    .setTitle(" Title: "+model.getTitle());
////                            shareBuilder.setImageUris(downloadFileList);
//                            shareBuilder.setImageUri(imgBitmapUri);
//                            if(cond == 2){
//                                shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
//                            }
//                            shareBuilder.build().share();
////new Handler().postDelayed(new Runnable() {
////    @Override
////    public void run() {
//        if(alertDialog.isShowing()){  alertDialog.dismiss();  }
////    }
////},5000);
//
//
//                        }
//                    });
//
//
//                              } catch(Exception e){ }
//                          }
//
//                          @Override
//                          public void onBitmapFailed(Drawable errorDrawable) {
//                              Log.i("onBitmapLoaded:-->","onBitmapFailed");
//                              if(alertDialog.isShowing()){  alertDialog.dismiss();  }
//                          }
//
//                          @Override
//                          public void onPrepareLoad(Drawable placeHolderDrawable) {
//                              Log.i("onBitmapLoaded:-->","onPrepareLoad");
////                              if(alertDialog.isShowing()){  alertDialog.dismiss();  }
////                              final Bitmap bitmap = drawableToBitmap(placeHolderDrawable);
////                              try {
////                                  Log.i("onBitmapLoaded:-->","Loaded");
////                                  String root = Environment.getExternalStorageDirectory().toString();
////                                  File myDir = new File(root + "/Reeseller");
////
////                                  if (!myDir.exists()) {
////                                      myDir.mkdirs();
////                                  }
////
////                                  String name = new Date().toString() + ".jpg";
//////                                  String name =  "1.JPG";
////                                  Log.i("name:-->",""+name);
////                                  myDir = new File(myDir, name);
////                                  FileOutputStream out = new FileOutputStream(myDir);
////                                  bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
////
//////                                  SimpleToast.ok(activity,"Downloaded successfully");
////
////                                  out.flush();
////                                  out.close();
////                                  activity.runOnUiThread(new Runnable() {
////                                      @Override
////                                      public void run() {
////
////                                          String imgBitmapPath = MediaStore.Images.Media.insertImage(activity.getContentResolver(),bitmap,"title",null);
////                                          Uri imgBitmapUri = Uri.parse(imgBitmapPath);
////                                          downloadFileList.add(imgBitmapUri);
////
////                                          List<String> imgsCollestion = AppHelper.getImageURL(model.getImage());
////                                          Log.i("fCount:-->",""+downloadFileCount+"  "+imgsCollestion.size() );
////
////
////
////                                          AndroidShare.Builder shareBuilder = new AndroidShare
////                                                  .Builder(activity)
////
////                                                  .setContent(model.getTitle())
////                                                  .setTitle(" Title: "+model.getTitle());
//////                            shareBuilder.setImageUris(downloadFileList);
////                                          shareBuilder.setImageUri(imgBitmapUri);
////                                          if(cond == 2){
////                                              shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
////                                          }
////                                          shareBuilder.build().share();
//////new Handler().postDelayed(new Runnable() {
//////    @Override
//////    public void run() {
////                                          if(alertDialog.isShowing()){  alertDialog.dismiss();  }
//////    }
//////},5000);
////
////
////                                      }
////                                  });
////
////
////                              } catch(Exception e){ }
//
//
//                          }
//                      }
//                );

    }

    private void openBottomShareView(final String title, final Uri imgBitmapUri) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(activity);
        final View parentView = activity.getLayoutInflater().inflate(R.layout.share_instagram_otherview,null);
        bottomSheetDialog.setContentView(parentView);
        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View)parentView.getParent());
        bottomSheetBehavior.setPeekHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,1000,activity.getResources().getDisplayMetrics() ));

// change the state of the bottom sheet
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetDialog.show();

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        TextView TvShareTitle = (TextView) parentView.findViewById(R.id.tvTitle);   TvShareTitle.setText(title);
        final LinearLayout LLInstagram = (LinearLayout) parentView.findViewById(R.id.llInstagram);
        final LinearLayout LLInstagramStories = (LinearLayout) parentView.findViewById(R.id.llInstagramStories); LLInstagramStories.setVisibility(View.GONE);
        final LinearLayout LLMore = (LinearLayout) parentView.findViewById(R.id.llMore);

        LLInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLInstagram);
                bottomSheetDialog.dismiss();
//                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//                shareIntent.setType("image/*");
//                shareIntent.putExtra(Intent.EXTRA_STREAM, imgBitmapUri); // set uri
//                shareIntent.setPackage("com.instagram.android");
//                activity.startActivity(shareIntent);


                Intent feedIntent = new Intent(Intent.ACTION_SEND);
                feedIntent.setType("image/*");
//                feedIntent.putExtra(Intent.EXTRA_STREAM, imgBitmapUri.get(0));
//                feedIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imgBitmapUri);
//                feedIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, (ArrayList<? extends Parcelable>) imgBitmapUri);
                feedIntent.putExtra(Intent.EXTRA_STREAM,  imgBitmapUri);
//                feedIntent.putExtra(Intent.EXTRA_TEXT, title);
                feedIntent.setPackage("com.instagram.android");

                Intent storiesIntent = new Intent("com.instagram.share.ADD_TO_STORY");
                storiesIntent.setDataAndType(imgBitmapUri,  "jpg");
                storiesIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                storiesIntent.setPackage("com.instagram.android");
                activity.grantUriPermission(
                        "com.instagram.android", imgBitmapUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);

                Intent chooserIntent = Intent.createChooser(feedIntent, title);
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {storiesIntent});
                activity.startActivity(chooserIntent);


            }
        });
        LLInstagramStories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLInstagramStories);
                bottomSheetDialog.dismiss();

                Intent storiesIntent = new Intent("com.instagram.share.ADD_TO_STORY");
                storiesIntent.setDataAndType(imgBitmapUri, "image/*");
                storiesIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                storiesIntent.setPackage("com.instagram.android");
                activity.grantUriPermission(
                        "com.instagram.android", imgBitmapUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                activity.startActivity(storiesIntent);
            }
        });
        LLMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLMore);
                bottomSheetDialog.dismiss();
                AndroidShare.Builder shareBuilder = new AndroidShare
                        .Builder(activity)
                        .setContent(title)
                        .setTitle(title);
//                            shareBuilder.setImageUris(downloadFileList);
                shareBuilder.setImageUri(imgBitmapUri);
                shareBuilder.build().share();

            }
        });


    }//bottomSheet

    public static Bitmap drawableToBitmap (Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        int width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : 1;
        int height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : 1;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }
    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            // Use methods on Context to access package-specific directories on external storage.
            // This way, you don't need to request external read/write permission.
            // See https://youtu.be/5xVh-7ywKpE?t=25m25s
            File file =  new File(activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            // **Warning:** This will fail for API >= 24, use a FileProvider as shown below instead.
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    private class AsyncGettingBitmapFromUrl extends AsyncTask<String, Void, Bitmap> {

        private final DBGroupProductModel model;
        String imageUrl;
        int cond;
        public AsyncGettingBitmapFromUrl(String imageUrl, DBGroupProductModel model, int cond) {
            this.imageUrl = imageUrl;
            this.model = model;
            this.cond = cond;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            System.out.println("doInBackground");

            Bitmap bitmap = null;

            bitmap = AppHelper.downloadImage(imageUrl);

            return bitmap;
        }

        @SuppressLint("WrongThread")
        @Override
        protected void onPostExecute(final Bitmap bm) {

            System.out.println("bitmap" + bm);
            try {
                Log.i("onBitmapLoaded:-->","Loaded");
                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/Reeseller");

                if (!myDir.exists()) {
                    myDir.mkdirs();
                }

                String name = new Date().toString() + ".jpg";
//                                  String name =  "1.JPG";
                Log.i("name:-->",""+name);
                myDir = new File(myDir, name);
                FileOutputStream out = new FileOutputStream(myDir);
                bm.compress(Bitmap.CompressFormat.JPEG, 90, out);

//                                  SimpleToast.ok(activity,"Downloaded successfully");

                out.flush();
                out.close();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String imgBitmapPath = MediaStore.Images.Media.insertImage(activity.getContentResolver(),bm,"title",null);
                        Uri imgBitmapUri = Uri.parse(imgBitmapPath);
                        downloadFileList.add(imgBitmapUri);

                        List<String> imgsCollestion = AppHelper.getImageURL(imageUrl);
                        Log.i("fCount:-->",""+downloadFileCount+"  "+imgsCollestion.size() );



                        AndroidShare.Builder shareBuilder = new AndroidShare
                                .Builder(activity)

                                .setContent(model.getTitle())
                                .setTitle(" Title: "+model.getTitle());
//                            shareBuilder.setImageUris(downloadFileList);
                        shareBuilder.setImageUri(imgBitmapUri);
                        if(cond == 2){
                            shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
                        }
                        shareBuilder.build().share();

                    }
                });


            } catch(Exception e){
                // some action
            }




        }
    }
}
