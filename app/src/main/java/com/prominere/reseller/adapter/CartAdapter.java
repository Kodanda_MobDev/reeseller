package com.prominere.reseller.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.CartItemsActivity;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.CartModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.numberPicker.Enums.ActionEnum;
import com.prominere.reseller.util.numberPicker.Interface.ValueChangedListener;
import com.prominere.reseller.util.numberPicker.NumberPicker;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import org.json.JSONObject;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.prominere.reseller.activity.reseller.CartItemsActivity.TvFirstOrderrVal;
import static com.prominere.reseller.activity.reseller.CartItemsActivity.TvCODCharges;
import static com.prominere.reseller.activity.reseller.CartItemsActivity.TvShippingCharges;
import static com.prominere.reseller.activity.reseller.CartItemsActivity.TvProductCharges;
import static com.prominere.reseller.activity.reseller.CartItemsActivity.TvTotalPrice;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {


    private final Activity activity;
    private final ArrayList<CartModel> NoOfItems;
    private RequestQueue mQueue;

    public CartAdapter(Activity activity, ArrayList<CartModel> productList) {
        this.activity = activity;
        this.NoOfItems=productList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.cartitem_lv_row,parent,false);
        mQueue = Volley.newRequestQueue(activity);

        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final CartModel model = NoOfItems.get(position);


        holder.productTitle.setText(model.getTitle());
        holder.productPrice.setText("₹"+model.getProductprice()+".00");
        Log.i("Cart:--> ",""+model.getTitle()+"  :  "+model.getProductprice() );

        int pp =   Integer.parseInt(model.getQuantity())*Integer.parseInt(model.getProductprice());
        int sc =   Integer.parseInt(model.getQuantity())*(model.getShipping_charge());
        int gs =   Integer.parseInt(model.getQuantity())*(model.getGst());

        holder.TvOtherCharges.setText("( ₹"+pp+".00 + "+"₹"+sc+".00 + "+"₹"+gs+".00 ) ");
//        Double sum =  Double.parseDouble(pp +(model.getShipping_charge())+model.getGst()+"");
        Double sum =  Double.parseDouble(pp +sc+gs+"");
        holder.TvTLPrice.setText("₹"+(sum) +"0" );



        String[] values = model.getAvail_attributes().split(",");

//        if(checkAlphabet){
            ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(activity, android.R.layout.simple_spinner_dropdown_item,values);
//                    activity.getResources().getStringArray(R.array.size_alphabets));
            holder.sizeAtrr.setAdapter(spinnerArrayAdapter);
//            holder.sizeAtrr.setSelection(AppHelper.setValueToSpinner(holder.sizeAtrr,model.getAttribute()));
            holder.sizeAtrr.setSelection(AppHelper.setValueToSpinner(holder.sizeAtrr,  model.getAttribute().replaceAll(",", "") ));
        holder.TvSizeAttribute.setText( model.getAttribute().replaceAll(",", "")  );
//        }else{
//            ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(activity, android.R.layout.simple_spinner_dropdown_item,values);
////                    activity.getResources().getStringArray(R.array.size_numbers));
//            holder.sizeAtrr.setAdapter(spinnerArrayAdapter);
//            holder.sizeAtrr.setSelection(AppHelper.setValueToSpinner(holder.sizeAtrr,model.getAttribute()));
//        }

        holder.sizeAtrr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                ((TextView) parent.getChildAt(0)).setGravity(Gravity.CENTER);
                ((TextView) parent.getChildAt(0)).setTextSize(activity.getResources().getDimension(R.dimen.padding2));
                model.setAttribute(holder.sizeAtrr.getSelectedItem().toString());
                holder.TvSizeAttribute.setText( model.getAttribute()  );
                cartAttributeUpdate(model,holder.sizeAtrr.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//        holder.sizeAtrr.setText(model.getAttribute());
        holder.NumbPicker.setValue(Integer.parseInt(model.getQuantity()));
        holder.NumbPicker.setValueChangedListener(new ValueChangedListener() {
            @Override
            public void valueChanged(int value, ActionEnum action) {

                cartUpdate(model,value);

                Log.d("Model PriceQuant: ", String.valueOf(model.getPrice()+" : "+value));
//                int cartUpdateCounter = Integer.parseInt(model.getQuantity());
//                cartUpdateCounter += 1;
                model.setQuantity(""+value);
                int pp =   Integer.parseInt(model.getQuantity())*Integer.parseInt(model.getProductprice());
                int sc =   Integer.parseInt(model.getQuantity())*(model.getShipping_charge());
                int gs =   Integer.parseInt(model.getQuantity())*(model.getGst());

                holder.TvOtherCharges.setText("( ₹"+pp+".00 + "+"₹"+sc+".00 + "+"₹"+gs+".00 ) ");
//                holder.TvOtherCharges.setText("( ₹"+pp+".00 + "+"₹"+model.getShipping_charge()+".00 + "+"₹"+model.getGst()+".00 ) ");
//                Double sum =  Double.parseDouble(pp +(model.getShipping_charge())+model.getGst()+"");
                Double sum =  Double.parseDouble(pp +sc+gs+"");
                holder.TvTLPrice.setText("₹"+(sum) +"0" );
                if(!holder.EtCashColletFrCust.getText().toString().equalsIgnoreCase("") || !holder.EtCashColletFrCust.getText().toString().isEmpty()){
                    if( sum > Double.parseDouble(holder.EtCashColletFrCust.getText().toString()) ){
                        CartItemsActivity.cartMAmt = true;
                        holder.EtCashColletFrCust.setError("Amount is lessthan product price");
                    }else{
                        CartItemsActivity.cartMAmt = false;
                        holder.EtCashColletFrCust.setError(null);
                    }
                }


                int total = 0; int shipping_charge = 0;  int codCharges = 0; int foVal = 0;
                int productCharges = 0; int scharge = 0; int gstcharge = 0;

                for(int i=0;i<NoOfItems.size();i++){
                    CartModel mdl = NoOfItems.get(i);
                    Log.i("Model:-->"+i,"  "+mdl.toString() );

                    int productPrice =   Integer.parseInt(mdl.getQuantity())*Integer.parseInt(mdl.getProductprice());
                    int sChargePrice =   Integer.parseInt(mdl.getQuantity())*(mdl.getShipping_charge());
                    int gstPrice =   Integer.parseInt(mdl.getQuantity())*(mdl.getGst());

                    productCharges = productCharges+ productPrice;
                    scharge = scharge+ sChargePrice;
                    gstcharge = gstcharge+ gstPrice;

                    total = total+(  productPrice + sChargePrice+gstPrice );
                    shipping_charge = shipping_charge+(Integer.parseInt(mdl.getQuantity() ) * mdl.getShipping_charge() );
                    codCharges = codCharges+( Integer.parseInt(mdl.getQuantity() ) * mdl.getGst() );
                    foVal = foVal+( mdl.getMargin() );

                }
                Log.d("totalcash", String.valueOf(total));
                TvProductCharges.setText("₹"+productCharges+".00");
                TvCODCharges.setText("₹"+codCharges+".00");
                TvShippingCharges.setText("₹"+shipping_charge+".00");
                TvFirstOrderrVal.setText("₹"+foVal+".00");
                TvTotalPrice.setText("₹"+total+".00");
            }
        });
        holder.TvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(holder.TvRemove);
                RemovCartItem(model);
            }
        });
        holder.EtCashColletFrCust.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().isEmpty()) {
                    model.setCtcType(true);
                    BigInteger numb = new BigInteger( editable.toString().trim());

                    // create int object
                    int res;
                    // compare bi1 with bi2
                    res = numb.compareTo(new BigInteger("0"));
                    if( res == 0 ){//Both values are equal
                        model.setCashToCollectMoney(Double.parseDouble("0"));
                        CartItemsActivity.cartMAmt = false;
                    }else if( res == 1 ){//First Value is greater
                        model.setCashToCollectMoney(Double.parseDouble(editable.toString()));

                        int pp =   Integer.parseInt(model.getQuantity())*Integer.parseInt(model.getProductprice());
                        Double sum =  Double.parseDouble(pp +(model.getShipping_charge())+model.getGst()+"");


                        if( Integer.parseInt(editable.toString()) < sum ){
                            CartItemsActivity.cartMAmt = true;
                            holder.EtCashColletFrCust.setError("Amount is lessthan product price");
                        }else{
                            CartItemsActivity.cartMAmt = false;
                            holder.EtCashColletFrCust.setError(null);
                        }

                    }else if( res == -1 ){//Second value is greater

                    }
                }else{
                    model.setCashToCollectMoney(Double.parseDouble("0"));
                    model.setCtcType(false);
                    CartItemsActivity.cartMAmt = false;
                }
            }
        });

        if(!model.getImage().isEmpty()) {
            AppHelper.setImageByUrl(activity,model.getImage(),holder.imgItem);
        }




    }//onBindViewHolder



    @Override
    public int getItemCount() {
        return NoOfItems.size();
    }

    public ArrayList<CartModel> getcartLsit() {
        return NoOfItems;
    }

    public  class ViewHolder extends RecyclerView.ViewHolder {
        private final NumberPicker NumbPicker;
        private final Typeface fontAwesomeFont;
        private final EditText EtCashColletFrCust;
        ImageView imgItem;
        TextView TvSizeAttribute,TvOtherCharges,TvTLPrice,productTitle,productPrice,TvRemove;
        Spinner sizeAtrr;
        LinearLayout LLItem;
        public ViewHolder(View itemView) {
            super(itemView);
            fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");

            LLItem = (LinearLayout) itemView.findViewById(R.id.llItem);
            imgItem = (ImageView) itemView.findViewById(R.id.img);

            TvOtherCharges = (TextView) itemView.findViewById(R.id.tvOtherCharges);
            TvSizeAttribute = (TextView) itemView.findViewById(R.id.tvSizeAttribute);
            productTitle = (TextView) itemView.findViewById(R.id.tvProduct);
            TvTLPrice = (TextView) itemView.findViewById(R.id.tvTLPrice);
            productPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            sizeAtrr = (Spinner) itemView.findViewById(R.id.tvSize);
            TvRemove = (TextView) itemView.findViewById(R.id.tvRemove);          TvRemove.setTypeface(fontAwesomeFont);
            NumbPicker =(NumberPicker)itemView.findViewById(R.id.numbPicker);
            EtCashColletFrCust = (EditText)itemView.findViewById(R.id.etCashColletFromCust);

        }
    }


    private static boolean isLetterOrDigit(char c) {
        return (c >= 'a' && c <= 'z') ||
                (c >= 'A' && c <= 'Z') ;
    }

    private void RemovCartItem(final CartModel model) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog, null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->", url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        Log.e("AddToWishList:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if (mainObj.getString("status").equalsIgnoreCase("success")) {
//
//                                for(int i=0;i<NoOfItems.size();i++){
//                                    if(model.getId().equalsIgnoreCase(NoOfItems.get(i).getId()) ){
//
//                                    }
//                                }
//
                                Intent in = new Intent(activity, CartItemsActivity.class);
                                activity.startActivity(in);
                                activity.finish();

                            } else {
                                SimpleToast.error(activity, AppConstants.apiIssueMsg);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("action", ApiHelper.removefromcart);
                params.put("cartid", model.getId());
                params.put("userid", SessionSave.getsession(AppConstants.userid, activity));
                params.put("quantity",model.getQuantity());
                Log.i("Ob:--> ", params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return AppConstants.volleyTimeOut;
            }

            @Override
            public int getCurrentRetryCount() {
                return AppConstants.volleyTimeOut;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
            }
        });

        mQueue.add(sr);
    }
    private void cartUpdate(final CartModel model, final int val) {

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->", url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("AddToWishList:", "success! response: " + response.toString());
//                        try {
//                            JSONObject mainObj = new JSONObject(response);
//                            if (mainObj.getString("status").equalsIgnoreCase("success")) {
//                                Intent in = new Intent(activity, CartItemsActivity.class);
//                                activity.startActivity(in);
//                                activity.finish();
//
//                            } else {
//                                SimpleToast.error(activity, AppConstants.apiIssueMsg);
//                            }
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("action", ApiHelper.cart_quantityupdate);
                params.put("cartid", model.getId());
                params.put("userid", SessionSave.getsession(AppConstants.userid, activity));
                params.put("quantity",""+val);
                Log.i("Ob:--> ", params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return AppConstants.volleyTimeOut;
            }

            @Override
            public int getCurrentRetryCount() {
                return AppConstants.volleyTimeOut;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
            }
        });

        mQueue.add(sr);
    }

    private void cartAttributeUpdate(final CartModel model, final String val) {

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->", url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("UpdateAttribute: ", "success! response: " + response.toString());
//                        try {
//                            JSONObject mainObj = new JSONObject(response);
//                            if (mainObj.getString("status").equalsIgnoreCase("success")) {
//                                Intent in = new Intent(activity, CartItemsActivity.class);
//                                activity.startActivity(in);
//                                activity.finish();
//
//                            } else {
//                                SimpleToast.error(activity, AppConstants.apiIssueMsg);
//                            }
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("action", ApiHelper.cart_attributeupdate);
                params.put("cartid", model.getId());
                params.put("userid", SessionSave.getsession(AppConstants.userid, activity));
                params.put("attribute",""+val);
                Log.i("Ob:--> ", params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return AppConstants.volleyTimeOut;
            }

            @Override
            public int getCurrentRetryCount() {
                return AppConstants.volleyTimeOut;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
            }
        });

        mQueue.add(sr);
    }

    public static void selectSpinnerItemByValue(Spinner spnr, long value) {
        SimpleCursorAdapter adapter = (SimpleCursorAdapter) spnr.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            if(adapter.getItemId(position) == value) {
                spnr.setSelection(position);
                return;
            }
        }
    }


}
