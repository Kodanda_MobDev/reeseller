package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.ProductDetailsActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.ProductModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SearchAdapter  extends BaseAdapter {

    private final Activity activity;
    private final ArrayList<ProductModel> NoOfItems;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private LinearLayout LLItem;
    private ImageView ImgItem,ImgLove;
    private TextView productTitle;
    private TextView productDiscPrice,productPrice;

    public SearchAdapter(Activity activity, ArrayList<ProductModel> productList) {
        this.activity = activity;
        this.NoOfItems=productList;
    }



    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.search_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
//        queue = Volley.newRequestQueue(activity);


        final ProductModel model = NoOfItems.get(position);

        LLItem = (LinearLayout) convertView.findViewById(R.id.llItem);
        ImgItem = (ImageView) convertView.findViewById(R.id.imgProduct);
        ImgLove = (ImageView) convertView.findViewById(R.id.imgLove);
        productTitle = (TextView) convertView.findViewById(R.id.tvProduct);
        productPrice = (TextView) convertView.findViewById(R.id.tvPrice);
        productDiscPrice = (TextView) convertView.findViewById(R.id.tvDiscountPrice);

        productTitle.setText(model.getTitle());
        productPrice.setText("₹"+model.getDiscountprice()+".00");
        productDiscPrice.setText("₹"+model.getPrice()+".00");  productDiscPrice.setPaintFlags(productDiscPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if(model.getWishliststatus().equalsIgnoreCase("0")){
            ImgLove.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_gray_love));
        }else{
            ImgLove.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_appcolor_love));
        }


        LLItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(cond.equalsIgnoreCase(AppConstants.search)){
                    Intent in = new Intent(activity, ProductDetailsActivity.class);
                    in.putExtra(AppConstants.productid,model.getId());
//                    in.putExtra(AppConstants.producttitle,model.getCategory());
                    activity.startActivity(in);
                    activity.finish();
//                }else{
//                    Intent in = new Intent(activity, ProductDetailsActivity.class);
//                    in.putExtra(AppConstants.productid,model.getId());
////                    in.putExtra(AppConstants.producttitle,model.getCategory());
//                    activity.startActivity(in);
//                }

            }
        });


        if(!model.getImage().isEmpty()) {
//            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);
            Glide.with(activity).load(model.getImage()).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(ImgItem);
//            Picasso.with(activity)
//                    .load(model.getImage())
//                    .into(ImgItem, new Callback() {
//                        @Override
//                        public void onSuccess() {
////                            progressbar.setVisibility(View.GONE);
//                        }
//
//                        @Override
//                        public void onError() {
//
//                        }
//                    });

        }


        return convertView;
    }



}
