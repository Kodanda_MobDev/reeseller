package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prominere.reseller.R;
import com.prominere.reseller.activity.SubCategoryActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.ProductCategory;
import com.prominere.reseller.model.ProductSubCategory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class DashBoardAdapter  extends BaseAdapter {

    private final Activity activity;
    private final ArrayList<ProductCategory> NoOfItems;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private TextView itemTitle;
    private RecyclerView recycler_view_list;
    private TextView btnMore;

    public DashBoardAdapter(Activity activity, ArrayList<ProductCategory> allSampleData) {
        this.activity=activity;
        this.NoOfItems=allSampleData;
    }



    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.db_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
//        queue = Volley.newRequestQueue(activity);


        final ProductCategory model = NoOfItems.get(position);

        itemTitle = (TextView) convertView.findViewById(R.id.itemTitle);
        recycler_view_list = (RecyclerView) convertView.findViewById(R.id.recycler_view_list);
        btnMore= (TextView) convertView.findViewById(R.id.btnMore);

        Typeface tf = Typeface.createFromAsset(activity.getAssets(),"materialdrawerfont.ttf");
        itemTitle.setTypeface(tf);


        itemTitle.setText(model.getCname());
        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(activity, SubCategoryActivity.class);
                in.putExtra(AppConstants.category,model.getId());
                activity.startActivity(in);
            }
        });

try {


        JSONArray subCatList = model.getSubcategories();
        ArrayList<ProductSubCategory> subCatModels = new ArrayList<ProductSubCategory>();
        if(subCatList.length()>0){
            for(int i=0;i<subCatList.length();i++){
                ProductSubCategory mdl = new ProductSubCategory();
                JSONObject obj = subCatList.getJSONObject(i);
                    mdl.setId(obj.getString("id"));
//                    mdl.setCategory(obj.getString("category"));
                    mdl.setCategory(model.getId());
                    mdl.setCname(obj.getString("cname"));
                    mdl.setSlug(obj.getString("slug"));
                    mdl.setAttr(obj.getString("attr"));
                    mdl.setStatus(obj.getString("status"));
                    mdl.setImage(obj.getString("image"));
                    mdl.setProductcount(obj.getString("productcount"));
                subCatModels.add(mdl);
            }
        }


        SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(activity, subCatModels);

        recycler_view_list.setHasFixedSize(true);
        recycler_view_list.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        recycler_view_list.setAdapter(itemListDataAdapter);


        recycler_view_list.setNestedScrollingEnabled(false);
//        }
}catch (Exception e){e.printStackTrace();}


        return convertView;
    }



    }
