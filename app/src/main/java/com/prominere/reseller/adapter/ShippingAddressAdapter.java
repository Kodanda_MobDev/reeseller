package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.AddressModel;
import com.prominere.reseller.activity.reseller.AddAddressActivity;

import java.util.ArrayList;

public class ShippingAddressAdapter  extends BaseAdapter {
    private final Activity activity;
    private ArrayList<AddressModel> NoOfItems;
    private LayoutInflater inflater;
    private Typeface materialFont,fontAwesomeFont;
    private RequestQueue mQueue;
    private TextView TvCustName,TvPhNo,TvAddress,TvEdit;
    private RadioButton RBSelection;
    int preSelectedIndex = -1;

    public ShippingAddressAdapter(Activity activity, ArrayList<AddressModel> productList) {
        this.activity = activity;
        this.NoOfItems=productList;
    }

    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.shippingaddress_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
        materialFont = Typeface.createFromAsset(activity.getAssets(), "materialdrawerfont.ttf");
        mQueue = Volley.newRequestQueue(activity);


        final AddressModel model = NoOfItems.get(position);


        TvCustName = (TextView) convertView.findViewById(R.id.tvCustName);
        TvEdit = (TextView) convertView.findViewById(R.id.tvEdit);
        TvAddress = (TextView) convertView.findViewById(R.id.tvAddress);
        TvPhNo = (TextView) convertView.findViewById(R.id.tvPhNo);
        RBSelection = (RadioButton) convertView.findViewById(R.id.rbSa);

        TvCustName.setText(model.getCustomername());   TvCustName.setTypeface(materialFont);
        TvAddress.setText(model.getHousenumber()+","+model.getStreet()+","+model.getCity()+","+model.getLandmark()+","+model.getState()+","+model.getPincode());
        TvPhNo.setText(model.getPhone());   TvAddress.setTypeface(materialFont);   TvPhNo.setTypeface(materialFont);

        if(model.getRbselection() == true){
            Log.i("getRbselection","true");
            RBSelection.setChecked(true);
        }else{
            Log.i("getRbselection","false");
            RBSelection.setChecked(false);
        }

        RBSelection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                Log.i("isChecked : ",""+isChecked);
                if(isChecked){

                    model.setRbselection(true);

                    NoOfItems.set(position, model);

                    if (preSelectedIndex > -1){

                        AddressModel preRecord = NoOfItems.get(preSelectedIndex);
                        preRecord.setRbselection(false);

                        NoOfItems.set(preSelectedIndex, preRecord);

                    }

                    preSelectedIndex = position;

                    //now update adapter so we are going to make a update method in adapter
                    //now declare adapter final to access in inner method

                    updateRecords(NoOfItems);

                }
//                if(isChecked){
//                    for(int i=0;i<NoOfItems.size();i++){
//                        AddressModel mdl = NoOfItems.get(i);
//                        if(model.getCustomername().equalsIgnoreCase(mdl.getCustomername())){
//                            model.setRbselection(true);
//                        }else{
//                            model.setRbselection(false);
//                        }
//                        NoOfItems.set(i,model);
//                        Log.i("Models:--> ",NoOfItems.toString());
//                        notifyDataSetChanged();
//                    }
//
////                    ShippingAddressAdapter dbradapter = new ShippingAddressAdapter(activity, NoOfItems);
////                    LvProducts.setAdapter(dbradapter);
////                    notifyDataSetChanged();
//                }
            }
        });


        TvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(activity, AddAddressActivity.class);
                in.putExtra(AppConstants.addressId,model.getId());
                activity.startActivity(in);
            }
        });

        return convertView;
    }

    public void updateRecords(ArrayList<AddressModel> users){
        NoOfItems = users;

        notifyDataSetChanged();
    }

}
