package com.prominere.reseller.adapter;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.ProductModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.prominere.reseller.activity.reseller.ProductDetailsActivity;
import com.prominere.reseller.activity.reseller.WishListItemsActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WishListItemsAdapter  extends BaseAdapter {
    private final Activity activity;
    private final ArrayList<ProductModel> NoOfItems;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private ImageView imgItem1,imgItem2,imgItem3;
    private TextView TvRemove,TvProductCategory,TvDiscPercentage,TvDiscountPrice,productPrice,productTitle;
    private LinearLayout LLDownload,LLItem,LLShareWhatsApp,LLChat,LLShare;
    private RequestQueue mQueue;
    private TextView TvWishItem;
    private LinearLayout LLMoreImages;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int REQUEST_EXTERNAL_STORAGE = 2;
    private String shareWhatsAppContent;
    private ArrayList<Bitmap> downloadShare;

    public WishListItemsAdapter(Activity activity, ArrayList<ProductModel> productList) {
        this.activity = activity;
        this.NoOfItems=productList;
    }



    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.wishlist_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
        mQueue = Volley.newRequestQueue(activity);


        final ProductModel model = NoOfItems.get(position);

        LLItem = (LinearLayout) convertView.findViewById(R.id.llItem);
        LLShare = (LinearLayout) convertView.findViewById(R.id.llShare);
        LLChat = (LinearLayout) convertView.findViewById(R.id.llChat);
        LLShareWhatsApp = (LinearLayout) convertView.findViewById(R.id.llShareWhatsApp);
        LLDownload = (LinearLayout) convertView.findViewById(R.id.llDownload);
        productTitle = (TextView) convertView.findViewById(R.id.tvProduct);
        productPrice = (TextView) convertView.findViewById(R.id.tvPrice);
        TvDiscountPrice =(TextView)convertView.findViewById(R.id.tvDiscountPrice);
        TvDiscPercentage =(TextView)convertView.findViewById(R.id.tvDiscPercentage);
        TvProductCategory = (TextView) convertView.findViewById(R.id.tvProductCategory);
        TvRemove = (TextView) convertView.findViewById(R.id.tvRemove);      TvRemove.setTypeface(fontAwesomeFont);
        imgItem1 = (ImageView) convertView.findViewById(R.id.img1);
        imgItem2 = (ImageView) convertView.findViewById(R.id.img2);
        imgItem3 = (ImageView) convertView.findViewById(R.id.img3);
        TvWishItem = (TextView) convertView.findViewById(R.id.tvLoveSymb);   TvWishItem.setTypeface(fontAwesomeFont);
        LLMoreImages = (LinearLayout) convertView.findViewById(R.id.llMoreImages);


        shareWhatsAppContent = "Product Details: \n"+model.getTitle()+"\n"+"Price:"+"₹"+model.getPrice()+".00"+"\n"+"Discount price:"+"₹"+model.getDiscountprice()+".00";

        productTitle.setText(model.getTitle());
        productPrice.setText("₹ "+model.getDiscountprice()+".00");
        TvDiscountPrice.setText("₹"+model.getPrice()+".00");    TvDiscountPrice.setPaintFlags(TvDiscountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);



        TvProductCategory.setText("₹ "+model.getCategory()+".00");
        Double discPrice = Double.parseDouble(model.getPrice()) - Double.parseDouble(model.getDiscountprice());

        Double discperc = (discPrice/Double.parseDouble(model.getPrice()))*100;
        DecimalFormat df = new DecimalFormat("#.##");
        String dx=df.format(discperc);
        discperc=Double.valueOf(dx);
        TvDiscPercentage.setText(""+discperc+"% Off");

        LLItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(activity, ProductDetailsActivity.class);
                in.putExtra(AppConstants.productid,model.getId());
//                     in.putExtra(AppConstants.producttitle,model.getCategory());
                activity.startActivity(in);
            }
        });
        TvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RemoveWishListItem(model);
            }
        });
        LLChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWhatsAppChat();
            }
        });
        LLDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppHelper.isStoragePermissionGranted(activity)) {
                    List<String> imgsCollestion = AppHelper.getImageURL(model.getImage() );
                    for(int i=0;i<imgsCollestion.size();i++) {
                        String img = imgsCollestion.get(i);
                        Log.i("img:-->" + i, " " + img);
                        if (img != null && !img.isEmpty()) {
                            downloadImages(img);
                        }
                    }
                } else {
                    ActivityCompat.requestPermissions(   activity,     PERMISSIONS_STORAGE,  REQUEST_EXTERNAL_STORAGE  );
                }
            }
        });
        LLShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareProduct(1,model);
            }
        });
        LLShareWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareProduct(2,model);
            }
        });
        if(model.getWishliststatus().equalsIgnoreCase("0")){
            TvWishItem.setText(activity.getResources().getString(R.string.fai_lovewb));
        }else{
            TvWishItem.setText(activity.getResources().getString(R.string.fai_love));
        }

        TvWishItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getWishliststatus().equalsIgnoreCase("0"))
                {
                    AddToWishList(model);
                }else{
                    RemoveWishList(model);
                }

            }
        });

        if(!model.getImage().isEmpty()) {

            List<String> imgsCollestion = AppHelper.getImageURL(model.getImage() );

            if(imgsCollestion.size()>3){
                LLMoreImages.setVisibility(View.VISIBLE);
            }else{
                LLMoreImages.setVisibility(View.GONE);
            }
            for(int i=0;i<imgsCollestion.size();i++){
                String img = imgsCollestion.get(i);
                Log.i("img:-->"+i," "+img);
                if(img != null && !img.isEmpty()){

                    switch (i){
                        case 0 :  Glide.with(activity).load(img).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform()).into(imgItem1);
                            break;
                        case 1 :  Glide.with(activity).load(img).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform()).into(imgItem2);
                            break;
                        case 2 :  Glide.with(activity).load(img).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform()).into(imgItem3);
                            break;

                    }
                }
            }
        }


        return convertView;
    }

    private void downloadImages1(String ImageUrl, final List<String> imgsCollestion, final String title, final int cond) {

        Picasso.with(activity)
                .load(ImageUrl)
                .into(new Target() {
                          @Override
                          public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                              downloadShare.add(bitmap);
                              try {
                                  String root = Environment.getExternalStorageDirectory().toString();
                                  File myDir = new File(root + "/Reeseller");

                                  if (!myDir.exists()) {
                                      myDir.mkdirs();
                                  }

                                  String name = new Date().toString() + ".jpg";
                                  myDir = new File(myDir, name);
                                  FileOutputStream out = new FileOutputStream(myDir);
                                  bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);

//                                  SimpleToast.ok(activity,"Downloaded successfully");

                                  out.flush();
                                  out.close();

                                  if(imgsCollestion.size() == downloadShare.size()){
                                      activity.runOnUiThread(new Runnable() {
                                          @Override
                                          public void run() {


                                              Intent shareIntent = new Intent(Intent.ACTION_SEND);

                                              if(cond == 2) {
                                                  shareIntent.setPackage("com.whatsapp");
                                              }
                                              shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                              for(int i=0;i<downloadShare.size();i++){
                                                  String imgBitmapPath = MediaStore.Images.Media.insertImage(activity.getContentResolver(), downloadShare.get(i), "title", null);
                                                  Uri imgBitmapUri = Uri.parse(imgBitmapPath);
                                                  shareIntent.putExtra(Intent.EXTRA_STREAM,imgBitmapUri);

                                              }

                                              shareIntent.setType("image/png");
                                              shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                              shareIntent.putExtra(Intent.EXTRA_TEXT, title);
//                            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject text");
                                              activity.startActivity(Intent.createChooser(shareIntent, "Share via"));

                                          }
                                      });

                                  }

                              } catch(Exception e){
                                  // some action
                              }
                          }

                          @Override
                          public void onBitmapFailed(Drawable errorDrawable) {
                          }

                          @Override
                          public void onPrepareLoad(Drawable placeHolderDrawable) {
                          }
                      }
                );

    }
    private void downloadImages( String ImageUrl) {

        Picasso.with(activity)
                .load(ImageUrl)
                .into(new Target() {
                          @Override
                          public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                              try {
                                  String root = Environment.getExternalStorageDirectory().toString();
                                  File myDir = new File(root + "/Reeseller");

                                  if (!myDir.exists()) {
                                      myDir.mkdirs();
                                  }

                                  String name = new Date().toString() + ".jpg";
                                  myDir = new File(myDir, name);
                                  FileOutputStream out = new FileOutputStream(myDir);
                                  bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);

                                  SimpleToast.ok(activity,"Downloaded successfully");

                                  out.flush();
                                  out.close();
                              } catch(Exception e){
                                  // some action
                              }
                          }

                          @Override
                          public void onBitmapFailed(Drawable errorDrawable) {
                          }

                          @Override
                          public void onPrepareLoad(Drawable placeHolderDrawable) {
                          }
                      }
                );

    }

    private void RemoveWishListItem(final ProductModel model) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("AddToWishList:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                Intent in = new Intent(activity,WishListItemsActivity.class);
                                activity.startActivity(in);
                                activity.finish();

                            }else{
                                SimpleToast.error(activity,AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.removefromwishlist);
                params.put("productid",model.getId());
                params.put("userid", SessionSave.getsession(AppConstants.userid,activity));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }


    private void AddToWishList(final ProductModel model) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// {"status":"success","message":"Product added to wishlist"}
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("AddToWishList:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                SimpleToast.ok(activity,mainObj.getString("message"));
//                                TvWishItem.setText(activity.getResources().getString(R.string.fai_love));
//                                model.setWishliststatus("1");
//                                notifyDataSetChanged();

                                Intent in = new Intent(activity,WishListItemsActivity.class);
                                activity.startActivity(in);
                                activity.finish();
                            }else{
                                SimpleToast.error(activity,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.addtowishlist);
                params.put("productid",model.getId());
                params.put("userid", SessionSave.getsession(AppConstants.userid,activity));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }
    private void RemoveWishList(final ProductModel model) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// {"status":"success","message":"Product added to wishlist"}
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("AddToWishList:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                SimpleToast.ok(activity,mainObj.getString("message"));
//                                TvWishItem.setText(activity.getResources().getString(R.string.fai_lovewb));
//                                model.setWishliststatus("0");
//                                notifyDataSetChanged();
                                Intent in = new Intent(activity,WishListItemsActivity.class);
                                activity.startActivity(in);
                                activity.finish();
                            }else{
                                SimpleToast.error(activity,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.removefromwishlist);
                params.put("productid",model.getId());
                params.put("userid", SessionSave.getsession(AppConstants.userid,activity));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }
    private void shareProduct(final int i, final ProductModel model) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                if(i == 2){

                                    downloadShare = new ArrayList<Bitmap>();
                                    if (AppHelper.isStoragePermissionGranted(activity)) {
                                        List<String> imgsCollestion = AppHelper.getImageURL(model.getImage());
                                        for(int i=0;i<imgsCollestion.size();i++) {
                                            String img = imgsCollestion.get(i);
                                            Log.i("img:-->" + i, " " + img);
                                            if (imgsCollestion.get(0) != null && !imgsCollestion.get(0).isEmpty()) {

                                                downloadImages1(imgsCollestion.get(i),imgsCollestion,model.getTitle(),2 );
                                            }
                                        }
                                    } else {
                                        ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
                                    }



//                                    if (AppHelper.isStoragePermissionGranted(activity)) {
//                                        List<String> imgsCollestion = AppHelper.getImageURL(model.getImage());
//                    for(int i=0;i<imgsCollestion.size();i++) {
//                        String img = imgsCollestion.get(i);
//                        Log.i("img:-->" + i, " " + img);
//                                        if (imgsCollestion.get(0) != null && !imgsCollestion.get(0).isEmpty()) {
//                                            downloadShare = new ArrayList<Bitmap>();
//                                            downloadImages1(imgsCollestion.get(i),imgsCollestion );
//                                        }
//                     }
//                                    } else {
//                                        ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
//                                    }

                                }else{

                                    downloadShare = new ArrayList<Bitmap>();
                                    if (AppHelper.isStoragePermissionGranted(activity)) {
                                        List<String> imgsCollestion = AppHelper.getImageURL(model.getImage());
                                        for(int i=0;i<imgsCollestion.size();i++) {
                                            String img = imgsCollestion.get(i);
                                            Log.i("img:-->" + i, " " + img);
                                            if (imgsCollestion.get(0) != null && !imgsCollestion.get(0).isEmpty()) {

                                                downloadImages1(imgsCollestion.get(i),imgsCollestion,model.getTitle(),1 );
                                            }
                                        }
                                    } else {
                                        ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
                                    }


                                }
                            }else{
                                SimpleToast.error(activity,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.share_product);
                params.put("userid", SessionSave.getsession(AppConstants.userid,activity));
                params.put("productid",""+model.getId());
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }


    private void openWhatsAppChat() {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView( activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                String link = (mainObj.getString("whatsapplink"));
//                                Intent i = new Intent(Intent.ACTION_SENDTO, Uri.parse("content://com.android.contacts/data/" + c.getString(0)));
                                PackageManager packageManager = activity.getPackageManager();
                                Intent i = new Intent(Intent.ACTION_VIEW);

                                try {
//                                    String url = "https://api.whatsapp.com/send?phone="+ phone +"&text=" + URLEncoder.encode(message, "UTF-8");
                                    i.setPackage("com.whatsapp");
                                    i.setData(Uri.parse(link));
                                    if (i.resolveActivity(packageManager) != null) {
                                        activity.startActivity(i);
                                    }
                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                            }else{
                                //    Toast.makeText(LoginActivity.this,"Invalid credentials",Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",AppConstants.adminwhatsapp);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(AppConstants.volleyTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });

        mQueue.add(sr);



    }//LogInByVolley





}
