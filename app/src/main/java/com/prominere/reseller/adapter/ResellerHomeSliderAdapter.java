package com.prominere.reseller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.model.BannerModel;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.List;

public class ResellerHomeSliderAdapter extends SliderViewAdapter<ResellerHomeSliderAdapter.SliderAdapterVH> {

    private final List<BannerModel> productCatList;
    private Context context;

    public ResellerHomeSliderAdapter(Context context, List<BannerModel> productCatList) {
        this.context = context;
        this.productCatList = productCatList;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {

        final BannerModel mdl = productCatList.get(position);
//        viewHolder.textViewDescription.setText(""+productCatList.get(position).getCname());

        if(!mdl.getBanner().isEmpty()) {
            Glide.with(context).load(mdl.getBanner()).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(viewHolder.imageViewBackground);
//            Picasso.with(context).load(mdl.getBanner()).into(viewHolder.imageViewBackground);
        }
//        viewHolder.imageViewBackground.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent in = new Intent(context, SubCategoryActivity.class);
//                in.putExtra(AppConstants.category,mdl.getId());
//                context.startActivity(in);
//            }
//        });
//        Glide.with(viewHolder.itemView)
//                        .load(mdl.getImage())
//                        .into(viewHolder.imageViewBackground);
//



    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return productCatList.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}