package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.dashboard.account.OrderDetailsActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.OrderModel;
import com.prominere.reseller.util.CircleImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyOrdersAdapter   extends BaseAdapter {


    private final Activity activity;
    private final ArrayList<OrderModel> NoOfItems;
    private LayoutInflater inflater;
    private Typeface matDrawerFont,fontAwesomeFont;
    private TextView TvDateTime,    TvPrice,TvCustName;
    private ImageView ImgItem;

    public MyOrdersAdapter(Activity activity, ArrayList<OrderModel> productsubCatList) {
        this.activity=activity;
        this.NoOfItems=productsubCatList;
    }


    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.myorder_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
        matDrawerFont = Typeface.createFromAsset(activity.getAssets(), "materialdrawerfont.ttf");
//        queue = Volley.newRequestQueue(activity);


        final OrderModel model = NoOfItems.get(position);

        ImgItem = (ImageView) convertView.findViewById(R.id.itemImage);
        TvCustName = (TextView) convertView.findViewById(R.id.tvName);  TvCustName.setText(model.getCustomer());    TvCustName.setTypeface(matDrawerFont);
        TvPrice = (TextView) convertView.findViewById(R.id.tvPrice);  TvPrice.setText("₹"+model.getPrice()+".00");            TvPrice.setTypeface(matDrawerFont);
        TvDateTime = (TextView) convertView.findViewById(R.id.tvDateTime);  TvDateTime.setText(model.getOrderdate());TvDateTime.setTypeface(matDrawerFont);
        LinearLayout LLMain = (LinearLayout) convertView.findViewById(R.id.llMain);

        if(!model.getProductimage().isEmpty()) {
//            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);
            Glide.with(activity).load(model.getProductimage()).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(ImgItem);
//            Picasso.with(activity)
//                    .load(model.getProductimage())
//                    .into(ImgItem, new Callback() {
//                        @Override
//                        public void onSuccess() {
////                            progressbar.setVisibility(View.GONE);
//                        }
//
//                        @Override
//                        public void onError() {
//
//                        }
//                    });
        }
        LLMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                SimpleToast.ok(activity,""+model.getId()+" "+model.getOrderid());
                Intent in = new Intent(activity, OrderDetailsActivity.class);
                in.putExtra(AppConstants.orderid,""+model.getId());
                activity.startActivity(in);
            }
        });

        return convertView;
    }



}
