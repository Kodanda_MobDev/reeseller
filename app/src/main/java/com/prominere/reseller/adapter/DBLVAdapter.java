package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.ProductSubCategory;
import com.prominere.reseller.model.SectionDataModel;
import com.prominere.reseller.util.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DBLVAdapter  extends BaseAdapter {
    private final Activity activity;
    private final ArrayList<SectionDataModel> NoOfItems;
    private final HashMap<String, ArrayList<ProductSubCategory>> subItems;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private TextView itemTitle;
    private RecyclerView recycler_view_list;
    private TextView btnMore;
    private RequestQueue queue;

    public DBLVAdapter(Activity activity, ArrayList<SectionDataModel> allSampleData, HashMap<String, ArrayList<ProductSubCategory>> subItems) {
        this.activity=activity;
        this.NoOfItems=allSampleData;
        this.subItems=subItems;
    }


    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.db_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
        queue = Volley.newRequestQueue(activity);


        final SectionDataModel model = NoOfItems.get(position);

        itemTitle = (TextView) convertView.findViewById(R.id.itemTitle);
        recycler_view_list = (RecyclerView) convertView.findViewById(R.id.recycler_view_list);
        btnMore= (TextView) convertView.findViewById(R.id.btnMore);

        Typeface tf = Typeface.createFromAsset(activity.getAssets(),"materialdrawerfont.ttf");
        itemTitle.setTypeface(tf);


        itemTitle.setText(model.getHeaderTitle());
//        btnMore.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent in = new Intent(activity, SubCategoryActivity.class);
//                in.putExtra(AppConstants.category,model.getCategoryId());
//                activity.startActivity(in);
//            }
//        });

//        if(subItems.containsKey(model.getCategoryId())){

            ArrayList<ProductSubCategory> subCatModels = subItems.get(model.getCategoryId());
            SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(activity, subCatModels);

            recycler_view_list.setHasFixedSize(true);
            recycler_view_list.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
            recycler_view_list.setAdapter(itemListDataAdapter);


            recycler_view_list.setNestedScrollingEnabled(false);
//        }



//        String url = "http://sampletemplates.net.in/reseller/api/example/user";
//        Log.i("subcategorie Api:-->",url);
//        StringRequest sr = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
////                        if(mProgressDialog.isShowing()){mProgressDialog.dismiss();}
//                        Log.e("subCategory", "success! response: " + response.toString());
//                        try {
//                            JSONObject mainObj = new JSONObject(response);
//
//                            if(mainObj.getString("status").equalsIgnoreCase("success")){
//                                JSONArray mainArray = new JSONArray(mainObj.getString("subcategories"));
//                                ArrayList<ProductSubCategory> singleSectionItems = new ArrayList<ProductSubCategory>();
//                                for(int i=0;i<mainArray.length();i++){
//                                    JSONObject obj = mainArray.getJSONObject(i);
//                                    ProductSubCategory model = new ProductSubCategory();
//                                    model.setId(obj.getString("id"));
//                                    model.setCname(obj.getString("cname"));
//                                    model.setSlug(obj.getString("slug"));
//                                    model.setCategory(obj.getString("category"));
//                                    model.setAttr(obj.getString("attr"));
//                                    model.setStatus(obj.getString("status"));
//                                    model.setImage(obj.getString("image"));
//                                    singleSectionItems.add(model);
//                                }
//
//
//
//                                SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(activity, singleSectionItems);
//
//                                recycler_view_list.setHasFixedSize(true);
//                                recycler_view_list.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
//                                recycler_view_list.setAdapter(itemListDataAdapter);
//
//
//                                recycler_view_list.setNestedScrollingEnabled(false);
//
//
//
//                            }else{
//                                //     Toast.makeText(,"Invalid credentials",Toast.LENGTH_LONG).show();
//                            }
//
//                        }catch (Exception e){e.printStackTrace();}
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                    //    if(mProgressDialog.isShowing()){mProgressDialog.dismiss();}
//                        Log.e("HttpClient", "error: " + error.toString());
//                    }
//                })
//        {
//            @Override
//            protected Map<String,String> getParams(){
//                Map<String,String> params = new HashMap<String, String>();
//                params.put("action","getsubcategories_product");
//                params.put("category",model.getCategoryId());
//                params.put("password", SessionSave.getsession(AppConstants.password,activity));
//                Log.i("Ob:--> ",params.toString());
//                return params;
//            }
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String,String> params = new HashMap<String, String>();
////                params.put("Content-Type","application/x-www-form-urlencoded");
//                params.put("x-api-key","CODEX@123");
//                return params;
//            }
//        };
//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });
//        queue.add(sr);



        return convertView;
    }


}
