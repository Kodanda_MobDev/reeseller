package com.prominere.reseller.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.model.ProductSubCategory;
import com.prominere.reseller.model.SectionDataModel;

import java.util.ArrayList;
import java.util.List;

public class PreviewAdapter  extends PagerAdapter {


    private final Activity activity;
    private final List<String> NoOfItems;
    private final LayoutInflater layoutInflater;
    private LayoutInflater inflater;

    public PreviewAdapter(Activity activity, List<String> imgsList) {
        this.activity = activity ;
        this.NoOfItems = imgsList ;
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return NoOfItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.product_preview_row, container, false);

        ImageView Img = (ImageView) itemView.findViewById(R.id.img);
//        imageView.setImageResource(images[position]);
        String url = NoOfItems.get(position);
        if(!url.isEmpty()) {
            Glide.with(activity).load(url).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(Img);

        }

        container.addView(itemView);


        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }








//
//
//    @Override
//    public int getCount() {
//        return NoOfItems.size();
//    }
//
//
//    @Override
//    public Object getItem(int position) {
//        return NoOfItems.get(position);
//    }
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        if (inflater == null)
//            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        if (convertView == null)
//            convertView = inflater.inflate(R.layout.product_preview_row, null);
////        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
//
//
//        String url = NoOfItems.get(position);
//
//        ImageView Img = (ImageView) convertView.findViewById(R.id.img);
//        if(!url.isEmpty()) {
//            Glide.with(activity).load(url).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
//                    .into(Img);
//
//        }
//
//        return convertView;
//    }



}
