package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.VideoPreviewActivity;
import com.prominere.reseller.model.HowToUseModel;

import java.util.ArrayList;

public class HowToUseAdapter  extends BaseAdapter {
    private final Activity activity;
    private final ArrayList<HowToUseModel> NoOfItems;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private RequestQueue queue;
    private TextView tvTitle;
    private ImageView ImgPlay,ItemImage,imgMore;
    private LinearLayout LLItem;

    public HowToUseAdapter(Activity activity, ArrayList<HowToUseModel> productList) {
        this.activity=activity;
        this.NoOfItems=productList;
    }


    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.howtouse_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
        queue = Volley.newRequestQueue(activity);

        Typeface tf = Typeface.createFromAsset(activity.getAssets(),"materialdrawerfont.ttf");
        final HowToUseModel model = NoOfItems.get(position);

        tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        ItemImage = (ImageView) convertView.findViewById(R.id.itemImage);
        imgMore = (ImageView) convertView.findViewById(R.id.imgMore);
        ImgPlay = (ImageView) convertView.findViewById(R.id.imgPlay);
        LLItem = (LinearLayout) convertView.findViewById(R.id.llItem);

        LLItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(activity, VideoPreviewActivity.class);
                in.putExtra("VideoLink",model.getVideo());
                activity.startActivity(in);
            }
        });

        if(!model.getThumb_image().isEmpty()) {
//            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);
            Glide.with(activity).load(model.getThumb_image()).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(ItemImage);
        }



//        ImgPlay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

      tvTitle.setText(model.getTitle());       tvTitle.setTypeface(tf);



        return convertView;
    }//onCreate


}
