package com.prominere.reseller.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.model.VendorDBModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class VendorDBAdapter extends RecyclerView.Adapter<VendorDBAdapter.ViewHolder> {
    private final Activity activity;
    private final ArrayList<VendorDBModel> NoOfItems;

    public VendorDBAdapter(Activity activity, ArrayList<VendorDBModel> productList) {
        this.activity = activity;
        this.NoOfItems=productList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.vendordb_lv_row,parent,false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VendorDBModel model = NoOfItems.get(position);

        holder.productTitle.setText(model.getTitle());
        holder.TvDate.setText(model.getOrderdate());
        holder.TvPrice.setText("₹"+model.getPrice()+".00");
        if(!model.getImage().isEmpty()) {

            Glide.with(activity).load(model.getImage()).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(holder.imgItem);
//            Picasso.with(activity)
//                    .load(model.getImage())
//                    .into(holder.imgItem, new Callback() {
//                        @Override
//                        public void onSuccess() {
////                            progressbar.setVisibility(View.GONE);
//                        }
//
//                        @Override
//                        public void onError() {
//
//                        }
//                    });
        }

    }

    @Override
    public int getItemCount() {
        return NoOfItems.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder {

        private final Typeface fontAwesomeFont;
        public ImageView imgItem;
        public TextView productTitle,TvDate,TvPrice;
        public ViewHolder(View itemView) {
            super(itemView);
            fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");

            imgItem = (ImageView) itemView.findViewById(R.id.imgProduct);
            productTitle = (TextView) itemView.findViewById(R.id.tvProductName);
            TvDate = (TextView) itemView.findViewById(R.id.tvDate);
            TvPrice = (TextView) itemView.findViewById(R.id.tvPrice);

        }
    }




}
