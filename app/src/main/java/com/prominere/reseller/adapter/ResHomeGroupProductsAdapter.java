package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.GroupProductDetailsActivity;
import com.prominere.reseller.activity.reseller.ProductDetailsActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.GroupProductModel;

import java.util.ArrayList;
import java.util.List;

public class ResHomeGroupProductsAdapter  extends BaseAdapter {


    private final Activity activity;
    private final ArrayList<GroupProductModel> NoOfItems;
    private final ListView LvProduct;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private RequestQueue mQueue;
    private ImageView imgItem1,imgItem2,imgItem3;
    private TextView TvProduct,TvPrice,TvWishItem,TvProdCategory;
    private LinearLayout LLMoreImages,LLItem;
    private TextView TvCounts;

    public ResHomeGroupProductsAdapter(Activity activity, ArrayList<GroupProductModel> productList, ListView lvProduct) {
        this.activity = activity;
        this.NoOfItems=productList;
        this.LvProduct=lvProduct;
    }



    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.mycatalog_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
        mQueue = Volley.newRequestQueue(activity);


        final GroupProductModel model = NoOfItems.get(position);

        LLItem = (LinearLayout) convertView.findViewById(R.id.llItem);
        imgItem1 = (ImageView) convertView.findViewById(R.id.img1);
        imgItem2 = (ImageView) convertView.findViewById(R.id.img2);
        imgItem3 = (ImageView) convertView.findViewById(R.id.img3);
        TvWishItem = (TextView) convertView.findViewById(R.id.tvLoveSymb);   TvWishItem.setTypeface(fontAwesomeFont);
        TvProdCategory = (TextView) convertView.findViewById(R.id.tvProductCategory);
        TvProduct = (TextView) convertView.findViewById(R.id.tvProduct);
        TvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
        LLMoreImages = (LinearLayout) convertView.findViewById(R.id.llMoreImages);
        TvCounts = (TextView) convertView.findViewById(R.id.tvCount);

        TvProdCategory.setText(model.getGroupname()); TvProduct.setVisibility(View.GONE);
//        TvProduct.setText(model.getTitle());
        TvPrice.setText("₹ "+model.getDiscountprice()+".00");  TvWishItem.setVisibility(View.GONE);

//        if(model.getWishliststatus().equalsIgnoreCase("0")){
//            TvWishItem.setText(activity.getResources().getString(R.string.fai_lovewb));
//        }else{
//            TvWishItem.setText(activity.getResources().getString(R.string.fai_love));
//        }

        LLItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(activity, GroupProductDetailsActivity.class);
                in.putExtra(AppConstants.productid,model.getId());
//                     in.putExtra(AppConstants.producttitle,model.getCategory());
                activity.startActivity(in);
            }
        });
//        TvWishItem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(model.getWishliststatus().equalsIgnoreCase("0"))
//                {
//                    AddToWishList(model);
//                }else{
//                    RemoveWishList(model);
//                }
//
//            }
//        });

        if(!model.getImages().isEmpty()) {

            List<String> imgsCollestion = AppHelper.getImageURL(model.getImages() );

            if(imgsCollestion.size()>2){
                LLMoreImages.setVisibility(View.VISIBLE);
                int count= imgsCollestion.size()-2;
                TvCounts.setText("+"+count);
            }else{
                LLMoreImages.setVisibility(View.GONE);
            }
            for(int i=0;i<imgsCollestion.size();i++){
                String img = imgsCollestion.get(i);
                Log.i("img:-->"+i," "+img);
                if(img != null && !img.isEmpty()){

                    switch (i){
                        case 0 :  Glide.with(activity).load(img).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform()).into(imgItem1); // loadImages(img,imgItem1);
                            break;
                        case 1 :  Glide.with(activity).load(img).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform()).into(imgItem2); // loadImages(img,imgItem2);
                            break;
                        case 2 :   Glide.with(activity).load(img).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform()).into(imgItem3); // loadImages(img,imgItem3);
                            break;

                    }
                }
            }
        }


        return convertView;
    }



}
