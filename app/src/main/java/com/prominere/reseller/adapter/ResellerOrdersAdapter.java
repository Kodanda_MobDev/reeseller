package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.dashboard.account.OrderDetailsActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.ResellerOrderModel;
import com.prominere.reseller.util.CircleImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ResellerOrdersAdapter  extends RecyclerView.Adapter<ResellerOrdersAdapter.ViewHolder> {

    private final Activity activity;
    private final ArrayList<ResellerOrderModel> NoOfItems;
    Typeface fontAwesomeFont,matDrawerFont;

    public ResellerOrdersAdapter(Activity activity, ArrayList<ResellerOrderModel> list) {
        this.activity = activity;
        this.NoOfItems=list;
    }


    @NonNull
    @Override
    public ResellerOrdersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.myorder_lv_row,parent,false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ResellerOrdersAdapter.ViewHolder holder, int position) {
        final ResellerOrderModel model = NoOfItems.get(position);
     //   holder.TvOrderId.setText("Order ID # "+model.getId());
        holder.TvProductTitle.setText(model.getProductname());      holder.TvProductTitle.setTypeface(matDrawerFont);
        holder.TvDateCreated.setText(model.getDateadded());         holder.TvDateCreated.setTypeface(matDrawerFont);
        holder.TvCustName.setText(model.getCustomername());         holder.TvCustName.setTypeface(matDrawerFont);
      //  holder.TvQuantity.setText(model.getQuantity());           holder.TvQuantity.setTypeface(matDrawerFont);
        holder.TvPrice.setText("₹"+model.getOrderprice()+".00");          holder.TvPrice.setTypeface(matDrawerFont);

        holder.LLMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(activity, OrderDetailsActivity.class);
                in.putExtra(AppConstants.orderid,""+model.getId());
                activity.startActivity(in);
            }
        });
        if(!model.getProductimage().isEmpty()) {

            Glide.with(activity).load(model.getProductimage()).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(holder.ImgProduct);
//            Picasso.with(activity)
//                    .load(model.getProductimage())
//                    .into(holder.ImgProduct, new Callback() {
//                        @Override
//                        public void onSuccess() {
////                            progressbar.setVisibility(View.GONE);
//                        }
//
//                        @Override
//                        public void onError() {
//
//                        }
//                    });
        }



    }

    @Override
    public int getItemCount() {
        return NoOfItems.size();
    }
    public  class  ViewHolder extends RecyclerView.ViewHolder{


        private final ImageView ImgProduct;
        private final TextView TvDateCreated,TvCustName,TvProductTitle,TvPrice;
        private final LinearLayout LLMain;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
            matDrawerFont = Typeface.createFromAsset(activity.getAssets(), "materialdrawerfont.ttf");
//        queue = Volley.newRequestQueue(activity);

            ImgProduct = (ImageView) itemView.findViewById(R.id.itemImage);
            TvProductTitle = (TextView) itemView.findViewById(R.id.tvProductTitle);
            TvDateCreated = (TextView) itemView.findViewById(R.id.tvDateTime);
            TvCustName = (TextView) itemView.findViewById(R.id.tvName);
            TvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            LLMain = (LinearLayout) itemView.findViewById(R.id.llMain);

        }
    }




}
