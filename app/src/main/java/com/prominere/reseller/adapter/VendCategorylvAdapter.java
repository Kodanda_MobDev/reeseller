package com.prominere.reseller.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.prominere.reseller.R;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.CatSelectionModel;
import com.prominere.reseller.model.SubCatModel;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import java.util.ArrayList;
import java.util.List;

public class VendCategorylvAdapter extends BaseAdapter {


     LinearLayout LLAttributes,LLSubCat;
    private  int selectedSubCatId , selectedCatId ;
    int pId;
    Spinner spSubCategory;
    List<SubCatModel> subCatList;
    Activity activity;
    List<CatSelectionModel> users;
    LayoutInflater inflater;
    int preSelectedIndex = -1;

    //short to create constructer using command+n for mac & Alt+Insert for window


    public VendCategorylvAdapter(Activity activity, List<CatSelectionModel> users, Spinner spSubCategory, List<SubCatModel> subCat, int pId, int selectedCatId, int selectedSubCatId, LinearLayout LLSubCat,LinearLayout LLAttributes) {

        this.activity   = activity;
        this.users      = users;
        this.subCatList      = subCat;
        this.spSubCategory      = spSubCategory;
        this.pId      = pId;
        this.selectedCatId  = selectedCatId;
        this.selectedSubCatId      = selectedSubCatId;
        this.LLSubCat      = LLSubCat;
        this.LLAttributes      = LLAttributes;

        inflater        = activity.getLayoutInflater();
    }

    public VendCategorylvAdapter(Activity activity, List<CatSelectionModel> users) {
        this.activity   = activity;
        this.users      = users;

        inflater        = activity.getLayoutInflater();
    }


    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        ViewHolder holder = null;

        if (view == null){

            view = inflater.inflate(R.layout.list_view_item, viewGroup, false);

            holder = new ViewHolder();

            holder.tvUserName = (TextView)view.findViewById(R.id.tv_user_name);
            holder.ivCheckBox = (CheckBox) view.findViewById(R.id.iv_check_box);

            view.setTag(holder);
        }else
            holder = (ViewHolder)view.getTag();

        Log.i("users:--Adapter:->",users.toString());


        final CatSelectionModel model = users.get(i);

        holder.tvUserName.setText(model.getCatName());

        if (model.isSelected())
            holder.ivCheckBox.setChecked(true);

        else
            holder.ivCheckBox.setChecked(false);

        if(pId != 0 ){

            if (model.isSelected()){
                model.setSelected(true);
                users.set(i, model);
                if (preSelectedIndex > -1) {
                    CatSelectionModel preRecord = users.get(preSelectedIndex);
                    preRecord.setSelected(false);
                    users.set(preSelectedIndex, preRecord);
                }
                preSelectedIndex = i;
                updateRecords(users);
            }


            final List<String> subNames = new ArrayList<String>();

            for(int j=0;j<subCatList.size();j++){

                SubCatModel mdl = subCatList.get(j);
                if(selectedCatId ==  Integer.parseInt(mdl.getCatId())){
                    subNames.add(mdl.getSubname());
                    Log.i("MDL:-->"+i,""+mdl.toString()) ;
                }
            }
            if(subNames.size()>0) {
                spSubCategory.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, subNames));
                    for(int j=0;j<subCatList.size();j++){

                        SubCatModel mdl = subCatList.get(j);
                        if(selectedSubCatId ==  Integer.parseInt(mdl.getId())){
                            spSubCategory.setSelection(AppHelper.setValueToSpinner(spSubCategory,mdl.getSubname()));

                            Log.i("MDL:-->"+i,""+mdl.toString()) ;
                        }
                    }
            }else {
                spSubCategory.setAdapter(null);
            }


        }//if


        final ViewHolder finalHolder = holder;
        holder.ivCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isSel) {

            if(isSel) {
                Log.i("Sel:","IsChecked");
                final List<String> subNames = new ArrayList<String>();
              //  CatSelectionModel model = users.get(i); //changed it to model because viewers will confused about it

                for(int i=0;i<subCatList.size();i++){

                    SubCatModel mdl = subCatList.get(i);
                    if(model.getCatId().equalsIgnoreCase(mdl.getCatId())){
                        subNames.add(mdl.getSubname());
                            Log.i("MDL:-->"+i,""+mdl.toString()) ;
                    }
                }

                if(subNames.size()>0) {
                    spSubCategory.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, subNames));
               //    LLSubCat.setVisibility(View.VISIBLE);
                }else {
                //    LLSubCat.setVisibility(View.GONE);
                    spSubCategory.setAdapter(null);
                }

                model.setSelected(true);

                users.set(i, model);

                if (preSelectedIndex > -1) {

                    CatSelectionModel preRecord = users.get(preSelectedIndex);
                    preRecord.setSelected(false);

                    users.set(preSelectedIndex, preRecord);

                }

                preSelectedIndex = i;

                //now update adapter so we are going to make a update method in adapter
                //now declare adapter final to access in inner method

                updateRecords(users);

             }


            }
        });


        return view;

    }

    public void updateRecords(List<CatSelectionModel>  users){
        this.users = users;

        Log.i("Update:" ,users.toString());
        notifyDataSetChanged();
    }

    class ViewHolder{

        TextView tvUserName;
        CheckBox ivCheckBox;

    }


}
