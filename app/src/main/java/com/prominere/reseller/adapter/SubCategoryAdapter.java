package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.ProductsActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.SubCategoryModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SubCategoryAdapter  extends BaseAdapter {
    private final Activity activity;
    private final ArrayList<SubCategoryModel> NoOfItems;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private ImageView imgItem;
    private TextView itemTitle;
    private LinearLayout LLItem;

    public SubCategoryAdapter(Activity activity, ArrayList<SubCategoryModel> productsubCatList) {
        this.activity=activity;
        this.NoOfItems=productsubCatList;
    }


    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.subcat_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
//        queue = Volley.newRequestQueue(activity);


        final SubCategoryModel model = NoOfItems.get(position);

        imgItem = (ImageView) convertView.findViewById(R.id.itemImage);
        itemTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        LLItem = (LinearLayout) convertView.findViewById(R.id.llItem);

        itemTitle.setText(model.getCname());

        LLItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(activity, ProductsActivity.class);
                in.putExtra(AppConstants.category,model.getCategory());
                in.putExtra(AppConstants.subCategory,model.getId());
                activity.startActivity(in);
//                activity.finish();
            }
        });

        if(!model.getImage().isEmpty()) {

            List<String> listOfImgs = AppHelper.getImageURL(model.getImage());
            if(listOfImgs.size()>0){
                for(int i=0;i<listOfImgs.size();i++){
                    Log.i("Images:--> "+i," "+listOfImgs.get(i));
                    switch (i){
                        case 0:
                            Glide.with(activity).load(listOfImgs.get(i)).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                                    .into(imgItem);
                            break;
                    }
                }
            }


        }


        return convertView;
    }



}
