package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.CartModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.numberPicker.NumberPicker;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.prominere.reseller.activity.reseller.CartItemsActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CartItemsAdapter  extends BaseAdapter {
    private final Activity activity;
    private final ArrayList<CartModel> NoOfItems;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private ImageView imgItem;
    private TextView TvRemove,TvSize,productPrice,productTitle;
    private LinearLayout LLItem;
    private RequestQueue mQueue;
    private NumberPicker NumbPicker;

    public CartItemsAdapter(Activity activity, ArrayList<CartModel> productList) {
        this.activity = activity;
        this.NoOfItems=productList;
    }

    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.cartitem_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
        mQueue = Volley.newRequestQueue(activity);


        final CartModel model = NoOfItems.get(position);

        LLItem = (LinearLayout) convertView.findViewById(R.id.llItem);
        imgItem = (ImageView) convertView.findViewById(R.id.img);
        productTitle = (TextView) convertView.findViewById(R.id.tvProduct);
        productPrice = (TextView) convertView.findViewById(R.id.tvPrice);
     //   TvSize = (TextView) convertView.findViewById(R.id.tvSize);
        TvRemove = (TextView) convertView.findViewById(R.id.tvRemove);          TvRemove.setTypeface(fontAwesomeFont);
        NumbPicker =(NumberPicker)convertView.findViewById(R.id.numbPicker);

        productTitle.setText(model.getTitle());
        productPrice.setText(model.getPrice());
//        TvSize.setText(model.getAttribute());
        NumbPicker.setValue(Integer.parseInt(model.getQuantity()));

        NumbPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


//        LLItem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent in = new Intent(activity, ProductDetailsActivity.class);
//                in.putExtra(AppConstants.productid,model.getId());
//                activity.startActivity(in);
//            }
//        });
        TvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RemovCartItem(model,position);
            }
        });
        imgItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        if(!model.getImage().isEmpty()) {
//            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);
            Glide.with(activity).load(model.getImage()).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(imgItem);
//            Picasso.with(activity)
//                    .load(model.getImage())
//                    .into(imgItem, new Callback() {
//                        @Override
//                        public void onSuccess() {
////                            progressbar.setVisibility(View.GONE);
//                        }
//
//                        @Override
//                        public void onError() {
//
//                        }
//                    });

        }


        return convertView;
    }

    private void RemovCartItem(final CartModel model, int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog, null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->", url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        Log.e("AddToWishList:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if (mainObj.getString("status").equalsIgnoreCase("success")) {
//
//                                for(int i=0;i<NoOfItems.size();i++){
//                                    if(model.getId().equalsIgnoreCase(NoOfItems.get(i).getId()) ){
//
//                                    }
//                                }
                                    Intent in = new Intent(activity, CartItemsActivity.class);
                                    activity.startActivity(in);
                                    activity.finish();

                            } else {
                                SimpleToast.error(activity, AppConstants.apiIssueMsg);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("action", ApiHelper.removefromcart);
                params.put("cartid", model.getId());
                params.put("userid", SessionSave.getsession(AppConstants.userid, activity));
                params.put("quantity",model.getQuantity());
                Log.i("Ob:--> ", params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return AppConstants.volleyTimeOut;
            }

            @Override
            public int getCurrentRetryCount() {
                return AppConstants.volleyTimeOut;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
            }
        });

        mQueue.add(sr);
    }


    }