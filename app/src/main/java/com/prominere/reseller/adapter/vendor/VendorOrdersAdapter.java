package com.prominere.reseller.adapter.vendor;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.dashboard.account.OrderDetailsActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.CartModel;
import com.prominere.reseller.model.OrderModel;
import com.prominere.reseller.util.CircleImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class VendorOrdersAdapter extends RecyclerView.Adapter<VendorOrdersAdapter.ViewHolder> {
    private final Activity activity;
    private final ArrayList<OrderModel> NoOfItems;
     Typeface fontAwesomeFont,matDrawerFont;
    public VendorOrdersAdapter(Activity activity, ArrayList<OrderModel> list) {
        this.activity = activity;
        this.NoOfItems=list;
    }

    @NonNull
    @Override
    public VendorOrdersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.vendororder_item_lv_row,parent,false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VendorOrdersAdapter.ViewHolder holder, int position) {
        final OrderModel model = NoOfItems.get(position);
        holder.TvOrderId.setText("Order ID # "+model.getOrderid());
        holder.TvProductTitle.setText(model.getProductname());    holder.TvProductTitle.setTypeface(matDrawerFont);
        holder.TvOrderDate.setText(model.getOrderdate());         holder.TvOrderDate.setTypeface(matDrawerFont);
        holder.TvCustName.setText(model.getCustomer());           holder.TvCustName.setTypeface(matDrawerFont);
        holder.TvQuantity.setText(model.getQuantity());           holder.TvQuantity.setTypeface(matDrawerFont);
        holder.TvPrice.setText("₹"+model.getPrice()+".00");             holder.TvPrice.setTypeface(matDrawerFont);
        holder.TvSts.setText(model.getStatus());
        holder.TvDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(activity, OrderDetailsActivity.class);
                in.putExtra(AppConstants.orderid,""+model.getId());
                activity.startActivity(in);
            }
        });
        if(!model.getProductimage().isEmpty()) {
//            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);
            Glide.with(activity).load(model.getProductimage()).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(holder.ImgItem);
//            Picasso.with(activity)
//                    .load(model.getProductimage())
//                    .into(holder.ImgItem, new Callback() {
//                        @Override
//                        public void onSuccess() {
////                            progressbar.setVisibility(View.GONE);
//                        }
//
//                        @Override
//                        public void onError() {
//
//                        }
//                    });
        }



    }

    @Override
    public int getItemCount() {
        return NoOfItems.size();
    }
    public  class  ViewHolder extends RecyclerView.ViewHolder{

        private final ImageView ImgItem;
        private final TextView TvOrderId,TvProductTitle,TvOrderDate,TvCustName, TvQuantity,TvPrice,TvSts,TvDetails;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
            matDrawerFont = Typeface.createFromAsset(activity.getAssets(), "materialdrawerfont.ttf");
//        queue = Volley.newRequestQueue(activity);

            TvOrderId = (TextView) itemView.findViewById(R.id.tvOrderId);
            ImgItem = (ImageView) itemView.findViewById(R.id.itemImage);
            TvProductTitle = (TextView) itemView.findViewById(R.id.tvProductTitle);
            TvOrderDate = (TextView) itemView.findViewById(R.id.tvOrderDate);
            TvCustName = (TextView) itemView.findViewById(R.id.tvName);
            TvQuantity = (TextView) itemView.findViewById(R.id.tvQuantity);
            TvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
             TvDetails = (TextView) itemView.findViewById(R.id.TvDetails);
             TvSts = (TextView) itemView.findViewById(R.id.tvStatus);
            LinearLayout LLMain = (LinearLayout) itemView.findViewById(R.id.llMain);

        }
    }

}
