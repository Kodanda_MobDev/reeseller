package com.prominere.reseller.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.ResellerWalletActivity;
import com.prominere.reseller.model.WalletWRModel;

import java.util.List;

public class WRWalletAdapter extends RecyclerView.Adapter<WRWalletAdapter.ViewHolder> {

    private final List<WalletWRModel> NoOfItems;
    private final Activity activity;

    public WRWalletAdapter(Activity activity, List<WalletWRModel> wrwalletList) {
        this.activity = activity;
        this.NoOfItems=wrwalletList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.wallet_lv_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WalletWRModel model = NoOfItems.get(position);
        holder.TvDate.setText(model.getDateadded().substring(0,model.getDateadded().indexOf(" ")));
//        holder.TvAmt.setText("₹"+model.getAmount());
//        holder.TvAmount.setText("₹"+model.getAmount());
        holder.TvTransType.setText(model.getStatus());
        holder.TvCreatedDate.setText(model.getDateadded());
        holder.TvTransactionID.setText("ID: "+model.getTransactionid());

//        if(model.getTransaction().toUpperCase().equalsIgnoreCase("PLUS")){
            holder.TvStatus.setText(model.getStatus());
//            holder.TvStatus.setTextColor(activity.getResources().getColor(R.color.insta_green));
            holder.TvAmount.setText("+ ₹ "+model.getAmount()+".00");
//            holder.TvAmount.setTextColor(activity.getResources().getColor(R.color.insta_green));
//        }else{
//            holder.TvStatus.setText("Withdrawal");
//            holder.TvStatus.setTextColor(activity.getResources().getColor(R.color.red));
//            holder.TvAmount.setText("- ₹ "+model.getAmount());
//            holder.TvAmount.setTextColor(activity.getResources().getColor(R.color.red));
//        }
    }

    @Override
    public int getItemCount() {
        return NoOfItems.size();
    }


    public  class ViewHolder extends RecyclerView.ViewHolder {

        private final Typeface fontAwesomeFont;
        private final TextView TvDate,TvAmount,TvAmt,TvStatus,TvCreatedDate,TvTransType,TvTransactionID;

        public ViewHolder(View itemView) {
            super(itemView);
            fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");

            TvDate = (TextView) itemView.findViewById(R.id.tvDate);
            TvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
            TvAmt = (TextView) itemView.findViewById(R.id.tvAmt);
            TvAmount = (TextView) itemView.findViewById(R.id.tvAmount);
            TvTransType = (TextView) itemView.findViewById(R.id.tvTransType);
            TvTransactionID = (TextView) itemView.findViewById(R.id.tvTransactionID);
            TvCreatedDate = (TextView) itemView.findViewById(R.id.tvCreatedDate);
        }
    }

}
