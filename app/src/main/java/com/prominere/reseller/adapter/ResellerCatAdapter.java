package com.prominere.reseller.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.SubCategoryActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.ResellerCatModel;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ResellerCatAdapter extends RecyclerView.Adapter<ResellerCatAdapter.ViewHolder> {
    private final Activity activity;
    private final List<ResellerCatModel> NoOfItems;

    public ResellerCatAdapter(Activity activity, List<ResellerCatModel> productList) {
        this.activity = activity;
        this.NoOfItems=productList;
    }

    @NonNull
    @Override
    public ResellerCatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_single_card,parent,false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ResellerCatModel model = NoOfItems.get(position);
        holder.productTitle.setText(model.getCname());
        if(!model.getImage().isEmpty()) {
//            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);
//            loadImages(model.getImage(),holder.imgItem);
            Glide.with(activity).load(model.getImage()).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(holder.imgItem);
        }
        holder.LLMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Integer.parseInt(model.getProductcount())>0){
                    Intent in = new Intent(activity, SubCategoryActivity.class);
                    in.putExtra(AppConstants.category,""+model.getId());
                    activity.startActivity(in);
                }else{
                    SimpleToast.error(activity,model.getCname()+" Products are empty");
                }

            }
        });



    }

    @Override
    public int getItemCount() {
        return NoOfItems.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder {

        private final Typeface fontAwesomeFont;
        private final LinearLayout LLMain;
        public ImageView imgItem;
        public TextView productTitle;
        public ViewHolder(View itemView) {
            super(itemView);
            fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");

            imgItem = (ImageView) itemView.findViewById(R.id.itemImage);
            productTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            LLMain = (LinearLayout) itemView.findViewById(R.id.llMain);
        }
    }

    private void loadImages(String url, ImageView img) {
        if(!url.isEmpty()) {
//            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);

            AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
            builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
            final AlertDialog alertDialog = builder.show();

            Picasso.with(activity)
                    .load(url)
                    .into(img, new Callback() {
                        @Override
                        public void onSuccess() {
                            if(alertDialog.isShowing()){alertDialog.dismiss();}
//                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            if(alertDialog.isShowing()){alertDialog.dismiss();}
                        }
                    });

        }

    }



}
