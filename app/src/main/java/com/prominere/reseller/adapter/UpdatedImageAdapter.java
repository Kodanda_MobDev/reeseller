package com.prominere.reseller.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prominere.reseller.R;
import com.prominere.reseller.helper.AppHelper;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UpdatedImageAdapter   extends RecyclerView.Adapter<UpdatedImageAdapter.ViewHolder> {


    private final Activity activity;
    private final List<String> NoOfItems;
    private final RecyclerView RCView;
    private ArrayList<Bitmap> btList;

    public UpdatedImageAdapter(Activity act, List<String> lst,RecyclerView RC) {
        activity = act ;
        NoOfItems = lst ;
        RCView = RC ;

    }




    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.photo_item,parent,false);


        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        String url = NoOfItems.get(position);
        btList  = new ArrayList<Bitmap>();
        new DownLoadImageTask(holder.imgItem).execute(url.trim());

        holder.ImgCearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
      AppHelper.setDelayToView(holder.ImgCearBtn);
                RCView.setVisibility(View.GONE);

                btList.remove(position);


                NoOfItems.remove(position);
                notifyDataSetChanged();
                RCView.setVisibility(View.VISIBLE);
            }
        });
        RCView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return NoOfItems.size();
    }

    public List<Bitmap> getUpdatedList() {

        return btList;
    }

    public  class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imgItem;
        private final ImageButton ImgCearBtn;

        public ViewHolder(View itemView) {
            super(itemView);
         //   fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");

            imgItem = (ImageView) itemView.findViewById(R.id.iv_photo);
            ImgCearBtn = (ImageButton) itemView.findViewById(R.id.ib_remove);
           }
    }



    private class DownLoadImageTask extends AsyncTask<String,Void,Bitmap> {
        ImageView imageView;
        private AlertDialog alertDialog;

        public DownLoadImageTask(ImageView imageView){
            this.imageView = imageView;
        }

        @Override
        protected void onPreExecute() {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
            builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
            alertDialog = builder.show();

            super.onPreExecute();
        }

        /*
                    doInBackground(Params... params)
                        Override this method to perform a computation on a background thread.
                 */
        protected Bitmap doInBackground(String...urls){
            String urlOfImage = urls[0];
            Bitmap logo = null;
            try{
                InputStream is = new URL(urlOfImage).openStream();
                /*
                    decodeStream(InputStream is)
                        Decode an input stream into a bitmap.
                 */
                logo = BitmapFactory.decodeStream(is);
            }catch(Exception e){ // Catch the download exception
                e.printStackTrace();
            }
            return logo;
        }

        /*
            onPostExecute(Result result)
                Runs on the UI thread after doInBackground(Params...).
         */
        protected void onPostExecute(Bitmap result){
            if(alertDialog.isShowing()){alertDialog.dismiss();}
            imageView.setImageBitmap(result);
             btList.add(result);
        }
    }


}
