package com.prominere.reseller.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.ProductsActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.ProductSubCategory;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder> {

    private ArrayList<ProductSubCategory> itemsList;
    private Context mContext;
    private Typeface textFont;

    public SectionListDataAdapter(Context context, ArrayList<ProductSubCategory> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }
    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_single_card, null);
        textFont = Typeface.createFromAsset(mContext.getAssets(), "materialdrawerfont.ttf");
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }
    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        final ProductSubCategory singleItem = itemsList.get(i);

        holder.tvTitle.setText(singleItem.getCname());
  //      holder.tvTitle.setTypeface(textFont);

Log.i("singleItem: "," "+singleItem.toString());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                Intent in = new Intent(mContext, ProductsActivity.class);
                in.putExtra(AppConstants.category,singleItem.getCategory());
                in.putExtra(AppConstants.subCategory,singleItem.getId());
                mContext.startActivity(in);
            }
        });


        if(!singleItem.getImage().isEmpty()) {

            Glide.with(mContext).load(singleItem.getImage()).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into(holder.itemImage);
//            Picasso.with(mContext)
//                    .load(singleItem.getImage())
//                    .into(holder.itemImage, new Callback() {
//                        @Override
//                        public void onSuccess() {
////                            progressbar.setVisibility(View.GONE);
//                        }
//
//                        @Override
//                        public void onError() {
//
//                        }
//                    });

        }else{
            holder.itemImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.no_profile_photo));
        }
//        LoadImage task = new LoadImage();
//        task.execute(singleItem.getImage());

//    if(!singleItem.getImage().isEmpty()){
//        Glide.with(mContext)
//                .load(singleItem.getImage())
//                .override(300, 200)
//                .into(holder.itemImage);
//
//    }


    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;
        protected ImageView itemImage;
        protected View view;


        public SingleItemRowHolder(View view) {
            super(view);
            this.view = view;
            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);




        }

    }


    private class LoadImage extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
        }
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpURLConnection.setFollowRedirects(false);
                HttpURLConnection con =  (HttpURLConnection) new URL(params[0]).openConnection();
                con.setRequestMethod("HEAD");
                System.out.println(con.getResponseCode());
                return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            boolean bResponse = result;
            if (bResponse==true)
            {
                Toast.makeText(mContext, "File exists!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(mContext, "File does not exist!", Toast.LENGTH_SHORT).show();
            }
        }
    }


}