package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.ProductModel;
import com.prominere.reseller.activity.reseller.ProductDetailsActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends BaseAdapter {
    private final Activity activity;
    private final ArrayList<ProductModel> NoOfItems;
    private final String cond;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private ImageView imgItem;
    private TextView productPrice,productTitle;
    private LinearLayout LLItem;

    public ProductAdapter(Activity activity, ArrayList<ProductModel> productList,String cond) {
        this.activity = activity;
        this.NoOfItems=productList;
        this.cond=cond;
    }



    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.product_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
//        queue = Volley.newRequestQueue(activity);


        final ProductModel model = NoOfItems.get(position);

        LLItem = (LinearLayout) convertView.findViewById(R.id.llItem);
        imgItem = (ImageView) convertView.findViewById(R.id.img);
        productTitle = (TextView) convertView.findViewById(R.id.tvProduct);
        productPrice = (TextView) convertView.findViewById(R.id.tvPrice);

        productTitle.setText(model.getTitle());
        productPrice.setText("₹"+model.getDiscountprice()+".00");

        LLItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cond.equalsIgnoreCase(AppConstants.search)){
                    Intent in = new Intent(activity, ProductDetailsActivity.class);
                    in.putExtra(AppConstants.productid,model.getId());
//                    in.putExtra(AppConstants.producttitle,model.getCategory());
                    activity.startActivity(in);
//                    activity.finish();
                }else{
                    Intent in = new Intent(activity, ProductDetailsActivity.class);
                    in.putExtra(AppConstants.productid,model.getId());
//                    in.putExtra(AppConstants.producttitle,model.getCategory());
                    activity.startActivity(in);
                }
            }
        });


        if(!model.getImage().isEmpty()) {
            List<String> listOfImgs = AppHelper.getImageURL(model.getImage());
            if(listOfImgs.size()>0){
                for(int i=0;i<listOfImgs.size();i++){
                    Log.i("Images:--> "+i," "+listOfImgs.get(i));
                    switch (i){
                        case 0:
                            Glide.with(activity).load(listOfImgs.get(i)).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                                    .into(imgItem);
                            break;
                    }
                }
            }

        }


        return convertView;
    }



}
