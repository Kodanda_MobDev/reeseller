package com.prominere.reseller.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.vendoractivity.VendorAddProductActivity;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.ProductModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class DBProductAdapter extends RecyclerView.Adapter<DBProductAdapter.ViewHolder>  {


    private final Activity activity;
    private final ArrayList<ProductModel> NoOfItems;
    private ArrayList<ProductModel> arraylist;
    private final RequestQueue mQueue;

    public DBProductAdapter(Activity activity, ArrayList<ProductModel> productList) {
        this.activity = activity;
        this.NoOfItems=productList;
        mQueue = Volley.newRequestQueue(activity);
        this.arraylist = new ArrayList<ProductModel>();
        this.arraylist.addAll(NoOfItems);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.dbproduct_lv_row,parent,false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final ProductModel model = NoOfItems.get(position);

        holder.productTitle.setText(model.getTitle());
        holder.productPrice.setText("₹"+model.getDiscountprice()+".00");
        holder.TvDiscPrice.setText("₹"+model.getPrice()+".00");     holder.TvDiscPrice.setPaintFlags(holder.TvDiscPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.TvProductCat.setText(model.getCategory());

        AppHelper.loadImageViewByUrl(activity,holder.ImgItem,model.getImage());
        holder.ImgUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(activity, VendorAddProductActivity.class);
                in.putExtra(AppConstants.productid,model.getId());
                activity.startActivity(in);
            }
        });
        holder.ImgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RemoveProduct(model,position);
            }
        });
    }

    private void RemoveProduct(final ProductModel model, final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(activity.getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Remove Products Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("AddToWishList:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
//                                RefreshData();
                                NoOfItems.remove(position);
                                notifyDataSetChanged();
                            }else{
                                SimpleToast.error(activity, AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.delete_product);
                params.put("productid",model.getId());
                params.put("vendorid", SessionSave.getsession(AppConstants.userid,activity));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }


    @Override
    public int getItemCount() {
        return NoOfItems.size();
    }


    public void filter(String charText , TextView emptyAlert) {
        charText = charText.toLowerCase(Locale.getDefault());
        NoOfItems.clear();
        if (charText.length() == 0) {
            NoOfItems.addAll(clearListFromDuplicates(arraylist));
        }
        else
        {
            for (ProductModel wp : arraylist)
            {
                if (wp.getTitle().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    if( !NoOfItems.contains(wp)){
                        NoOfItems.add(wp);
                    }
                }
            }
        }
        if(NoOfItems.size() == 0){   emptyAlert.setVisibility(View.VISIBLE);
        }else{     emptyAlert.setVisibility(View.GONE);         }
        notifyDataSetChanged();
    }
    public static List<ProductModel> clearListFromDuplicates(List<ProductModel> list1) {
        Map<String, ProductModel> cleanMap = new LinkedHashMap<String, ProductModel>();
        for (int i = 0; i < list1.size(); i++) {
            cleanMap.put(list1.get(i).getId(), list1.get(i));
        }
        List<ProductModel> list = new ArrayList<ProductModel>(cleanMap.values());
        return list;
    }



    public  class ViewHolder extends RecyclerView.ViewHolder {
        private final Typeface fontAwesomeFont;
        public ImageView ImgUpdate,ImgRemove,ImgItem;
        public TextView productTitle,TvProductCat,productPrice,TvRemove,TvDiscPrice;
        public ViewHolder(View itemView) {
            super(itemView);
            fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");

            productTitle = (TextView) itemView.findViewById(R.id.tvProductname);
            productPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            ImgItem = (ImageView) itemView.findViewById(R.id.imgItem);
            TvProductCat = (TextView) itemView.findViewById(R.id.tvProductCat);
            TvDiscPrice = (TextView) itemView.findViewById(R.id.tvDiscountPrice);
            ImgUpdate = (ImageView) itemView.findViewById(R.id.imgEdit);
            ImgRemove = (ImageView) itemView.findViewById(R.id.imRemove);
        }
    }


}
