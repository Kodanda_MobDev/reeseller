package com.prominere.reseller.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.model.NotificationModel;

import java.util.ArrayList;

public class NotificationsAdapter  extends BaseAdapter {
    private final Activity activity;
    private final ArrayList<NotificationModel> NoOfItems;
    private LayoutInflater inflater;
    private Typeface fontAwesomeFont;
    private RequestQueue mQueue;
    private TextView TvDate,Tvnotif;

    public NotificationsAdapter(Activity activity, ArrayList<NotificationModel> productList) {
        this.activity = activity;
        this.NoOfItems=productList;
    }


    @Override
    public int getCount() {
        return NoOfItems.size();
    }
    @Override
    public Object getItem(int position) {
        return NoOfItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.notification_lv_row, null);
        fontAwesomeFont = Typeface.createFromAsset(activity.getAssets(), "fontawesome-webfont.ttf");
        mQueue = Volley.newRequestQueue(activity);


        NotificationModel model = NoOfItems.get(position);

        Tvnotif = (TextView) convertView.findViewById(R.id.tvNotif);
        TvDate = (TextView) convertView.findViewById(R.id.tvDate);

        Tvnotif.setText(model.getMessage());
        TvDate.setText(model.getDate());

        return convertView;
    }






}
