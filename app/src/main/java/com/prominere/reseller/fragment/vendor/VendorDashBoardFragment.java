package com.prominere.reseller.fragment.vendor;

import android.app.AlertDialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.adapter.VendorDBAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.VendorDBModel;
import com.prominere.reseller.util.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VendorDashBoardFragment extends Fragment {


    private View rootView;
    private TextView TvMonth,TvYear,TvWeek,TvDay,TvLifeTimeSale,TvTotalSale,TvDate,TvEmptyBox;
    private ArrayList<TextView> TvFilterList;
    private RecyclerView LvProducts;
    private RequestQueue mQueue;

    public VendorDashBoardFragment() {
    }
 @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView != null)        {
            ViewGroup parent=(ViewGroup)rootView.getParent();
            if(parent != null)
            {   parent.removeView(rootView);    }
        }//if
        try {
            rootView = inflater.inflate(R.layout.vendor_dashboard_fragment , container, false);

            TvFilterList = new ArrayList<TextView>();

            mQueue = Volley.newRequestQueue(getActivity());
            LvProducts = (RecyclerView)rootView.findViewById(R.id.lvProducts);
            TvEmptyBox = (TextView)rootView.findViewById(R.id.imgEmptyBox);
            TvDate = (TextView)rootView.findViewById(R.id.tvDate);
            TvTotalSale = (TextView)rootView.findViewById(R.id.tvTotalSale);
            TvLifeTimeSale = (TextView)rootView.findViewById(R.id.tvLifeTimeSale);
            TvDay = (TextView)rootView.findViewById(R.id.tvDay);    TvFilterList.add(TvDay);          setBackgroundsTvbyClick(TvDay);
            TvWeek = (TextView)rootView.findViewById(R.id.tvWeek);   TvFilterList.add(TvWeek);        setBackgroundsTvbyClick(TvWeek);
            TvMonth = (TextView)rootView.findViewById(R.id.tvMonth);   TvFilterList.add(TvMonth);     setBackgroundsTvbyClick(TvMonth);
            TvYear = (TextView)rootView.findViewById(R.id.tvYear);   TvFilterList.add(TvYear);        setBackgroundsTvbyClick(TvYear);
            TvDay.performClick();
            getProducts();


        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }//onCreateView

    private void getFilterDate(final String filtVal) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Products:", "success! response: " + response.toString());
     //{"status":"success","orders":[{"date":"Mon, 13 May","totalsale":0,"lifetimesale":0}]}
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                JSONArray mainArray = new JSONArray(mainObj.getString("orders"));
                                if(mainArray.length()>0){
//                                    TvEmptyBox.setVisibility(View.GONE);
                                    JSONObject obj = mainArray.getJSONObject(0);
                                    TvDate.setText(obj.getString("date"));
                                    if(obj.getString("totalsale").equalsIgnoreCase("null")){
                                        TvTotalSale.setText("Total Sale: ₹0.00");
                                    }else{
                                        TvTotalSale.setText("Total Sale: ₹"+ obj.getString("totalsale")+".00");
                                    }
                                     if(obj.getString("lifetimesale").equalsIgnoreCase("null")){
                                         TvLifeTimeSale.setText("₹0.00");
                                     }else {
                                         TvLifeTimeSale.setText("₹" + obj.getString("lifetimesale") + ".00");
                                     }
                                }else{
//                                    TvEmptyBox.setVisibility(View.VISIBLE);
                                }


                            }else{
                                TvEmptyBox.setVisibility(View.VISIBLE);
                                //   SimpleToast.error(CartItemsActivity.this, AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.home_salefilter);
                params.put("vendorid", SessionSave.getsession(AppConstants.userid,getActivity()));
                params.put("filtervalue",filtVal);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    private void getProducts() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Products:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            ArrayList<VendorDBModel> productList = new ArrayList<VendorDBModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){

                                JSONArray mainArray = new JSONArray(mainObj.getString("orders"));
                                if(mainArray.length()>0) {
                                    TvEmptyBox.setVisibility(View.GONE);
                                    int total = 0;
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        VendorDBModel catList = new VendorDBModel();
                                        catList.setId(obj.getString("id"));
                                        catList.setTitle(obj.getString("title"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setOrderid(obj.getString("orderid"));
                                        catList.setOrderdate(obj.getString("orderdate"));
                                        catList.setPrice(obj.getString("price"));
                                        productList.add(catList);
                                    }
                                    VendorDBAdapter dbradapter = new VendorDBAdapter(getActivity(), productList);

                                    LvProducts.setLayoutManager(new LinearLayoutManager(getActivity()) );

                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                    TvEmptyBox.setVisibility(View.VISIBLE);
                                }

                            }else{
                                TvEmptyBox.setVisibility(View.VISIBLE);
                                //   SimpleToast.error(CartItemsActivity.this, AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.vendor_home);
//                params.put("vendorid","2");
                params.put("vendorid", SessionSave.getsession(AppConstants.userid,getActivity()));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }



    private void setBackgroundsTvbyClick(final TextView Tv) {
        Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for(int i=0;i<TvFilterList.size();i++){
                    final TextView tvFilter = TvFilterList.get(i);
                    if(Tv.getText().toString() ==  tvFilter.getText().toString()){
                         switch (Tv.getText().toString()){

                            case "6 Months":       getFilterDate("6months");
                                Tv.setTextColor(getResources().getColor(R.color.white));
                                Tv.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg_righttop_crop));
                                break;
                            case "Day":  getFilterDate("day");
                                Tv.setTextColor(getResources().getColor(R.color.white));
                                Tv.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg_lefttop_crop));
                                break;
                            default:    getFilterDate(Tv.getText().toString().toLowerCase());
                                Tv.setTextColor(getResources().getColor(R.color.white));
                                Tv.setBackgroundColor(getResources().getColor(R.color.appColor));
                                break;
                        }

                    }else{
                        switch (tvFilter.getText().toString()){
                            case "6 Months":
                                tvFilter.setTextColor(getResources().getColor(R.color.appColor));
                                tvFilter.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg_righttop_crop));
                                tvFilter.getBackground().setColorFilter(getResources().getColor(R.color.border_color),PorterDuff.Mode.SRC_ATOP);
                                break;
                            case "Day":
                                tvFilter.setTextColor(getResources().getColor(R.color.appColor));
                                tvFilter.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg_lefttop_crop));
                                tvFilter.getBackground().setColorFilter(getResources().getColor(R.color.border_color),PorterDuff.Mode.SRC_ATOP);
                                break;
                            default:
                                tvFilter.setTextColor(getResources().getColor(R.color.appColor));
                                tvFilter.setBackgroundColor(getResources().getColor(R.color.border_color));
                                break;
                        }

                    }
                }

            }
        });

    }

}
