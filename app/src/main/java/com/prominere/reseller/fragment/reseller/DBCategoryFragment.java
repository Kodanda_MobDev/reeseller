package com.prominere.reseller.fragment.reseller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.adapter.DBLVAdapter;
import com.prominere.reseller.adapter.DashBoardAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.ProductCategory;
import com.prominere.reseller.model.ProductSubCategory;
import com.prominere.reseller.model.SectionDataModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.activity.reseller.SearchActivity;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DBCategoryFragment  extends Fragment {


    private View rootView;
    private RequestQueue mQueue;
    private SliderView sliderView;
    private ArrayList<SectionDataModel> allSampleData;
    private String actionType  ;
    private ArrayList<ProductCategory> productCatList;
    private ListView LvDB;
    private HashMap<String, ArrayList<ProductSubCategory>> subItems;
    private TextView TvSearch;

    public DBCategoryFragment() {
 }
 @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView != null)        {
            ViewGroup parent=(ViewGroup)rootView.getParent();
            if(parent != null)
            {   parent.removeView(rootView);    }
        }//if
        try {
            rootView = inflater.inflate(R.layout.dashboard_fragment , container, false);

            mQueue = Volley.newRequestQueue(getActivity());
            sliderView = (SliderView)rootView.findViewById(R.id.imageSlider);
//            my_recycler_view = (RecyclerView)rootView.findViewById(R.id.db_recycler_view);
            LvDB = (ListView)rootView.findViewById(R.id.lvDB);
            TvSearch = (TextView)rootView.findViewById(R.id.tvSearch);

            allSampleData = new ArrayList<SectionDataModel>();

            getProductCategories();
//          getUserDetails();
            TvSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppHelper.setDelayToView(TvSearch);
                    Intent in = new Intent(getActivity(), SearchActivity.class);
                    startActivity(in);
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }//onCreateView


    private void getProductCategories() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            productCatList = new ArrayList<ProductCategory>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                JSONArray mainArray = new JSONArray(mainObj.getString("categories"));
                                if(mainArray.length()>0){

                                    for(int i=0;i<mainArray.length();i++){
                                        SectionDataModel dm = new SectionDataModel();
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        ProductCategory catList = new ProductCategory();
                                        catList.setId(obj.getString("id"));
                                        catList.setCname(obj.getString("cname"));
                                        catList.setSlug(obj.getString("slug"));
                                        catList.setStatus(obj.getString("status"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setSubcategories(new JSONArray(obj.getString("subcategories")));
                                        productCatList.add(catList);
                                        dm.setHeaderTitle(obj.getString("cname"));
                                        dm.setCategoryId(obj.getString("id"));
                                        allSampleData.add(dm);
                                    }

//                                for(int i=0;i<productCatList.size();i++){
//                                    SectionDataModel dm = new SectionDataModel();
//                                    dm.setHeaderTitle(productCatList.get(i).getCname());
//                                    ArrayList<ProductCategory> singleItem = new ArrayList<ProductCategory>();
//                                    singleItem.add(productCatList.get(i));
//                                    dm.setAllItemsInSection(singleItem);
//                                    allSampleData.add(dm);
//
//                                }

//
//                                    ResellerHomeSliderAdapter adapter = new ResellerHomeSliderAdapter(getActivity(),productCatList);
//
//                                    sliderView.setSliderAdapter(adapter);
//                                    sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
//                                    sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
//                                    sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
//                                    sliderView.setIndicatorSelectedColor(Color.WHITE);
//                                    sliderView.setIndicatorUnselectedColor(Color.GRAY);
//                                    sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
//                                    sliderView.startAutoCycle();

                                    //   getSubCategory(allSampleData);

//
                                    DashBoardAdapter dbradapter = new DashBoardAdapter(getActivity(), productCatList);
//                                DBLVAdapter dbradapter = new DBLVAdapter(getActivity(), allSampleData);
                                    LvDB.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
                                    AppHelper.setListViewHeightBasedOnChildren(LvDB);


//                                my_recycler_view.setHasFixedSize(true);
//
//                                DBRecyclerViewDataAdapter dbradapter = new DBRecyclerViewDataAdapter(getActivity(), allSampleData);
//
//                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
//                                my_recycler_view.setLayoutManager(linearLayoutManager);
//                              //  my_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity() , LinearLayoutManager.VERTICAL, false));
//
//                                my_recycler_view.setAdapter(dbradapter);
//                                dbradapter.notifyDataSetChanged();
                                }else{
                               //     SimpleToast.error(getActivity(),"Failed to execute Api");
                                }

                            }else{
                              //  SimpleToast.error(getActivity(),"Failed to execute Api");
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.getcategories_product);
                params.put("phone",SessionSave.getsession(AppConstants.phone,getActivity()));
                params.put("password",SessionSave.getsession(AppConstants.password,getActivity()));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(AppConstants.volleyTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });

        mQueue.add(sr);



    }

    private void getUserDetails() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();


        if(SessionSave.getsession(AppConstants.usertype,getActivity()).equalsIgnoreCase("Vendor")){
            actionType = "getprofile_vendor";
        }else{
            actionType = "getprofile_reseller";
        }


        String url = ApiHelper.appDomain;
        Log.i("UserDetailsApi:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                JSONArray mainArray = new JSONArray(mainObj.getString("data"));
                                //{"status":"success","data":[{"id":"2","name":"Rady","city":"kakinada","state":"AP","address":"PP,Cambodia","email":"yoemrattana168@gmail.com","phone":"099","gst":"45345345345",
                                // "company":"GVK","billing_add1":"Main road ","billing_add2":"Main road , near temple street","pickup_add1":"Main road ","pickup_add2":"Main road "}]}

                                if(SessionSave.getsession(AppConstants.usertype,getActivity()).equalsIgnoreCase("Vendor")){
                                    JSONObject obj = mainArray.getJSONObject(0);
                                    SessionSave.saveSession(AppConstants.id, AppHelper.checkStringEmptyOrNot(obj.getString("id")), getActivity());
                                    SessionSave.saveSession(AppConstants.name, AppHelper.checkStringEmptyOrNot(obj.getString("name")), getActivity());
                                    SessionSave.saveSession(AppConstants.city, AppHelper.checkStringEmptyOrNot(obj.getString("city")), getActivity());
                                    SessionSave.saveSession(AppConstants.state, AppHelper.checkStringEmptyOrNot(obj.getString("state")), getActivity());
                                    SessionSave.saveSession(AppConstants.address, AppHelper.checkStringEmptyOrNot(obj.getString("address")), getActivity());
                                    SessionSave.saveSession(AppConstants.email, AppHelper.checkStringEmptyOrNot(obj.getString("email")), getActivity());
                                    SessionSave.saveSession(AppConstants.phone, AppHelper.checkStringEmptyOrNot(obj.getString("phone")), getActivity());
                                    SessionSave.saveSession(AppConstants.gst, AppHelper.checkStringEmptyOrNot(obj.getString("gst")), getActivity());
                                    SessionSave.saveSession(AppConstants.company, AppHelper.checkStringEmptyOrNot(obj.getString("company")), getActivity());
                                    SessionSave.saveSession(AppConstants.billing_add1, AppHelper.checkStringEmptyOrNot(obj.getString("billing_add1")), getActivity());
                                    SessionSave.saveSession(AppConstants.billing_add2, AppHelper.checkStringEmptyOrNot(obj.getString("billing_add2")), getActivity());
                                    SessionSave.saveSession(AppConstants.pickup_add1, AppHelper.checkStringEmptyOrNot(obj.getString("pickup_add1")), getActivity());
                                    SessionSave.saveSession(AppConstants.pickup_add2, AppHelper.checkStringEmptyOrNot(obj.getString("pickup_add2")), getActivity());

                                }else {
                                    JSONObject obj = mainArray.getJSONObject(0);
                                    SessionSave.saveSession(AppConstants.id, AppHelper.checkStringEmptyOrNot(obj.getString("id")), getActivity());
                                    SessionSave.saveSession(AppConstants.name, AppHelper.checkStringEmptyOrNot(obj.getString("name")), getActivity());
                                    SessionSave.saveSession(AppConstants.email, AppHelper.checkStringEmptyOrNot(obj.getString("email")), getActivity());
                                    SessionSave.saveSession(AppConstants.phone, AppHelper.checkStringEmptyOrNot(obj.getString("phone")), getActivity());
                                    SessionSave.saveSession(AppConstants.city, AppHelper.checkStringEmptyOrNot(obj.getString("city")), getActivity());
                                    SessionSave.saveSession(AppConstants.state, AppHelper.checkStringEmptyOrNot(obj.getString("state")), getActivity());
                                    SessionSave.saveSession(AppConstants.address1, AppHelper.checkStringEmptyOrNot(obj.getString("address1")), getActivity());
                                    SessionSave.saveSession(AppConstants.address2, AppHelper.checkStringEmptyOrNot(obj.getString("address2")), getActivity());
                                    SessionSave.saveSession(AppConstants.pincode, AppHelper.checkStringEmptyOrNot(obj.getString("pincode")), getActivity());

                                }

                                getProductCategories();
                            }else{
                                //    Toast.makeText(LoginActivity.this,"Invalid credentials",Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",actionType);
                params.put("userid",SessionSave.getsession(AppConstants.userid,getActivity()));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }//LogInByVolley

    private void getSubCategory(ArrayList<SectionDataModel> allSampleData) {
 subItems = new HashMap<String, ArrayList<ProductSubCategory>>();
        for(int i=0;i<allSampleData.size();i++){
            Log.i("CatId",allSampleData.get(i).getCategoryId());
            getSubItem(allSampleData,allSampleData.get(i).getCategoryId());
        }

    }

    private void getSubItem(final ArrayList<SectionDataModel> catList, final String categoryId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("subcategorie Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("subCategory", "success! response: " + response.toString());
                        if(response.toString().contains(getActivity().getResources().getString(R.string.phpErrorOccured))){
                        AppHelper.showServerErrorPopUp(getActivity(),"getsubcategories_product");
                        }else{
                            try {
                                JSONObject mainObj = new JSONObject(response);

                                if(mainObj.getString("status").equalsIgnoreCase("success")){
                                    JSONArray mainArray = new JSONArray(mainObj.getString("subcategories"));
                                    ArrayList<ProductSubCategory> singleSectionItems = new ArrayList<ProductSubCategory>();
                                    for(int i=0;i<mainArray.length();i++){
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        ProductSubCategory model = new ProductSubCategory();
                                        model.setId(obj.getString("id"));
                                        model.setCname(obj.getString("cname"));
                                        model.setSlug(obj.getString("slug"));
                                        model.setCategory(obj.getString("category"));
                                        model.setAttr(obj.getString("attr"));
                                        model.setStatus(obj.getString("status"));
                                        model.setImage(obj.getString("image"));
                                        singleSectionItems.add(model);
                                    }
                                    subItems.put(categoryId,singleSectionItems);

                                    if(catList.get(catList.size()-1).getCategoryId().equalsIgnoreCase(categoryId)){
                                        DBLVAdapter dbradapter = new DBLVAdapter(getActivity(), allSampleData,subItems);
                                        LvDB.setAdapter(dbradapter);
                                        dbradapter.notifyDataSetChanged();
                                        AppHelper.setListViewHeightBasedOnChildren(LvDB);
                                    }


                                }else{
                                    //     Toast.makeText(,"Invalid credentials",Toast.LENGTH_LONG).show();
                                }

                            }catch (Exception e){e.printStackTrace();}

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.getsubcategories_product);
                params.put("category",categoryId);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }


}
