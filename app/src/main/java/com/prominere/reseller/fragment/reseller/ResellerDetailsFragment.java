package com.prominere.reseller.fragment.reseller;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.helper.validations.ValidationDTO;
import com.prominere.reseller.helper.validations.ValidationHelper;
import com.prominere.reseller.helper.validations.ValidationUtils;
import com.prominere.reseller.util.CircleImageView;
import com.prominere.reseller.util.FilePath;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.VolleyMultipartRequest;
import com.prominere.reseller.util.cropview.CropImage;
import com.prominere.reseller.util.cropview.CropImageView;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class ResellerDetailsFragment extends Fragment {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 200;
    private static final int REQUEST_TAKE_PHOTO = 2;
    private final int GALLERY = 1;
    private View rootView;
    private RequestQueue mQueue;
    private String uId, filePath="",vendorImgUrl="";
    private CircleImageView ImgProfilePic;
    private  EditText EtName,EtEmail,EtPhone,EtCity, EtState,EtPincode,EtAddrs1,EtAddrs2;
    private Button BtnUpdate;
    static final int PICK_IMAGE_REQUEST = 1;

    private ImageView ImgUpdate;
    private AlertDialog optionsDialog;
    private Dialog PPUploadDialog;
    private ImageView ImgClose;

    public ResellerDetailsFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView != null)        {
            ViewGroup parent=(ViewGroup)rootView.getParent();
            if(parent != null)
            {   parent.removeView(rootView);    }
        }//if
        try {
            rootView = inflater.inflate(R.layout.reseller_details_fragment , container, false);

            mQueue = Volley.newRequestQueue(getActivity());


            ImgProfilePic = (CircleImageView)rootView.findViewById(R.id.imgProfilePic);
            ImgUpdate = (ImageView)rootView.findViewById(R.id.imgUpdate);
            EtName = (EditText)rootView.findViewById(R.id.etname);
            EtEmail = (EditText)rootView.findViewById(R.id.etEmail);
            EtPhone = (EditText)rootView.findViewById(R.id.etPhone);
            EtCity = (EditText)rootView.findViewById(R.id.etCity);
            EtState = (EditText)rootView.findViewById(R.id.etstate);
            EtAddrs1 = (EditText)rootView.findViewById(R.id.etAddrs1);
            EtAddrs2 = (EditText)rootView.findViewById(R.id.etAddrs2);
            EtPincode = (EditText)rootView.findViewById(R.id.etPinCode);
            BtnUpdate = (Button)rootView.findViewById(R.id.update);
            ImgClose = (ImageView)rootView.findViewById(R.id.imgBack);

            getResellerDetails();


            isGrantExternalRW(getActivity());

            BtnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppHelper.setDelayToView(BtnUpdate);

                    validations();
                    

                }
            });
            ImgProfilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    ShowPicPopUp();
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        Log.i("Camera Permission","DENIED");
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},  MY_PERMISSIONS_REQUEST_CAMERA);
                        checkPermissionOnAppPermScreen("Camera");
                    }else if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        Log.i("Storage Permission","DENIED");
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_STORAGE);
                        checkPermissionOnAppPermScreen("Storage");
                    }
                    else{
                        ShowUploadOptions();
//                        CropImage.activity()
//                                .setGuidelines(CropImageView.Guidelines.ON)
//                                .setActivityTitle("")
//                                .setCropShape(CropImageView.CropShape.RECTANGLE)
//                                .setCropMenuCropButtonTitle("Done")
//                                .setRequestedSize(400, 400)
//                                .setCropMenuCropButtonIcon(R.drawable.ic_done)
//                                .start(getActivity());
//
                        Log.i("C & S Permission","GRANTED");

                    }
                }
            });
            ImgUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        Log.i("Camera Permission","DENIED");
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},  MY_PERMISSIONS_REQUEST_CAMERA);
                        checkPermissionOnAppPermScreen("Camera");
                    }else if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        Log.i("Storage Permission","DENIED");
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_STORAGE);
                        checkPermissionOnAppPermScreen("Storage");
                    }
                    else{
                        ShowUploadOptions();
//                        CropImage.activity()
//                                .setGuidelines(CropImageView.Guidelines.ON)
//                                .setActivityTitle("")
//                                .setCropShape(CropImageView.CropShape.RECTANGLE)
//                                .setCropMenuCropButtonTitle("Done")
//                                .setRequestedSize(400, 400)
//                                .setCropMenuCropButtonIcon(R.drawable.ic_done)
//                                .start(getActivity());

                        Log.i("C & S Permission","GRANTED");

                    }
                }
            });
            ImgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new AccountFragment()).commit();
                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }//onCreateView

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }



    private void validations() {
        try{
            ValidationHelper helper=new ValidationHelper();
            String[] strIds = getResources().getStringArray(R.array.resellerDetails_ids_array);
            String[] strErrMsgs = getResources().getStringArray(R.array.resellerDetails_errors_array);
            String[] strCompTypeArr = getResources().getStringArray(R.array.resellerDetails_comptypes_array);
            ArrayList<ValidationDTO> aList = new ArrayList<ValidationDTO>();

            int iPos = 0;
            for(String strCompType:strCompTypeArr){
                ValidationDTO valDTO=new ValidationDTO();
                valDTO.setComponentType(strCompType);
                valDTO.setComponentID(ValidationUtils.getIdResourceByName(getActivity(),strIds[iPos]));
                valDTO.setErrorMessage(strErrMsgs[iPos]);
                aList.add(valDTO);
                iPos++;
            }
            boolean isValidData = helper.validateData(getActivity().getBaseContext(), aList,getActivity().getWindow().getDecorView() );
            if (!isValidData) {
                return;
            }else{

//                //    updateVendorDetails();
//                if ( (vendorImgUrl.isEmpty() && filePath.isEmpty() ) || ImgProfilePic.getDrawable() == null ) {
//                    Toast.makeText(getActivity(), "Image not selected!", Toast.LENGTH_LONG).show();
//                } else {
                    uploadBitmap();
//                }

            }

        }catch (Exception e){     e.printStackTrace();    }

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("OnActivityResult-->"," RC:"+requestCode+" ResCode:"+resultCode );
        super.onActivityResult(requestCode, resultCode, data);

//        if (resultCode == RESULT_OK) {
        if (resultCode == -1) {

            if(requestCode == PICK_IMAGE_REQUEST){
                if(data != null) {
                    Uri picUri = data.getData();

//                    filePath = getPath(picUri);

                    String filePath;
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= 24) {
                        filePath = AppHelper.getFilePathForN( picUri ,getActivity() );
                    } else {
                        filePath = FilePath.getPath(getActivity(), picUri);
                    }

                    Log.d("picUri", picUri.toString());
                    Log.d("filePath", filePath);


                    try {

                        Bitmap picBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), picUri);
                        // imageView.setImageBitmap(bitmap);
                        ByteArrayOutputStream bao = new ByteArrayOutputStream();
                        picBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
                      //  ImgProfilePic.setImageURI(picUri);
                        // uploadImage(bitmap);


                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = false;



                        InputStream imageStream = null;
                        try {


                            imageStream = getActivity().getContentResolver().openInputStream(picUri);
                            BitmapFactory.decodeStream(imageStream, null, options);
                            imageStream.close();

                            int MAX_HEIGHT = 1024;
                            int MAX_WIDTH = 1024;
                            // Calculate inSampleSize
                            options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

                            // Decode bitmap with inSampleSize set
                            options.inJustDecodeBounds = false;
                            imageStream = getActivity().getContentResolver().openInputStream(picUri);
                            Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

                            img = rotateImageIfRequired(img, picUri);

//                    Bitmap bm = getPic(filePath);
                            ImgProfilePic.setImageBitmap(img);

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else{
                    SimpleToast.error(getActivity(),"Please move file to internal storage. ");
                }



            }

            if(requestCode == REQUEST_TAKE_PHOTO ) {
                Log.i("Camera----> ", "Request");
                Log.i("mCurrentPhotoPath----> ", "" + filePath);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = false;



                InputStream imageStream = null;
                try {
                    Uri uri = getImageUri(filePath);

                    imageStream = getActivity().getContentResolver().openInputStream(uri);
                    BitmapFactory.decodeStream(imageStream, null, options);
                    imageStream.close();

                    int MAX_HEIGHT = 1024;
                    int MAX_WIDTH = 1024;
                    // Calculate inSampleSize
                    options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

                    // Decode bitmap with inSampleSize set
                    options.inJustDecodeBounds = false;
                    imageStream = getActivity().getContentResolver().openInputStream(uri);
                    Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

                    img = rotateImageIfRequired(img, uri);

//                    Bitmap bm = getPic(filePath);
                    ImgProfilePic.setImageBitmap(img);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }




            }

            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {

                    if (data != null) {
                        try {

                            Uri contentURI = result.getUri();

                            try {

                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                                // imageView.setImageBitmap(bitmap);
                                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);

                                ImgProfilePic.setImageURI(contentURI);
                                // uploadImage(bitmap);


                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Toast.makeText(getActivity(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
                }
            }


            if( resultCode == RESULT_OK) {
                if (requestCode == GALLERY) {
                    if (data != null) {
                        Uri contentURI = data.getData();
                        try {

//
//                            Uri uri = Uri.fromFile(File.createTempFile("temp", ".jpg", getActivity().getCacheDir()));
//                            OutputStream outputStream = getActivity().getContentResolver().openOutputStream(uri);
//                            Bitmap pBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//                            pBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
//                            pBitmap = rotateImageIfRequired(pBitmap, uri);
//                            outputStream.close();
//                            Picasso.with(getActivity()).load(uri).into(ImgProfilePic);
//

                             Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                           // ImgProfilePic.setImageBitmap(bitmap);

//                            bitmap = rotateImageIfRequired(bitmap, contentURI);
//                            ImgProfilePic.setImageBitmap(bitmap);
                            loadBitmapByPicasso(getActivity(),bitmap,ImgProfilePic);



                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }


        }

    }

    private void loadBitmapByPicasso(Context pContext, Bitmap pBitmap, ImageView pImageView) {
        try {
            Uri uri = Uri.fromFile(File.createTempFile("temp_file_name", ".jpg", pContext.getCacheDir()));
            OutputStream outputStream = pContext.getContentResolver().openOutputStream(uri);
            pBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            pBitmap = rotateImageIfRequired(pBitmap, uri);
            outputStream.close();
            Picasso.with(pContext).load(uri).into(pImageView);
        } catch (Exception e) {
            Log.e("LoadBitmapByPicasso", e.getMessage());
        }
    }

    private Uri getImageUri(String fPath) {
            Uri m_imgUri = null;
            File m_file;
            try {
                m_file = new File(fPath);
                m_imgUri = Uri.fromFile(m_file);
            } catch (Exception p_e) {
            }
            return m_imgUri;

    }
    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }
    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;      }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
//        final float totalReqPixelsCap = reqWidth * reqHeight * 3;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }



    private void imageBrowse() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void getResellerDetails() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                       if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);
//{"status":"success","data":[{"id":"24","name":"Ram","email":"kodanda582@gmail.com","phone":"9642448821","city":"",
// "state":"","address1":"Hyd","address2":"","pincode":""}]}
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                JSONArray mainArray = new JSONArray(mainObj.getString("data"));
                                JSONObject obj = mainArray.getJSONObject(0);
                                uId = obj.getString("id");
                                EtName.setText(obj.getString("name"));
                                EtEmail.setText(obj.getString("email"));
                                EtPhone.setText(obj.getString("phone"));
                                EtCity.setText(obj.getString("city"));
                                EtState.setText(obj.getString("state"));
                                EtPincode.setText(obj.getString("pincode"));
                                EtAddrs1.setText(obj.getString("address1"));
                                EtAddrs2.setText(obj.getString("address2"));
                                vendorImgUrl = obj.getString("image");

                                SessionSave.saveSession(AppConstants.image,vendorImgUrl,getActivity());
                                SessionSave.saveSession(AppConstants.name,obj.getString("name"),getActivity());
                                SessionSave.saveSession(AppConstants.phone,obj.getString("phone"),getActivity());

                                if( !(vendorImgUrl.isEmpty()||vendorImgUrl.equalsIgnoreCase("null") ) ){
                                    loadImage(obj.getString("image"));
                                }


                            }else{
                                Toast.makeText(getActivity(),"Failed to get Vendor Details", Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.getprofile_reseller);
                params.put("userid", SessionSave.getsession(AppConstants.userid,getActivity()));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }//getResellerDetails

    private void loadImage(String image) {

        if (!image.isEmpty()){


            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
            builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
            final AlertDialog alertDialog = builder.show();

                  Picasso.with(getActivity())
                    .load(  image )
                    .into(ImgProfilePic, new Callback() {
                        @Override
                        public void onSuccess() {
                            if(alertDialog.isShowing()){ alertDialog.dismiss(); }
//                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            if(alertDialog.isShowing()){ alertDialog.dismiss(); }
                        }
                    });


//            Glide.with(getActivity()).load(image ).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
//                    .into( ImgProfilePic );

        }else{
            ImgProfilePic.setImageDrawable(getResources().getDrawable(R.drawable.no_profile_photo));
        }
    }

    public static boolean isGrantExternalRW(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (context.checkSelfPermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

            ((Activity)context).requestPermissions(new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            },1);

            return false;
        }

        return true;
    }
    private String getPath(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getActivity(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

//    class ImageUploadTask extends AsyncTask<Void, Void, String> {
//        private MultipartEntity entity;
//        private AlertDialog alertDialog;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
//            builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
//            alertDialog = builder.show();
//
//            entity = new MultipartEntity( HttpMultipartMode.BROWSER_COMPATIBLE);
//
//            try {
//                entity.addPart("action",new StringBody(ApiHelper.updateprofile_reseller));
//                entity.addPart("userid", new StringBody(uId));
//                entity.addPart("name", new StringBody(EtName.getText().toString()));
//                entity.addPart("email", new StringBody(EtEmail.getText().toString()));
//                entity.addPart("city", new StringBody(EtCity.getText().toString()));
//                entity.addPart("state",new StringBody( EtState.getText().toString()));
//                entity.addPart("address1", new StringBody(EtAddrs1.getText().toString()));
//                entity.addPart("address2",new StringBody( EtAddrs2.getText().toString()));
//                entity.addPart("pincode", new StringBody(EtPincode.getText().toString()));
//
//            }catch (Exception e){e.printStackTrace();}
//        }
//
//        @Override
//        protected String doInBackground(Void... unsued) {
//            try {
//                HttpClient httpClient = new DefaultHttpClient();
//                HttpContext localContext = new BasicHttpContext();
////                HttpPost httpPost = new HttpPost(getString(R.string.WebServiceURL) + "/cfc/iphonewebservice.cfc?method=uploadPhoto");
//              String url = ApiHelper.appDomain;
//                HttpPost httppost = new HttpPost(url);
//
//
//                ByteArrayOutputStream bos = new ByteArrayOutputStream();
//                picBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
//                byte[] data = bos.toByteArray();
//
//
//                Log.i("FilePath: ", Environment.DIRECTORY_DCIM+ filePath);
//                File file = new File( filePath);
////                File file = new File(Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DCIM).toString()+ filePath);
//                entity.addPart("userfile", new FileBody(file));
//
//                Log.i("Entity: ",entity.toString());
//
//                httppost.setEntity(entity);
//
//                httppost.setHeader(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
////                httppost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
//                HttpResponse response = httpClient.execute(httppost, localContext);
//                BufferedReader reader = new BufferedReader( new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
//
//                String sResponse = reader.readLine();
//                return sResponse;
//            } catch (Exception e) {
////                if (dialog.isShowing())
////                    dialog.dismiss();
////                Toast.makeText(getApplicationContext(),
////                        getString(R.string.exception_message),
////                        Toast.LENGTH_LONG).show();
//                Log.e(e.getClass().getName(), e.getMessage(), e);
//                return null;
//            }
//
//            // (null);
//        }
//
//        @Override
//        protected void onProgressUpdate(Void... unsued) {
//
//        }
//
//        @Override
//        protected void onPostExecute(String sResponse) {
//            //{"status":"success","message":"Successfully updated the profile."}
//            Log.i("Resp:",""+sResponse);
//            try {
//                if (alertDialog.isShowing()){alertDialog.dismiss();}
//
//                if (sResponse != null) {
//                    JSONObject JResponse = new JSONObject(sResponse);
//                    String sts = JResponse.getString("status");
//                    if(sts.equalsIgnoreCase("success")){
//                        SimpleToast.ok(getActivity(),JResponse.getString("message"));
//                    }else{
//                        SimpleToast.error(getActivity(),"Not Updated.Please try again later.");
//                    }
//                }
//            } catch (Exception e) {
////                Toast.makeText(getApplicationContext(),
////                        getString(R.string.exception_message),
////                        Toast.LENGTH_LONG).show();
//                Log.e(e.getClass().getName(), e.getMessage(), e);
//            }
//        }
//    }



//    private void ShowPicPopUp() {
//        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//
//
//
//        LayoutInflater inflater = getLayoutInflater();
//        View dialoglayout = inflater.inflate(R.layout.profileview_popup, null);
////                dialoglayout.setMinimumHeight(500);
//        ImageView PPImg = (ImageView)dialoglayout.findViewById(R.id.imageView);
//        TextView TvUpdatePic = (TextView)dialoglayout.findViewById(R.id.tvUpdatePic);
//        ImageView PPImgGallery = (ImageView)dialoglayout.findViewById(R.id.imgGallery);
//        LinearLayout LLProfPic = (LinearLayout)dialoglayout.findViewById(R.id.llProfPic);
//
//
//
//        BitmapDrawable drawable = (BitmapDrawable) ImgProfilePic.getDrawable();
//        Bitmap bitmap = drawable.getBitmap();
//        PPImg.setImageBitmap(bitmap);
//
//        builder.setView(dialoglayout);
//        builder.setCancelable(true);
//
//        optionsDialog = builder.show();
//
//        LLProfPic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                optionsDialog.dismiss();
//
//                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                    Log.i("Camera Permission","DENIED");
//                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},  MY_PERMISSIONS_REQUEST_CAMERA);
//                    checkPermissionOnAppPermScreen("Camera");
//                }else if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
//                    Log.i("Storage Permission","DENIED");
//                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_STORAGE);
//                    checkPermissionOnAppPermScreen("Storage");
//                }
//                else{     optionsDialog.dismiss();
//                    ShowUploadOptions();
//
//                    Log.i("C & S Permission","GRANTED");
//
//                }
//
//
//
//
//
//            }
//        });
//    }
    public void checkPermissionOnAppPermScreen(String perm) {
        try {
            View parentLayout = getActivity().findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, perm+" Permission are mandatory to access.", Snackbar.LENGTH_LONG)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    });

// Changing message text color
            snackbar.setActionTextColor(Color.RED);

// Changing action button text color
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        }catch (Exception e){e.printStackTrace();}
    }
    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    private void captureImage() {
        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory() + "/Reeseller";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");

        // Save a file: path for use with ACTION_VIEW intents
        filePath = image.getAbsolutePath();
        Log.i("Path ", "photo path = " + filePath);
        return image;
    }


    private Bitmap getPic(String fileName) {
        /* There isn't enough memory to open up more than a couple camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

        /* Get the size of the ImageView */
        int targetW = 278;
        int targetH = 200;

        int degree = getRotateDegreeFromExif(fileName);

        Matrix matrix = new Matrix();
        matrix.postRotate(degree);/*from   w  w w.  j  a v  a2 s  .co  m*/

        /* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        /* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

        /* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(fileName, bmOptions);
        if (bitmap == null)
            return null;
        Bitmap rotatedImage = Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        bitmap = null;
        return rotatedImage;
    }
    static private int getRotateDegreeFromExif(String filePath) {
        int degree = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(filePath);
            int orientation = exifInterface.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                degree = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                degree = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                degree = 270;
            }
            if (degree != 0) {
                exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION,
                        "0");
                exifInterface.saveAttributes();
            }
        } catch (IOException e) {
            degree = -1;
            e.printStackTrace();
        }

        return degree;
    }



    private void ShowUploadOptions() {
        try{
            View PopUpView = View.inflate(getActivity(), R.layout.upload_options_view, null);
            PopUpView.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in_enter));
            this.PPUploadDialog = new Dialog(getActivity(), R.style.NewDialog);
            this.PPUploadDialog.setContentView(PopUpView);
            this.PPUploadDialog.setCancelable(true);
            this.PPUploadDialog.show();

            Window window = this.PPUploadDialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER | Gravity.CENTER;
            window.setGravity(Gravity.CENTER);
            window.setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent_background));
            wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
            wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
            wlp.dimAmount = 0.0f;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            wlp.windowAnimations = R.anim.slide_move;

            window.setAttributes(wlp);
            window.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

            TextView TvCamera=(TextView)PopUpView.findViewById(R.id.tvCamera);
            TextView TvOther=(TextView)PopUpView.findViewById(R.id.tvOther);
            TextView TvHeading=(TextView)PopUpView.findViewById(R.id.tvPpUpHeading);        TvHeading.setText("Choose option");
            ImageView v = (ImageView) PopUpView.findViewById(R.id.closeDialog);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PPUploadDialog.dismiss();
                }
            });
            TvCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    takePhoto();//Camera only
                    PPUploadDialog.dismiss();

                    if (Build.VERSION.SDK_INT >= 23) {
                        String[] PERMISSIONS = {Manifest.permission.CAMERA,android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        if (!hasPermissions(getActivity(), PERMISSIONS)) {
                            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, REQUEST_TAKE_PHOTO );
                        } else {
                            captureImage();
                        }
                    }else{
                        captureImage();
                    }
                }
            });
            TvOther.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Requesting storage permission
//                    requestStoragePermission();
//                    showFileChooser();
                    PPUploadDialog.dismiss();
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(galleryIntent, GALLERY);
                }
            });


        }catch (Exception e){e.printStackTrace();}
    }
    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    private void uploadBitmap() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        Log.i("Api:--> ", ApiHelper.appDomain);
        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, ApiHelper.appDomain,
                new Response.Listener<NetworkResponse>() {


                    @Override
                    public void onResponse(NetworkResponse response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}


                        try {

                            Log.i("Response:-->",""+new String(response.data));
                            String  resp = new String(response.data);

                            if( resp.contains("</div>")){
                                String[] sp = resp.split("</div>");

                                JSONObject obj = new JSONObject(sp[1]);
                                String sts = obj.getString("status");
                                if(sts.equalsIgnoreCase("success")){
                                    SimpleToast.ok(getActivity(),obj.getString("message"));
//                                finish();
                                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new ResellerDetailsFragment()).commit();

                                }else{
                                    SimpleToast.error(getActivity(),"Not Updated.Please try again later.");
                                }
                            }else{
                                JSONObject obj = new JSONObject(resp);
                                String sts = obj.getString("status");
                                if(sts.equalsIgnoreCase("success")){
                                    SimpleToast.ok(getActivity(),obj.getString("message"));
//                                finish();
                                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new ResellerDetailsFragment()).commit();

                                }else{
                                    SimpleToast.error(getActivity(),"Not Updated.Please try again later.");
                                }
                            }
                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.i("Response:-->",""+error.getMessage());
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("action", ApiHelper.updateprofile_reseller);
                params.put("userid", uId);
                params.put("name", EtName.getText().toString());
                params.put("email", EtEmail.getText().toString());
                params.put("city", EtCity.getText().toString());
                params.put("state", EtState.getText().toString());
                params.put("pincode", EtPincode.getText().toString());
                params.put("address1", EtAddrs1.getText().toString());
                params.put("address2", EtAddrs2.getText().toString());
                Log.i("PARAMS:",params.toString());
                
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();

                if ( vendorImgUrl.isEmpty()||vendorImgUrl.equalsIgnoreCase("null")  ){

                }else{
                    if(ImgProfilePic.getDrawable() != null){
                        BitmapDrawable drawable = (BitmapDrawable) ImgProfilePic.getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
//BitmapFactory.decodeFile( easyPicker.getImagesPath().get(0))

                        params.put("userfile", new DataPart(imagename+".png", getFileDataFromDrawable(  bitmap )));

                        Log.i("PARAMS:",params.toString());

                    }
                }

                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        //adding the request to volley
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }



}
