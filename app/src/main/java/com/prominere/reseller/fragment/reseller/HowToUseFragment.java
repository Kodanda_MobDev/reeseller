package com.prominere.reseller.fragment.reseller;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.adapter.HowToUseAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.HowToUseModel;
import com.prominere.reseller.util.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HowToUseFragment  extends Fragment {


    private GridView GVHUC  ;
    private View rootView;
    private RequestQueue mQueue;
    private TextView TvNoRecordFound;

    public HowToUseFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView != null)        {
            ViewGroup parent=(ViewGroup)rootView.getParent();
            if(parent != null)
            {   parent.removeView(rootView);    }
        }//if
        try {
            rootView = inflater.inflate(R.layout.howtouse_fragment , container, false);

            mQueue = Volley.newRequestQueue(getActivity());
            GVHUC =(GridView)rootView.findViewById(R.id.lvhuc);
            TvNoRecordFound =(TextView)rootView.findViewById(R.id.tvnoRecordFound);
            getListByVolley();

        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }//onCreateView

    private void getListByVolley() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            if(!response.isEmpty())
                            {
                                JSONObject mainObj = new JSONObject(response);
                                ArrayList<HowToUseModel> productList = new ArrayList<HowToUseModel>();
                                if (mainObj.getString("status").equalsIgnoreCase("success")) {
                                    TvNoRecordFound.setVisibility(View.GONE);
                                    JSONArray mainArray = new JSONArray(mainObj.getString("videos"));
                                    if (mainArray.length() > 0) {
                                        for (int i = 0; i < mainArray.length(); i++) {
                                            JSONObject obj = mainArray.getJSONObject(i);
                                            HowToUseModel model = new HowToUseModel();
                                            model.setId(obj.getString("id"));
                                            model.setVideo(obj.getString("video"));
                                            model.setStatus(obj.getString("status"));
                                            model.setTitle(obj.getString("title"));
                                            model.setThumb_image(obj.getString("thumb_image"));

                                            productList.add(model);
                                        }

                                        HowToUseAdapter dbradapter = new HowToUseAdapter(getActivity(), productList);
                                        GVHUC.setAdapter(dbradapter);
                                        dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                    } else {
                                        TvNoRecordFound.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    TvNoRecordFound.setVisibility(View.VISIBLE);
                                }
                            }
                        }catch (Exception e){e.printStackTrace();}
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action","howtouse");
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }


}
