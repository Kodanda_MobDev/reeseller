package com.prominere.reseller.fragment.vendor;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.adapter.MyOrdersAdapter;
import com.prominere.reseller.adapter.vendor.VendorOrdersAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.OrderModel;
import com.prominere.reseller.util.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VendorDBOrders extends Fragment {

    private View rootView;
    private RequestQueue mQueue;
    private TextView StsAll,StsShipped,StsPending,StsDelivered,StsCancelled,TvEmptyView,StsReturngoods;
    private ArrayList<OrderModel> ordersList,PendingFilterList,DeliveredFilterList,ReturnFilterList,CancelledFilterList,ShippedFilterList;
    private RecyclerView RVOrders;

    public VendorDBOrders() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView != null)        {
            ViewGroup parent=(ViewGroup)rootView.getParent();
            if(parent != null)
            {   parent.removeView(rootView);    }
        }//if
        try {
            rootView = inflater.inflate(R.layout.vendor_dborders_fragment , container, false);

            mQueue = Volley.newRequestQueue(getActivity());
            StsAll = (TextView)rootView.findViewById(R.id.stsAll);
            StsPending = (TextView)rootView.findViewById(R.id.stsPending);
            StsShipped = (TextView)rootView.findViewById(R.id.stsShipped);
            StsDelivered = (TextView)rootView.findViewById(R.id.stsDelivered);
            StsCancelled = (TextView)rootView.findViewById(R.id.stsCancelled);
            StsReturngoods = (TextView)rootView.findViewById(R.id.stsReturngoods);
            TvEmptyView = (TextView)rootView.findViewById(R.id.tvnoRecordFound);
            RVOrders = (RecyclerView)rootView.findViewById(R.id.rvOrders);

            changeBgColor(StsAll,StsPending,StsShipped,StsDelivered,StsCancelled,StsReturngoods);
            StsAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeBgColor(StsAll,StsPending,StsShipped,StsDelivered,StsCancelled,StsReturngoods);
                    setfilter(ordersList);
                }
            });
            StsPending.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeBgColor(StsPending,StsShipped,StsDelivered,StsCancelled,StsReturngoods,StsAll);
                    setfilter(PendingFilterList);      }
            });
            StsShipped.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeBgColor(StsShipped,StsDelivered,StsCancelled,StsReturngoods,StsAll,StsPending);
                    setfilter(ShippedFilterList);      }
            });
            StsDelivered.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeBgColor(StsDelivered,StsShipped,StsCancelled,StsReturngoods,StsAll,StsPending);
                    setfilter(DeliveredFilterList);      }
            });

            StsCancelled.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeBgColor(StsCancelled,StsReturngoods,StsAll,StsPending,StsShipped,StsDelivered);
                    setfilter(CancelledFilterList);      }
            });
            StsReturngoods.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeBgColor(StsReturngoods,StsAll,StsPending,StsShipped,StsDelivered,StsCancelled);
                    setfilter(ReturnFilterList);  }
            });




        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }//onCreateView

    @Override
    public void onResume() {
        super.onResume();
        getOrdrsList();//getProduct Orders
    }

    private void changeBgColor(TextView tv1, TextView tv2, TextView tv3, TextView tv4, TextView tv5, TextView tv6) {
        tv1.setBackground(getResources().getDrawable(R.drawable.appcolor_draw_edittext_bg));     tv1.setTextColor(getResources().getColor(R.color.white));
        tv2.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg));     tv2.setTextColor(getResources().getColor(R.color.black));
        tv3.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg));     tv3.setTextColor(getResources().getColor(R.color.black));
        tv4.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg));     tv4.setTextColor(getResources().getColor(R.color.black));
        tv5.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg));     tv5.setTextColor(getResources().getColor(R.color.black));
        tv6.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg));     tv6.setTextColor(getResources().getColor(R.color.black));
     }

    private void setfilter(ArrayList<OrderModel> list) {
        RVOrders.setAdapter(null);
        if(list.size()>0){
            TvEmptyView.setVisibility(View.GONE);
            VendorOrdersAdapter dbradapter = new VendorOrdersAdapter(getActivity(), list);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            RVOrders.setLayoutManager(llm);
//            RVOrders.setHasFixedSize(true);
            RVOrders.setAdapter(dbradapter);
//            RVOrders.setLayoutManager(new LinearLayoutManager(getActivity()) );
            dbradapter.notifyDataSetChanged();
        }else{
            TvEmptyView.setVisibility(View.VISIBLE);
        }
    }

    private void getOrdrsList() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            ordersList = new ArrayList<OrderModel>();
                            PendingFilterList = new ArrayList<OrderModel>();
                            ShippedFilterList = new ArrayList<OrderModel>();
                            DeliveredFilterList = new ArrayList<OrderModel>();
                            CancelledFilterList = new ArrayList<OrderModel>();
                            ReturnFilterList = new ArrayList<OrderModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                changeBgColor(StsAll,StsPending,StsShipped,StsDelivered,StsCancelled,StsReturngoods);
                                JSONArray mainArray = new JSONArray(mainObj.getString("orders"));
                                if(mainArray.length()>0){
                                    TvEmptyView.setVisibility(View.GONE);
                                    for(int i=0;i<mainArray.length();i++){
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        OrderModel model = new OrderModel();
                                        model.setId(Integer.parseInt(obj.getString("id")));
                                        model.setOrderid(Integer.parseInt(obj.getString("orderid")));
                                        model.setProductname(obj.getString("productname"));
                                        model.setQuantity(obj.getString("quantity"));
                                        model.setPrice(obj.getString("price"));
                                        model.setMargin(obj.getString("margin"));
                                        model.setProductimage(obj.getString("productimage"));
                                        model.setOrderdate(obj.getString("orderdate"));
                                        model.setCustomer(obj.getString("customer"));
                                        model.setStatus(obj.getString("status"));

                                        model.setOrdertotal(obj.getString("ordertotal"));
                                        model.setAttribute(obj.getString("attribute"));

                                        switch (obj.getString("status").toLowerCase()){
                                            case "pending": PendingFilterList.add(model); break;
                                            case "shipped": ShippedFilterList.add(model); break;
                                            case "delivered": DeliveredFilterList.add(model); break;
                                            case "completed": DeliveredFilterList.add(model); break;
                                            case "cancelled": CancelledFilterList.add(model); break;
                                            case "return": ReturnFilterList.add(model); break;
                                            default:    break;
                                        }

                                        ordersList.add(model);
                                    }

                                    VendorOrdersAdapter dbradapter = new VendorOrdersAdapter(getActivity(), ordersList);

                                    RVOrders.setLayoutManager(new LinearLayoutManager(getActivity()) );

                                    RVOrders.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();

                                }else{
                                    TvEmptyView.setText(mainObj.getString("message"));
                                    TvEmptyView.setVisibility(View.VISIBLE);
                                }
                            }else{
                                TvEmptyView.setText(mainObj.getString("message"));
                                TvEmptyView.setVisibility(View.VISIBLE);
//                                SimpleToast.error(SubCategoryActivity.this,"Failed to execute Api");
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.vendororders);
                params.put("vendorid", SessionSave.getsession(AppConstants.userid,getActivity()));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }



}
