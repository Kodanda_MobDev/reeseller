package com.prominere.reseller.fragment.reseller;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.MyBankAccountDetails;
import com.prominere.reseller.activity.MyCatalogs;
import com.prominere.reseller.activity.dashboard.account.MyOrdersActivity;
import com.prominere.reseller.activity.dashboard.account.SettingsActivity;
import com.prominere.reseller.activity.reseller.ReesellerDetailsActivity;
import com.prominere.reseller.activity.reseller.ResellerWalletActivity;
import com.prominere.reseller.activity.dashboard.account.NotificationsActivity;
import com.prominere.reseller.fragment.vendor.VendorDetailsFragment;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.util.CircleImageView;
import com.prominere.reseller.util.SessionSave;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class AccountFragment  extends Fragment {


    private  TextView tvTitle;
    private  ImageView ImgDBClose;
    private View rootView;
    private RequestQueue mQueue;
    private TextView TvPhNo,TvName,TvSettings,TvMyOrders,TvEditProfile;
    private LinearLayout LLNotification,LLSettings,LLWallet,LLMyOrders,LLCatalog,LLMyBankDetails,LLRatingApp,LLLogout;
    private Dialog ratePreviewDialog;
    private CircleImageView Img;

    public AccountFragment() {
    }

    public AccountFragment(TextView tvTitle, ImageView ImgDBClose) {
        this.tvTitle = tvTitle;
        this.ImgDBClose = ImgDBClose;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView != null)        {
            ViewGroup parent=(ViewGroup)rootView.getParent();
            if(parent != null)
            {   parent.removeView(rootView);    }
        }//if
        try {
            rootView = inflater.inflate(R.layout.account_fragment , container, false);



            mQueue = Volley.newRequestQueue(getActivity());
        TvName =(TextView)rootView.findViewById(R.id.nav_header_textView);
        TvPhNo =(TextView)rootView.findViewById(R.id.tvPhNo);
        TvEditProfile = (TextView)rootView.findViewById(R.id.tvEditProf);
        TvMyOrders = (TextView)rootView.findViewById(R.id.tvMyorders);
        TvSettings = (TextView)rootView.findViewById(R.id.tvSettings);

        LLNotification = (LinearLayout)rootView.findViewById(R.id.llNotification);
        LLSettings = (LinearLayout)rootView.findViewById(R.id.llSettings);
        LLCatalog = (LinearLayout)rootView.findViewById(R.id.llCatalog);
        LLMyOrders = (LinearLayout)rootView.findViewById(R.id.llMyOrders);
        LLWallet = (LinearLayout)rootView.findViewById(R.id.llWallet);
        LLMyBankDetails = (LinearLayout)rootView.findViewById(R.id.llMyBankDetails);
        LLRatingApp = (LinearLayout)rootView.findViewById(R.id.llRatingApp);
        LLLogout = (LinearLayout)rootView.findViewById(R.id.llLogOut);
        Img = (CircleImageView)rootView.findViewById(R.id.imageView);


//       Glide.with(getActivity()).load( SessionSave.getsession(AppConstants.image,getActivity()) ).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
//                    .into( Img );

        TvEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SessionSave.getsession(AppConstants.usertype,getActivity()).equalsIgnoreCase("Vendor")){
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new VendorDetailsFragment()).commit();
                }else{
//                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new ResellerDetailsFragment()).commit();
             Intent in = new Intent(getActivity(), ReesellerDetailsActivity.class);
             startActivity(in);
                }
            }
        });
        LLLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
                builder.setMessage(R.string.areYouSureExit) .setTitle(R.string.logout);


                builder.setMessage("Are you sure want to logout ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                AppHelper.logOutClicked( getActivity() );
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle("Logout");
                alert.setIcon(getResources().getDrawable(R.drawable.applogo));
                alert.show();
            }
        });
        LLWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLWallet);
                Intent in = new Intent(getActivity(), ResellerWalletActivity.class);
                in.putExtra(AppConstants.wallet_transactions, ApiHelper.transactions_reseller);
                startActivity(in);
            }
        });
        LLMyBankDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLMyBankDetails);
                Intent in = new Intent(getActivity(), MyBankAccountDetails.class);
                startActivity(in);
            }
        });
        LLMyOrders.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AppHelper.setDelayToView(LLMyOrders);
            Intent in = new Intent(getActivity(), MyOrdersActivity.class);
            startActivity(in);
         }
        });
        LLCatalog.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AppHelper.setDelayToView(LLCatalog);
            Intent in = new Intent(getActivity(), MyCatalogs.class);
            startActivity(in);
         }
        });
        LLNotification.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AppHelper.setDelayToView(LLNotification);
            Intent in = new Intent(getActivity(), NotificationsActivity.class);
            startActivity(in);
         }
        });
        LLSettings.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AppHelper.setDelayToView(LLSettings);
            Intent in = new Intent(getActivity(), SettingsActivity.class);
            startActivity(in);
         }
        });
        LLRatingApp.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //https://play.google.com/store?hl=en
//            ShowRatingPopUp();
            String url = "https://play.google.com/store?hl=en";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
         }
        });


        }catch (Exception e){   e.printStackTrace();  }
        return rootView;
    }//onCreateView

    @Override
    public void onResume() {
        super.onResume();

        TvName.setText(SessionSave.getsession(AppConstants.name,getActivity()));
        TvPhNo.setText(SessionSave.getsession(AppConstants.phone,getActivity()));
        Log.i("ACCOUNT:-->",  SessionSave.getsession(AppConstants.image,getActivity()) );
        loadImage(   SessionSave.getsession(AppConstants.image,getActivity())  );

    }

    private void ShowRatingPopUp() {
        View preView = View.inflate(getActivity(), R.layout.rate_popup, null);
//        this.mDialog = new Dialog(BookingConfirmAct.this, R.style.NewDialog);
        this.ratePreviewDialog = new Dialog(getActivity());
        this.ratePreviewDialog.setContentView(preView);
        this.ratePreviewDialog.setCancelable(false);
        if (!ratePreviewDialog.isShowing()) {
            this.ratePreviewDialog.show();
        }


        Window window = this.ratePreviewDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
//        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent_background));
//        wlp.width = WindowManager.LayoutParams.FILL_PARENT;
//        wlp.height = WindowManager.LayoutParams.FILL_PARENT;
//        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.windowAnimations = R.anim.zoom_in_enter;
        window.setAttributes(wlp);

        ImageView ImgClose = (ImageView) this.ratePreviewDialog.findViewById(R.id.imgClose);
        final RatingBar RateBar = (RatingBar) this.ratePreviewDialog.findViewById(R.id.ratingBar);
        AppHelper.removeSwipeForRatingBar(RateBar);

        RateBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                Log.i("Rate:--> ",String.format("Rating is %f : fromUser %b",rating,fromUser));

            }
        });


        ImgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ratePreviewDialog.isShowing()){ratePreviewDialog.dismiss();}
            }
        });

    }//ratingpopUp

    private void loadImage(String image) {

        if (!image.isEmpty()){


            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
            builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
            final AlertDialog alertDialog = builder.show();

            Picasso.with(getActivity())
                    .load(  image )
                    .into(Img, new Callback() {
                        @Override
                        public void onSuccess() {
                            if(alertDialog.isShowing()){ alertDialog.dismiss(); }
//                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            if(alertDialog.isShowing()){ alertDialog.dismiss(); }
                        }
                    });


//            Glide.with(getActivity()).load(image ).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
//                    .into( ImgProfilePic );

        }else{
            Img.setImageDrawable(getResources().getDrawable(R.drawable.no_profile_photo));
        }
    }


}
