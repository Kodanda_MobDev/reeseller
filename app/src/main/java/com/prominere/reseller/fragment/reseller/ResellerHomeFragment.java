package com.prominere.reseller.fragment.reseller;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.SubCategoryActivity;
import com.prominere.reseller.activity.reseller.SearchActivity;
import com.prominere.reseller.adapter.ResHomeGroupProductsAdapter;
import com.prominere.reseller.adapter.ResHomeProductsAdapter;
import com.prominere.reseller.adapter.ResellerCatAdapter;
import com.prominere.reseller.adapter.ResellerSubCatAdapter;
import com.prominere.reseller.adapter.ResellerHomeSliderAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.BannerModel;
import com.prominere.reseller.model.GroupProductModel;
import com.prominere.reseller.model.ProductModel;
import com.prominere.reseller.model.ProductSubCategory;
import com.prominere.reseller.model.ResellerCatModel;
import com.prominere.reseller.util.SessionSave;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResellerHomeFragment  extends Fragment  {


    private View rootView;
    private RequestQueue mQueue;
    private TextView TvSearch,TvSubCatName,SubCategoryViewAll,CategoryViewAll;
    private SliderView sliderView;
    private RecyclerView SubCatRecyclerView,CatRecyclerView;
    private ListView LvProduct;

    public ResellerHomeFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView != null)        {
            ViewGroup parent=(ViewGroup)rootView.getParent();
            if(parent != null)
            {   parent.removeView(rootView);    }
        }//if
        try {
            rootView = inflater.inflate(R.layout.reseller_home_fragment , container, false);

            mQueue = Volley.newRequestQueue(getActivity());
            sliderView = (SliderView)rootView.findViewById(R.id.imageSlider);
            CatRecyclerView = (RecyclerView)rootView.findViewById(R.id.db_recycler_view);
            SubCatRecyclerView = (RecyclerView)rootView.findViewById(R.id.db_subCat_recycler_view);
            CategoryViewAll = (TextView)rootView.findViewById(R.id.tvCatViewAll);    AppHelper.setUnderLineForTextview(CategoryViewAll,"View All");
            SubCategoryViewAll = (TextView)rootView.findViewById(R.id.tvSubCatViewAll);  AppHelper.setUnderLineForTextview(SubCategoryViewAll,"View All");
            TvSubCatName = (TextView)rootView.findViewById(R.id.tvSubCatName);
            LvProduct = (ListView)rootView.findViewById(R.id.lvProducts);
            TvSearch = (TextView)rootView.findViewById(R.id.tvSearch);




            TvSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppHelper.setDelayToView(TvSearch);
                    Intent in = new Intent(getActivity(), SearchActivity.class);
                    startActivity(in);
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }//onCreateView

    @Override
    public void onResume() {
        super.onResume();
        getProductCategories();
    }

    private void getProductCategories() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("Products:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            List<BannerModel> bannersList = new ArrayList<BannerModel>();
                            final List<ResellerCatModel> categoryList = new ArrayList<ResellerCatModel>();
                            List<ProductSubCategory> subCategoryList = new ArrayList<ProductSubCategory>();
                            ArrayList<GroupProductModel> productList = new ArrayList<GroupProductModel>();

                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                 JSONArray bannerArray = new JSONArray(mainObj.getString("banner"));
                                 JSONArray catArray = new JSONArray(mainObj.getString("categories"));
                                 JSONArray subCatArray = new JSONArray(mainObj.getString("subcategories"));
                                 JSONArray productsArray = new JSONArray(mainObj.getString("products"));
                                 TvSubCatName.setText(mainObj.getString("subcategoryname"));
                                if(bannerArray.length()>0) {
                                    for (int i = 0; i < bannerArray.length(); i++) {
                                        JSONObject obj = bannerArray.getJSONObject(i);
                                        BannerModel catList = new BannerModel();
                                        catList.setId(obj.getInt("id"));
                                        catList.setBanner(obj.getString("banner"));
                                        bannersList.add(catList);
                                    }
                                    ResellerHomeSliderAdapter adapter = new ResellerHomeSliderAdapter(getActivity(),bannersList);

                                    sliderView.setSliderAdapter(adapter);
                                    sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
                                    sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                                    sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                                    sliderView.setIndicatorSelectedColor(Color.WHITE);
                                    sliderView.setIndicatorUnselectedColor(Color.GRAY);
                                    sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
                                    sliderView.startAutoCycle();

                                }
                                if(catArray.length()>0) {
                                    for (int i = 0; i < catArray.length(); i++) {
                                        JSONObject obj = catArray.getJSONObject(i);

                                        ResellerCatModel catList = new ResellerCatModel();

                                        catList.setId(obj.getInt("id"));
                                        catList.setCname(obj.getString("cname"));
                                        catList.setSlug(obj.getString("slug"));
                                        catList.setStatus(obj.getString("status"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setProductcount(obj.getString("productcount"));
                                        catList.setMainsubcategory_count(obj.getString("mainsubcategory_count"));
                                        categoryList.add(catList);
                                    }
                                    ResellerCatAdapter adapter = new ResellerCatAdapter(getActivity(),categoryList);
                                    LinearLayoutManager horizontalLayoutManagaer
                                            = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                                    CatRecyclerView.setLayoutManager(horizontalLayoutManagaer);
                                    CatRecyclerView.setAdapter(adapter);

                                }
                                SubCategoryViewAll.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        for(int i=0;i<categoryList.size();i++){
                                            ResellerCatModel model = categoryList.get(i);
                                            if(TvSubCatName.getText().toString().equalsIgnoreCase(model.getCname())){
                                                Log.i("SubCAt: ",""+model.getId()+"  "+model.getCname());
                                                Intent in = new Intent(getActivity(), SubCategoryActivity.class);
                                                in.putExtra(AppConstants.category,""+model.getId());
                                                startActivity(in);
                                            }
                                        }
                                    }
                                });
                                
                                
                                
                                if(subCatArray.length()>0) {
                                    for (int i = 0; i < subCatArray.length(); i++) {
                                        JSONObject obj = subCatArray.getJSONObject(i);

                                        ProductSubCategory model = new ProductSubCategory();

                                        model.setId(obj.getString("id"));
                                        model.setCname(obj.getString("cname"));
                                        model.setSlug(obj.getString("slug"));
                                        model.setCategory(obj.getString("category"));
//                                        model.setAttr(obj.getString("attr"));
                                        model.setStatus(obj.getString("status"));
                                        model.setImage(obj.getString("image")  );
                                        model.setProductcount(obj.getString("productcount"));
                                        model.setSubcategorycount(obj.getString("subcategorycount"));
                                        subCategoryList.add(model);
                                    }
                                    ResellerSubCatAdapter adapter = new ResellerSubCatAdapter(getActivity(),subCategoryList);
                                    LinearLayoutManager horizontalLayoutManagaer
                                            = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                                    SubCatRecyclerView.setLayoutManager(horizontalLayoutManagaer);
                                    SubCatRecyclerView.setAdapter(adapter);

                                }
                                if(productsArray.length()>0) {
                                    for (int i = 0; i < productsArray.length(); i++) {
                                        JSONObject obj = productsArray.getJSONObject(i);

                                        GroupProductModel catList = new GroupProductModel();

                                        catList.setId(obj.getString("id"));
                                        catList.setGroupname(obj.getString("groupname"));
                                        catList.setDescription(obj.getString("description"));
                                        catList.setImages(obj.getString("images"));
                                        catList.setPrice(obj.getString("price"));
                                        catList.setDiscountprice(obj.getString("discountprice"));
//                                        catList.setId(obj.getString("id"));
//                                        catList.setTitle(obj.getString("title"));
//                                        catList.setPrice(obj.getString("price"));
//                                        catList.setDiscountprice(obj.getString("discountprice"));
//                                        catList.setImage(obj.getString("image")  );
//                                        catList.setCategory(obj.getString("category"));
//                                        catList.setWishliststatus(obj.getString("wishliststatus"));
                                        productList.add(catList);
                                    }
                                    ResHomeGroupProductsAdapter dbradapter = new ResHomeGroupProductsAdapter(getActivity(), productList,LvProduct);
                                    LvProduct.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
                                    AppHelper.setListViewHeightBasedOnChildren(LvProduct);

                                }

                            }else{
                              //  TvEmptyBox.setVisibility(View.VISIBLE);
                                //   SimpleToast.error(CartItemsActivity.this, AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.reseller_home);
                params.put("userid", SessionSave.getsession(AppConstants.userid,getActivity()));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }



}
