package com.prominere.reseller.fragment.vendor;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.adapter.DBProductAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.model.ProductModel;
import com.prominere.reseller.util.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class VendorDBProductsFragment extends Fragment {


    private View rootView;
    private RequestQueue mQueue;
    private RecyclerView LvProducts;
    private TextView TvEmptyBox;
    private EditText EtSearch;
    private ImageView ImgClose;
    private DBProductAdapter dbradapter;
    private ArrayList<ProductModel> productList;

    public VendorDBProductsFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView != null)        {
            ViewGroup parent=(ViewGroup)rootView.getParent();
            if(parent != null)
            {   parent.removeView(rootView);    }
        }//if
        try {
            rootView = inflater.inflate(R.layout.dbproducts_fragment , container, false);

            mQueue = Volley.newRequestQueue(getActivity());
            EtSearch = (EditText) rootView.findViewById(R.id.etSearch);
            ImgClose = (ImageView) rootView.findViewById(R.id.imgClose);
            LvProducts = (RecyclerView)rootView.findViewById(R.id.lvProducts);
            TvEmptyBox = (TextView)rootView.findViewById(R.id.imgEmptyBox);

            EtSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    Log.i("Count-->", "" + count + "Str-->" + s);
                    if (s.length() == 0) {
                        EtSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_search, 0);
                        ImgClose.setVisibility(View.GONE);
                        getProducts();
                    } else {
                        EtSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        ImgClose.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!(s.length() == 0)) {
                        if (productList.size() > 0) {
                            String text = EtSearch.getText().toString().toLowerCase(Locale.getDefault());
                            dbradapter.filter(text, TvEmptyBox);
//                        TvnoOfCounts.setText("No Of Records: " + ListViewNotif.getAdapter().getCount());
                        }
                    }
                }
            });
            ImgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EtSearch.setText("");
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }//onCreateView

    @Override
    public void onResume() {
        super.onResume();
        getProducts();
    }

    private void getProducts() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Products:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                             productList = new ArrayList<ProductModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                TvEmptyBox.setVisibility(View.GONE);
                                JSONArray mainArray = new JSONArray(mainObj.getString("products"));
                                if(mainArray.length()>0) {
                                    int total = 0;
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        ProductModel catList = new ProductModel();
                                        catList.setId(obj.getString("id"));
                                        catList.setTitle(obj.getString("title"));
                                        catList.setPrice(obj.getString("price"));
                                        catList.setDiscountprice(obj.getString("discountprice"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setCategory(obj.getString("category"));
                                        productList.add(catList);
                                    }
                                     dbradapter = new DBProductAdapter(getActivity(), productList);

                                    LvProducts.setLayoutManager(new LinearLayoutManager(getActivity()) );

                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                    TvEmptyBox.setVisibility(View.VISIBLE);
                                }

                            }else{
                                TvEmptyBox.setVisibility(View.VISIBLE);
                                //   SimpleToast.error(CartItemsActivity.this, AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.vendor_productlist);
//                params.put("vendorid","2");
                params.put("vendorid", SessionSave.getsession(AppConstants.userid,getActivity()));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }


}
