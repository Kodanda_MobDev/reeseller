package com.prominere.reseller.helper;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.LoginActivity;
import com.prominere.reseller.activity.dashboard.NoInternetConnActivity;
import com.prominere.reseller.activity.reseller.MainNavigationActivity;
import com.prominere.reseller.activity.reseller.ProductDetailsActivity;
import com.prominere.reseller.fragment.reseller.ResellerDetailsFragment;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.share.AndroidShare;
import com.prominere.reseller.util.share.AndroidSharePlatform;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppHelper {


    public static int shreImgsCountSize,shreImgsCount;
    public static ArrayList<Uri> shreImgsDownloadFileList;

    public static int setValueToSpinner(Spinner spinner, String string) {
        int index = 0;
        for(int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(string)){
                Log.i("spinner:"+i+"",spinner.getItemAtPosition(i).toString());
                index = i;
                break;
            }
        }
        return index;
    }//setValueToSpinner


    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }//emailValidator
    public static boolean isVaidPassword(EditText editText) {
        String password = editText.getText().toString().trim();
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";
        Pattern patern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = patern.matcher(password);
        return matcher.matches();
    }
    public static void setStatusBarColor(Activity activity, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity ,color) );
        }
    }


    public static String checkStringEmptyOrNot(String str) {
        String retStr ;
        if(str.isEmpty()||str.equalsIgnoreCase("null")){
            retStr="";
        }else{
            retStr = str;
        }
        return  retStr;
    }
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);

            if(listItem != null){
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();

            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    public static void showServerErrorPopUp(Activity activity,String apiTitle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.areYouSureExit) .setTitle("Alert");

        builder.setMessage("A PHP Error was encountered for the Api "+apiTitle)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        //  Action for 'NO' Button
//                        dialog.cancel();
//                    }
//                }
//                );
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Alert");
        alert.show();
    }

    public static void setUnderLineForTextview(TextView tv, String content) {
        tv.setText(content);
        tv.setPaintFlags(tv.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
//        tv.setText(Html.fromHtml("<p><u>"+content+"</u></p>").toString());
//        SpannableString text = new SpannableString(content);
//        text.setSpan(new UnderlineSpan(), 25, 6, 0);
//        tv.setText(text);

    }

    public static void logOutClicked(Activity activity) {
        SessionSave.saveSession(AppConstants.userid,"",activity);

        Intent in = new Intent(activity, LoginActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(in);
        activity.finish();

    }

    public static void setDelayToView(final View view) {
        view.setEnabled(false);
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setEnabled(true);
            }
        }, 2000);

    }


    public static void loadImageViewByUrl(Activity activity, ImageView imgView, String url) {
        if(!url.isEmpty()) {

            Glide.with(activity).load( url ).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
                    .into( imgView );

//            Picasso.with(activity)
//                    .load(url)
//                    .into(imgView, new Callback() {
//                        @Override
//                        public void onSuccess() {
////                            progressbar.setVisibility(View.GONE);
//                        }
//                        @Override
//                        public void onError() {
//                        }
//                    });
        }
    }//loadImageViewByUrl


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    } // Author: silentnuke


    public static void setImageByUrl(Activity activity, String url, ImageView imgItem) {

        if(url.contains(",")){
            String parts[] = url.split(",");
            System.out.print(parts[0]);
            //            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);
            Picasso.with(activity)
                    .load(parts[0])
                    .into(imgItem, new Callback() {
                        @Override
                        public void onSuccess() {
//                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });

        }else{
            //            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);
            Picasso.with(activity)
                    .load(url)
                    .into(imgItem, new Callback() {
                        @Override
                        public void onSuccess() {
//                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }
    }//setImgURL

    public static Bitmap rotateImageIfRequired(Bitmap img, Uri contentURI) {

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(contentURI.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return rotateImage(img, 90);
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return rotateImage(img, 180);
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return rotateImage(img, 270);
                default:
                    return img;
            }
    }
    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }


    public static List<String> getImageURL(String image) {

        if(image.contains("&")){
            String[] result = image.split("&");
            List<String> list = Arrays.asList(result);
            List<String> duplicateList = new ArrayList<String>();

            for (String str : list) {
                if (str != null && !str.isEmpty()) {
                    duplicateList.add(str);
                }
            }

            return duplicateList ;
        }else if(image.contains(",")){
            String[] result = image.split(",");
            List<String> list = Arrays.asList(result);
            List<String> duplicateList = new ArrayList<String>();

            for (String str : list) {
                if (str != null && !str.isEmpty()) {
                    duplicateList.add(str);
                }
            }

            return duplicateList;
        }else

            return Collections.singletonList(image);


    }

    public static void removeSwipeForRatingBar(RatingBar rateBar) {

        rateBar.setOnTouchListener(new View.OnTouchListener()
        {
            private float downXValue;

            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    downXValue = event.getX();
                    return false;
                }

                if(event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    // When true is returned, view will not handle this event.
                    return true;
                }

                if(event.getAction() == MotionEvent.ACTION_UP)
                {
                    float currentX = event.getX();
                    float difference = 0;
                    // Swipe on left side
                    if(currentX < downXValue)
                        difference = downXValue - currentX;
                        // Swipe on right side
                    else if(currentX > downXValue)
                        difference = currentX - downXValue;

                    if(difference < 10 )
                        return false;

                    return true;
                }
                return false;
            }
        });


    }


    public static boolean isStoragePermissionGranted(Activity activity) {
        return ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }


    public static String getFilePathForN(Uri uri, Context context) {
        Uri returnUri = uri;
        Cursor returnCursor = context.getContentResolver().query(returnUri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = new File(context.getFilesDir(), name);
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1 * 1024 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }
            Log.e("File Size", "Size " + file.length());
            inputStream.close();
            outputStream.close();
            Log.e("File Path", "Path " + file.getPath());
            Log.e("File Size", "Size " + file.length());
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return file.getPath();
    }


    public static void showSnackBar(Activity activity, View view, String str) {
        Snackbar snackbar = Snackbar.make(view, str, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public static boolean WhatsAppInstalledOrNot(Activity activity,String uri) {
        PackageManager pm = activity.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
    public static void notFoundExceptionDialog(Activity activity,String err) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(err) .setTitle(R.string.app_name);

        builder.setMessage(err)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        //  Action for 'NO' Button
//                        dialog.cancel();
//                    }
//                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.show();

    }

    public static void checkInternetAvailability(Activity activity) {
        ConnectivityManager ConnectionManager=(ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true )
        {
//            Toast.makeText(activity, "Network Available", Toast.LENGTH_LONG).show();
        }
        else
        {
            Intent in = new Intent(activity, NoInternetConnActivity.class);
            activity.startActivity(in);
            activity.finish();
//            Toast.makeText(activity, "Network Not Available", Toast.LENGTH_LONG).show();

        }

    }

    public static  Bitmap downloadImage(String url) {
        Bitmap bitmap = null;
        InputStream stream = null;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize = 1;

        try {
            stream = getHttpConnection(url);
            bitmap = BitmapFactory.decodeStream(stream, null, bmOptions);
            stream.close();
        }
        catch (IOException e1) {
            e1.printStackTrace();
            System.out.println("downloadImage"+ e1.toString());
        }
        return bitmap;
    }

    // Makes HttpURLConnection and returns InputStream

    public static  InputStream getHttpConnection(String urlString)  throws IOException {

        InputStream stream = null;
        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();

        try {
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            httpConnection.setRequestMethod("GET");
            httpConnection.connect();

            if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                stream = httpConnection.getInputStream();
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("downloadImage" + ex.toString());
        }
        return stream;
    }

    public static boolean checkAppInstalledOrNot(Activity activity, String uri) {
        PackageManager pm = activity.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


    public static void downloadFile(final int cond, String uRl, final Activity activity, int imgCount, int totalImgsCount, final String title) {
    Log.i("Img URL","imgCount: "+imgCount+" totalImgsCount: "+totalImgsCount+" url:"+uRl);
    Log.i("Img URL",uRl);
//        ArrayList<Uri> downloadFileList = new ArrayList<Uri>();
        try {
            File myDir = new File(Environment.getExternalStorageDirectory(), "Reeseller/");
            if (!myDir.exists()) {
                myDir.mkdirs();
            }

            DownloadManager mgr = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
//            activity.registerReceiver(attachmentDownloadCompleteReceive, new IntentFilter(  DownloadManager.ACTION_DOWNLOAD_COMPLETE));

            Uri downloadUri = Uri.parse(uRl);

            DownloadManager.Request request = new DownloadManager.Request(
                    downloadUri);
            String fname = new Date().getTime() + ".jpg";
            request.setAllowedNetworkTypes(
                    DownloadManager.Request.NETWORK_WIFI
                            | DownloadManager.Request.NETWORK_MOBILE).setAllowedOverMetered(true)
                    .setAllowedOverRoaming(true).setTitle("Reeseller - " + "Downloading " + uRl).
                    setVisibleInDownloadsUi(true)
                    .setDestinationInExternalPublicDir("Reeseller" + "/", fname);

            mgr.enqueue(request);

            AppHelper.shreImgsDownloadFileList.add(downloadUri);
//            activity.registerReceiver(new BroadcastReceiver() {
//                @Override
//                public void onReceive(Context context, Intent intent) {
//                    String action = intent.getAction();
//                    if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
//                        shreImgsCount = shreImgsCount + 1 ;
//
//                        Log.i("shreImgsCount"+shreImgsCount," == shreImgsCountSize:"+shreImgsCountSize);
//                        if(shreImgsCount == shreImgsCountSize){
//                            SimpleToast.ok(context,"Allcpompleted");
//                            Log.e("AllDownload:","Allcpompleted");
//                            if(cond == 1){//DownloAd files
//                                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//                                builder.setMessage("Images downloaded in 'Reeseller' album.")
//                                        .setTitle("Download Successful");
//                                builder.setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
//                                //Creating dialog box
//                                AlertDialog alert = builder.create();
//                                alert.show();
//                            }
//                            if(cond == 2){//Share Others
//                                AndroidShare.Builder shareBuilder = new AndroidShare
//                                        .Builder(activity)
////                                                        .setContent(TvTitle.getText().toString())
//                                        .setTitle(" Title: "+title);
//                                shareBuilder.setImageUris(AppHelper.shreImgsDownloadFileList);
//                                shareBuilder.build().share();
//                            }
//                            if(cond == 3){//Share to WhatsApp
//                                AndroidShare.Builder shareBuilder = new AndroidShare
//                                        .Builder(activity)
//                                        .setContent(title)
//                                        .setTitle(" Title: "+title);
//                                shareBuilder.setImageUris(AppHelper.shreImgsDownloadFileList);
//                                shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
//                                shareBuilder.build().share();
//                            }
//
//
//
//                        }
////                        long downloadId = intent.getLongExtra( DownloadManager.EXTRA_DOWNLOAD_ID, 0);
////                        Log.i("onReceive: "  ,"Downlod completed");
//                    }
//                }
//            }, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        }catch (IllegalStateException e) {
            Toast.makeText(activity, "Please insert an SD card to download file", Toast.LENGTH_SHORT).show();
        }

        if(imgCount == totalImgsCount ){
            if(cond == 1){//DownloAd files
                       AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Images downloaded in 'Reeseller' album.")
                        .setTitle("Download Successful");
                builder.setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                alert.show();
            }
            if(cond == 2){//Share Others
                AndroidShare.Builder shareBuilder = new AndroidShare
                        .Builder(activity)
//                                                        .setContent(TvTitle.getText().toString())
                        .setTitle(" Title: "+title);
                shareBuilder.setImageUris(AppHelper.shreImgsDownloadFileList);
                shareBuilder.build().share();
            }
            if(cond == 3){//Share to WhatsApp
                AndroidShare.Builder shareBuilder = new AndroidShare
                        .Builder(activity)
                        .setContent(title)
                        .setTitle(" Title: "+title);
                shareBuilder.setImageUris(AppHelper.shreImgsDownloadFileList);
                shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
                shareBuilder.build().share();
            }


        }


    }


    BroadcastReceiver attachmentDownloadCompleteReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                long downloadId = intent.getLongExtra(
                        DownloadManager.EXTRA_DOWNLOAD_ID, 0);
               Toast.makeText(context,"Download completed",Toast.LENGTH_SHORT).show();
               Log.i("onReceive: "  ,"Downlod completed");
            }
        }
    };

    public static void close(InputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(OutputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void showAlertDowloadFolderDialog(Activity activity) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Images downloaded in 'Reeseller' album.")
                .setTitle("Download Successful");


        builder.setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
//                                              .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                                                  public void onClick(DialogInterface dialog, int id) {
//                                                      //  Action for 'NO' Button
//                                                      dialog.cancel();
//                                                  }
//                                              });
        //Creating dialog box
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void downloadFileToStorage(Activity activity, String img, int i, int size) {
            try {
                File myDir = new File(Environment.getExternalStorageDirectory(), "Reeseller/");
                if (!myDir.exists()) {
                    myDir.mkdirs();
                }

                DownloadManager mgr = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
//            activity.registerReceiver(attachmentDownloadCompleteReceive, new IntentFilter(  DownloadManager.ACTION_DOWNLOAD_COMPLETE));

                Uri downloadUri = Uri.parse(img);

                DownloadManager.Request request = new DownloadManager.Request(
                        downloadUri);
                String fname = new Date().getTime() + ".jpg";
                request.setAllowedNetworkTypes(
                        DownloadManager.Request.NETWORK_WIFI
                                | DownloadManager.Request.NETWORK_MOBILE).setAllowedOverMetered(true)
                        .setAllowedOverRoaming(true).setTitle("Reeseller - " + "Downloading " + img).
                        setVisibleInDownloadsUi(true)
                        .setDestinationInExternalPublicDir("Reeseller" + "/", fname);

                mgr.enqueue(request);

                if(i == size ){
//                                      SimpleToast.ok(ProductDetailsActivity.this,"Downloaded successfully");
                    AppHelper.showAlertDowloadFolderDialog(activity);
                    //   new generatePictureStyleNotification(ProductDetailsActivity.this,TvTitle.getText().toString(), "",ImageUrl,i).execute();

                }


            }catch (IllegalStateException e) {
                Toast.makeText(activity , "Please insert an SD card to download file", Toast.LENGTH_SHORT).show();
            }


    }
}
