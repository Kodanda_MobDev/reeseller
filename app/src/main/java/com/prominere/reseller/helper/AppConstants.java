package com.prominere.reseller.helper;

public class AppConstants {

    public static String appName = "Reeseller";
    public static String chat = "Chat";
    public static String orders = "Orders";
    public static String account = "Account";
    public static String howToUse = "HowToUse";
    public static String userid = "userid";
    public static String password = "password";
    public static String type = "type";
    public static String usertype = "usertype";
    public static String id = "id";
    public static String name = "name";
    public static String email = "email";
    public static String phone = "phone";
    public static String city = "city";
    public static String state = "state";
    public static String address = "address";
    public static String address1 = "address1";
    public static String address2 = "address2";
    public static String pincode = "pincode";
    public static String gst = "gst";
    public static String company = "company";
    public static String image = "image";
    public static String billing_add1 = "billing_add1";
    public static String billing_add2 = "billing_add2";
    public static String pickup_add1 = "pickup_add1";
    public static String pickup_add2 = "pickup_add2";
    public static int volleyTimeOut = 50000;

    public static String category = "category";
    public static String subCategory = "subCategory";
    public static String productid = "productid";
    public static String producttitle = "producttitle";
    public static String apiIssueMsg = "Api is not working, please try again later.";
    public static String search = "Search";
    public static String addressId = "ShipppingAddressID";
    public static String myBankDetails="My Bank Details";
    public static String myCatalogs="My Catalogs";
    public static String Notifications="Notifications";
    public static String termsAndConitions="Terms And Conditions";
    public static String orderid = "orderid";
    public static String cartcount = "cartcount";
    public static String wallet_transactions = "wallet_transactions";

    public static String adminwhatsapp = "adminwhatsapp";
    public static String groupslist = "groupslist";
    public static String noInternetConnection = "There is no internet connection" ;
}
