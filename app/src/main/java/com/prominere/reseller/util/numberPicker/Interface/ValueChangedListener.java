package com.prominere.reseller.util.numberPicker.Interface;


import com.prominere.reseller.util.numberPicker.Enums.ActionEnum;

/**
 * Created by travijuu on 19/12/16.
 */

public interface ValueChangedListener {

    void valueChanged(int value, ActionEnum action);
}
