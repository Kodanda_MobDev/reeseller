package com.prominere.reseller.util.simpleToast;

public interface IconFontDescriptor {
    String ttfFileName();
    Icon[] characters();
}
