package com.prominere.reseller.util.simpleToast;

import android.widget.TextView;

import androidx.core.view.ViewCompat;

public interface HasOnViewAttachListener {
    void setOnViewAttachListener(OnViewAttachListener listener);

    interface OnViewAttachListener {
        void onAttach();

        void onDetach();
    }

    class HasOnViewAttachListenerDelegate {

        private final TextView view;
        private OnViewAttachListener listener;

        public HasOnViewAttachListenerDelegate(TextView view) {
            this.view = view;
        }

        public void setOnViewAttachListener(OnViewAttachListener listener) {
            if (this.listener != null)
                this.listener.onDetach();
            this.listener = listener;
            if (ViewCompat.isAttachedToWindow(view) && listener != null) {
                listener.onAttach();
            }
        }

        public void onAttachedToWindow() {
            if (listener != null) listener.onAttach();
        }

        public void onDetachedFromWindow() {
            if (listener != null) listener.onDetach();
        }

    }
}