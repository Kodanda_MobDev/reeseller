package com.prominere.reseller.util.numberPicker.Interface;

/**
 * Created by travijuu on 26/05/16.
 */
public interface LimitExceededListener {

    void limitExceeded(int limit, int exceededValue);
}
