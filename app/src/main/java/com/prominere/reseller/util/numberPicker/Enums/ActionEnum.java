package com.prominere.reseller.util.numberPicker.Enums;

/**
 * Created by travijuu on 26/05/16.
 */
public enum ActionEnum {
    INCREMENT, DECREMENT, MANUAL
}
