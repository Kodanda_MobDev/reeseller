package com.prominere.reseller.util.share;

/**
 * Created by allenliu on 2017/10/16.
 */

public interface AndroidShareListener {
    void shareSuccess();
    void shareCancel();
}
