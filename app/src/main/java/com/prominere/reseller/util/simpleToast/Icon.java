package com.prominere.reseller.util.simpleToast;

public interface Icon {
    String key();
    char character();
}
