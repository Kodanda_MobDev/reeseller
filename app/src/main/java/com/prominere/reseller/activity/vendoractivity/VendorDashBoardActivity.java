package com.prominere.reseller.activity.vendoractivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.LoginActivity;
import com.prominere.reseller.activity.dashboard.account.NotificationsActivity;
import com.prominere.reseller.activity.reseller.HelpActivity;
import com.prominere.reseller.activity.reseller.ResellerWalletActivity;
import com.prominere.reseller.fragment.vendor.VendorDetailsFragment;
import com.prominere.reseller.fragment.vendor.VendorDBProductsFragment;
import com.prominere.reseller.fragment.vendor.VendorDBOrders;
import com.prominere.reseller.fragment.vendor.VendorDashBoardFragment;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.DBIconsView;
import com.prominere.reseller.util.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VendorDashBoardActivity extends AppCompatActivity {

    private ImageView ImgLogOut,ImgNotif,ImgWallet;
    private TextView TvAddProdTitle,TvTitle,TVAddProdTitle, TvDBHeadBgColor,TvProductHeadBgColor , TvOrderHeadBgColor,TvAccHeadBgColor;
    private LinearLayout LLVAccount,LLVOrders,LLVProducts,LLVDashBoard;
    private RequestQueue mQueue;
    private TextView TvAcc,TvOrder,TvProduct,TvDB;
    private  ImageView IMgDB,ImdOrder,ImgProduct,ImgAcc;
    private ArrayList<DBIconsView> LvDBIconsList;
    private ImageView ImgDBClose;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vendor_dash_board_activity);

        mQueue = Volley.newRequestQueue(VendorDashBoardActivity.this);


        ImgDBClose = (ImageView)findViewById(R.id.imgDBClose);
        TvTitle = (TextView)findViewById(R.id.tvTitle);
        ImgWallet = (ImageView)findViewById(R.id.wallet);
        ImgNotif = (ImageView)findViewById(R.id.imgNotif);
        ImgLogOut = (ImageView)findViewById(R.id.logOut);
        TvAddProdTitle = (TextView)findViewById(R.id.tvAddProdTitle);
        TVAddProdTitle = (TextView)findViewById(R.id.tvAddProdTitle);
        LLVDashBoard = (LinearLayout)findViewById(R.id.llVDashBoard);  IMgDB = (ImageView)findViewById(R.id.imgDb);      TvDB = (TextView)findViewById(R.id.tvDB);              TvDBHeadBgColor = (TextView)findViewById(R.id.tvDBHeadBgColor);
        LLVProducts = (LinearLayout)findViewById(R.id.llVProducts);    ImgProduct = (ImageView)findViewById(R.id.imgProduct);  TvProduct = (TextView)findViewById(R.id.tvProd); TvProductHeadBgColor = (TextView)findViewById(R.id.tvProductHeadBgColor);
        LLVOrders = (LinearLayout)findViewById(R.id.llVOrders);        ImdOrder = (ImageView)findViewById(R.id.imgOrder);  TvOrder = (TextView)findViewById(R.id.tvOrder);      TvOrderHeadBgColor = (TextView)findViewById(R.id.tvOrderHeadBgColor);
        LLVAccount = (LinearLayout)findViewById(R.id.llVAccount);      ImgAcc = (ImageView)findViewById(R.id.imgAcc);    TvAcc = (TextView)findViewById(R.id.tvAcc);            TvAccHeadBgColor = (TextView)findViewById(R.id.tvAccHeadBgColor);

        LvDBIconsList = new ArrayList<DBIconsView>();
        LvDBIconsList.add(new DBIconsView(LLVDashBoard,IMgDB,TvDB,TvDBHeadBgColor));
        LvDBIconsList.add(new DBIconsView(LLVProducts,ImgProduct,TvProduct,TvProductHeadBgColor));
        LvDBIconsList.add(new DBIconsView(LLVOrders,ImdOrder,TvOrder,TvOrderHeadBgColor));
        LvDBIconsList.add(new DBIconsView(LLVAccount,ImgAcc,TvAcc,TvAccHeadBgColor));

        setBgToBootomViews(LLVDashBoard);
        TvTitle.setText("Dashboard");
        getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new VendorDashBoardFragment()).commit();

        ImgWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(VendorDashBoardActivity.this, ResellerWalletActivity.class);
                in.putExtra(AppConstants.wallet_transactions,ApiHelper.transactions_vendor);
                startActivity(in);
            }
        });
        ImgNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(ImgNotif);
                Intent in = new Intent(VendorDashBoardActivity.this, NotificationsActivity.class);
                startActivity(in);
            }
        });


        TvAddProdTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(TvAddProdTitle);
                Intent in = new Intent(VendorDashBoardActivity.this, VendorAddProductActivity.class);
//                Intent in = new Intent(VendorDashBoardActivity.this, AddProductActivity.class);
                startActivity(in);
            }
        });


        LLVDashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLVDashBoard);
                TVAddProdTitle.setVisibility(View.GONE); ImgNotif.setVisibility(View.VISIBLE); ImgWallet.setVisibility(View.VISIBLE); ImgLogOut.setVisibility(View.GONE);
                setBgToBootomViews(LLVDashBoard);
                TvTitle.setText("Dashboard");
                getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new VendorDashBoardFragment()).commit();
            }
        });
        LLVProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLVProducts);
//                TVAddProdTitle.setVisibility(View.GONE);  ImgWallet.setVisibility(View.GONE);
                TVAddProdTitle.setVisibility(View.VISIBLE);  ImgNotif.setVisibility(View.GONE); ImgWallet.setVisibility(View.GONE); ImgLogOut.setVisibility(View.GONE);
                setBgToBootomViews(LLVProducts);
                TvTitle.setText("Products");
                getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new VendorDBProductsFragment()).commit();
            }
        });
        LLVOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLVOrders);
                TVAddProdTitle.setVisibility(View.GONE); ImgNotif.setVisibility(View.VISIBLE); ImgWallet.setVisibility(View.VISIBLE); ImgLogOut.setVisibility(View.GONE);
                setBgToBootomViews(LLVOrders);
                TvTitle.setText("Orders");
                getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new VendorDBOrders()).commit();
            }
        });
        LLVAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLVAccount);
//                TVAddProdTitle.setVisibility(View.GONE);  ImgWallet.setVisibility(View.GONE); ImgLogOut.setVisibility(View.VISIBLE);
//                setBgToBootomViews(LLVAccount);
//                TvTitle.setText("Account");
//                getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new VendorDetailsFragment()).commit();
                Intent in = new Intent(VendorDashBoardActivity.this,VendorDetailsActivity.class);
                startActivity(in);
            }
        });
        ImgLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(ImgLogOut);

                AlertDialog.Builder builder = new AlertDialog.Builder( VendorDashBoardActivity.this );
              //  builder.setMessage(R.string.areYouSureExit) .setTitle(R.string.logout);


                builder.setMessage("Are you sure want to logout ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                SessionSave.saveSession(AppConstants.userid,"",VendorDashBoardActivity.this);

                                Intent in = new Intent(VendorDashBoardActivity.this, LoginActivity.class);
                                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(in);
                                finish();

//                                AppHelper.logOutClicked( VendorDashBoardActivity.this );
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle("Logout");
                alert.setIcon(getResources().getDrawable(R.drawable.applogo));
                alert.show();
            }
        });



        getUserDetails();
    }//onCreate

    private void setBgToBootomViews(LinearLayout ll) {
        for(int i =0;i<LvDBIconsList.size();i++){
            if(ll == LvDBIconsList.get(i).getLinearLayout()){
                DBIconsView model = LvDBIconsList.get(i);
                model.getTextView().setTextColor(getResources().getColor(R.color.appColor));
                model.getHeaadbgColor().setVisibility(View.VISIBLE);
                updateIcons(model);
//                model.getImageView().setBackgroundTintList(getResources().getColor(R.color.appColor));
            }else{
                DBIconsView model = LvDBIconsList.get(i);
                model.getTextView().setTextColor(getResources().getColor(R.color.black));
                model.getHeaadbgColor().setVisibility(View.GONE);
//                model.getImageView().setB(getResources().getColor(R.color.appColor));
            }
        }

    }

    private void updateIcons(DBIconsView model) {

      switch (model.getTextView().getText().toString()){
          case "Dashboard":
                           IMgDB.setImageDrawable(getResources().getDrawable(R.drawable.appcolor_db));
                           ImgProduct.setImageDrawable(getResources().getDrawable(R.drawable.black_db_products));
                           ImdOrder.setImageDrawable(getResources().getDrawable(R.drawable.black_db_orders));
                           ImgAcc.setImageDrawable(getResources().getDrawable(R.drawable.black_db_account));
                           break;
          case "Products":
                           IMgDB.setImageDrawable(getResources().getDrawable(R.drawable.black_db));
                           ImgProduct.setImageDrawable(getResources().getDrawable(R.drawable.appcolor_db_products));
                           ImdOrder.setImageDrawable(getResources().getDrawable(R.drawable.black_db_orders));
                           ImgAcc.setImageDrawable(getResources().getDrawable(R.drawable.black_db_account));
                           break;
          case "Orders":
                           IMgDB.setImageDrawable(getResources().getDrawable(R.drawable.black_db));
                           ImgProduct.setImageDrawable(getResources().getDrawable(R.drawable.black_db_products));
                           ImdOrder.setImageDrawable(getResources().getDrawable(R.drawable.appcolor_db_orders));
                           ImgAcc.setImageDrawable(getResources().getDrawable(R.drawable.black_db_account));
                           break;
          case "Account":
                           IMgDB.setImageDrawable(getResources().getDrawable(R.drawable.black_db));
                           ImgProduct.setImageDrawable(getResources().getDrawable(R.drawable.black_db_products));
                           ImdOrder.setImageDrawable(getResources().getDrawable(R.drawable.black_db_orders));
                           ImgAcc.setImageDrawable(getResources().getDrawable(R.drawable.appcolor_db_account));
                           break;

      }

    }


    private void getUserDetails() {
        final String actionType;
        AlertDialog.Builder builder = new AlertDialog.Builder(VendorDashBoardActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();


        if(SessionSave.getsession(AppConstants.usertype, VendorDashBoardActivity.this).equalsIgnoreCase("Vendor")){
            actionType = "getprofile_vendor";
        }else{
            actionType = "getprofile_reseller";
        }

        String url = ApiHelper.appDomain;
        Log.i("UserDetailsApi:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                JSONArray mainArray = new JSONArray(mainObj.getString("data"));
                                //{"status":"success","data":[{"id":"2","name":"Rady","city":"kakinada","state":"AP","address":"PP,Cambodia","email":"yoemrattana168@gmail.com","phone":"099","gst":"45345345345",
                                // "company":"GVK","billing_add1":"Main road ","billing_add2":"Main road , near temple street","pickup_add1":"Main road ","pickup_add2":"Main road "}]}

                                    JSONObject obj = mainArray.getJSONObject(0);
                                    SessionSave.saveSession(AppConstants.id, AppHelper.checkStringEmptyOrNot(obj.getString("id")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.name, AppHelper.checkStringEmptyOrNot(obj.getString("name")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.city, AppHelper.checkStringEmptyOrNot(obj.getString("city")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.state, AppHelper.checkStringEmptyOrNot(obj.getString("state")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.address, AppHelper.checkStringEmptyOrNot(obj.getString("address")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.email, AppHelper.checkStringEmptyOrNot(obj.getString("email")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.phone, AppHelper.checkStringEmptyOrNot(obj.getString("phone")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.gst, AppHelper.checkStringEmptyOrNot(obj.getString("gst")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.company, AppHelper.checkStringEmptyOrNot(obj.getString("company")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.billing_add1, AppHelper.checkStringEmptyOrNot(obj.getString("billing_add1")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.billing_add2, AppHelper.checkStringEmptyOrNot(obj.getString("billing_add2")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.pickup_add1, AppHelper.checkStringEmptyOrNot(obj.getString("pickup_add1")), VendorDashBoardActivity.this);
                                    SessionSave.saveSession(AppConstants.pickup_add2, AppHelper.checkStringEmptyOrNot(obj.getString("pickup_add2")), VendorDashBoardActivity.this);
                              

                            }else{
                                //    Toast.makeText(LoginActivity.this,"Invalid credentials",Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",actionType);
                params.put("userid", SessionSave.getsession(AppConstants.userid, VendorDashBoardActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }//LogInByVolley

    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(VendorDashBoardActivity.this);
    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(VendorDashBoardActivity.this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage("Are you sure you want to exit?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        dialog.cancel();
                        finish();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        dialog.cancel();
                    }
                })
                .setIcon(getResources().getDrawable(R.drawable.applogo))
                .show();

    }
}
