package com.prominere.reseller.activity.reseller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gocashfree.cashfreesdk.CFPaymentService;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.MyBankAccountDetails;
import com.prominere.reseller.adapter.WRWalletAdapter;
import com.prominere.reseller.adapter.WalletAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.WalletModel;
import com.prominere.reseller.model.WalletWRModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_APP_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_NAME;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_NOTE;

public class ResellerWalletActivity extends AppCompatActivity {

    private RequestQueue mQueue;
    private TextView TvAddBank,TvWalletAmt;
    private RecyclerView RVList;
    private String walletAction;
    private LinearLayout LLLoadDate,LLAll,LLWithDrawlReq,LLCredit,LLDebit,LLEmptyBox;
    private Button BtnAddMoneyWallet1,BtnAddMoneyWallet,BtnWithdrawl;
    private Dialog AddWalletPopUpDialog;
    private  TextView TvWdContent,TvAll,TvWithDrawlReq,TvCredit,TvDebit,TvBLAll,TvBLCredit,TvBLWithDrawlReq,TvBLDebit;
    ArrayList<WalletModel> walletList;
    String temporderid ,orderamount ;
    private ImageView ImgBack;
    private LinearLayout LLMain;
    private TextView ImgEmptyBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reseller_wallet_activity);


//        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
////        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
////        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_from_right);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
//        setSupportActionBar(toolbar);
//        setTitle("My Wallet");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);

        mQueue = Volley.newRequestQueue(ResellerWalletActivity.this);

       walletAction = getIntent().getStringExtra(AppConstants.wallet_transactions);

        LLMain = (LinearLayout)findViewById(R.id.llMain);
        TvWdContent = (TextView)findViewById(R.id.tvWdContent);
        ImgEmptyBox = (TextView)findViewById(R.id.imgEmptyBox);
        ImgBack = (ImageView)findViewById(R.id.imgBack);
        TvAddBank = (TextView)findViewById(R.id.tvAddBank);
        TvWalletAmt = (TextView)findViewById(R.id.tvWalletAmt);
        BtnWithdrawl = (Button)findViewById(R.id.btnWithDrawl);
        RVList = (RecyclerView)findViewById(R.id.rvList);
        LLLoadDate = (LinearLayout)findViewById(R.id.llLoadDate);
        LLEmptyBox = (LinearLayout)findViewById(R.id.llEmptyView);
        LLAll = (LinearLayout)findViewById(R.id.llAll);
        LLCredit = (LinearLayout)findViewById(R.id.llCredit);
        LLDebit = (LinearLayout)findViewById(R.id.llDebit);
        LLWithDrawlReq = (LinearLayout)findViewById(R.id.llWithDrawlReq);
        BtnAddMoneyWallet = (Button)findViewById(R.id.btnAddmoneyToWallet);
        BtnAddMoneyWallet1 = (Button)findViewById(R.id.btnAddmoneyToWallet1);

        TvAll = (TextView)findViewById(R.id.tvAll);        TvBLAll = (TextView)findViewById(R.id.tvBLAll);
        TvCredit = (TextView)findViewById(R.id.tvCredit);  TvBLCredit = (TextView)findViewById(R.id.tvBLCredit);
        TvDebit = (TextView)findViewById(R.id.tvDebit);    TvBLDebit = (TextView)findViewById(R.id.tvBLDebit);
        TvWithDrawlReq = (TextView)findViewById(R.id.tvWithDrawlReq);    TvBLWithDrawlReq = (TextView)findViewById(R.id.tvBLWithDrawlReq);

        if(SessionSave.getsession(AppConstants.usertype,ResellerWalletActivity.this).equalsIgnoreCase("Vendor")){
            TvAddBank.setVisibility(View.VISIBLE);
        }else{
            TvAddBank.setVisibility(View.GONE);
        }



        BtnAddMoneyWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              AddWalletPopUp(0);
            }
        });
        BtnAddMoneyWallet1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              AddWalletPopUp(0);
            }
        });
        BtnWithdrawl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddWalletPopUp(1);
            }
        });
        TvAddBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent( ResellerWalletActivity.this, MyBankAccountDetails.class);
                startActivity(in);
                finish();
            }
        });


        LLAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TvAll.setTextColor(getResources().getColor(R.color.appColor));        TvBLAll.setVisibility(View.VISIBLE);
                TvCredit.setTextColor(getResources().getColor(R.color.gray_666));        TvBLCredit.setVisibility(View.INVISIBLE);
                TvDebit.setTextColor(getResources().getColor(R.color.gray_666));        TvBLDebit.setVisibility(View.INVISIBLE);
                TvWithDrawlReq.setTextColor(getResources().getColor(R.color.gray_666));        TvBLWithDrawlReq.setVisibility(View.INVISIBLE);
                WalletAdapter dbradapter = new WalletAdapter(ResellerWalletActivity.this, walletList);
                RVList.setLayoutManager(new LinearLayoutManager(ResellerWalletActivity.this));
                RVList.setAdapter(dbradapter);
                dbradapter.notifyDataSetChanged();
                if(walletList.size()>0){
                    ImgEmptyBox.setVisibility(View.GONE); RVList.setVisibility(View.VISIBLE);
                }else{
                    ImgEmptyBox.setVisibility(View.VISIBLE); RVList.setVisibility(View.GONE);
                }
            }
        });
        LLCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TvAll.setTextColor(getResources().getColor(R.color.gray_666));        TvBLAll.setVisibility(View.INVISIBLE);
                TvCredit.setTextColor(getResources().getColor(R.color.appColor));        TvBLCredit.setVisibility(View.VISIBLE);
                TvDebit.setTextColor(getResources().getColor(R.color.gray_666));        TvBLDebit.setVisibility(View.INVISIBLE);
                TvWithDrawlReq.setTextColor(getResources().getColor(R.color.gray_666));        TvBLWithDrawlReq.setVisibility(View.INVISIBLE);

                ArrayList<WalletModel> creditWalletList = new ArrayList<WalletModel>();
                for(int i=0;i<walletList.size();i++){
                    WalletModel model = walletList.get(i);
                    if(model.getTransaction().toUpperCase().equalsIgnoreCase("PLUS")){
                        creditWalletList.add(model);
                    }
                }
                WalletAdapter dbradapter = new WalletAdapter(ResellerWalletActivity.this, creditWalletList);
                RVList.setLayoutManager(new LinearLayoutManager(ResellerWalletActivity.this));
                RVList.setAdapter(dbradapter);
                dbradapter.notifyDataSetChanged();
                if(creditWalletList.size()>0){
                    ImgEmptyBox.setVisibility(View.GONE); RVList.setVisibility(View.VISIBLE);
                }else{
                    ImgEmptyBox.setVisibility(View.VISIBLE); RVList.setVisibility(View.GONE);
                }

             }
        });
        LLDebit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TvAll.setTextColor(getResources().getColor(R.color.gray_666));        TvBLAll.setVisibility(View.INVISIBLE);
                TvCredit.setTextColor(getResources().getColor(R.color.gray_666));        TvBLCredit.setVisibility(View.INVISIBLE);
                TvDebit.setTextColor(getResources().getColor(R.color.appColor));        TvBLDebit.setVisibility(View.VISIBLE);
                TvWithDrawlReq.setTextColor(getResources().getColor(R.color.gray_666));        TvBLWithDrawlReq.setVisibility(View.INVISIBLE);

                ArrayList<WalletModel> debitWalletList = new ArrayList<WalletModel>();
                for(int i=0;i<walletList.size();i++){
                    WalletModel model = walletList.get(i);
                    if(model.getTransaction().toUpperCase().equalsIgnoreCase("MINUS")){
                        debitWalletList.add(model);
                    }
                }
                WalletAdapter dbradapter = new WalletAdapter(ResellerWalletActivity.this, debitWalletList);
                RVList.setLayoutManager(new LinearLayoutManager(ResellerWalletActivity.this));
                RVList.setAdapter(dbradapter);
                dbradapter.notifyDataSetChanged();
                if(debitWalletList.size()>0){
                    ImgEmptyBox.setVisibility(View.GONE); RVList.setVisibility(View.VISIBLE);
                }else{
                    ImgEmptyBox.setVisibility(View.VISIBLE); RVList.setVisibility(View.GONE);
                }
             }
        });
        LLWithDrawlReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TvAll.setTextColor(getResources().getColor(R.color.gray_666));        TvBLAll.setVisibility(View.INVISIBLE);
                TvCredit.setTextColor(getResources().getColor(R.color.gray_666));        TvBLCredit.setVisibility(View.INVISIBLE);
                TvDebit.setTextColor(getResources().getColor(R.color.gray_666));        TvBLDebit.setVisibility(View.INVISIBLE);
                TvWithDrawlReq.setTextColor(getResources().getColor(R.color.appColor));        TvBLWithDrawlReq.setVisibility(View.VISIBLE);

                getWithDrawlReq();
                
             }
        });
        ImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(SessionSave.getsession(AppConstants.usertype,ResellerWalletActivity.this).equalsIgnoreCase("Vendor")){
            BtnAddMoneyWallet1.setVisibility(View.GONE);
        }else{
            BtnAddMoneyWallet1.setVisibility(View.VISIBLE);
        }



    }//onCreate

    private void getWithDrawlReq() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ResellerWalletActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            if(response.contains("<div")){
                                String[] arrSplit = response.split("</div>");
                                setWithDrawReqToViews(arrSplit[1]);
                            }else{
                                setWithDrawReqToViews(response);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.withdraw_requests);
                params.put("userid", SessionSave.getsession(AppConstants.userid,ResellerWalletActivity.this));
//                params.put("userid","9");
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    private void setWithDrawReqToViews(String response) {
      try {

          JSONObject mainObj = new JSONObject(response);
          if(mainObj.getString("status").equalsIgnoreCase("success")){
              List<WalletWRModel> wrwalletList = new ArrayList<WalletWRModel>();
              JSONArray mainArray = new JSONArray(mainObj.getString("list"));
              if(mainArray.length()>0) {
                  ImgEmptyBox.setVisibility(View.GONE);  RVList.setVisibility(View.VISIBLE);
                  for (int i = 0; i < mainArray.length(); i++) {
                      JSONObject obj = mainArray.getJSONObject(i);

                      WalletWRModel model = new WalletWRModel();

                      model.setId(obj.getString("id"));
                      model.setTransactionid(obj.getString("transactionid"));
                      model.setDateadded(obj.getString("dateadded"));
                      model.setAmount(obj.getString("amount"));
                      model.setStatus(obj.getString("status"));

                      wrwalletList.add(model);
                  }

                  WRWalletAdapter dbradapter = new WRWalletAdapter(ResellerWalletActivity.this, wrwalletList);
                  RVList.setLayoutManager(new LinearLayoutManager(ResellerWalletActivity.this));

                  RVList.setAdapter(dbradapter);
                  dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
              }else{
                  ImgEmptyBox.setVisibility(View.VISIBLE); RVList.setVisibility(View.GONE);
              }

          }else{
              ImgEmptyBox.setVisibility(View.VISIBLE); RVList.setVisibility(View.GONE);
              //   SimpleToast.error(CartItemsActivity.this, AppConstants.apiIssueMsg);
          }
      }catch (Exception e){
          e.printStackTrace();
      }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWalletDetails();
        AppHelper.checkInternetAvailability(ResellerWalletActivity.this);

    }

    private void AddWalletPopUp(final int diff) {
        try{
            View PopUpView = View.inflate(ResellerWalletActivity.this, R.layout.add_wallet__view, null);
            PopUpView.startAnimation(AnimationUtils.loadAnimation(ResellerWalletActivity.this, R.anim.zoom_in_enter));
            this.AddWalletPopUpDialog = new Dialog(ResellerWalletActivity.this, R.style.NewDialog);
            this.AddWalletPopUpDialog.setContentView(PopUpView);
            this.AddWalletPopUpDialog.setCancelable(true);
            this.AddWalletPopUpDialog.show();

            Window window = AddWalletPopUpDialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER | Gravity.CENTER;
            window.setGravity(Gravity.CENTER);
            wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
            wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
            wlp.dimAmount = 0.0f;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            wlp.windowAnimations = R.anim.slide_move;

            window.setAttributes(wlp);
            window.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

            TextView TvHeading=(TextView)PopUpView.findViewById(R.id.tvPpUpHeading);
            Button BtnAddMoney=(Button)PopUpView.findViewById(R.id.btnAddMoney);
            final EditText EtAmt=(EditText)PopUpView.findViewById(R.id.etAmt);
            ImageView v = (ImageView) PopUpView.findViewById(R.id.closeDialog);

            if(diff == 0){
                TvHeading.setText("Add Money");
            }else{
                TvHeading.setText("Withdrawal");
                BtnAddMoney.setText("Send Request");
            }

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AddWalletPopUpDialog.dismiss();
                }
            });
            BtnAddMoney.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(EtAmt.getText().toString().trim().isEmpty()){
                        AppHelper.showSnackBar(ResellerWalletActivity.this,LLMain,"Please enter valid amount");
//                        SimpleToast.error(ResellerWalletActivity.this,"Please enter valid amount");
                    }else {
                        BigInteger numb = new BigInteger(EtAmt.getText().toString().trim());
                        BigInteger numb1 = new BigInteger("0");
                        int comparevalue = numb.compareTo(numb1);
                        if (comparevalue == 1) {
                            AddWithdrawlMoney(diff, EtAmt.getText().toString());
                        } else {
                            AppHelper.showSnackBar(ResellerWalletActivity.this,LLMain,"Please enter valid amount");
//                            SimpleToast.error(ResellerWalletActivity.this, "Please enter valid amount");
                        }
                    }
                }
            });


        }catch (Exception e){e.printStackTrace();}
    }

    private void AddWithdrawlMoney(final int diff, final String amt) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ResellerWalletActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {//{"status":"success","tempid":756791506,"userid":"42","paymenttoken":"US9JCN4MzUIJiOicGbhJCLiQ1VKJiOiAXe0Jye.C89JyMiBDM2M2YzUTZ0UWNiojI0xWYz9lIsQjN1MDO3QDO1EjOiAHelJCLiIlTJJiOik3YuVmcyV3QyVGZy9mIsIjOiQnb19WbBJXZkJ3biwiI2ATNxkzN2UzNiojIklkclRmcvJye.kg9r0ZzPy13yLzbBP5-_gjSw1eaAwwEAPPesKrchaoW9FSrir6BwZ_PGn0qvrNWAA_","amount":"2","clientid":"96831a4aa64a667b5b5fb3d73869"}

                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")) {
                             //   SimpleToast.ok(ResellerWalletActivity.this,mainObj.getString("message"));


                                if(diff == 0){
                                    if(AddWalletPopUpDialog.isShowing()){ AddWalletPopUpDialog.dismiss(); }
                                    getWalletDetails();
                                     temporderid = mainObj.getString("tempid");
                                    String paymentToken = mainObj.getString("paymenttoken");
                                     orderamount = mainObj.getString("amount");
                                    String clientid = mainObj.getString("clientid");

                                    String stage = "TEST";
                                    String orderNote = "TEST Order";

                                    Map<String, String> params = new HashMap<>();

                                    params.put(PARAM_APP_ID, clientid  );
                                    params.put(PARAM_ORDER_ID, temporderid);
                                    params.put(PARAM_ORDER_AMOUNT, orderamount);
                                    params.put(PARAM_ORDER_NOTE, orderNote);
                                    params.put(PARAM_CUSTOMER_NAME, SessionSave.getsession(AppConstants.name,ResellerWalletActivity.this));
                                    params.put(PARAM_CUSTOMER_PHONE, SessionSave.getsession(AppConstants.phone,ResellerWalletActivity.this) );
                                    params.put(PARAM_CUSTOMER_EMAIL, SessionSave.getsession(AppConstants.email,ResellerWalletActivity.this));


                                    for(Map.Entry entry : params.entrySet()) {
                                        Log.d("CFSKDSample", entry.getKey() + " " + entry.getValue());
                                    }

                                    CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
                                    cfPaymentService.setOrientation(ResellerWalletActivity.this, 0);

                                    // Use the following method for initiating Payments
                                    // First color - Toolbar background
                                    // Second color - Toolbar text and back arrow color
                                    cfPaymentService.doPayment(ResellerWalletActivity.this, params, paymentToken, stage, "#FD595A", "#FFFFFF");



                                }else {
                                    if(mainObj.getString("message").equalsIgnoreCase("Requested amount is lessthan your wallet amount!")){
                                        AppHelper.showSnackBar(ResellerWalletActivity.this,LLMain,mainObj.getString("message"));
//                                        SimpleToast.error(ResellerWalletActivity.this,mainObj.getString("message"));
                                    }else{
                                        AppHelper.showSnackBar(ResellerWalletActivity.this,LLMain,mainObj.getString("message"));
//                                        SimpleToast.ok(ResellerWalletActivity.this,mainObj.getString("message"));
                                        if(AddWalletPopUpDialog.isShowing()){ AddWalletPopUpDialog.dismiss(); }
                                        getWalletDetails();

                                    }


                                }
                            }else{
                                AppHelper.showSnackBar(ResellerWalletActivity.this,LLMain,mainObj.getString("message"));
//                                SimpleToast.error(ResellerWalletActivity.this,mainObj.getString("message"));
                            }


                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                if(diff == 0){
                    params.put("action",ApiHelper.addmoney);
                }else{
                    params.put("action",ApiHelper.withdraw);
                }


                params.put("userid", SessionSave.getsession(AppConstants.userid,ResellerWalletActivity.this));
                params.put("amount",amt);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    private void getWalletDetails() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ResellerWalletActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            if(response.contains("<div")){
                                String[] arrSplit = response.split("</div>");
                                setToViews(arrSplit[1]);
                            }else{
                                setToViews(response);
                            }



                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",walletAction);
                params.put("userid", SessionSave.getsession(AppConstants.userid,ResellerWalletActivity.this));
//                params.put("userid","9");
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    private void setToViews(String response) {
      try {
          TvAll.setTextColor(getResources().getColor(R.color.appColor));        TvBLAll.setVisibility(View.VISIBLE);
          TvCredit.setTextColor(getResources().getColor(R.color.gray_666));        TvBLCredit.setVisibility(View.INVISIBLE);
          TvDebit.setTextColor(getResources().getColor(R.color.gray_666));        TvBLDebit.setVisibility(View.INVISIBLE);
          TvWithDrawlReq.setTextColor(getResources().getColor(R.color.gray_666));        TvBLWithDrawlReq.setVisibility(View.INVISIBLE);


          JSONObject mainObj = new JSONObject(response);
             walletList = new ArrayList<WalletModel>();
          if(mainObj.getString("status").equalsIgnoreCase("success")){
              //   TvEmptyBox.setVisibility(View.GONE);
              if(mainObj.getString("wallet") .equalsIgnoreCase("null") || mainObj.getString("wallet") .isEmpty()){
                  TvWalletAmt.setText("₹0" );
              }else {
                  TvWalletAmt.setText("₹" + AppHelper.checkStringEmptyOrNot(mainObj.getString("wallet")));
              }
              JSONArray mainArray = new JSONArray(mainObj.getString("list"));
              if(mainArray.length()>0) {
                  LLEmptyBox.setVisibility(View.GONE);  LLLoadDate.setVisibility(View.VISIBLE);
                  for (int i = 0; i < mainArray.length(); i++) {
                      JSONObject obj = mainArray.getJSONObject(i);

                      WalletModel model = new WalletModel();

                      model.setId(obj.getString("id"));
                      model.setTransactionid(obj.getString("transactionid"));
                      model.setDateadded(obj.getString("dateadded"));
                      model.setAmount(obj.getString("amount"));
                      model.setTransaction(obj.getString("transaction"));
                      model.setTransactiontype(obj.getString("transactiontype"));

                      walletList.add(model);
                  }

                  WalletAdapter dbradapter = new WalletAdapter(ResellerWalletActivity.this, walletList);

                  RVList.setLayoutManager(new LinearLayoutManager(ResellerWalletActivity.this));

                  RVList.setAdapter(dbradapter);
                  dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
              }else{
                  LLEmptyBox.setVisibility(View.VISIBLE); LLLoadDate.setVisibility(View.GONE);
              }

          }else{
              LLEmptyBox.setVisibility(View.VISIBLE); LLLoadDate.setVisibility(View.GONE);
              //   SimpleToast.error(CartItemsActivity.this, AppConstants.apiIssueMsg);
          }
      }catch (Exception e){e.printStackTrace();}
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Same request code for all payment APIs.
        Log.d("WalletAct:-->", "ReqCode : " + CFPaymentService.REQ_CODE);
        Log.d("WalletAct:-->", "API Response : ");
        //Prints all extras. Replace with app logic.
        if (data != null) {
            Bundle bundle = data.getExtras();
            String status = null;
            if (bundle != null)

                status =  bundle.getString("txStatus");
            Log.i("Staus:--> ",""+status);
            for (String key : bundle.keySet()) {
                if (bundle.getString(key) != null) {
                    Log.d("WalletAct:-->", key + " : " + bundle.getString(key));
                }
            }
            if(status.equalsIgnoreCase("SUCCESS")){
                updateWalletStatus();

            }else{
                AppHelper.showSnackBar(ResellerWalletActivity.this,LLMain,status );
//                SimpleToast.error(ResellerWalletActivity.this,status);
            }
        }
    }

    private void updateWalletStatus() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ResellerWalletActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")) {
                                //   SimpleToast.ok(ResellerWalletActivity.this,mainObj.getString("message"));
                                if(AddWalletPopUpDialog.isShowing()){ AddWalletPopUpDialog.dismiss(); }

                               getWalletDetails();

                            }else{
                                AppHelper.showSnackBar(ResellerWalletActivity.this,LLMain,mainObj.getString("message") );
//                                SimpleToast.error(ResellerWalletActivity.this,mainObj.getString("message"));
                            }


                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                params.put("action",ApiHelper.addmoneysuccess);
                params.put("userid", SessionSave.getsession(AppConstants.userid,ResellerWalletActivity.this));
                params.put("amount",orderamount);
                params.put("tempid",temporderid);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
