package com.prominere.reseller.activity.dashboard;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.OTPActivity;
import com.prominere.reseller.activity.dashboard.account.MyOrdersActivity;
import com.prominere.reseller.adapter.DBProductAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.ProductModel;
import com.prominere.reseller.util.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DBProductsActivity extends AppCompatActivity {

    private RequestQueue mQueue;
    private TextView TvEmptyBox;
    private RecyclerView LvProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dbproducts_activity);


        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        setTitle("Products");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
        LvProducts = (RecyclerView)findViewById(R.id.lvProducts);
        TvEmptyBox = (TextView)findViewById(R.id.imgEmptyBox);
        mQueue = Volley.newRequestQueue(DBProductsActivity.this);
        getProducts();
    }

    private void getProducts() {

        AlertDialog.Builder builder = new AlertDialog.Builder(DBProductsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Products:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            ArrayList<ProductModel> productList = new ArrayList<ProductModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                TvEmptyBox.setVisibility(View.GONE);  LvProducts.setVisibility(View.VISIBLE);
                                JSONArray mainArray = new JSONArray(mainObj.getString("black_db_products"));
                                if(mainArray.length()>0) {
                                    int total = 0;
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        ProductModel catList = new ProductModel();
                                        catList.setId(obj.getString("id"));
                                        catList.setTitle(obj.getString("title"));
                                        catList.setPrice(obj.getString("price"));
                                        catList.setDiscountprice(obj.getString("discountprice"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setCategory(obj.getString("category"));
                                        productList.add(catList);
                                    }
                                    DBProductAdapter dbradapter = new DBProductAdapter(DBProductsActivity.this, productList);

                                    LvProducts.setLayoutManager(new LinearLayoutManager(DBProductsActivity.this) );

                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                    TvEmptyBox.setVisibility(View.VISIBLE);  LvProducts.setVisibility(View.GONE);
                                }
                            }else{
                                TvEmptyBox.setVisibility(View.VISIBLE);  LvProducts.setVisibility(View.GONE);
                                //   SimpleToast.error(CartItemsActivity.this, AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(DBProductsActivity.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.vendor_productlist);
//                params.put("vendorid","2");
                params.put("vendorid", SessionSave.getsession(AppConstants.userid,DBProductsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }


    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(DBProductsActivity.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
