package com.prominere.reseller.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.MainNavigationActivity;
import com.prominere.reseller.activity.vendoractivity.VendorDashBoardActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.util.SessionSave;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(SessionSave.getsession(AppConstants.userid,SplashScreenActivity.this).isEmpty()){
                    Intent in = new Intent(SplashScreenActivity.this,LoginActivity.class);
                    startActivity(in);
                    finish();
                }else{
                    if(SessionSave.getsession(AppConstants.usertype,SplashScreenActivity.this).equalsIgnoreCase("Vendor")){
                        Intent in = new Intent(SplashScreenActivity.this, VendorDashBoardActivity.class);
                        startActivity(in);
                        finish();
                    }else{
                        Intent in = new Intent(SplashScreenActivity.this, MainNavigationActivity.class);
                        startActivity(in);
                        finish();
                    }
                }




            }
        },2000);

    }
}
