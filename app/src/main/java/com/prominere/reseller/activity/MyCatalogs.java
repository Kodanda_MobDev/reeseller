package com.prominere.reseller.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.CartItemsActivity;
import com.prominere.reseller.activity.reseller.SearchActivity;
import com.prominere.reseller.adapter.MyCatalogsAdapter;
import com.prominere.reseller.adapter.WishListItemsAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.ProductModel;
import com.prominere.reseller.util.SessionSave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyCatalogs extends AppCompatActivity {

    private TextView TvWishlist,TvEmptyBox,TvShare;
    private ListView LvProducts;
    private RequestQueue mQueue;
    private LinearLayout LLShared,LLWishList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_catalogs_activity);



        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
 //       overridePendingTransition(R.anim.enter_from_right, R.anim.enter_from_right);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        setTitle(AppConstants.myCatalogs);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);


        TvWishlist = (TextView)findViewById(R.id.tvWishList);
        TvShare = (TextView)findViewById(R.id.tvShare);
        LLWishList = (LinearLayout)findViewById(R.id.llWishList);
        LLShared = (LinearLayout)findViewById(R.id.llShared);
        final ImageView ImgWishlist = (ImageView) findViewById(R.id.imgWishList);
        final ImageView ImgShared = (ImageView) findViewById(R.id.ImgShared);

        LvProducts = (ListView)findViewById(R.id.lvProducts);
        TvEmptyBox = (TextView)findViewById(R.id.imgEmptyBox);
        mQueue = Volley.newRequestQueue(MyCatalogs.this);

        LLWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImgWishlist.setImageDrawable(getResources().getDrawable(R.drawable.appcolor_wishlist)); ImgShared.setImageDrawable(getResources().getDrawable(R.drawable.black_share));
                TvWishlist.setTextColor(getResources().getColor(R.color.appColor));TvShare.setTextColor(getResources().getColor(R.color.black));
                LvProducts.setAdapter(null);
                getWishlistProduct();
//                getProducts();
            }
        });
        LLShared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImgWishlist.setImageDrawable(getResources().getDrawable(R.drawable.black_wishlist)); ImgShared.setImageDrawable(getResources().getDrawable(R.drawable.appcolor_share));
                TvWishlist.setTextColor(getResources().getColor(R.color.black));  TvShare.setTextColor(getResources().getColor(R.color.appColor));

                LvProducts.setAdapter(null);
                getSharedProduct();
            }
        });

     getWishlistProduct();
//        getProducts();

    }//onCreate

    private void getWishlistProduct() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MyCatalogs.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            ArrayList<ProductModel> productList = new ArrayList<ProductModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                TvEmptyBox.setVisibility(View.GONE);
                                JSONArray mainArray = new JSONArray(mainObj.getString("products"));
                                if(mainArray.length()>0) {
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        ProductModel catList = new ProductModel();
                                        catList.setId(obj.getString("id"));
                                        catList.setTitle(obj.getString("title"));
                                        catList.setPrice(obj.getString("price"));
                                        catList.setDiscountprice(obj.getString("discountprice"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setCategory(obj.getString("category"));
                                        catList.setWishliststatus(obj.getString("wishliststatus"));

                                        productList.add(catList);
                                    }

                                    MyCatalogsAdapter dbradapter = new MyCatalogsAdapter(0,MyCatalogs.this, productList);
                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                    TvEmptyBox.setVisibility(View.VISIBLE);
                                }

                            }else{
                                TvEmptyBox.setVisibility(View.VISIBLE);
//                                SimpleToast.error(WishListItemsActivity.this,AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(MyCatalogs.this, AppConstants.noInternetConnection  );
                        }
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.wishlist);
                params.put("userid", SessionSave.getsession(AppConstants.userid,MyCatalogs.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }
    private void getProducts() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MyCatalogs.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            ArrayList<ProductModel> productList = new ArrayList<ProductModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                TvEmptyBox.setVisibility(View.GONE);
                                JSONArray mainArray = new JSONArray(mainObj.getString("products"));
                                if(mainArray.length()>0) {
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        ProductModel catList = new ProductModel();
                                        catList.setId(obj.getString("id"));
                                        catList.setTitle(obj.getString("title"));
                                        catList.setPrice(obj.getString("price"));
                                        catList.setDiscountprice(obj.getString("discountprice"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setCategory(obj.getString("category"));
                                        catList.setWishliststatus(obj.getString("wishliststatus"));

                                        productList.add(catList);
                                    }

                                    WishListItemsAdapter dbradapter = new WishListItemsAdapter(MyCatalogs.this, productList);
                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                    TvEmptyBox.setVisibility(View.VISIBLE);
                                }

                            }else{
                                TvEmptyBox.setVisibility(View.VISIBLE);
//                                SimpleToast.error(WishListItemsActivity.this,AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(MyCatalogs.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.wishlist);
                params.put("userid", SessionSave.getsession(AppConstants.userid,MyCatalogs.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }




    private void getSharedProduct() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MyCatalogs.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            ArrayList<ProductModel> productList = new ArrayList<ProductModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                TvEmptyBox.setVisibility(View.GONE);
                                JSONArray mainArray = new JSONArray(mainObj.getString("products"));
                                if(mainArray.length()>0) {
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        ProductModel catList = new ProductModel();
                                        catList.setId(obj.getString("id"));
                                        catList.setTitle(obj.getString("title"));
                                        catList.setPrice(obj.getString("price"));
                                        catList.setDiscountprice(obj.getString("discountprice"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setCategory(obj.getString("category"));
                                        catList.setWishliststatus(obj.getString("wishliststatus"));

                                        productList.add(catList);
                                    }
                                    MyCatalogsAdapter dbradapter = new MyCatalogsAdapter(1,MyCatalogs.this, productList);

//                                    WishListItemsAdapter dbradapter = new WishListItemsAdapter(MyCatalogs.this, productList);
                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                    TvEmptyBox.setVisibility(View.VISIBLE);
                                }

                            }else{
                                TvEmptyBox.setVisibility(View.VISIBLE);
//                                SimpleToast.error(WishListItemsActivity.this,AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(MyCatalogs.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.shared_productlist);
                params.put("userid", SessionSave.getsession(AppConstants.userid,MyCatalogs.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(MyCatalogs.this);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_cart_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.miSearch:
                Intent sin = new Intent(MyCatalogs.this, SearchActivity.class);
                startActivity(sin);
                finish();
                break;
            case R.id.miok:
                if(new  CartItemsActivity().isRunning == false) {
                    Intent min = new Intent(MyCatalogs.this, CartItemsActivity.class);
                    startActivity(min);
                    finish();
                }
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
