package com.prominere.reseller.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.helper.validations.ValidationDTO;
import com.prominere.reseller.helper.validations.ValidationHelper;
import com.prominere.reseller.helper.validations.ValidationUtils;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {

    private Spinner SpTypes;
    private CheckBox CBTermsCond;
    private EditText EtName,EtMobNumb, EtEmail, EtCompName,EtAddress ,EtWhatsAppNumber,EtGstNumb, EtPassword,EtConfPassword;
    private RequestQueue queue;
    private String typeId;
    private TextView TvTermsAndConditions;
    private Typeface fontAwesomeFont;
    private TextView TvConfirmPSWShow,TvPSWShow;
    private boolean hideSeek = false;
    private boolean hideSeek1 = false;
    private LinearLayout LLCompNameGST;
    private LinearLayout LLWhatsAppNumber,LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity);

        queue = Volley.newRequestQueue(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);


//        overridePendingTransition(R.anim.down_from_top, R.anim.down_from_top);
        AppHelper.setStatusBarColor(this, R.color.statusBarColor);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign Up");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);

        fontAwesomeFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");


        LLMain =(LinearLayout)findViewById(R.id.llMain);
        LLWhatsAppNumber =(LinearLayout)findViewById(R.id.llWhatsAppNumber);
        EtName =(EditText)findViewById(R.id.et_Name);
        EtMobNumb =(EditText)findViewById(R.id.et_MobNumber);
        EtEmail =(EditText)findViewById(R.id.et_email);
        EtCompName =(EditText)findViewById(R.id.et_compName);
        EtAddress =(EditText)findViewById(R.id.et_address);
        EtGstNumb =(EditText)findViewById(R.id.et_GSTNumb);
        EtWhatsAppNumber =(EditText)findViewById(R.id.et_WhatsAppNumber);
        EtPassword =(EditText)findViewById(R.id.etPassword);                //  EtPassword.requestFocus();     EtPassword.setSelection(EtPassword.length());
        EtConfPassword =(EditText)findViewById(R.id.etConfirmPassword);     //  EtConfPassword.requestFocus(); EtConfPassword.setSelection(EtConfPassword.length());
        SpTypes =(Spinner)findViewById(R.id.spTypeSel);
        TvPSWShow=(TextView)findViewById(R.id.tvPswShow);               TvPSWShow.setTypeface(fontAwesomeFont);  // AppHelper.CallBounceView(TvPSWShow);
        TvConfirmPSWShow=(TextView)findViewById(R.id.tvConfirmPswShow); TvConfirmPSWShow.setTypeface(fontAwesomeFont);  // AppHelper.CallBounceView(TvPSWShow);

        LLCompNameGST =(LinearLayout)findViewById(R.id.llCompNameGST);
        CBTermsCond =(CheckBox)findViewById(R.id.cb_termsCond);
        TvTermsAndConditions =(TextView)findViewById(R.id.tvTermsAndCond);



        TvTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(RegistrationActivity.this,TermsAndConditionsActivity.class);
                startActivity(in);
            }
        });
        SpTypes.setSelection(0);   LLCompNameGST.setVisibility(View.GONE);
        SpTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.DKGRAY);
                ((TextView) parent.getChildAt(0)).setTextSize(getResources().getDimension(R.dimen.padding2));
                String  sType =  parent.getSelectedItem().toString();
                Log.i("SType:-->",sType);
                if(sType.equalsIgnoreCase("Reseller")){
                    typeId = "1";
                    LLCompNameGST.setVisibility(View.GONE);
                   Log.i("TuypeId :-->",""+typeId);
                    LLWhatsAppNumber.setVisibility(View.GONE);
                }else if(sType.equalsIgnoreCase("Vendor")){
                    typeId = "2";
                    LLCompNameGST.setVisibility(View.VISIBLE);
                    Log.i("TuypeId :-->",""+typeId);
                    LLWhatsAppNumber.setVisibility(View.VISIBLE);
                }else{
                    typeId = "";
                    LLWhatsAppNumber.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        TvPSWShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hideSeek) {// show password
                    TvPSWShow.setTextColor(getResources().getColor(R.color.black));
                    TvPSWShow.setText(R.string.fai_remove_eye);
                    EtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    hideSeek = false;

                } else { // hide password
                    TvPSWShow.setTextColor(getResources().getColor(R.color.black));
                    TvPSWShow.setText(R.string.fai_eye);
                    EtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    hideSeek=true;
                }
            }
        });
        TvConfirmPSWShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hideSeek1) {// show password
                    TvConfirmPSWShow.setTextColor(getResources().getColor(R.color.black));
                    TvConfirmPSWShow.setText(R.string.fai_remove_eye);
                    EtConfPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    hideSeek1 = false;

                } else { // hide password
                    TvConfirmPSWShow.setTextColor(getResources().getColor(R.color.black));
                    TvConfirmPSWShow.setText(R.string.fai_eye);
                    EtConfPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    hideSeek1=true;
                }
            }
        });


    }//onCreate

    public void submitClicked(View view) {
        validations();
    }
    private void validations() {
        try{
            ValidationHelper helper=new ValidationHelper();
            String[] strIds = getResources().getStringArray(R.array.regdVendor_ids_array);
            String[] strErrMsgs = getResources().getStringArray(R.array.regdVendor_errors_array);
            String[] strCompTypeArr = getResources().getStringArray(R.array.regdVendor_comptypes_array);
            ArrayList<ValidationDTO> aList = new ArrayList<ValidationDTO>();

            int iPos = 0;
            for(String strCompType:strCompTypeArr){
                ValidationDTO valDTO=new ValidationDTO();
                valDTO.setComponentType(strCompType);
                valDTO.setComponentID(ValidationUtils.getIdResourceByName(RegistrationActivity.this,strIds[iPos]));
                valDTO.setErrorMessage(strErrMsgs[iPos]);
                aList.add(valDTO);
                iPos++;
            }
            boolean isValidData = helper.validateData(RegistrationActivity.this.getBaseContext(), aList,getWindow().getDecorView() );
            if (!isValidData) {
                return;
            }else{
                if((!EtEmail.getText().toString().isEmpty()) && (AppHelper.emailValidator(EtEmail.getText().toString() )) ){
//                    if(AppHelper.isVaidPassword(EtPassword) ){//Valid Password
                        if(EtPassword.getText().toString().equalsIgnoreCase(EtConfPassword.getText().toString())){

                            if(CBTermsCond.isChecked() ){

                                  registrationByVolley();

                            }else{
                                AppHelper.showSnackBar(RegistrationActivity.this,LLMain,"Please check Terms&Conditions");
//                                SimpleToast.error(RegistrationActivity.this,"Please check Terms&Conditions");
                            }

                        }else{
                            AppHelper.showSnackBar(RegistrationActivity.this,LLMain,"Passwords are not Matching");
//                            SimpleToast.error(RegistrationActivity.this,"Passwords are not Matching");
                        }
//                    }else{//Not Valid
//                        EtPassword.setError(getString(R.string.login_regd_password_validation));
//                        SimpleToast.error(RegistrationActivity.this,"Invalid password(Authentication is case-sensitive)");
//                        }
                }else{
                    if(EtEmail.getText().toString().isEmpty()){
                        if(CBTermsCond.isChecked() ){

                            registrationByVolley();


                        }else{
                            AppHelper.showSnackBar(RegistrationActivity.this,LLMain,"Please check Terms&Conditions");
//                            SimpleToast.error(RegistrationActivity.this,"Please check Terms&Conditions");
                        }


                    }else{
                        EtEmail.setError("InValid Email Address.");
                        AppHelper.showSnackBar(RegistrationActivity.this,LLMain,"InValid Email Address.");
//                        SimpleToast.error(RegistrationActivity.this,"InValid Email Address.");
                    }
                }//emailVerification
            }
        }catch (Exception e){     e.printStackTrace();    }
    }//Validations

    private void registrationByVolley() {

        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                Intent in = new Intent(RegistrationActivity.this,OTPActivity.class);
                                in.putExtra("otp",mainObj.getString("otp"));
                                in.putExtra("userid",mainObj.getString("userid"));
                                in.putExtra("type",typeId);
                                in.putExtra("phone",EtMobNumb.getText().toString());
                                in.putExtra("password",EtPassword.getText().toString());
                                startActivity(in);
                                finish();
                            }else{
                                AppHelper.showSnackBar(RegistrationActivity.this,LLMain,mainObj.getString("message"));
//                                 SimpleToast.error(RegistrationActivity.this,mainObj.getString("message"));
                            //    Toast.makeText(RegistrationActivity.this,"Registration not done.please try again later.",Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.register_otp);
                params.put("email",EtEmail.getText().toString());
                params.put("type",typeId);
                params.put("phone",EtMobNumb.getText().toString());
                params.put("address",EtAddress.getText().toString());
                params.put("password",EtPassword.getText().toString());
                params.put("confirmpassword",EtConfPassword.getText().toString());
                params.put("name",EtName.getText().toString());
                if( typeId == "2"){
                    params.put("gst_pan",EtGstNumb.getText().toString());
                    params.put("company",EtCompName.getText().toString());
                    params.put("whatsappnumber",EtWhatsAppNumber.getText().toString());
                }else{
                    params.put("gst_pan"," ");
                    params.put("company"," ");
                    params.put("whatsappnumber"," ");
                }

                Log.i("obj:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });
        queue.add(sr);



    }

    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(RegistrationActivity.this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent in = new Intent(RegistrationActivity.this,LoginActivity.class);
                startActivity(in);
                finish();
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent in = new Intent(RegistrationActivity.this,LoginActivity.class);
        startActivity(in);
        finish();
    }

}
