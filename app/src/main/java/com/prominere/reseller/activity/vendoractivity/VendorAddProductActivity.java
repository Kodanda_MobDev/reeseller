package com.prominere.reseller.activity.vendoractivity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.CompoundButtonCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.prominere.reseller.BuildConfig;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.HelpActivity;
import com.prominere.reseller.adapter.UpdatedImageAdapter;
import com.prominere.reseller.adapter.VendCategorylvAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.helper.validations.ValidationDTO;
import com.prominere.reseller.helper.validations.ValidationHelper;
import com.prominere.reseller.helper.validations.ValidationUtils;
import com.prominere.reseller.model.CatSelectionModel;
import com.prominere.reseller.model.CategoryModel;
import com.prominere.reseller.model.MainSubCatModel;
import com.prominere.reseller.model.SubCatModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.VolleyMultipartRequest;
import com.prominere.reseller.util.filePicker.PhotoPicker;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class VendorAddProductActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = VendorAddProductActivity.class.getSimpleName();

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 2;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private RequestQueue mQueue;
    private ListView LVCategories;
    int preSelectedIndex = -1;
    private Spinner SpGroup,SpCategory,SpMainSubCategory,SpSubCategory;

    private int selectedCatId,selectedMainSubCatId,selectedSubCatId,selectedGroupId;
    private Button BtnSubmit;
    private PhotoPicker easyPicker;
    private List<String> filePathsList;
    private EditText EtTitle,EtWeight,EtShippingCharge,EtGST,EtHsncode,EtDiscPrice,EtPrice,EtShortDesc,EtDesc;
    private CheckBox RbSmall,RbMedium,RbL,RbX,RbXL,RbXXL;
    private int pId;
    private StringBuilder seleAttr;
    //    private RadioGroup RbGroup;
    private RecyclerView RCUpdatedImgs;
    private UpdatedImageAdapter adapter;
    private LinearLayout LLAttributes,LLSubCat,LLCheckBoxes;
    private String [] permissions = {"android.permission.WRITE_EXTERNAL_STORAGE","android.permission.CAMERA"};
    private ImageView ImgDummyAdd;
    private FloatingActionButton FABAddGroup;
    private Dialog gDialog;
    private String selectedAttr;
    private LinearLayout LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vendor_add_product_activity);



        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
//        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_from_right);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        setTitle("Add Product");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);


//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            requestPermissions(permissions, 0 );
//        }
//        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
//        } else {
////            if (savedInstanceState == null)
////                getSupportLoaderManager().initLoader(0, null, this);
//        }


        LLMain = (LinearLayout)findViewById(R.id.llMain);
        BtnSubmit =(Button)findViewById(R.id.btnSubmit);
        ImgDummyAdd =(ImageView)findViewById(R.id.imgDummyAdd);
        LLSubCat =(LinearLayout)findViewById(R.id.llSubCat);
        LLAttributes =(LinearLayout)findViewById(R.id.llAttributes);
        RCUpdatedImgs =(RecyclerView)findViewById(R.id.rc_updatedimgs);
        SpCategory =(Spinner)findViewById(R.id.spCategory);
        SpGroup =(Spinner)findViewById(R.id.spGroup);
        SpMainSubCategory =(Spinner)findViewById(R.id.spMainSubCategory);
        SpSubCategory =(Spinner)findViewById(R.id.spSubCategory);
        LVCategories = (ListView)findViewById(R.id.lvCategories);
        easyPicker = (PhotoPicker)findViewById(R.id.pp_easypicker_strip);
        EtTitle = (EditText)findViewById(R.id.etTitle);
        EtDesc = (EditText)findViewById(R.id.etDesc);
        EtShortDesc = (EditText)findViewById(R.id.etshortDesc);
        EtPrice = (EditText)findViewById(R.id.etPrice);
        EtDiscPrice = (EditText)findViewById(R.id.etDiscountPrice);
        EtHsncode = (EditText)findViewById(R.id.etHsncode);
        EtGST = (EditText)findViewById(R.id.etGST);
        EtWeight = (EditText)findViewById(R.id.etWeight);
        EtShippingCharge = (EditText)findViewById(R.id.etShippingCharge);
        LLCheckBoxes = (LinearLayout)findViewById(R.id.llCheckBoxes);
//         RbGroup = (RadioGroup) findViewById(R.id.rdGroup);
        RbSmall = (CheckBox) findViewById(R.id.rbSmall);
        RbMedium = (CheckBox) findViewById(R.id.rbMedium);
        RbL = (CheckBox) findViewById(R.id.rbL);
        RbX = (CheckBox) findViewById(R.id.rbX);
        RbXL = (CheckBox) findViewById(R.id.rbXL);
        RbXXL = (CheckBox) findViewById(R.id.rbXXL);
        FABAddGroup = (FloatingActionButton) findViewById(R.id.fabAddGroup);

        mQueue = Volley.newRequestQueue(this);

        if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.
            easyPicker.setVisibility(View.VISIBLE);
            easyPicker.performClick();
            ImgDummyAdd.setVisibility(View.GONE);
        } else {  //No user has not granted the permissions yet. Request now.
            ActivityCompat.requestPermissions(VendorAddProductActivity.this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                    0);
        }

        Intent intent = getIntent();
        if(intent.hasExtra(AppConstants.productid)) {
            pId = Integer.parseInt(intent.getStringExtra(AppConstants.productid));
            getProductDetails(pId);
            setTitle("Update Product");
        }else{
            setTitle("Add Product");
            pId = 0;
            getCategoryAndSubCategory();
            getGroupsData();
//           getCategorys();
        }
        requestPermissions();
        BtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(BtnSubmit);
                validations();
            }
        });

        FABAddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddGroupPopUp();
            }
        });


        ImgDummyAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.

                    easyPicker.setVisibility(View.VISIBLE);
                    easyPicker.performClick();
                    ImgDummyAdd.setVisibility(View.GONE);
                } else {  //No user has not granted the permissions yet. Request now.
                    requestPermissions();
                }


//        if (ContextCompat.checkSelfPermission(VendorAddProductActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            Log.i("Camera Permission", "DENIED");
//            ActivityCompat.requestPermissions(VendorAddProductActivity.this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
//            checkPermissionOnAppPermScreen("Camera");
//        } else if (ContextCompat.checkSelfPermission(VendorAddProductActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            Log.i("Storage Permission", "DENIED");
//            ActivityCompat.requestPermissions(VendorAddProductActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_STORAGE);
//            checkPermissionOnAppPermScreen("Storage");
//        } else {
//            easyPicker.setVisibility(View.VISIBLE);
//            easyPicker.performClick();
//            view.setVisibility(View.GONE);
//        }
            }
        });
//        easyPicker.performClick();
//        easyPicker.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (ContextCompat.checkSelfPermission(VendorAddProductActivity.this,Manifest.permission.CAMERA)
//                        != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions(VendorAddProductActivity.this, new String[]{Manifest.permission.CAMERA},MY_PERMISSIONS_REQUEST_CAMERA);
//                }
//            }
//        });

    } // onCreate

    private void AddGroupPopUp() {
        View preView = View.inflate(VendorAddProductActivity.this, R.layout.add_group_popup, null);
//        this.mDialog = new Dialog(BookingConfirmAct.this, R.style.NewDialog);
        this.gDialog = new Dialog(VendorAddProductActivity.this);
        this.gDialog.setContentView(preView);
        this.gDialog.setCancelable(false);
        if (!gDialog.isShowing()) {
            this.gDialog.show();
        }


        Window window = this.gDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
//        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent_background));
//        wlp.width = WindowManager.LayoutParams.FILL_PARENT;
//        wlp.height = WindowManager.LayoutParams.FILL_PARENT;
//        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.windowAnimations = R.anim.zoom_in_enter;
        window.setAttributes(wlp);

        ImageView ImgClose = (ImageView) this.gDialog.findViewById(R.id.imgClose);
        final EditText EtGroupName = (EditText) this.gDialog.findViewById(R.id.etGroupName);
        final EditText EtDesc = (EditText) this.gDialog.findViewById(R.id.etDescription);
        final Button BtnAdd = (Button) this.gDialog.findViewById(R.id.btnAdd);

        BtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(EtGroupName.getText().toString().isEmpty()){
                    EtGroupName.setError("Please Enter Group");
                }else {
                    AddGroupByVolley(EtGroupName.getText().toString(), EtDesc.getText().toString());
                }
            }
        });

        ImgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(gDialog.isShowing()){gDialog.dismiss();}
            }
        });

    }

    private void AddGroupByVolley(final String gName, final String desc) {

        AlertDialog.Builder builder = new AlertDialog.Builder(VendorAddProductActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Edit Product:", "success! response: " + response.toString());
                        try {

                            JSONObject obj = new JSONObject(new String(response));

                            String sts = obj.getString("status");
                            if(sts.equalsIgnoreCase("success")){
                                AppHelper.showSnackBar(VendorAddProductActivity.this,LLMain,obj.getString("message") );
//                                SimpleToast.ok(VendorAddProductActivity.this,obj.getString("message"));
                                if(gDialog.isShowing()) { gDialog.dismiss(); }
                                getGroupsData();
                            }else{
                                AppHelper.showSnackBar(VendorAddProductActivity.this,LLMain,"Not Updated.Please try again later." );

//                                SimpleToast.error(VendorAddProductActivity.this,"Not Updated.Please try again later.");
                            }
                        }catch (Exception e){e.printStackTrace();}
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.addgroup);
                params.put("vendorid", SessionSave.getsession(AppConstants.userid,VendorAddProductActivity.this));
                params.put("groupname", ""+gName);
                params.put("description", ""+desc);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }


    private void getProductDetails(final int pId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(VendorAddProductActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Edit Product:", "success! response: " + response.toString());
                        try {//{"status":"success","data":[{"id":"7","title":"Darmavaram silk","price":"250","discountprice":"300","image":"http:\/\/sampletemplates.net.in\/reseller\/assets\/image\/black_db_products\/19238085321570849839.jpg","category":"13","subcategory":"35","features":"Soft and Compfort","description":"Homemade sari","attributes":"X,L"}]}
                            if(response.contains("</div>")){
                                String[] sp = response.split("</div>");

                                JSONObject mainObj = new JSONObject(sp[1]);
                                setViews(mainObj);
                            }else{
                                JSONObject mainObj = new JSONObject(response);
                                setViews(mainObj);
                            }
                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.edit_product);
                params.put("vendorid", SessionSave.getsession(AppConstants.userid,VendorAddProductActivity.this));
                params.put("productid", ""+pId);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    private void setViews(JSONObject mainObj) {
        try {

            if(mainObj.getString("status").equalsIgnoreCase("success")){
                JSONArray mainArray = new JSONArray(mainObj.getString("data"));
                JSONObject obj = mainArray.getJSONObject(0);
                EtTitle.setText(obj.getString("title"));
                EtPrice.setText(obj.getString("price"));
                EtDiscPrice.setText(obj.getString("discountprice"));
                selectedCatId = Integer.parseInt(!obj.getString("category").isEmpty()? obj.getString("category") : "0");
                selectedSubCatId = Integer.parseInt(!obj.getString("subcategory").isEmpty()?obj.getString("subcategory"):"0");
                selectedMainSubCatId = Integer.parseInt(!obj.getString("mainsubcategory").isEmpty() ? obj.getString("mainsubcategory") : "0" );
                selectedGroupId = Integer.parseInt(!obj.getString("groupid").isEmpty()?obj.getString("groupid"):"0");
                EtShortDesc.setText(obj.getString("features"));
                EtHsncode.setText(obj.getString("hsncode"));
                EtGST.setText(obj.getString("gst"));
                EtWeight.setText(obj.getString("weight"));
                EtShippingCharge.setText(obj.getString("shippingcharge"));

                if(!obj.getString("image").isEmpty()) {
                    if(obj.getString("image").contains(",")){
                        List<String> lst = new ArrayList<String>(Arrays.asList(obj.getString("image").split(",")));
                        for(int i=0;i<lst.size();i++){
                            Log.i("Image:-->"+i,""+lst.get(i));
                        }

                        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                        RCUpdatedImgs.setLayoutManager(layoutManager);

                        adapter = new UpdatedImageAdapter(VendorAddProductActivity.this,lst,RCUpdatedImgs);
                        RCUpdatedImgs.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        RCUpdatedImgs.setVisibility(View.VISIBLE);

                    }else{//singleImage
                        List<String> lst =new ArrayList<String>();
                        lst.add(obj.getString("image"));
                        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                        RCUpdatedImgs.setLayoutManager(layoutManager);

                        adapter = new UpdatedImageAdapter(VendorAddProductActivity.this,lst,RCUpdatedImgs);
                        RCUpdatedImgs.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        RCUpdatedImgs.setVisibility(View.VISIBLE);
                    }
//                    Picasso.with(VendorAddProductActivity.this)
//                            .load(obj.getString("image"))
//                            .into(ImgProduct, new Callback() {
//                                @Override
//                                public void onSuccess() {
////                            progressbar.setVisibility(View.GONE);
//                                }
//
//                                @Override
//                                public void onError() {
//
//                                }
//                            });
                }


                EtDesc.setText(obj.getString("description"));
                selectedAttr = obj.getString("attributes");
//                String[] s = selectedAttr.split(",");
//
//                setAttributesToViews( stt );
//                for(int i=0;i<s.length;i++){
//
//                    final int childCount = LLCheckBoxes.getChildCount();
//                    for (int j = 0; j < childCount; j++) {
//                        View element = LLCheckBoxes.getChildAt(j);
//                        // CheckBox
//                        if (element instanceof CheckBox) {
//                            CheckBox checkBox = (CheckBox)element;
//                            if(!s[i].isEmpty()&& s[i].equalsIgnoreCase(checkBox.getText().toString())){
//                                checkBox.setChecked(true);
//                            }
//                        }
//                    }
//
////                    switch (s[i]){
////                        case "S":  RbSmall.setVisibility(View.VISIBLE);  RbSmall.setChecked(true);   break;
////                        case "M":  RbMedium.setVisibility(View.VISIBLE);  RbMedium.setChecked(true);  break;
////                        case "L":  RbL.setVisibility(View.VISIBLE);  RbL.setChecked(true);       break;
////                        case "X":  RbX.setVisibility(View.VISIBLE);  RbX.setChecked(true);       break;
////                        case "XL": RbXL.setVisibility(View.VISIBLE);  RbXL.setChecked(true);      break;
////                        case "XXL":RbXXL.setVisibility(View.VISIBLE);  RbXXL.setChecked(true);     break;
////                    }
//                }

                getCategoryAndSubCategory();
                getGroupsData();
//                getCategorys();
            }



        }catch (Exception e){e.printStackTrace();}
    }



    private void validations() {
        try{
            ValidationHelper helper=new ValidationHelper();
            String[] strIds = getResources().getStringArray(R.array.addVendorProduct_ids_array);
            String[] strErrMsgs = getResources().getStringArray(R.array.addVendorProduct_errors_array);
            String[] strCompTypeArr = getResources().getStringArray(R.array.addVendorProduct_comptypes_array);
            ArrayList<ValidationDTO> aList = new ArrayList<ValidationDTO>();

            int iPos = 0;
            for(String strCompType:strCompTypeArr){
                ValidationDTO valDTO=new ValidationDTO();
                valDTO.setComponentType(strCompType);
                valDTO.setComponentID(ValidationUtils.getIdResourceByName(VendorAddProductActivity.this,strIds[iPos]));
                valDTO.setErrorMessage(strErrMsgs[iPos]);
                aList.add(valDTO);
                iPos++;
            }
            boolean isValidData = helper.validateData(VendorAddProductActivity.this, aList,getWindow().getDecorView() );
            if (!isValidData) {
                return;
            }else{
                if(Integer.parseInt(EtDiscPrice.getText().toString()) < Integer.parseInt(EtPrice.getText().toString()) ){
                    AppHelper.setDelayToView(BtnSubmit);
                    if(pId == 0 ) {
                        if ( easyPicker.getImagesPath().size() >0) {

                            seleAttr = new StringBuilder("");


                            final int childCount = LLCheckBoxes.getChildCount();
                            for (int i = 0; i < childCount; i++) {
                                View element = LLCheckBoxes.getChildAt(i);
                                // CheckBox
                                if (element instanceof CheckBox) {
                                    CheckBox checkBox = (CheckBox)element;
                                    appendAttributes(checkBox);
                                }
                            }
                            if(seleAttr.toString().isEmpty()){
                                AppHelper.showSnackBar(VendorAddProductActivity.this,LLMain,"Please Select Size" );
//                                SimpleToast.error(VendorAddProductActivity.this,"Please Select Size");
                            }else {
                                uploadBitmap();
                            }

                        }else{
                            AppHelper.showSnackBar(VendorAddProductActivity.this,LLMain,"Image is not Uploaded" );
//                            SimpleToast.ok(VendorAddProductActivity.this,"Image is not Uploaded");
                        }
                    }
                    else{
                        List<Bitmap> updList = adapter.getUpdatedList();
                        if(  (updList.size() == 0) && (easyPicker.getImagesPath().size() == 0) ){
                            AppHelper.showSnackBar(VendorAddProductActivity.this,LLMain,"Image is not Uploaded" );
//                            SimpleToast.ok(VendorAddProductActivity.this,"Image is not Uploaded");
                        }else{

                            seleAttr = new StringBuilder("");


                            final int childCount = LLCheckBoxes.getChildCount();
                            for (int i = 0; i < childCount; i++) {
                                View element = LLCheckBoxes.getChildAt(i);
                                // CheckBox
                                if (element instanceof CheckBox) {
                                    CheckBox checkBox = (CheckBox)element;
                                    appendAttributes(checkBox);
                                }
                            }
                            if(seleAttr.toString().isEmpty()){
                                AppHelper.showSnackBar(VendorAddProductActivity.this,LLMain,"Please Select Size" );
//                                SimpleToast.error(VendorAddProductActivity.this,"Please Select Size");
                            }else {
                                updateUploadBitmap();
                            }

                        }
                    }
                }else{
                    AppHelper.showSnackBar(VendorAddProductActivity.this,LLMain,"Discount price should be lesser than price" );
//                    SimpleToast.error(VendorAddProductActivity.this,"Discount price should be lesser than price");
                }

            }
        }catch (Exception e){     e.printStackTrace();    }
    }//Validations



    private void uploadBitmap() {

        AlertDialog.Builder builder = new AlertDialog.Builder(VendorAddProductActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();




//        appendAttributes(RbSmall);
//        appendAttributes(RbMedium);
//        appendAttributes(RbL);
//        appendAttributes(RbX);
//        appendAttributes(RbXL);
//        appendAttributes(RbXXL);

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, ApiHelper.appDomain,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            Log.i("Response:-->",""+new String(response.data));
                            JSONObject obj = new JSONObject(new String(response.data));

                            String sts = obj.getString("status");
                            if(sts.equalsIgnoreCase("success")){
                                AppHelper.showSnackBar(VendorAddProductActivity.this,LLMain,obj.getString("message") );
//                                SimpleToast.ok(VendorAddProductActivity.this,obj.getString("message"));
                                finish();
                            }else{
                                AppHelper.showSnackBar(VendorAddProductActivity.this,LLMain,"Not Updated.Please try again later." );
//                                SimpleToast.error(VendorAddProductActivity.this,"Not Updated.Please try again later.");
                            }
//                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.i("Response:-->",""+error.getMessage());
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("action", ApiHelper.add_product);
                params.put("title", EtTitle.getText().toString());
                params.put("price", EtPrice.getText().toString());
                params.put("dprice", EtDiscPrice.getText().toString());
                params.put("category", String.valueOf(selectedCatId));
                params.put("subcategorymain", String.valueOf(selectedMainSubCatId));
                params.put("subcategory", String.valueOf(selectedSubCatId));
                params.put("attributes", ( seleAttr.substring(1,seleAttr.length())));
                params.put("features", EtShortDesc.getText().toString());
                params.put("description", EtDesc.getText().toString());
                params.put("userid", SessionSave.getsession(AppConstants.userid,VendorAddProductActivity.this)    );
                params.put("hsncode", EtHsncode.getText().toString()    );
                params.put("gst", EtGST.getText().toString()    );
                params.put("weight", EtWeight.getText().toString()    );
                params.put("shippingcharge", EtShippingCharge.getText().toString()    );
                params.put("groupid", String.valueOf(selectedGroupId));

                Log.i("PARAMS:",params.toString());


                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();

//BitmapFactory.decodeFile( easyPicker.getImagesPath().get(0))
                for(int i=0;i< easyPicker.getImagesPath().size();i++){
//                    params.put("userfile["+i+"]", new DataPart(imagename+""+i+1 + ".png", getFileDataFromDrawable(BitmapFactory.decodeFile( easyPicker.getImagesPath().get(i)))));
                    params.put("userfile["+i+"]", new DataPart(imagename+""+i+1 + ".png", getFileDataFromDrawable(getCompressedBitmap(( easyPicker.getImagesPath().get(i))) )));

                }
                Log.i("PARAMS:",params.toString());

                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }


    public  Bitmap getCompressedBitmap(String imagePath) {
        float maxHeight = 1920.0f;
        float maxWidth = 1080.0f;
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 85, out);

        byte[] byteArray = out.toByteArray();

        Bitmap updatedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        return updatedBitmap;
    }
    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    private void updateUploadBitmap() {

        AlertDialog.Builder builder = new AlertDialog.Builder(VendorAddProductActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();








//        appendAttributes(RbSmall);
//        appendAttributes(RbMedium);
//        appendAttributes(RbL);
//        appendAttributes(RbX);
//        appendAttributes(RbXL);
//        appendAttributes(RbXXL);

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, ApiHelper.appDomain,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            Log.i("Response:-->",""+new String(response.data));
                            JSONObject obj = new JSONObject(new String(response.data));

                            String sts = obj.getString("status");
                            if(sts.equalsIgnoreCase("success")){
                                AppHelper.showSnackBar(VendorAddProductActivity.this,LLMain,obj.getString("message") );
//                                SimpleToast.ok(VendorAddProductActivity.this,obj.getString("message"));
                                finish();
                            }else{
                                AppHelper.showSnackBar(VendorAddProductActivity.this,LLMain,"Not Updated.Please try again later." );
//                                SimpleToast.error(VendorAddProductActivity.this,"Not Updated.Please try again later.");
                            }
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.i("Response:-->",""+error.getMessage());
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("action", ApiHelper.update_product);
                params.put("title", EtTitle.getText().toString());
                params.put("price", EtPrice.getText().toString());
                params.put("dprice", EtDiscPrice.getText().toString());
                params.put("category", ""+selectedCatId);
                params.put("subcategory", ""+selectedSubCatId);
                params.put("subcategorymain", String.valueOf(selectedMainSubCatId));
                params.put("attributes", ( seleAttr.substring(0,seleAttr.length())));
                params.put("features", EtShortDesc.getText().toString());
                params.put("description", EtDesc.getText().toString());
                params.put("userid", SessionSave.getsession(AppConstants.userid,VendorAddProductActivity.this)    );
                params.put("hsncode", EtHsncode.getText().toString()    );
                params.put("gst", EtGST.getText().toString()    );
                params.put("weight", EtWeight.getText().toString()    );
                params.put("shippingcharge", EtShippingCharge.getText().toString()    );
                params.put("groupid", String.valueOf(selectedGroupId));
                params.put("id", ""+pId   );
                Log.i("PARAMS:",params.toString());

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);

                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();

                if(pId == 0) {//AddProduct Time
//BitmapFactory.decodeFile( easyPicker.getImagesPath().get(0))
                    for (int i = 0; i < easyPicker.getImagesPath().size(); i++) {
                        params.put("userfile[" + i + "]", new DataPart(imagename + "" + i + 1 + ".png", getFileDataFromDrawable(BitmapFactory.decodeFile(easyPicker.getImagesPath().get(i)))));
                    }
                }else if(adapter.getUpdatedList().size() == 0 ) {
                    for (int i = 0; i < easyPicker.getImagesPath().size(); i++) {
                        params.put("userfile[" + i + "]", new DataPart(imagename + "" + i + 1 + ".png", getFileDataFromDrawable(BitmapFactory.decodeFile(easyPicker.getImagesPath().get(i)))));
                    }
                }else{
                    List<Bitmap> updList = adapter.getUpdatedList();

                    for (int i = 0; i < updList.size(); i++) {
                        params.put("userfile[" + i + "]", new DataPart(imagename + "" + i + ".png", getFileDataFromDrawable(updList.get(i)) ) );
                    }
                    for (int i = 0; i < easyPicker.getImagesPath().size(); i++) {
                        params.put("userfile[" + (updList.size()+i) + "]", new DataPart(imagename + "" + i + 1 + ".png", getFileDataFromDrawable(BitmapFactory.decodeFile(easyPicker.getImagesPath().get(i)))));
                    }
                }
                Log.i("PARAMS:",params.toString());

                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }
    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case 0:
//                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
////                    getSupportLoaderManager().initLoader(0, null, this);
//                break;
//            default:
//                break;
//        }
        Log.i(TAG, "onRequestPermissionResult");

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If img_user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Permission granted, updates requested, starting location updates");
                ImgDummyAdd.setVisibility(View.GONE);
                easyPicker.setVisibility(View.VISIBLE);
                easyPicker.performClick();



            } else {
                // Permission denied.
                showSnackbar(R.string.permission_denied_explanation,
                        R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        mPhotoPickerStrip.onActivityResult(requestCode, resultCode, data);
        easyPicker.onActivityResult(requestCode, resultCode, data);
        filePathsList = easyPicker.getImagesPath();
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri sourceUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        return new CursorLoader(this, sourceUri, new String[]{MediaStore.MediaColumns.DATA}, null, null, MediaStore.Audio.Media.TITLE);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data.moveToFirst()) {
            ArrayList<String> images = new ArrayList<>();

            int i = 0;
            do {
                images.add(data.getString(0));
                i++;
            } while (data.moveToNext() && i < 6);

            easyPicker.restoreImages(images);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


    private void getCategoryAndSubCategory() {

        AlertDialog.Builder builder = new AlertDialog.Builder(VendorAddProductActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);
//
////                                   final List<CatSelectionModel> catList = new ArrayList<CatSelectionModel>();
//                                    final ArrayList<String> catNames = new ArrayList<String>(); // subNames.add("Select");
//                                    final ArrayList<String> catIds = new ArrayList<String>();  // subIds.add("");
//                                    final ArrayList<String> mainSubCatNames = new ArrayList<String>();//  catNames.add("Select");
//                                    final ArrayList<String> mainSubCatIds = new ArrayList<String>();  //  catIds.add("");
////                            final ArrayList<String> mainCatIds = new ArrayList<String>();  //  catIds.add("");
//                                    final List<MainSubCatModel> mainSubCat = new ArrayList<MainSubCatModel>(); // subCat.add("");
//                                    final List<SubCatModel> subCat = new ArrayList<SubCatModel>(); // subCat.add("");
//
//                                    final ArrayList<String> subCatNames = new ArrayList<String>();
//                                    final ArrayList<String> subCatIds = new ArrayList<String>();

                            final List<CategoryModel> catList = new ArrayList<CategoryModel>();
                            final List<MainSubCatModel> mainSubCatList = new ArrayList<MainSubCatModel>();
                            final List<SubCatModel> subCatList = new ArrayList<SubCatModel>();

                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                JSONArray mainArray = new JSONArray(mainObj.getString("categories"));
                                if(mainArray.length()>0){
                                    for(int i=0;i<mainArray.length();i++){
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        Log.i("pId:-->",""+pId);
                                        CategoryModel cModel = new CategoryModel();
                                        cModel.setCname( obj.getString("cname") );
                                        cModel.setId( obj.getString("id") );
                                        catList.add(cModel);

                                        if(obj.has("mainsubcategories")){
                                            JSONArray subCatArray = new JSONArray(obj.getString("mainsubcategories"));
                                            if(subCatArray.length()>0){

                                                for(int j=0;j<subCatArray.length();j++){

                                                    JSONObject sObj = subCatArray.getJSONObject(j);
                                                    Log.i("SubCat:==>"+j,""+sObj.getString("subcategories"));
                                                    MainSubCatModel mdl = new MainSubCatModel();
                                                    //        subNames.add(sObj.getString("subname")); subIds.add(sObj.getString("id"));
                                                    mdl.setCatName( obj.getString("cname") );
                                                    mdl.setCatId( obj.getString("id") );
                                                    mdl.setMainSubCatId( sObj.getString("id") );
                                                    mdl.setMainSubCatname( sObj.getString("mainsubname") );
                                                    JSONArray lsubCatArray = new JSONArray();
                                                    if(sObj.has("subcategories")){
                                                        lsubCatArray = new JSONArray( sObj.getString("subcategories") );
                                                        mdl.setMainsubname( lsubCatArray );
                                                    }

//
//                                                            mainSubCatIds.add( sObj.getString("id") );
//                                                            mainSubCatNames.add( sObj.getString("mainsubname"));
////                                                           mainCatIds.add(  obj.getString("id")   );
//
//
//                                                           mdl.setAttributes( sObj.getString("attributes") );
                                                    mainSubCatList.add(mdl);
                                                    if(lsubCatArray.length()>0){
                                                        for(int k=0;k<lsubCatArray.length();k++){
                                                            JSONObject kObj = lsubCatArray.getJSONObject(k);
                                                            SubCatModel smdl = new SubCatModel();
                                                            smdl.setId(kObj.getString("id"));
                                                            smdl.setSubname(kObj.getString("subname"));
                                                            smdl.setAttributes(kObj.getString("attributes"));
                                                            smdl.setCatId( obj.getString("id"));
                                                            smdl.setCatName( obj.getString("cname") );
                                                            smdl.setMainSubCatId( sObj.getString("id") );
                                                            smdl.setMainSubCatName( sObj.getString("mainsubname") );

//                                                                    subCatNames.add(kObj.getString("subname"));
//                                                                    subCatIds.add(kObj.getString("id"));

                                                            subCatList.add(smdl);

                                                        }
                                                    }else{
                                                        SpSubCategory.setAdapter(null);
                                                    }
                                                }


                                            }
                                        }



                                    }


                                    SpCategory.setAdapter(new ArrayAdapter<CategoryModel>(VendorAddProductActivity.this,android.R.layout.simple_spinner_dropdown_item,catList));
                                            if(pId != 0 ){
                                                for(int i=0;i<catList.size();i++){
                                                    if(selectedCatId == Integer.parseInt(catList.get(i).getId())){
                                                        SpCategory.setSelection(AppHelper.setValueToSpinner(SpCategory,catList.get(i).getCname()));
                                                    }

                                                }
                                            }

                                 //   SpMainSubCategory.setAdapter(new ArrayAdapter<MainSubCatModel>(VendorAddProductActivity.this,android.R.layout.simple_spinner_dropdown_item,mainSubCatList));


                                    SpCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                                            List<MainSubCatModel> msc = new ArrayList<MainSubCatModel>();
                                            String selection = parent.getSelectedItem().toString();
                                            for(int i=0;i<catList.size();i++){
                                                if(selection.equalsIgnoreCase(catList.get(i).getCname())){
                                                    selectedCatId = Integer.parseInt(catList.get(i).getId());
                                                            msc = setMainSubCategories(selectedCatId,mainSubCatList,SpMainSubCategory);
                                                }
                                            }
                                            SpMainSubCategory.setAdapter(new ArrayAdapter<MainSubCatModel>(VendorAddProductActivity.this,android.R.layout.simple_spinner_dropdown_item,msc));

                                            if(pId != 0 ){
                                                for(int i=0;i<msc.size();i++){
                                                    if(selectedMainSubCatId == Integer.parseInt(msc.get(i).getMainSubCatId())){
                                                        SpMainSubCategory.setSelection(AppHelper.setValueToSpinner(SpMainSubCategory,msc.get(i).getMainSubCatname()));
                                                    }

                                                }
                                            }


                                        }
                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {                }
                                    });



                                    SpMainSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            //       ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

                                            String selection = parent.getSelectedItem().toString();
                                            List<SubCatModel> msc = new ArrayList<SubCatModel>();
                                            for(int i=0;i<mainSubCatList.size();i++){
                                                if(selection.equalsIgnoreCase(mainSubCatList.get(i).getMainSubCatname())){
                                                    selectedMainSubCatId = Integer.parseInt(mainSubCatList.get(i).getMainSubCatId());
                                                            msc = setubCategories(selectedMainSubCatId,subCatList);
                                                }
                                            }
                                          SpSubCategory.setAdapter(new ArrayAdapter<SubCatModel>(VendorAddProductActivity.this,android.R.layout.simple_spinner_dropdown_item,msc));
                                            if(pId != 0 ){
                                                for(int i=0;i<msc.size();i++){
                                                    Log.i("selectedSubCatId:-->",""+selectedSubCatId);
                                                    if(selectedSubCatId == Integer.parseInt(msc.get(i).getId())){

                                                        SpSubCategory.setSelection(AppHelper.setValueToSpinner(SpSubCategory,msc.get(i).getSubname()));


//                                                        String[] s = selectedAttr.split(",");
//
//                                                        setAttributesToViews( selectedAttr );
//                                                        for(int i1=0;i1<s.length;i1++){
//
//                                                            final int childCount = LLCheckBoxes.getChildCount();
//                                                            for (int j = 0; j < childCount; j++) {
//                                                                View element = LLCheckBoxes.getChildAt(j);
//                                                                // CheckBox
//                                                                if (element instanceof CheckBox) {
//                                                                    CheckBox checkBox = (CheckBox)element;
//                                                                    if(!s[j].isEmpty()&& s[j].equalsIgnoreCase(checkBox.getText().toString())){
//                                                                        checkBox.setChecked(true);
//                                                                    }
//                                                                }
//                                                            }
//
//
//                                                        }


                                                    }




                                                }
                                            }




                                        }
                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {                }
                                    });




                                    SpSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                                                   ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

                                            String selection = parent.getSelectedItem().toString();
//        if(((LinearLayout) LLAttributes).getChildCount() > 0)
//            ((LinearLayout) LLAttributes).removeAllViews();
//
                                            for(int i=0;i<subCatList.size();i++){
                                                SubCatModel mdl = subCatList.get(i);
                                                if(selection.equalsIgnoreCase(mdl.getSubname())){
                                                    selectedSubCatId = Integer.parseInt(mdl.getId());

                                                    setAttributesToViews((mdl.getAttributes()));

                                                }
                                            }
                                        }
                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {                }
                                    });


                                }
                            }else{
                                Log.i("Category Api:->","Is Not working");
//                                SimpleToast.error(AddProductActivity.this,"Pease check");
                            }

                        }catch (Exception e){e.printStackTrace();}



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.allcategories);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);


    }

    private void getGroupsData() {

        AlertDialog.Builder builder = new AlertDialog.Builder(VendorAddProductActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            final ArrayList<String> gNames = new ArrayList<String>(); // subNames.add("Select");
                            final ArrayList<String> gIds = new ArrayList<String>();  // subIds.add("");

                            if(mainObj.getString("status").equalsIgnoreCase("success")) {
                                JSONArray mainArray = new JSONArray(mainObj.getString("list"));
                                for(int i=0;i<mainArray.length();i++){
                                    JSONObject obj = mainArray.getJSONObject(i);
                                    gNames.add( obj.getString("groupname") );
                                    gIds.add( obj.getString("id") );
                                }
                                SpGroup.setAdapter(new ArrayAdapter<String>(VendorAddProductActivity.this,android.R.layout.simple_spinner_dropdown_item,gNames));

                                for(int i=0;i<gNames.size();i++){
                                    if(selectedGroupId == Integer.parseInt(gIds.get(i))){
                                        SpGroup.setSelection(AppHelper.setValueToSpinner(SpGroup,gNames.get(i)));
                                    }

                                }
                            }


                            SpGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    //       ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

                                    String selection = parent.getSelectedItem().toString();
                                    for(int i=0;i<gNames.size();i++){
                                        if(selection.equalsIgnoreCase(gNames.get(i))){
                                            selectedGroupId = Integer.parseInt(gIds.get(i));
                                        }
                                    }
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {                }
                            });



                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",AppConstants.groupslist);
                params.put("vendorid",SessionSave.getsession(AppConstants.userid,VendorAddProductActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(AppConstants.volleyTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });

        mQueue.add(sr);



    }



    private void setAttributesToViews(String att) {




        LinearLayout layout = (LinearLayout) findViewById(R.id.llCheckBoxes);
//        LLAttributes.removeView(layout);
        layout.removeAllViews();
//        ViewGroup parent = (ViewGroup) LLAttributes.getParent();
//        if (parent != null) {
//            parent.removeView(LLAttributes);
//        }

        String[] arrOfStr = att.split("\"");

//            List<String>  attlist = new ArrayList<String>();
        Set<String> attlist = new HashSet<String>();
        for (String a : arrOfStr)
            attlist.add(a.trim());
        attlist.remove("[");
        attlist.remove("]");
        attlist.remove("[]");
        attlist.remove(",");


        List<String> aList = new ArrayList<String>(attlist);

        for(int i=0;i<aList.size();i++){
            CheckBox cb = new CheckBox(this);
            cb.setText(aList.get(i));
//            cb.setTextSize(27);
            cb.setTextColor(Color.rgb(150, 190, 200));
            cb.setTypeface(Typeface.MONOSPACE);

            if (Build.VERSION.SDK_INT < 21) {
                CompoundButtonCompat.setButtonTintList(cb, ColorStateList.valueOf(getResources().getColor(R.color.appColor)));//Use android.support.v4.widget.CompoundButtonCompat when necessary else
            } else {
                cb.setButtonTintList(ColorStateList.valueOf(getResources().getColor(R.color.appColor)));//setButtonTintList is accessible directly on API>19
            }

            cb.setTextAppearance(VendorAddProductActivity.this,R.style.ToggleButton);
//            cb.setBackground(getResources().getDrawable(R.drawable.toggle_background));
            LLCheckBoxes.addView(cb);


            cb.setOnCheckedChangeListener(this);

        }

        if(pId != 0 ) {


            String[] arrOfStr1 = selectedAttr.split(",");

            Set<String> attlist1 = new HashSet<String>();
            for (String a : arrOfStr1)
                attlist1.add(a.trim());
            attlist1.remove("[");
            attlist1.remove("]");
            attlist1.remove("[]");
            attlist1.remove(",");


            List<String> aList1 = new ArrayList<String>(attlist1);


            final int childCount = LLCheckBoxes.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View element = LLCheckBoxes.getChildAt(i);
                // CheckBox
                if (element instanceof CheckBox) {
                    CheckBox checkBox = (CheckBox)element;
                    for(int j=0;j<aList1.size();j++){
                        if(checkBox.getText().toString().equalsIgnoreCase(aList1.get(j))){
                            checkBox.setChecked(true);
                        }
                    }

                }
            }
        }

    }


    private void shoHideAttributes(String s) {
        switch (s){
            case "S" : RbSmall.setVisibility(View.VISIBLE);
            case "M" : RbMedium.setVisibility(View.VISIBLE);
            case "L" : RbL.setVisibility(View.VISIBLE);
            case "X" : RbX.setVisibility(View.VISIBLE);
            case "XL" : RbXL.setVisibility(View.VISIBLE);
            case "XXL" : RbXXL.setVisibility(View.VISIBLE);
        }
    }

    private List<MainSubCatModel> setMainSubCategories(int selectedCatId, List<MainSubCatModel> mainSubCat,Spinner sp) {
        List<MainSubCatModel> msinSubCat =  new ArrayList<MainSubCatModel>();
        for(int i=0;i<mainSubCat.size();i++){
            MainSubCatModel mdl = mainSubCat.get(i);
            if(mdl.getMainsubname().length() == 0){ sp.setAdapter(null); }
            if(selectedCatId == Integer.parseInt(mdl.getCatId())){
                msinSubCat.add(mdl);
            }
        }
        return msinSubCat;
    }
    private List<SubCatModel> setubCategories(int selectedMainSubCatId, List<SubCatModel> subCat) {
        List<SubCatModel> subCatNames =  new ArrayList<SubCatModel>();
        for(int i=0;i<subCat.size();i++){
            SubCatModel mdl = subCat.get(i);
            if(selectedMainSubCatId == Integer.parseInt(mdl.getMainSubCatId())){
                subCatNames.add(mdl);
            }
        }
        return subCatNames;
    }

    private void appendAttributes(CheckBox rb) {
        if(rb.isChecked()){
            seleAttr.append(","+rb.getText().toString().trim());
//            seleAttr =","+ seleAttr.concat(rb.getText().toString());
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(VendorAddProductActivity.this);
        builder.setMessage(R.string.areYouSureExit) .setTitle(R.string.logout);

        builder.setMessage("Are you sure want to go back ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Reeseller");
        alert.show();
    }


    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        int permissionState2 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;

    }
    private void requestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CAMERA);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE);


        // Provide an additional rationale to the img_user. This would happen if the img_user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale || shouldProvideRationale2) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(VendorAddProductActivity.this,
                                    new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(VendorAddProductActivity.this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }


    public void checkPermissionOnAppPermScreen(String perm) {
        try {
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, perm+" Permission are mandatory to access.", Snackbar.LENGTH_LONG)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    });

// Changing message text color
            snackbar.setActionTextColor(Color.RED);

// Changing action button text color
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        }catch (Exception e){e.printStackTrace();}
    }
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }

//
//    private void getCategorys() {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(VendorAddProductActivity.this, R.style.AppCompatAlertDialogStyle);
//        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
//        final AlertDialog alertDialog = builder.show();
//
//        String url = ApiHelper.appDomain;
//        Log.i("Api:-->",url);
//        StringRequest sr = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(final String response) {
//                        Log.e("HttpClient", "success! response: " + response.toString());
//                        if(alertDialog.isShowing()){alertDialog.dismiss();}
//                        try {
//                            Log.i("Category Data:-->",response.toString() );
//                            final ArrayList<String> scripts = new ArrayList<String>();
//                            final ArrayList<String> scriptIds = new ArrayList<String>();
//
//                            JSONObject obj=new JSONObject(response.toString() );
//                            JSONArray array=new JSONArray(obj.getString("categories"));
//                            for (int i = 0; i < array.length(); i++){
//                                JSONObject Obj = array.getJSONObject(i);
//                                scriptIds.add(Obj.getString("id"));
//                                scripts.add(Obj.getString("cname"));
//                            }
//                            SpCategory.setAdapter(new ArrayAdapter<String>(VendorAddProductActivity.this,android.R.layout.simple_spinner_dropdown_item,scripts));
//
//                            if(pId != 0 ){
//                                for(int i=0;i<scriptIds.size();i++){
//                                    if(selectedCatId == Integer.parseInt(scriptIds.get(i))){
//                                        SpCategory.setSelection(AppHelper.setValueToSpinner(SpCategory,scripts.get(i)));
//                                    }
//
//                                }
//                            }
//
//
//                            SpCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                                @Override
//                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
//
//                                    String selection = parent.getSelectedItem().toString();
//                                    if(selection.equalsIgnoreCase("Electronics")||selection.equalsIgnoreCase("Health & Beauty"))
//                                    {LLAttributes.setVisibility(View.GONE);
//                                    }else{LLAttributes.setVisibility(View.VISIBLE); }
//                                    for(int i=0;i<scripts.size();i++){
//                                        if(selection.equalsIgnoreCase(scripts.get(i))){
//                                            selectedCatId = Integer.parseInt(scriptIds.get(i));
//                                            getSubCategorys(selectedCatId);
//                                        }
//                                    }
//                                }
//                                @Override
//                                public void onNothingSelected(AdapterView<?> parent) {                }
//                            });
//
//
//                        }catch (Exception e){e.printStackTrace();}
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        if(alertDialog.isShowing()){alertDialog.dismiss();}
//                        Log.e("HttpClient", "error: " + error.toString());
//                    }
//                })
//        {
//            @Override
//            protected Map<String,String> getParams(){
//                Map<String,String> params = new HashMap<String, String>();
//                params.put("action",ApiHelper.getcategories_product);
//                params.put("phone",SessionSave.getsession(AppConstants.phone,VendorAddProductActivity.this));
//                params.put("password",SessionSave.getsession(AppConstants.password,VendorAddProductActivity.this));
//                Log.i("Ob:--> ",params.toString());
//                return params;
//            }
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String,String> params = new HashMap<String, String>();
//                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
//                return params;
//            }
//        };
//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });
//
//        mQueue.add(sr);
//
//
//    }
//    private void getSubCategorys(final int catId) {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(VendorAddProductActivity.this, R.style.AppCompatAlertDialogStyle);
//        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
//        final AlertDialog alertDialog = builder.show();
//
//        String url = ApiHelper.appDomain;
//        Log.i("Api:-->",url);
//        StringRequest sr = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(final String response) {
//                        Log.e("HttpClient", "success! response: " + response.toString());
//                        if(alertDialog.isShowing()){alertDialog.dismiss();}
//                        try {
//                            Log.i("subcategories Data:-->",response.toString() );
//                            final ArrayList<String> scripts = new ArrayList<String>();
//                            final ArrayList<String> scriptIds = new ArrayList<String>();
//
//                            JSONObject obj=new JSONObject(response.toString() );
//                            if(obj.getString("status").equalsIgnoreCase("success")){
//
//                                JSONArray array=new JSONArray(obj.getString("subcategories"));
//                                if(array.length()>0){
//
//                                    LLSubCat.setVisibility(View.VISIBLE);
//                                    for (int i = 0; i < array.length(); i++){
//                                        JSONObject Obj = array.getJSONObject(i);
//                                        scriptIds.add(Obj.getString("id"));
//                                        scripts.add(Obj.getString("cname"));
//                                    }
//                                    SpSubCategory.setAdapter(new ArrayAdapter<String>(VendorAddProductActivity.this,android.R.layout.simple_spinner_dropdown_item,scripts));
//
//                                    if(pId != 0 ){
//                                        for(int i=0;i<scriptIds.size();i++){
//                                            if(selectedSubCatId == Integer.parseInt(scriptIds.get(i))){
//                                                SpSubCategory.setSelection(AppHelper.setValueToSpinner(SpSubCategory,scripts.get(i)));
//                                                String[] s = selectedAttr.split(",");
//
//                                                setAttributesToViews( selectedAttr );
//                                                for(int i1=0;i1<s.length;i1++){
//
//                                                    final int childCount = LLCheckBoxes.getChildCount();
//                                                    for (int j = 0; j < childCount; j++) {
//                                                        View element = LLCheckBoxes.getChildAt(j);
//                                                        // CheckBox
//                                                        if (element instanceof CheckBox) {
//                                                            CheckBox checkBox = (CheckBox)element;
//                                                            if(!s[j].isEmpty()&& s[j].equalsIgnoreCase(checkBox.getText().toString())){
//                                                                checkBox.setChecked(true);
//                                                            }
//                                                        }
//                                                    }
//
//
//                                                }
//
//
//                                            }
//
//                                        }
//                                    }
//
//
//                                    SpSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                                        @Override
//                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                                            ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
//
//                                            String selection = parent.getSelectedItem().toString();
//                                            for(int i=0;i<scripts.size();i++){
//                                                if(selection.equalsIgnoreCase(scripts.get(i))){
//                                                    selectedSubCatId = Integer.parseInt(scriptIds.get(i));
//                                                }
//                                            }
//                                        }
//                                        @Override
//                                        public void onNothingSelected(AdapterView<?> parent) {                }
//                                    });
//                                }else{
//                                    LLSubCat.setVisibility(View.GONE);
//                                    selectedSubCatId = 0 ;
//                                    SpSubCategory.setAdapter(null);
//                                }
//
//                            }else{
//                                LLSubCat.setVisibility(View.GONE);
//                                selectedSubCatId = 0 ;
//                                SpSubCategory.setAdapter(null);
//                            }
//
//
//                        }catch (Exception e){e.printStackTrace();}
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        if(alertDialog.isShowing()){alertDialog.dismiss();}
//                        Log.e("HttpClient", "error: " + error.toString());
//                    }
//                })
//        {
//            @Override
//            protected Map<String,String> getParams(){
//                Map<String,String> params = new HashMap<String, String>();
//                params.put("action",ApiHelper.getsubcategories_category);
//                params.put("category", ""+catId );
//                Log.i("Ob:--> ",params.toString());
//                return params;
//            }
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String,String> params = new HashMap<String, String>();
//                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
//                return params;
//            }
//        };
//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });
//
//        mQueue.add(sr);
//
//
//    }


    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(VendorAddProductActivity.this);
    }



}


