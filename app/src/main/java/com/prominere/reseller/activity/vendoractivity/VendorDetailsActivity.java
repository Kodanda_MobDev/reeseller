package com.prominere.reseller.activity.vendoractivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.fragment.vendor.VendorDetailsFragment;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.helper.validations.ValidationDTO;
import com.prominere.reseller.helper.validations.ValidationHelper;
import com.prominere.reseller.helper.validations.ValidationUtils;
import com.prominere.reseller.util.CircleImageView;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.VolleyMultipartRequest;
import com.prominere.reseller.util.cropview.CropImage;
import com.prominere.reseller.util.cropview.CropImageView;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VendorDetailsActivity extends AppCompatActivity {

    private RelativeLayout RLImgs;
    private RequestQueue mQueue;
    private CircleImageView ImgProfilePic;
    private EditText EtName, EtEmail ,EtCompany,EtGSTPan,EtCity,EtState,EtCountry,EtBillingAddrs1,EtBillingAddrs2,EtPickupAddress1,EtPhoneNumb,EtPickupAddress2;
    private CheckBox ChBoxSameAddress;
    private String uId;
    private String vendorImgUrl;
    private ImageView ImgBack,ImgLogout;
    private static final int CAMPERMREQUEST = 112;
    private LinearLayout LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vendor_details_activity);

        mQueue = Volley.newRequestQueue(VendorDetailsActivity.this);

        AppHelper.setStatusBarColor(this,R.color.statusBarColor);


        LLMain = (LinearLayout)findViewById(R.id.llMain);
        RLImgs = (RelativeLayout)findViewById(R.id.rlImgs);

        ImgProfilePic = (CircleImageView)findViewById(R.id.imgProfilePic);
        ChBoxSameAddress = (CheckBox)findViewById(R.id.chBoxPickAddress);
        EtName = (EditText)findViewById(R.id.etname);
        EtEmail = (EditText)findViewById(R.id.etEmail);
        EtCompany = (EditText)findViewById(R.id.etCompany);
        EtGSTPan = (EditText)findViewById(R.id.etgst_pan);
        EtCity = (EditText)findViewById(R.id.etCity);
        EtState = (EditText)findViewById(R.id.etstate);
        EtCountry = (EditText)findViewById(R.id.etCountry);
        EtBillingAddrs1 = (EditText)findViewById(R.id.etBillingAddrs1);
        EtBillingAddrs2 = (EditText)findViewById(R.id.etBillingAddrs2);
        EtPickupAddress1 = (EditText)findViewById(R.id.etPickupAddress1);
        EtPickupAddress2 = (EditText)findViewById(R.id.etPickupAddress2);
        EtPhoneNumb = (EditText)findViewById(R.id.etPhone);
        ImgLogout = (ImageView)findViewById(R.id.imgLogout);
        ImgBack = (ImageView)findViewById(R.id.imgBack);
        final Button BtnUpdate = (Button)findViewById(R.id.update);
        BtnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(BtnUpdate);
                validations();
            }
        });
        ImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        RLImgs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= 23) {
                    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    if (!hasPermissions(VendorDetailsActivity.this, PERMISSIONS)) {
                        ActivityCompat.requestPermissions(VendorDetailsActivity.this, PERMISSIONS, CAMPERMREQUEST );
                    } else {
                        CropImage.activity()
                                .setGuidelines(CropImageView.Guidelines.ON)
                                .setActivityTitle("")
                                .setCropShape(CropImageView.CropShape.RECTANGLE)
                                .setCropMenuCropButtonTitle("Done")
                                .setRequestedSize(400, 400)
                                .setCropMenuCropButtonIcon(R.drawable.ic_done)
                                .start(VendorDetailsActivity.this);
                    }
                }else{
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setActivityTitle("")
                            .setCropShape(CropImageView.CropShape.RECTANGLE)
                            .setCropMenuCropButtonTitle("Done")
                            .setRequestedSize(400, 400)
                            .setCropMenuCropButtonIcon(R.drawable.ic_done)
                            .start(VendorDetailsActivity.this);
                }


            }
        });
        ChBoxSameAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isSel) {
                if(isSel){
                    EtPickupAddress1.setText(EtBillingAddrs1.getText().toString());
                    EtPickupAddress2.setText(EtBillingAddrs2.getText().toString());
                }else{
                    EtPickupAddress1.setText("");
                    EtPickupAddress2.setText("");
                }
            }
        });


        ImgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(ImgLogout);

                new AlertDialog.Builder(VendorDetailsActivity.this)
                        .setTitle(getResources().getString(R.string.app_name))
                        .setIcon(getResources().getDrawable(R.drawable.applogo))
                        .setMessage("Are you sure you want to Logout?")
                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                                dialog.cancel();
                                AppHelper.logOutClicked(VendorDetailsActivity.this);
                            }
                        })
                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        });

        getVendorDetails();

    }//onCreate

    private void validations() {
        try{
            ValidationHelper helper=new ValidationHelper();
            String[] strIds = getResources().getStringArray(R.array.resellerDetails_ids_array);
            String[] strErrMsgs = getResources().getStringArray(R.array.resellerDetails_errors_array);
            String[] strCompTypeArr = getResources().getStringArray(R.array.resellerDetails_comptypes_array);
            ArrayList<ValidationDTO> aList = new ArrayList<ValidationDTO>();

            int iPos = 0;
            for(String strCompType:strCompTypeArr){
                ValidationDTO valDTO=new ValidationDTO();
                valDTO.setComponentType(strCompType);
                valDTO.setComponentID(ValidationUtils.getIdResourceByName(VendorDetailsActivity.this,strIds[iPos]));
                valDTO.setErrorMessage(strErrMsgs[iPos]);
                aList.add(valDTO);
                iPos++;
            }
            boolean isValidData = helper.validateData(getBaseContext(), aList, getWindow().getDecorView() );
            if (!isValidData) {
                return;
            }else{


//                if ( (vendorImgUrl.isEmpty() && filePath.isEmpty() ) || ImgProfilePic.getDrawable() == null ) {
//                    Toast.makeText(getActivity(), "Image not selected!", Toast.LENGTH_LONG).show();
//                } else {
                uploadBitmap();
//                }

            }

        }catch (Exception e){     e.printStackTrace();    }

    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void uploadBitmap() {

        AlertDialog.Builder builder = new AlertDialog.Builder(VendorDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();


        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, ApiHelper.appDomain,
                new Response.Listener<NetworkResponse>() {


                    @Override
                    public void onResponse(NetworkResponse response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}


                        try {

                            Log.i("Response:-->",""+new String(response.data));
                            String  resp = new String(response.data);

                            if( resp.contains("</div>")){
                                String[] sp = resp.split("</div>");

                                JSONObject obj = new JSONObject(sp[1]);
                                String sts = obj.getString("status");
                                if(sts.equalsIgnoreCase("success")){
                                    AppHelper.showSnackBar(VendorDetailsActivity.this,LLMain,obj.getString("message") );
//                                    SimpleToast.ok(VendorDetailsActivity.this,obj.getString("message"));
                                getVendorDetails();
                                }else{
                                    AppHelper.showSnackBar(VendorDetailsActivity.this,LLMain,"Not Updated.Please try again later." );
//                                    SimpleToast.error(VendorDetailsActivity.this,"Not Updated.Please try again later.");
                                }
                            }else{
                                JSONObject obj = new JSONObject(resp);
                                String sts = obj.getString("status");
                                if(sts.equalsIgnoreCase("success")){
                                    AppHelper.showSnackBar(VendorDetailsActivity.this,LLMain,obj.getString("message") );
//                                    SimpleToast.ok( VendorDetailsActivity.this ,obj.getString("message"));
                                    getVendorDetails();
                                }else{
                                    AppHelper.showSnackBar(VendorDetailsActivity.this,LLMain,"Not Updated.Please try again later." );
//                                    SimpleToast.error( VendorDetailsActivity.this ,"Not Updated.Please try again later.");
                                }
                            }
                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.i("Response:-->",""+error.getMessage());
                        Toast.makeText( VendorDetailsActivity.this , error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("action", ApiHelper.updateprofile_vendor);
                params.put("userid", uId);
                params.put("name", EtName.getText().toString());
                params.put("email", EtEmail.getText().toString());
                params.put("company", EtCompany.getText().toString());
                params.put("gst_pan", EtGSTPan.getText().toString());
                params.put("city", EtCity.getText().toString());
                params.put("state", EtState.getText().toString());
                params.put("country", EtCountry.getText().toString());
                params.put("billing_address1", EtBillingAddrs1.getText().toString());
                params.put("billing_address2", EtBillingAddrs2.getText().toString());
                params.put("pickup_address1", EtPickupAddress1.getText().toString());
                params.put("pickup_address2", EtPickupAddress2.getText().toString());
                params.put("phone", EtPhoneNumb.getText().toString());
                Log.i("PARAMS:",params.toString());

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();


                if ( vendorImgUrl.isEmpty()||vendorImgUrl.equalsIgnoreCase("null")  ){

                }else{
                    if(ImgProfilePic.getDrawable() != null) {
                        BitmapDrawable drawable = (BitmapDrawable) ImgProfilePic.getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
//BitmapFactory.decodeFile( easyPicker.getImagesPath().get(0))
                        params.put("userfile", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));

                        Log.i("PARAMS:", params.toString());
                    }
                }


                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        //adding the request to volley
        Volley.newRequestQueue(VendorDetailsActivity.this).add(volleyMultipartRequest);
    }
    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    private void getVendorDetails() {

        AlertDialog.Builder builder = new AlertDialog.Builder( VendorDetailsActivity.this , R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")) {
                                JSONArray mainArray = new JSONArray(mainObj.getString("data"));
                                JSONObject obj = mainArray.getJSONObject(0);
                                uId = obj.getString("id");
                                EtName.setText(obj.getString("name"));
                                EtEmail.setText(obj.getString("email"));
                                EtCompany.setText(obj.getString("company"));
                                EtGSTPan.setText(obj.getString("gst"));
                                EtPhoneNumb.setText(obj.getString("phone"));
                                EtCity.setText(obj.getString("city"));
                                EtState.setText(obj.getString("state"));
                                EtCountry.setText(obj.getString("country"));
                                vendorImgUrl = obj.getString("image");

                                loadImage(obj.getString("image"));

//                                if (!obj.getString("image").isEmpty()){
//                                    Glide.with(getActivity()).load( obj.getString("image") ).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
//                                            .into( ImgProfilePic );
////                                    Picasso.with(getActivity()).load(obj.getString("image")).memoryPolicy(MemoryPolicy.NO_CACHE).into(ImgProfilePic);
//                                 }else{
//                                    ImgProfilePic.setImageDrawable(getResources().getDrawable(R.drawable.no_profile_photo));
//                                }
                                //           EtCountry.setText(obj.getString("name"));
//                                if(obj.getString("billing_add1").equalsIgnoreCase(obj.getString("pickup_add1"))){
//                                    EtBillingAddrs1.setText(obj.getString("billing_add1"));
//                                    EtBillingAddrs2.setText(obj.getString("billing_add2"));
//                                    EtPickupAddress1.setText(obj.getString("billing_add1"));
//                                    EtPickupAddress2.setText(obj.getString("billing_add2"));
//                                }else {
                                EtBillingAddrs1.setText(obj.getString("billing_add1"));
                                EtBillingAddrs2.setText(obj.getString("billing_add2"));
                                EtPickupAddress1.setText(obj.getString("pickup_add1"));
                                EtPickupAddress2.setText(obj.getString("pickup_add2"));
                                if( (obj.getString("billing_add1").equalsIgnoreCase(obj.getString("pickup_add1"))) && (obj.getString("billing_add2").equalsIgnoreCase(obj.getString("pickup_add2"))) ){
                                    ChBoxSameAddress.setChecked(true);
                                }else{
                                    ChBoxSameAddress.setChecked(false);
                                }
//                                }

                            }else{
                                Toast.makeText(VendorDetailsActivity.this,"Failed to get Vendor Details", Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action","getprofile_vendor");
                params.put("userid", SessionSave.getsession(AppConstants.userid,VendorDetailsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }//getVendorDetails

    private void loadImage(String image) {

        if (!image.isEmpty()){


            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder( VendorDetailsActivity.this , R.style.AppCompatAlertDialogStyle);
            builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
            final androidx.appcompat.app.AlertDialog alertDialog = builder.show();

            Picasso.with( VendorDetailsActivity.this )
                    .load(  image )
                    .into(ImgProfilePic, new Callback() {
                        @Override
                        public void onSuccess() {
                            if(alertDialog.isShowing()){ alertDialog.dismiss(); }
//                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            if(alertDialog.isShowing()){ alertDialog.dismiss(); }
                        }
                    });


//            Glide.with(getActivity()).load(image ).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
//                    .into( ImgProfilePic );

        }else{
            ImgProfilePic.setImageDrawable(getResources().getDrawable(R.drawable.no_profile_photo));
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMPERMREQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //do here
                    if (Build.VERSION.SDK_INT >= 23) {
                        String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        if (!hasPermissions(VendorDetailsActivity.this, PERMISSIONS)) {
                            ActivityCompat.requestPermissions(VendorDetailsActivity.this, PERMISSIONS, CAMPERMREQUEST );
                        } else {
                            CropImage.activity()
                                    .setGuidelines(CropImageView.Guidelines.ON)
                                    .setActivityTitle("")
                                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                                    .setCropMenuCropButtonTitle("Done")
                                    .setRequestedSize(400, 400)
                                    .setCropMenuCropButtonIcon(R.drawable.ic_done)
                                    .start(VendorDetailsActivity.this);

                        }
                    }else{
                        CropImage.activity()
                                .setGuidelines(CropImageView.Guidelines.ON)
                                .setActivityTitle("")
                                .setCropShape(CropImageView.CropShape.RECTANGLE)
                                .setCropMenuCropButtonTitle("Done")
                                .setRequestedSize(400, 400)
                                .setCropMenuCropButtonIcon(R.drawable.ic_done)
                                .start(VendorDetailsActivity.this);

                    }
                } else {
                    Toast.makeText(VendorDetailsActivity.this, "The app was not allowed to read your store.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                if (data != null) {
                    try {

                        Uri contentURI = result.getUri();

                        try {

                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                            // imageView.setImageBitmap(bitmap);
                            ByteArrayOutputStream bao = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);

                            ImgProfilePic.setImageBitmap(bitmap);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }

    }//onActivityResult


    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(VendorDetailsActivity.this);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }



}
