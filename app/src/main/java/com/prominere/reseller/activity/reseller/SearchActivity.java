package com.prominere.reseller.activity.reseller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.adapter.ProductAdapter;
import com.prominere.reseller.adapter.SearchAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.ProductModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.simpSearchView.SimpleOnQueryTextListener;
import com.prominere.reseller.util.simpSearchView.SimpleSearchView;
import com.prominere.reseller.util.simpSearchView.utils.DimensUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SearchActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private RequestQueue mQueue;
    private ListView LvProducts;
    private TextView TvEmptyBox;
//    private SimpleSearchView searchView;
    public static final int EXTRA_REVEAL_CENTER_PADDING = 40;
    private EditText EtSearch;
//    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);


        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
//        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_from_right);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
setTitle("Search");
        mQueue = Volley.newRequestQueue(SearchActivity.this);

        LvProducts = (ListView)findViewById(R.id.lvProducts);
        TvEmptyBox = (TextView)findViewById(R.id.imgEmptyBox);
//        searchView = findViewById(R.id.searchView);
        EtSearch = (EditText)findViewById(R.id.etSearch);

        EtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().length() == 0 ) {
//                    doGetRequest(dashBoardUrl);
                    TvEmptyBox.setVisibility(View.VISIBLE);
                    LvProducts.setVelocityScale(View.GONE);
                }else{
                    TvEmptyBox.setVisibility(View.GONE);
                    LvProducts.setVelocityScale(View.VISIBLE);
                    String text = editable.toString().toLowerCase(Locale.getDefault());
                    getProducts(text);
//                    adapter.filter(text);
//                    TvNoOFItems.setText("No Of Records: " + LV.getAdapter().getCount());
                }
            }
        });

//        searchView.setOnQueryTextListener(new SimpleOnQueryTextListener() {
//            @Override
//            public boolean onQueryTextChange(String s) {
//                if(s.length() == 0 ) {
////                    doGetRequest(dashBoardUrl);
//                    TvEmptyBox.setVisibility(View.VISIBLE);
//                    LvProducts.setVelocityScale(View.GONE);
//                }else{
//                    TvEmptyBox.setVisibility(View.GONE);
//                    LvProducts.setVelocityScale(View.VISIBLE);
//                    String text = s.toLowerCase(Locale.getDefault());
//                    getProducts(text);
////                    adapter.filter(text);
////                    TvNoOFItems.setText("No Of Records: " + LV.getAdapter().getCount());
//                }
//                return super.onQueryTextChange(s);
//            }
//        });

//        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_container);
//        swipeRefreshLayout.setOnRefreshListener(this);
//        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_green_dark,android.R.color.holo_orange_dark, android.R.color.holo_blue_dark);
//
//        swipeRefreshLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                swipeRefreshLayout.setRefreshing(true);
//                Log.i("Refresh :-->",""+EtSearch.getText().toString());
//                if(EtSearch.getText().toString().length()>0) {
//                    getProducts(EtSearch.getText().toString());
//                }
//            }
//        });
    }



    private void getProducts(final String search) {

        AlertDialog.Builder builder = new AlertDialog.Builder(SearchActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            ArrayList<ProductModel> productList = new ArrayList<ProductModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                TvEmptyBox.setVisibility(View.GONE);
                                LvProducts.setVisibility(View.VISIBLE);
                                JSONArray mainArray = new JSONArray(mainObj.getString("data"));
                                if(mainArray.length()>0) {
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        ProductModel catList = new ProductModel();
                                        catList.setId(obj.getString("id"));
                                        catList.setTitle(obj.getString("title"));
                                        catList.setPrice(obj.getString("price"));
                                        catList.setDiscountprice(obj.getString("discountprice"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setCategory(obj.getString("category"));
                                        catList.setWishliststatus(obj.getString("wishliststatus"));

                                        productList.add(catList);
                                    }

                                    SearchAdapter dbradapter = new SearchAdapter( SearchActivity.this, productList );
                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                 //   TvEmptyBox.setVisibility(View.VISIBLE);
                                }

                            }else{
                                TvEmptyBox.setVisibility(View.VISIBLE);
                                LvProducts.setAdapter(null);
                                LvProducts.setVisibility(View.GONE);

//                                SimpleToast.error(SearchActivity.this, AppConstants.apiIssueMsg);
                            }
//                            swipeRefreshLayout.setRefreshing(false);
                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.search);
                params.put("searchval",search);
                params.put("userid", SessionSave.getsession(AppConstants.userid,SearchActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.search_menu, menu);
//
//        setupSearchView(menu);
//        return true;
//    }

    private void setupSearchView(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
//        searchView.setMenuItem(item);
//        searchView.setTabLayout(tabLayout);

        // Adding padding to the animation because of the hidden menu item
//        Point revealCenter = searchView.getRevealAnimationCenter();
//        revealCenter.x -= DimensUtils.convertDpToPx(EXTRA_REVEAL_CENTER_PADDING, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (searchView.onActivityResult(requestCode, resultCode, data)) {
//            return;
//        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(SearchActivity.this);
    }

            @Override
    public void onBackPressed() {
//        if (searchView.onBackPressed()) {
//            return;
//        }
        super.onBackPressed();
        finish();
    }


    @Override
    public void onRefresh() {
        Log.i("Refresh :-->",""+EtSearch.getText().toString());
        if(EtSearch.getText().toString().length()>0) {
            getProducts(EtSearch.getText().toString());
        }

    }
}
