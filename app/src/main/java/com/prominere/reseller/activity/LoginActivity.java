package com.prominere.reseller.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.prominere.reseller.BuildConfig;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.MainNavigationActivity;
import com.prominere.reseller.activity.vendoractivity.VendorDashBoardActivity;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.helper.validations.ValidationDTO;
import com.prominere.reseller.helper.validations.ValidationHelper;
import com.prominere.reseller.helper.validations.ValidationUtils;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginAct: " ;
    private EditText EtEmail,EtPassword;
    private RequestQueue queue;
    private TextView TvPSWShow,TvForgotPSW;
    private Typeface fontAwesomeFont;
    private boolean hideSeek = false;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private LinearLayout LLMain;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        ActivityLoginBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
//        binding.setViewModel(new LoginViewModel());
//        binding.executePendingtvPswShowBindings();
        fontAwesomeFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");

         queue = Volley.newRequestQueue(this);

        LLMain= (LinearLayout)findViewById(R.id.llMain);
        TvForgotPSW= (TextView)findViewById(R.id.tvForgotPSW);
        EtEmail= (EditText)findViewById(R.id.etEmail);      //  EtEmail.setText("yoemrattana168@gmail.com");
        EtPassword= (EditText)findViewById(R.id.etPassword);//  EtPassword.setText("123456");
        TvPSWShow=(TextView)findViewById(R.id.tvPswShow);               TvPSWShow.setTypeface(fontAwesomeFont);  // AppHelper.CallBounceView(TvPSWShow);

        TvForgotPSW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(in);
                finish();
            }
        });
        TvPSWShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hideSeek) {// show password
                    TvPSWShow.setTextColor(getResources().getColor(R.color.black));
                    TvPSWShow.setText(R.string.fai_remove_eye);
                    EtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    hideSeek = false;

                } else { // hide password
                    TvPSWShow.setTextColor(getResources().getColor(R.color.black));
                    TvPSWShow.setText(R.string.fai_eye);
                    EtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    hideSeek=true;
                }
            }
        });


    }//onCreate

    public void onRegisterClicked(View view) {
//        Intent in = new Intent(LoginActivity.this,OTPActivity.class);
       Intent in = new Intent(LoginActivity.this, RegistrationActivity.class);
        startActivity(in);
        finish();
    }

    public void onLoggedIn(View view) {
        if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.
            validations();
        } else {  //No user has not granted the permissions yet. Request now.
            askRequestPermissions();
        }

    }
    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        int permissionState2 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;

    }
    private void askRequestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CAMERA);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE);


        // Provide an additional rationale to the img_user. This would happen if the img_user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale || shouldProvideRationale2) {
            ActivityCompat.requestPermissions(LoginActivity.this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(LoginActivity.this,
                                    new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(LoginActivity.this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }


    private void validations() {
        try{
            ValidationHelper helper=new ValidationHelper();
            String[] strIds = getResources().getStringArray(R.array.login_ids_array);
            String[] strErrMsgs = getResources().getStringArray(R.array.login_errors_array);
            String[] strCompTypeArr = getResources().getStringArray(R.array.login_comptypes_array);
            ArrayList<ValidationDTO> aList = new ArrayList<ValidationDTO>();

            int iPos = 0;
            for(String strCompType:strCompTypeArr){
                ValidationDTO valDTO=new ValidationDTO();
                valDTO.setComponentType(strCompType);
                valDTO.setComponentID(ValidationUtils.getIdResourceByName(LoginActivity.this,strIds[iPos]));
                valDTO.setErrorMessage(strErrMsgs[iPos]);
                aList.add(valDTO);
                iPos++;
            }
            boolean isValidData = helper.validateData(LoginActivity.this.getBaseContext(), aList,getWindow().getDecorView() );
            if (!isValidData) {
                return;
            }else{
                if((!EtEmail.getText().toString().isEmpty()) && (AppHelper.emailValidator(EtEmail.getText().toString() )) ){
                    LogInByVolley();
                }else{
                    AppHelper.showSnackBar(LoginActivity.this,LLMain,"Email is invalid");
                //    SimpleToast.error(LoginActivity.this,"Email is invalid");
                }
            }
        }catch (Exception e){     e.printStackTrace();    }
    }//Validations



//    @BindingAdapter({"toastMessage"})
//    public  static void runMe(View view,String messge){
//        if(messge!=null)
//
////            Toast.makeText(view.getContext(),messge,Toast.LENGTH_LONG).show();
//        if(messge.equalsIgnoreCase("Login was successful")){
////            Intent in = new Intent(view.getContext(),OTPActivity.class);
////            view.getContext().startActivity(in);
//            LogInByVolley();
//        }
//    }

    private void LogInByVolley() {

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                    if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);
//{"status":"success","data":{"userid":"39","type":"2","usertype":"Vendor"},"black_db_orders":[]}   Vendor
//Reselelr :   {"status":"success","userid":"41","type":"1","usertype":"Reseller","banner":[{"id":"1","banner":"5432345.png"}],"categories":[{"id":"12","cname":"Shirts","slug":"shirts","status":"1","image":"http:\/\/sampletemplates.net.in\/reseller\/assets\/image\/categories\/34534534.png"},{"id":"13","cname":"Clothing","slug":"clothing","status":"1","image":"http:\/\/sampletemplates.net.in\/reseller\/assets\/image\/categories\/45345345.png"},{"id":"14","cname":"SAREE","slug":"SILK","status":"1","image":""}],"subcategories":[{"id":"38","category":13,"cname":"WOMEN","slug":"SAREE","attr":"","status":"1","image":"","productcount":0}]}
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
//                                JSONArray mainArray = new JSONArray(mainObj.getString("data"));
//                                JSONObject obj = mainArray.getJSONObject(0);
                                if(mainObj.has("data")){
                                    JSONObject obj = new JSONObject(mainObj.getString("data"));
                                    SessionSave.saveSession(AppConstants.userid,obj.getString("userid"), LoginActivity.this);
                                    SessionSave.saveSession(AppConstants.type,obj.getString("type"), LoginActivity.this);
                                    SessionSave.saveSession(AppConstants.usertype,obj.getString("usertype"), LoginActivity.this);
                                    SessionSave.saveSession(AppConstants.password,EtPassword.getText().toString(), LoginActivity.this);

                                    if(obj.getString("usertype").equalsIgnoreCase("Vendor")){
                                        Intent in = new Intent(LoginActivity.this, VendorDashBoardActivity.class);
                                        startActivity(in);
                                        finish();
                                     }else{
                                        Intent in = new Intent(LoginActivity.this, MainNavigationActivity.class);
                                        startActivity(in);
                                        finish();
                                    }


                                }else{
                                    if(mainObj.has("usertype")) {
//                                    if(mainObj.getString("usertype").equalsIgnoreCase("Reseller")){
                                        SessionSave.saveSession(AppConstants.userid, mainObj.getString("userid"), LoginActivity.this);
                                        SessionSave.saveSession(AppConstants.type, mainObj.getString("type"), LoginActivity.this);
                                        SessionSave.saveSession(AppConstants.usertype, mainObj.getString("usertype"), LoginActivity.this);
                                        SessionSave.saveSession(AppConstants.password, EtPassword.getText().toString(), LoginActivity.this);

                                        if (mainObj.getString("usertype").equalsIgnoreCase("Vendor")) {
                                            Intent in = new Intent(LoginActivity.this, VendorDashBoardActivity.class);
                                            startActivity(in);
                                            finish();
                                        } else {
                                            Intent in = new Intent(LoginActivity.this, MainNavigationActivity.class);
                                            startActivity(in);
                                            finish();
                                        }

//                                    }

                                    }
                                }


                            }else{
                                Toast.makeText(LoginActivity.this,"Invalid credentials",Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(LoginActivity.this, AppConstants.noInternetConnection  );
                        }
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.login);
                params.put("email",EtEmail.getText().toString());
                params.put("password",EtPassword.getText().toString());
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        queue.add(sr);



    }//LogInByVolley

    @Override
    protected void onResume() {
        super.onResume();

     AppHelper.checkInternetAvailability(LoginActivity.this);



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case 0:
//                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
////                    getSupportLoaderManager().initLoader(0, null, this);
//                break;
//            default:
//                break;
//        }
        Log.i(TAG, "onRequestPermissionResult");

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If img_user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Permission granted, updates requested, starting location updates");

                validations();


            } else {
                // Permission denied.
                showSnackbar(R.string.permission_denied_explanation,
                        R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }



    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
       finishAffinity();
    }
}
