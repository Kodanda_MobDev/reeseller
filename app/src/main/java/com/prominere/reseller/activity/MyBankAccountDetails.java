package com.prominere.reseller.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.MainNavigationActivity;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.helper.validations.ValidationDTO;
import com.prominere.reseller.helper.validations.ValidationHelper;
import com.prominere.reseller.helper.validations.ValidationUtils;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.VolleyMultipartRequest;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MyBankAccountDetails extends AppCompatActivity {

    private RequestQueue mQueue;
    private EditText EtConfirmAccount,EtAccountNumber,EtBankName,EtACHolderName,EtIFSCCode;
    private Button BtnEdit;
    private ImageView ImgPassport;
    private LinearLayout LLAddPassport;
    private Dialog PPUploadDialog;
    private String filePath;
    private static final int REQUEST_TAKE_PHOTO = 2;
    private final int GALLERY = 1;
    private String bankImgUrl;
    private LinearLayout LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_bank_account_details_activity);


        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
 //       overridePendingTransition(R.anim.enter_from_right, R.anim.enter_from_right);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        setTitle(AppConstants.myBankDetails);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);

        mQueue = Volley.newRequestQueue(MyBankAccountDetails.this);


        LLMain = (LinearLayout)findViewById(R.id.llMain);
        LLAddPassport = (LinearLayout)findViewById(R.id.llAddPassport);
        EtAccountNumber = (EditText)findViewById(R.id.etAccNumber);
        EtConfirmAccount = (EditText)findViewById(R.id.et_ConfirmAccount);
        EtBankName = (EditText)findViewById(R.id.et_ACBankName);
        EtACHolderName = (EditText)findViewById(R.id.et_ACHolderName);
        EtIFSCCode = (EditText)findViewById(R.id.et_IFSCCode);
        ImgPassport = (ImageView)findViewById(R.id.imgPassport);
        BtnEdit = (Button)findViewById(R.id.btnEdit);
        BtnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validations();
            }
        });

        GetAccDetails();
        LLAddPassport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(MyBankAccountDetails.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    Log.i("Camera Permission","DENIED");
                    ActivityCompat.requestPermissions(MyBankAccountDetails.this, new String[]{Manifest.permission.CAMERA},  REQUEST_TAKE_PHOTO);
                    checkPermissionOnAppPermScreen("Camera");
                }else if(ContextCompat.checkSelfPermission(MyBankAccountDetails.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                    Log.i("Storage Permission","DENIED");
                    ActivityCompat.requestPermissions(MyBankAccountDetails.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY);
                    checkPermissionOnAppPermScreen("Storage");
                }
                else{
                    ShowUploadOptions();

                    Log.i("C & S Permission","GRANTED");

                }
            }
        });

    }//onCreate

    private void ShowUploadOptions() {
        try{
            View PopUpView = View.inflate(MyBankAccountDetails.this, R.layout.upload_options_view, null);
            PopUpView.startAnimation(AnimationUtils.loadAnimation(MyBankAccountDetails.this, R.anim.zoom_in_enter));
            this.PPUploadDialog = new Dialog( MyBankAccountDetails.this, R.style.NewDialog);
            this.PPUploadDialog.setContentView(PopUpView);
            this.PPUploadDialog.setCancelable(true);
            this.PPUploadDialog.show();

            Window window = this.PPUploadDialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER | Gravity.CENTER;
            window.setGravity(Gravity.CENTER);
            window.setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent_background));
            wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
            wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
            wlp.dimAmount = 0.0f;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            wlp.windowAnimations = R.anim.slide_move;

            window.setAttributes(wlp);
            window.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

            TextView TvCamera=(TextView)PopUpView.findViewById(R.id.tvCamera);
            TextView TvOther=(TextView)PopUpView.findViewById(R.id.tvOther);
            TextView TvHeading=(TextView)PopUpView.findViewById(R.id.tvPpUpHeading);        TvHeading.setText("Choose option");
            ImageView v = (ImageView) PopUpView.findViewById(R.id.closeDialog);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PPUploadDialog.dismiss();
                }
            });
            TvCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PPUploadDialog.dismiss();

                    if (Build.VERSION.SDK_INT >= 23) {
                        String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        if (!hasPermissions(MyBankAccountDetails.this, PERMISSIONS)) {
                            ActivityCompat.requestPermissions(MyBankAccountDetails.this, PERMISSIONS, REQUEST_TAKE_PHOTO );
                        } else {
                            captureImage();
                        }
                    }else{
                        captureImage();
                    }
                }
            });
            TvOther.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PPUploadDialog.dismiss();
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(galleryIntent, GALLERY);
                }
            });


        }catch (Exception e){e.printStackTrace();}
    }
    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (resultCode == RESULT_OK) {
        if (resultCode == -1) {


            if(requestCode == REQUEST_TAKE_PHOTO ) {

                    Bitmap bm = getPic(filePath);
                    ImgPassport.setImageBitmap(bm);

            }
            if( resultCode == Activity.RESULT_OK) {
                if (requestCode == GALLERY) {
                    if (data != null) {
                        Uri contentURI = data.getData();
                        try {

                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), contentURI);
                     //       bitmap = AppHelper.rotateImageIfRequired(bitmap, contentURI);
                            ImgPassport.setImageBitmap(bitmap);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }


        }


    }

    private Bitmap getPic(String fileName) {
        /* There isn't enough memory to open up more than a couple camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

        /* Get the size of the ImageView */
        int targetW = 278;
        int targetH = 200;

        int degree = getRotateDegreeFromExif(fileName);

        Matrix matrix = new Matrix();
        matrix.postRotate(degree);/*from   w  w w.  j  a v  a2 s  .co  m*/

        /* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        /* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

        /* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(fileName, bmOptions);
        if (bitmap == null)
            return null;
        Bitmap rotatedImage = Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        bitmap = null;
        return rotatedImage;
    }
    static private int getRotateDegreeFromExif(String filePath) {
        int degree = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(filePath);
            int orientation = exifInterface.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                degree = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                degree = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                degree = 270;
            }
            if (degree != 0) {
                exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION,
                        "0");
                exifInterface.saveAttributes();
            }
        } catch (IOException e) {
            degree = -1;
            e.printStackTrace();
        }

        return degree;
    }


    private void GetAccDetails() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MyBankAccountDetails.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("Address:", "success! response: " + response.toString());
                        try {
// {"status":"success","data":{"id":null,"bankname":null,"account_no":null,"account_holder":null,"ifsccode":null,"userid":null,"dateadded":null,"image":""}}
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                JSONObject obj = new JSONObject(mainObj.getString("data"));
                              //  String bankId = obj.getString("id");
                                bankImgUrl = obj.getString("image");
                                alertDialog.dismiss();

                                if(!bankImgUrl.isEmpty()){
                                    BtnEdit.setText("Edit");
                                    alertDialog.show();
                                    Picasso.with(MyBankAccountDetails.this)
                                            .load(bankImgUrl)
                                            .into(ImgPassport, new Callback() {
                                                @Override
                                                public void onSuccess() {
                                                    alertDialog.dismiss();
                                                }

                                                @Override
                                                public void onError() {
                                                    alertDialog.dismiss();
                                                }
                                            });

                                }else{
                                    BtnEdit.setText("Submit");
                                }

//                                Glide.with(MyBankAccountDetails.this).load(bankImgUrl).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
//                                        .into(ImgPassport);

//                                Picasso.with(MyBankAccountDetails.this).load(bankImgUrl).into(ImgPassport);
                               EtAccountNumber.setText(AppHelper.checkStringEmptyOrNot(obj.getString("account_no") ));
                               EtConfirmAccount.setText(AppHelper.checkStringEmptyOrNot(obj.getString("account_no") ));
                               EtACHolderName.setText(AppHelper.checkStringEmptyOrNot(obj.getString("account_holder")));
                               EtBankName.setText(AppHelper.checkStringEmptyOrNot(obj.getString("bankname")));
                               EtIFSCCode.setText(AppHelper.checkStringEmptyOrNot(obj.getString("ifsccode")));
                            }else{
                               Log.i("MYBankDetails:  ",":  Fails");
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(MyBankAccountDetails.this, AppConstants.noInternetConnection  );
                        }
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.view_bankdetails);
                params.put("userid", SessionSave.getsession(AppConstants.userid,MyBankAccountDetails.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    private void validations() {
        try{
            ValidationHelper helper=new ValidationHelper();
            String[] strIds = getResources().getStringArray(R.array.mybankdetails_ids_array);
            String[] strErrMsgs = getResources().getStringArray(R.array.mybankdetails_errors_array);
            String[] strCompTypeArr = getResources().getStringArray(R.array.mybankdetails_comptypes_array);
            ArrayList<ValidationDTO> aList = new ArrayList<ValidationDTO>();

            int iPos = 0;
            for(String strCompType:strCompTypeArr){
                ValidationDTO valDTO=new ValidationDTO();
                valDTO.setComponentType(strCompType);
                valDTO.setComponentID(ValidationUtils.getIdResourceByName(MyBankAccountDetails.this,strIds[iPos]));
                valDTO.setErrorMessage(strErrMsgs[iPos]);
                aList.add(valDTO);
                iPos++;
            }
            boolean isValidData = helper.validateData(MyBankAccountDetails.this.getBaseContext(), aList,getWindow().getDecorView() );
            if (!isValidData) {
                return;
            }else{
                if(EtAccountNumber.getText().toString().equalsIgnoreCase(EtConfirmAccount.getText().toString())){

                    if (  ImgPassport.getDrawable() == null ) {
                        Toast.makeText(MyBankAccountDetails.this, "Image not selected!", Toast.LENGTH_LONG).show();
                    } else {
                        updateBankDetails();
                    }
                }else{
                    AppHelper.showSnackBar(MyBankAccountDetails.this,LLMain,"Account Numbers are not Matching" );
//                    SimpleToast.error(MyBankAccountDetails.this,"Account Numbers are not Matching");
                }


            }//else
        }catch (Exception e){     e.printStackTrace();    }
    }

//    private void updateBankDetails() {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(MyBankAccountDetails.this, R.style.AppCompatAlertDialogStyle);
//        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
//        final AlertDialog alertDialog = builder.show();
//
//        String url = ApiHelper.appDomain;
//        Log.i("Cart Api:-->",url);
//        StringRequest sr = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        if(alertDialog.isShowing()){alertDialog.dismiss();}
//                        Log.e("Address:", "success! response: " + response.toString());
//                        try {
//
//                            JSONObject mainObj = new JSONObject(response);
//                            if(mainObj.getString("status").equalsIgnoreCase("success")){
//                                SimpleToast.ok(MyBankAccountDetails.this,mainObj.getString("message"));
//                                finish();
//                            }else{
//                                Log.i("MYBankDetails:  ",":  Fails");
//                            }
//
//                        }catch (Exception e){e.printStackTrace();}
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        if(alertDialog.isShowing()){alertDialog.dismiss();}
//                        Log.e("HttpClient", "error: " + error.toString());
//                    }
//                })
//        {
//            @Override
//            protected Map<String,String> getParams(){
//                Map<String,String> params = new HashMap<String, String>();
//                params.put("action",ApiHelper.update_bankdetails);
//                params.put("bankname",EtBankName.getText().toString());
//                params.put("account_no",EtAccountNumber.getText().toString());
//                params.put("account_holder",EtACHolderName.getText().toString());
//                params.put("ifsccode",EtIFSCCode.getText().toString());
//                params.put("userid", SessionSave.getsession(AppConstants.userid,MyBankAccountDetails.this));
//                Log.i("Ob:--> ",params.toString());
//                return params;
//            }
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String,String> params = new HashMap<String, String>();
//                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
//                return params;
//            }
//        };
//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });
//
//        mQueue.add(sr);
//
//    }
    private void updateBankDetails() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MyBankAccountDetails.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();


        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, ApiHelper.appDomain,
                new Response.Listener<NetworkResponse>() {


                    @Override
                    public void onResponse(NetworkResponse response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}


                        try {

                            Log.i("Response:-->",""+new String(response.data));
                            String  resp = new String(response.data);

                            if( resp.contains("</div>")){
                                String[] sp = resp.split("</div>");

                                JSONObject obj = new JSONObject(sp[1]);
                                String sts = obj.getString("status");
                                if(sts.equalsIgnoreCase("success")){
                                    AppHelper.showSnackBar(MyBankAccountDetails.this,LLMain,obj.getString("message") );
//                                    SimpleToast.ok(MyBankAccountDetails.this,obj.getString("message"));
//                                finish();

                                 GetAccDetails();

                                }else{
                                    AppHelper.showSnackBar(MyBankAccountDetails.this,LLMain,"Not Updated.Please try again later." );
//                                    SimpleToast.error(MyBankAccountDetails.this,"Not Updated.Please try again later.");
                                }
                            }else{
                                JSONObject obj = new JSONObject(resp);
                                String sts = obj.getString("status");
                                if(sts.equalsIgnoreCase("success")){
                                    AppHelper.showSnackBar(MyBankAccountDetails.this,LLMain,obj.getString("message") );
//                                    SimpleToast.ok(MyBankAccountDetails.this,obj.getString("message"));
//                                finish();
                                  GetAccDetails();
                                }else{
                                    AppHelper.showSnackBar(MyBankAccountDetails.this,LLMain,"Not Updated.Please try again later." );
//                                    SimpleToast.error(MyBankAccountDetails.this,"Not Updated.Please try again later.");
                                }
                            }
                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.i("Response:-->",""+error.getMessage());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(MyBankAccountDetails.this, AppConstants.noInternetConnection  );
                        }

                        //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.update_bankdetails);
                params.put("bankname",EtBankName.getText().toString());
                params.put("account_no",EtAccountNumber.getText().toString());
                params.put("account_holder",EtACHolderName.getText().toString());
                params.put("ifsccode",EtIFSCCode.getText().toString());
                params.put("userid", SessionSave.getsession(AppConstants.userid,MyBankAccountDetails.this));
                Log.i("Ob:--> ",params.toString());
                return params;

            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();

                if(ImgPassport.getDrawable() != null){
                    BitmapDrawable drawable = (BitmapDrawable) ImgPassport.getDrawable();
                    Bitmap bitmap = drawable.getBitmap();
//BitmapFactory.decodeFile( easyPicker.getImagesPath().get(0))
                    params.put("userfile", new DataPart(imagename+".png", getFileDataFromDrawable(  bitmap )));

                    Log.i("PARAMS:",params.toString());

                }


                return params;
            }
        };

        volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });
        //adding the request to volley
        Volley.newRequestQueue(MyBankAccountDetails.this).add(volleyMultipartRequest);
    }

    public void checkPermissionOnAppPermScreen(String perm) {
        try {
            View parentLayout = findViewById(android.R.id.content);
            Snackbar snackbar = Snackbar
                    .make(parentLayout, perm+" Permission are mandatory to access.", Snackbar.LENGTH_LONG)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    });

// Changing message text color
            snackbar.setActionTextColor(Color.RED);

// Changing action button text color
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        }catch (Exception e){e.printStackTrace();}
    }
    private void captureImage() {
        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory() + "/totalLSP";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");

        // Save a file: path for use with ACTION_VIEW intents
        filePath = image.getAbsolutePath();
        Log.i("Path ", "photo path = " + filePath);
        return image;
    }
    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(MyBankAccountDetails.this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
