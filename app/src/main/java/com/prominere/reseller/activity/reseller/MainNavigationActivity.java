package com.prominere.reseller.activity.reseller;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.dashboard.NoInternetConnActivity;
import com.prominere.reseller.activity.dashboard.account.MyOrdersActivity;
import com.prominere.reseller.fragment.reseller.AccountFragment;
import com.prominere.reseller.fragment.reseller.DBCategoryFragment;
import com.prominere.reseller.fragment.reseller.HowToUseFragment;
import com.prominere.reseller.fragment.reseller.ReesellerOrdersFragment;
import com.prominere.reseller.fragment.reseller.ResellerDetailsFragment;
import com.prominere.reseller.fragment.reseller.ResellerHomeFragment;
import com.prominere.reseller.fragment.vendor.VendorDetailsFragment;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.DBIconsView;
import com.prominere.reseller.util.CardDrawerLayout;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.badgecount.MenuItemBadge;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainNavigationActivity  extends AppCompatActivity {


    private CardDrawerLayout drawer;
    private ActionBarDrawerToggle mDrawerToggle;
    private TextView TvUserName,TvDashBoard,TvLogout,TvUserDetails,TvHome,TvChat,TvOrder,TvAcc,TvHTU,TvHomeBgColor,TvChatHeadBgColor,TvOrderHeadBgColor,TvHTUHeadBgColor,TvAccHeadBgColor;
    private String actionType;
    private RequestQueue mQueue;
    private LinearLayout LLMain,LLHome,LLChat,LLOrders,LLAccount,LLHTU;
    private LinearLayout LLResellerDB;
    private ImageView ImgHome,ImgOrder,ImgHTU,ImgAcc,ImgChat;
    private ArrayList<DBIconsView> LvDBIconsList;
    private MenuItem menuItemCart;
    private String isClicked = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_navigation_activity);



        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
//        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_from_right);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);

        setTitle(AppConstants.appName);
        mQueue = Volley.newRequestQueue(MainNavigationActivity.this);

        drawer = (CardDrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle= new ActionBarDrawerToggle( this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(mDrawerToggle);

        //the below line of code will allow you to hide the nav drawer icon
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerToggle.syncState();
        drawer.setViewScale(Gravity.START, 0.9f);
        drawer.setRadius(Gravity.START, 35);
        drawer.setViewElevation(Gravity.START, 20);


        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

//        if(AppHelper.isConnectingToInternet(MainNavigationActivity.this)){
        Log.i("Check Connection","Connected");
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new ResellerHomeFragment()).commit();
        }

        inIt();


    }//onCreate

    @Override
    protected void onResume() {
        super.onResume();



        ConnectivityManager ConnectionManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true )
        {
//            Toast.makeText(activity, "Network Available", Toast.LENGTH_LONG).show();
            if(!isClicked.equalsIgnoreCase("ACCOUNT")){
                updateBadgeCount();
            }
Log.i("isClicked:--->",""+isClicked);
            switch (isClicked){
                case "HOME" : setTitle(AppConstants.appName);
                    setBgToBootomViews(LLHome);
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new ResellerHomeFragment()).commit();
                    break;
                case "ORDERS" :  setTitle(AppConstants.orders);
                    setBgToBootomViews(LLOrders);
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new ReesellerOrdersFragment()).commit();
                    break;
                case "ACCOUNT" :  setTitle(AppConstants.account);
                    setBgToBootomViews(LLAccount);
                    //     getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new AccountFragment()).commit();
                    break;
                case "HTU" :  setTitle(AppConstants.howToUse);
                    setBgToBootomViews(LLHTU);
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new HowToUseFragment()).commit();
                    break;
            }
        }
        else
        {
            Intent in = new Intent(MainNavigationActivity.this, NoInternetConnActivity.class);
            startActivity(in);
            finish();
//            Toast.makeText(activity, "Network Not Available", Toast.LENGTH_LONG).show();

        }

    }//onResume

    private void inIt() {
        LLResellerDB = (LinearLayout)findViewById(R.id.llReseller);
        TvUserName = (TextView)findViewById(R.id.nav_header_textView);
        TvDashBoard = (TextView)findViewById(R.id.tvNVD1);
        TvUserDetails = (TextView)findViewById(R.id.tvNVD2);
        TvLogout = (TextView)findViewById(R.id.tvLogOut);
        LLHome = (LinearLayout)findViewById(R.id.llHome);
        LLMain = (LinearLayout)findViewById(R.id.llMain);
        LLChat = (LinearLayout)findViewById(R.id.llChat);
        LLOrders = (LinearLayout)findViewById(R.id.llOrders);
        LLAccount = (LinearLayout)findViewById(R.id.llAccount);
        LLHTU = (LinearLayout)findViewById(R.id.llhtu);

        LLHome = (LinearLayout)findViewById(R.id.llHome);      ImgHome = (ImageView)findViewById(R.id.imgHome);      TvHome = (TextView)findViewById(R.id.tvHome);  TvHomeBgColor = (TextView)findViewById(R.id.tvHomeHeadBgColor);
        LLChat = (LinearLayout)findViewById(R.id.llChat);      ImgChat = (ImageView)findViewById(R.id.imgChat);      TvChat = (TextView)findViewById(R.id.tvChat);   TvChatHeadBgColor = (TextView)findViewById(R.id.tvChatHeadBgColor);
        LLOrders = (LinearLayout)findViewById(R.id.llOrders);  ImgOrder = (ImageView)findViewById(R.id.imgOrders);   TvOrder = (TextView)findViewById(R.id.tvOrders); TvOrderHeadBgColor = (TextView)findViewById(R.id.tvOrderHeadBgColor);
        LLAccount = (LinearLayout)findViewById(R.id.llAccount);ImgAcc = (ImageView)findViewById(R.id.imgAcc);        TvAcc = (TextView)findViewById(R.id.tvAcc);     TvHTUHeadBgColor = (TextView)findViewById(R.id.tvHTUHeadBgColor);
        LLHTU = (LinearLayout)findViewById(R.id.llhtu);        ImgHTU = (ImageView)findViewById(R.id.imgHTU);        TvHTU = (TextView)findViewById(R.id.tvHTU);    TvAccHeadBgColor = (TextView)findViewById(R.id.tvAccHeadBgColor);

        LvDBIconsList = new ArrayList<DBIconsView>();

        LvDBIconsList.add(new DBIconsView(LLHome,ImgHome,TvHome,TvHomeBgColor));
        LvDBIconsList.add(new DBIconsView(LLChat,ImgChat,TvChat,TvChatHeadBgColor));
        LvDBIconsList.add(new DBIconsView(LLOrders,ImgOrder,TvOrder,TvOrderHeadBgColor));
        LvDBIconsList.add(new DBIconsView(LLAccount,ImgAcc,TvAcc,TvAccHeadBgColor));
        LvDBIconsList.add(new DBIconsView(LLHTU,ImgHTU,TvHTU,TvHTUHeadBgColor));



        getUserDetails();

        TvDashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBgToBootomViews(LLHome);
                getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new DBCategoryFragment()).commit();
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        TvUserDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SessionSave.getsession(AppConstants.usertype,MainNavigationActivity.this).equalsIgnoreCase("Vendor")){
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new VendorDetailsFragment()).commit();
                }else{
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new ResellerDetailsFragment()).commit();
                }
               drawer.closeDrawer(GravityCompat.START);
            }
        });
        TvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainNavigationActivity.this);
                builder.setMessage(R.string.areYouSureExit) .setTitle(R.string.logout);

                builder.setMessage("Are you sure want to close this application ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                AppHelper.logOutClicked(MainNavigationActivity.this);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle("Logout");
                alert.show();
            }
        });//onClick

        setBgToBootomViews(LLHome);

        LLHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLHome);
                isClicked = "HOME";
                setTitle(AppConstants.appName);
                setBgToBootomViews(LLHome);
                getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new ResellerHomeFragment()).commit();
            }
        });

        LLChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLChat);


                new AlertDialog.Builder(MainNavigationActivity.this)
                        .setTitle(getResources().getString(R.string.app_name))
                        .setMessage("We are redirecting to whatsapp admin account. If you have any queries, issues and suggestions please drop.")

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                                dialog.cancel();

                                //Put the package name here...
                                boolean installed = AppHelper.WhatsAppInstalledOrNot(MainNavigationActivity.this,"com.whatsapp");
                                if(installed) {
                                    setTitle(AppConstants.chat);
                                    setBgToBootomViews(LLChat);
//                                    openWhatsAppChat();
                                                String link = "https://wa.me/+917676227187";

                                                PackageManager packageManager = getPackageManager();
                                                Intent i = new Intent(Intent.ACTION_VIEW);

                                                try {
                                                    i.setPackage("com.whatsapp");
                                                    i.setData(Uri.parse(link));
                                                    if (i.resolveActivity(packageManager) != null) {
                                                        startActivity(i);
                                                    }
                                                } catch (Exception e){
                                                    e.printStackTrace();
                                                }


                                } else {
                                    AppHelper.showSnackBar(MainNavigationActivity.this,LLMain,"WhatsApp is not installed on your phone");

                                   // System.out.println("App is not currently installed on your phone");
                                }

                            }
                        })

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                                dialog.cancel();
                            }
                        })
                        .setIcon(getResources().getDrawable(R.drawable.applogo))
                        .show();




                //    getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new DBCategoryFragment()).commit();
            }
        });
        LLOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLOrders);
                isClicked = "ORDERS";
                setTitle(AppConstants.orders);
                setBgToBootomViews(LLOrders);
                     getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new ReesellerOrdersFragment()).commit();
            }
        });
        LLAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLAccount);
                isClicked = "ACCOUNT";
                setTitle(AppConstants.account);
                setBgToBootomViews(LLAccount);
                getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new AccountFragment()).commit();
//                if(SessionSave.getsession(AppConstants.usertype,MainNavigationActivity.this).equalsIgnoreCase("Vendor")){
//                    getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new VendorDetailsFragment()).commit();
//                }else{
//                    getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new ResellerDetailsFragment()).commit();
//                }
            }
        });
        LLHTU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLHTU);
                isClicked = "HTU";
                setTitle(AppConstants.howToUse);
                setBgToBootomViews(LLHTU);
                getSupportFragmentManager().beginTransaction().replace(R.id.main_navview, new HowToUseFragment()).commit();
            }
        });

    }//inIt

    private void setBgToBootomViews(LinearLayout ll) {
        for(int i =0;i<LvDBIconsList.size();i++){
            if(ll == LvDBIconsList.get(i).getLinearLayout()){
                DBIconsView model = LvDBIconsList.get(i);
                model.getTextView().setTextColor(getResources().getColor(R.color.appColor));
                model.getHeaadbgColor().setVisibility(View.VISIBLE);
                updateIcons(model);
//                model.getImageView().setBackgroundTintList(getResources().getColor(R.color.appColor));
            }else{
                DBIconsView model = LvDBIconsList.get(i);
                model.getTextView().setTextColor(getResources().getColor(R.color.black));
                model.getHeaadbgColor().setVisibility(View.GONE);
//                model.getImageView().setB(getResources().getColor(R.color.appColor));
            }
        }

    }

    private void getUserDetails() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainNavigationActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();


        if(SessionSave.getsession(AppConstants.usertype,MainNavigationActivity.this).equalsIgnoreCase("Vendor")){
            actionType = "getprofile_vendor";
        //    LLVendorsDB.setVisibility(View.VISIBLE); LLResellerDB.setVisibility(View.GONE);
        }else{
        //    LLVendorsDB.setVisibility(View.GONE); LLResellerDB.setVisibility(View.VISIBLE);
            actionType = "getprofile_reseller";
        }

        String url = ApiHelper.appDomain;
        Log.i("UserDetailsApi:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                JSONArray mainArray = new JSONArray(mainObj.getString("data"));
                                //{"status":"success","data":[{"id":"2","name":"Rady","city":"kakinada","state":"AP","address":"PP,Cambodia","email":"yoemrattana168@gmail.com","phone":"099","gst":"45345345345",
                                // "company":"GVK","billing_add1":"Main road ","billing_add2":"Main road , near temple street","pickup_add1":"Main road ","pickup_add2":"Main road "}]}

                                if(SessionSave.getsession(AppConstants.usertype,MainNavigationActivity.this).equalsIgnoreCase("Vendor")){
                                    JSONObject obj = mainArray.getJSONObject(0);
                                    SessionSave.saveSession(AppConstants.id, AppHelper.checkStringEmptyOrNot(obj.getString("id")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.name, AppHelper.checkStringEmptyOrNot(obj.getString("name")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.city, AppHelper.checkStringEmptyOrNot(obj.getString("city")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.state, AppHelper.checkStringEmptyOrNot(obj.getString("state")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.address, AppHelper.checkStringEmptyOrNot(obj.getString("address")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.email, AppHelper.checkStringEmptyOrNot(obj.getString("email")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.phone, AppHelper.checkStringEmptyOrNot(obj.getString("phone")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.gst, AppHelper.checkStringEmptyOrNot(obj.getString("gst")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.company, AppHelper.checkStringEmptyOrNot(obj.getString("company")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.billing_add1, AppHelper.checkStringEmptyOrNot(obj.getString("billing_add1")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.billing_add2, AppHelper.checkStringEmptyOrNot(obj.getString("billing_add2")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.pickup_add1, AppHelper.checkStringEmptyOrNot(obj.getString("pickup_add1")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.pickup_add2, AppHelper.checkStringEmptyOrNot(obj.getString("pickup_add2")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.image, AppHelper.checkStringEmptyOrNot(obj.getString("image")), MainNavigationActivity.this);

                                    TvUserName.setText(SessionSave.getsession(AppConstants.name,MainNavigationActivity.this));
                                }else {
                                    JSONObject obj = mainArray.getJSONObject(0);
                                    SessionSave.saveSession(AppConstants.id, AppHelper.checkStringEmptyOrNot(obj.getString("id")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.name, AppHelper.checkStringEmptyOrNot(obj.getString("name")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.email, AppHelper.checkStringEmptyOrNot(obj.getString("email")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.phone, AppHelper.checkStringEmptyOrNot(obj.getString("phone")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.city, AppHelper.checkStringEmptyOrNot(obj.getString("city")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.state, AppHelper.checkStringEmptyOrNot(obj.getString("state")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.address1, AppHelper.checkStringEmptyOrNot(obj.getString("address1")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.address2, AppHelper.checkStringEmptyOrNot(obj.getString("address2")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.pincode, AppHelper.checkStringEmptyOrNot(obj.getString("pincode")), MainNavigationActivity.this);
                                    SessionSave.saveSession(AppConstants.image, AppHelper.checkStringEmptyOrNot(obj.getString("image")), MainNavigationActivity.this);

                                    TvUserName.setText(SessionSave.getsession(AppConstants.name,MainNavigationActivity.this));

                                }

                            }else{
                                //    Toast.makeText(LoginActivity.this,"Invalid credentials",Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(MainNavigationActivity.this, AppConstants.noInternetConnection  );
                        }
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",actionType);
                params.put("userid",SessionSave.getsession(AppConstants.userid,MainNavigationActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(AppConstants.volleyTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });

        mQueue.add(sr);



    }//LogInByVolley
    private void updateBadgeCount() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainNavigationActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                int notifBadgeNumber = Integer.parseInt(mainObj.getString("cartcount"));
                                if(notifBadgeNumber>0){
                                    MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(notifBadgeNumber);
                                }else{
                                    MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(0);
                                }
                            }else{
                                MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(0);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(MainNavigationActivity.this, AppConstants.noInternetConnection  );
                        }
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",AppConstants.cartcount);
                params.put("userid",SessionSave.getsession(AppConstants.userid,MainNavigationActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(AppConstants.volleyTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });

        mQueue.add(sr);



    }
    private void openWhatsAppChat() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainNavigationActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                String link = (mainObj.getString("whatsapplink"));

                                PackageManager packageManager = getPackageManager();
                                Intent i = new Intent(Intent.ACTION_VIEW);

                                try {
//                                    String url = "https://api.whatsapp.com/send?phone="+ phone +"&text=" + URLEncoder.encode(message, "UTF-8");
                                    i.setPackage("com.whatsapp");
                                    i.setData(Uri.parse(link));
                                    if (i.resolveActivity(packageManager) != null) {
                                        startActivity(i);
                                    }
                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                            }else{
                                //    Toast.makeText(LoginActivity.this,"Invalid credentials",Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(MainNavigationActivity.this, AppConstants.noInternetConnection  );
                        }
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",AppConstants.adminwhatsapp);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(AppConstants.volleyTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }//LogInByVolley


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu_main, menu);


        menuItemCart = menu.findItem(R.id.miok);
        MenuItemBadge.update(this, menuItemCart, new MenuItemBadge.Builder()
                .iconDrawable(ContextCompat.getDrawable(this, R.drawable.ic_cart))
                .iconTintColor(Color.WHITE)
                .textBackgroundColor(Color.parseColor("#36B100"))
                .textColor(Color.WHITE));

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (mDrawerToggle.onOptionsItemSelected(item)) {
//            return true;
//        }

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
//            case R.id.miSearch:
//                Intent sin = new Intent(MainNavigationActivity.this,SearchActivity.class);
//                startActivity(sin);
//                break;
            case R.id.miCompose:
                Intent in = new Intent(MainNavigationActivity.this, WishListItemsActivity.class);
                startActivity(in);
                break;
            case R.id.miWallet:
                Intent in_w = new Intent(MainNavigationActivity.this, ResellerWalletActivity.class);
                in_w.putExtra(AppConstants.wallet_transactions,ApiHelper.transactions_reseller);
                startActivity(in_w);
                break;
            case R.id.miok:
                if(new  CartItemsActivity().isRunning == false) {
                    Intent min = new Intent(MainNavigationActivity.this, CartItemsActivity.class);
                    startActivity(min);
                }
                break;
        }
        return true;
    }
    private void updateIcons(DBIconsView model) {

        switch (model.getTextView().getText().toString()){
            case "Home":
                ImgHome.setImageDrawable(getResources().getDrawable(R.drawable.appcolor_home));
                ImgChat.setImageDrawable(getResources().getDrawable(R.drawable.black_chat));
                ImgOrder.setImageDrawable(getResources().getDrawable(R.drawable.black_db_orders));
                ImgAcc.setImageDrawable(getResources().getDrawable(R.drawable.black_db_account));
                ImgHTU.setImageDrawable(getResources().getDrawable(R.drawable.black_howtouse));
                break;
            case "Chat":
               ImgHome.setImageDrawable(getResources().getDrawable(R.drawable.black_home));
                ImgChat.setImageDrawable(getResources().getDrawable(R.drawable.appcolor_chat));
                ImgOrder.setImageDrawable(getResources().getDrawable(R.drawable.black_db_orders));
                ImgAcc.setImageDrawable(getResources().getDrawable(R.drawable.black_db_account));
                ImgHTU.setImageDrawable(getResources().getDrawable(R.drawable.black_howtouse));
                break;
            case "Orders":
               ImgHome.setImageDrawable(getResources().getDrawable(R.drawable.black_home));
                ImgChat.setImageDrawable(getResources().getDrawable(R.drawable.black_chat));
                ImgOrder.setImageDrawable(getResources().getDrawable(R.drawable.appcolor_db_orders));
                ImgAcc.setImageDrawable(getResources().getDrawable(R.drawable.black_db_account));
                ImgHTU.setImageDrawable(getResources().getDrawable(R.drawable.black_howtouse));
                break;
            case "Account":
               ImgHome.setImageDrawable(getResources().getDrawable(R.drawable.black_home));
                ImgChat.setImageDrawable(getResources().getDrawable(R.drawable.black_chat));
                ImgOrder.setImageDrawable(getResources().getDrawable(R.drawable.black_db_orders));
                ImgAcc.setImageDrawable(getResources().getDrawable(R.drawable.appcolor_db_account));
                ImgHTU.setImageDrawable(getResources().getDrawable(R.drawable.black_howtouse));
                break;
            case "How to use":
                ImgHome.setImageDrawable(getResources().getDrawable(R.drawable.black_home));
                ImgChat.setImageDrawable(getResources().getDrawable(R.drawable.black_chat));
                ImgOrder.setImageDrawable(getResources().getDrawable(R.drawable.black_db_orders));
                ImgAcc.setImageDrawable(getResources().getDrawable(R.drawable.black_db_account));
                ImgHTU.setImageDrawable(getResources().getDrawable(R.drawable.appcolor_howtouse));
                break;
        }

    }


    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainNavigationActivity.this);
        builder.setMessage(R.string.areYouSureExit) .setTitle(R.string.logout);

        builder.setMessage("Are you sure want to close this application ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                      finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        alert.show();
    }
}
