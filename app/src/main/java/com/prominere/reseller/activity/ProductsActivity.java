package com.prominere.reseller.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.CartItemsActivity;
import com.prominere.reseller.activity.reseller.ResellerWalletActivity;
import com.prominere.reseller.activity.reseller.SearchActivity;
import com.prominere.reseller.activity.reseller.WishListItemsActivity;
import com.prominere.reseller.adapter.ProductAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.ProductModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.badgecount.MenuItemBadge;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductsActivity extends AppCompatActivity {

    private String catId,subCatId;
    private RequestQueue mQueue;
    private ArrayList<ProductModel> productList;
    private GridView LvProducts;
    private TextView TvEmptyBox;
    private MenuItem menuItemCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.items_activity);


        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
    //    overridePendingTransition(R.anim.enter_from_right, R.anim.enter_from_right);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
        setTitle("");

        mQueue = Volley.newRequestQueue(ProductsActivity.this);

        catId = getIntent().getStringExtra(AppConstants.category);
        subCatId = getIntent().getStringExtra(AppConstants.subCategory);

        LvProducts = (GridView)findViewById(R.id.lvProducts);
        TvEmptyBox = (TextView)findViewById(R.id.imgEmptyBox);

        getProducts();
    }

    private void getProducts() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ProductsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            productList = new ArrayList<ProductModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                TvEmptyBox.setVisibility(View.GONE);
                                JSONArray mainArray = new JSONArray(mainObj.getString("products"));
                                if(mainArray.length()>0) {
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        ProductModel catList = new ProductModel();
                                        catList.setId(obj.getString("id"));
                                        catList.setTitle(obj.getString("title"));
                                        catList.setPrice(obj.getString("price"));
                                        catList.setDiscountprice(obj.getString("discountprice"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setCategory(obj.getString("category"));
                                        catList.setWishliststatus(obj.getString("wishliststatus"));

                                        productList.add(catList);
                                    }

                                    ProductAdapter dbradapter = new ProductAdapter(ProductsActivity.this, productList,"");
                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                    TvEmptyBox.setVisibility(View.VISIBLE);
                                }

                            }else{
                                TvEmptyBox.setVisibility(View.VISIBLE);
//                                SimpleToast.error(ProductsActivity.this,AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.get_products);
                params.put("category",catId);
                params.put("subcategory","");
                params.put("mainsubcatgeory",subCatId);
                params.put("userid", SessionSave.getsession(AppConstants.userid,ProductsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(AppConstants.volleyTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });

        mQueue.add(sr);



    }

    private void updateBadgeCount() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ProductsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);


                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                int notifBadgeNumber = Integer.parseInt(mainObj.getString("cartcount"));
                                if(notifBadgeNumber>0){
                                    MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(notifBadgeNumber);
                                }else{
                                    MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(0);
                                }
                            }else{
                                MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(0);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",AppConstants.cartcount);
                params.put("userid",SessionSave.getsession(AppConstants.userid,ProductsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(AppConstants.volleyTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });

        mQueue.add(sr);



    }//LogInByVolley


    @Override
    protected void onResume() {
        super.onResume();
        updateBadgeCount();
        AppHelper.checkInternetAvailability(ProductsActivity.this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);


        menuItemCart = menu.findItem(R.id.miok);
        MenuItemBadge.update(this, menuItemCart, new MenuItemBadge.Builder()
                .iconDrawable(ContextCompat.getDrawable(this, R.drawable.ic_cart))
                .iconTintColor(Color.WHITE)
                .textBackgroundColor(Color.parseColor("#36B100"))
                .textColor(Color.WHITE));
        
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.miSearch:
                Intent sin = new Intent(ProductsActivity.this, SearchActivity.class);
                startActivity(sin);
                finish();
                break;
            case R.id.miCompose:
                Intent in = new Intent(ProductsActivity.this, WishListItemsActivity.class);
                startActivity(in);
                finish();
                break;
            case R.id.miWallet:
                Intent win = new Intent(ProductsActivity.this, ResellerWalletActivity.class);
                win.putExtra(AppConstants.wallet_transactions,ApiHelper.transactions_reseller);
                startActivity(win);
                finish();
                break;
            case R.id.miok:
                if(new  CartItemsActivity().isRunning == false) {
                    Intent min = new Intent(ProductsActivity.this, CartItemsActivity.class);
                    startActivity(min);
                    finish();
                }
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }




}
