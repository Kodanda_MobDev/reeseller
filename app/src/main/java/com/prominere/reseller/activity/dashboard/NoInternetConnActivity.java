package com.prominere.reseller.activity.dashboard;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.prominere.reseller.R;
import com.prominere.reseller.activity.LoginActivity;
import com.prominere.reseller.activity.reseller.MainNavigationActivity;
import com.prominere.reseller.activity.vendoractivity.VendorDashBoardActivity;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.util.SessionSave;

public class NoInternetConnActivity extends AppCompatActivity {

    private Button BtnRetry;
    private LinearLayout LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.no_internet_conn_activity);

        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
        BtnRetry =  (Button)findViewById(R.id.btnRetry);
        LLMain =  (LinearLayout)findViewById(R.id.llMain);

        BtnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager ConnectionManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
                if(networkInfo != null && networkInfo.isConnected()==true )
                {
                      NetworkAvailReditect();
                }
                else
                {
                    AppHelper.showSnackBar(NoInternetConnActivity.this,LLMain,"Oops... Looks like you're not connected to the internet.");
                }
            }
        });

    }//onCreate

    private void NetworkAvailReditect() {
        if(SessionSave.getsession(AppConstants.userid,NoInternetConnActivity.this).isEmpty()){
            Intent in = new Intent(NoInternetConnActivity.this, LoginActivity.class);
            startActivity(in);
            finish();
        }else{
            if(SessionSave.getsession(AppConstants.usertype, NoInternetConnActivity.this).equalsIgnoreCase("Vendor")){
                Intent in = new Intent(NoInternetConnActivity.this, VendorDashBoardActivity.class);
                startActivity(in);
                finish();
            }else{
                Intent in = new Intent(NoInternetConnActivity.this, MainNavigationActivity.class);
                startActivity(in);
                finish();
            }


        }

    }
}
