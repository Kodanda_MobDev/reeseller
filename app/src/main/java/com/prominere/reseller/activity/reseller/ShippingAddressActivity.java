package com.prominere.reseller.activity.reseller;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gocashfree.cashfreesdk.CFPaymentService;
import com.prominere.reseller.R;
import com.prominere.reseller.adapter.ShippingAddressAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.helper.validations.ValidationDTO;
import com.prominere.reseller.helper.validations.ValidationHelper;
import com.prominere.reseller.helper.validations.ValidationUtils;
import com.prominere.reseller.model.AddressModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_APP_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_NAME;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_NOTE;

public class ShippingAddressActivity extends AppCompatActivity {

    private RequestQueue mQueue;
    public static ListView LvProducts;
    private TextView TvEmptyBox,AddNewAddress,ProceedBtn;
    private Dialog mDialog;
    EditText EtPName,EtPEmail,EtPState;
    private String paymentMethod;
    int preSelectedIndex = -1;
    private ArrayList<AddressModel> productList;
    private String TAG = "ShippingAddrsAct:-->";
    private String temporderid="";
    private LinearLayout LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shipping_address_activity);


        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
//        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_from_right);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        setTitle("Shipping Address");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);

        mQueue = Volley.newRequestQueue(ShippingAddressActivity.this);

        paymentMethod = getIntent().getStringExtra("paymentMethod");





        LLMain = (LinearLayout)findViewById(R.id.llMain);
        LvProducts = (ListView)findViewById(R.id.lvProducts);
        TvEmptyBox = (TextView)findViewById(R.id.imgEmptyBox);
        ProceedBtn = (TextView)findViewById(R.id.proceedBtn);
        AddNewAddress = (TextView)findViewById(R.id.addNewAddress);




        AddNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(ShippingAddressActivity.this,AddAddressActivity.class);
                startActivity(in);
            }
        });
        ProceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("LIST: --> ",productList.toString());
                Boolean sel = true;
               for(int i=0;i<productList.size();i++){
                   AddressModel mdl = productList.get(i);
                   if(mdl.getRbselection() == true){
//                       SimpleToast.ok(ShippingAddressActivity.this," ID "+mdl.getId()+" Name :"+mdl.getCustomername());
                       showProceedPopUp(mdl.getId());
                  }else{
                       if(sel){
                           sel = false;
                           AppHelper.showSnackBar(ShippingAddressActivity.this,LLMain,"Please select Address" );
//                           SimpleToast.error(ShippingAddressActivity.this,"Please select Address");
                       }

                   }
               }

//
            }
        });



    }//onCreate

    private void showProceedPopUp(final String aid) {


        final View localView = View.inflate(ShippingAddressActivity.this, R.layout.proceed_popup, null);
        localView.startAnimation(AnimationUtils.loadAnimation(ShippingAddressActivity.this, R.anim.zoom_in_enter));
        this.mDialog = new Dialog(ShippingAddressActivity.this, R.style.NewDialog);
        this.mDialog.setContentView(localView);
        this.mDialog.setCancelable(true);
        this.mDialog.show();

        Window window = this.mDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER | Gravity.CENTER;
        window.setGravity(Gravity.CENTER);
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.dimAmount = 0.0f;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.windowAnimations = R.anim.slide_move;

        window.setAttributes(wlp);
        window.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);



        EtPName = (EditText) this.mDialog.findViewById(R.id.etName);
        EtPEmail = (EditText) this.mDialog.findViewById(R.id.etEmail);
        EtPState = (EditText) this.mDialog.findViewById(R.id.etState);
        Button BtnSubmitProceed = (Button) this.mDialog.findViewById(R.id.btnsubmitProceed);

        BtnSubmitProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validations(localView,aid);
            }
        });


    }//showProceedPopUp

    private void validations(View localView, String aid) {
        try{
            ValidationHelper helper=new ValidationHelper();
            String[] strIds = getResources().getStringArray(R.array.shippingAddresspopup_ids_array);
            String[] strErrMsgs = getResources().getStringArray(R.array.shippingAddresspopup_errors_array);
            String[] strCompTypeArr = getResources().getStringArray(R.array.shippingAddresspopup_comptypes_array);
            ArrayList<ValidationDTO> aList = new ArrayList<ValidationDTO>();

            int iPos = 0;
            for(String strCompType:strCompTypeArr){
                ValidationDTO valDTO=new ValidationDTO();
                valDTO.setComponentType(strCompType);
                valDTO.setComponentID(ValidationUtils.getIdResourceByName(ShippingAddressActivity.this,strIds[iPos]));
                valDTO.setErrorMessage(strErrMsgs[iPos]);
                aList.add(valDTO);
                iPos++;
            }
            boolean isValidData = helper.validateData(ShippingAddressActivity.this.getBaseContext(), aList,localView );
            if (!isValidData) {
                return;
            }else{

                if((!EtPEmail.getText().toString().isEmpty()) && (AppHelper.emailValidator(EtPEmail.getText().toString() )) ){
                    submitOrderByVolley(aid);
                }else{
                    AppHelper.showSnackBar(ShippingAddressActivity.this,LLMain,"Email is invalid" );
//                    SimpleToast.error(ShippingAddressActivity.this,"Email is invalid");
                }

            }
        }catch (Exception e){     e.printStackTrace();    }
    }//Validations

    private void submitOrderByVolley(final String aid) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ShippingAddressActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Address:", "success! response: " + response.toString());
                        try {

                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){

                                CartItemsActivity.ctx.finish();
                                CartProceedActivity.ctx.finish();

                                //{"status":"success","message":"Thank you, your order created successfully"}
                                if(mainObj.has("message")){
                                    AppHelper.showSnackBar(ShippingAddressActivity.this,LLMain,mainObj.getString("message") );
//                                    SimpleToast.ok(ShippingAddressActivity.this,mainObj.getString("message"));


                                    AlertDialog.Builder builder = new AlertDialog.Builder(ShippingAddressActivity.this);
                                    builder.setTitle(R.string.app_name);

                                    builder.setMessage(mainObj.getString("message"))
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    if(mDialog.isShowing()) { mDialog.dismiss(); }
                                                    dialog.dismiss();
                                                    finish();
                                                }
                                            });
                                    //Creating dialog box
                                    AlertDialog alert = builder.create();

                                    alert.show();


                                }else{// Online  {"status":"success","data":{"temporderid":50,"paymenttoken":"Qe9JCN4MzUIJiOicGbhJCLiQ1VKJiOiAXe0Jye.9mQfikDZwUzYjZjYmVmYkVjI6ICdsF2cfJCLykjM5gzM1cTNxojIwhXZiwiIS5USiojI5NmblJnc1NkclRmcvJCLwUjM6ICduV3btFkclRmcvJCLiATNiojIklkclRmcvJye.GRUVmEY7zHe5gROD63jQh2ruQ-KbgKX2xhHz4zHs5tiLJOgK62g5uwAKu2h05-y9YM","orderamount":250}}



                                    JSONObject dataObj = new JSONObject(mainObj.getString("data"));
                                     temporderid = dataObj.getString("temporderid");
                                    String paymentToken = dataObj.getString("paymenttoken");
                                    String orderamount = dataObj.getString("orderamount");
                                    String clientid = dataObj.getString("clientid");

                                    String stage = "TEST";
                                    String orderNote = "TEST Order";

                                    Map<String, String> params = new HashMap<>();

                                    params.put(PARAM_APP_ID, clientid  );
                                    params.put(PARAM_ORDER_ID, temporderid);
                                    params.put(PARAM_ORDER_AMOUNT, orderamount);
                                    params.put(PARAM_ORDER_NOTE, orderNote);
                                    params.put(PARAM_CUSTOMER_NAME, SessionSave.getsession(AppConstants.name,ShippingAddressActivity.this));
                                    params.put(PARAM_CUSTOMER_PHONE, SessionSave.getsession(AppConstants.phone,ShippingAddressActivity.this) );
                                    params.put(PARAM_CUSTOMER_EMAIL, SessionSave.getsession(AppConstants.email,ShippingAddressActivity.this));


                                    for(Map.Entry entry : params.entrySet()) {
                                        Log.d("CFSKDSample", entry.getKey() + " " + entry.getValue());
                                    }

                                    CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
                                    cfPaymentService.setOrientation(ShippingAddressActivity.this, 0);

                                    // Use the following method for initiating Payments
                                    // First color - Toolbar background
                                    // Second color - Toolbar text and back arrow color
                                    cfPaymentService.doPayment(ShippingAddressActivity.this, params, paymentToken, stage, "#000000", "#FFFFFF");


                                }

                            }else{
                                AppHelper.showSnackBar(ShippingAddressActivity.this,LLMain,mainObj.getString("message") );
//                                SimpleToast.ok(ShippingAddressActivity.this,mainObj.getString("message"));
                                if(mDialog.isShowing()){mDialog.dismiss();}
                                finish();
                            }

                        }catch (Exception e){e.printStackTrace();}


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.proceed_order);
                params.put("userid", SessionSave.getsession(AppConstants.userid,ShippingAddressActivity.this));
                params.put("shipping_address_id",aid);
                params.put("name",EtPName.getText().toString());
                params.put("email",EtPEmail.getText().toString());
                params.put("state",EtPState.getText().toString());
                params.put("paymentmethod",paymentMethod);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    private void getShippingAddress() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ShippingAddressActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Address:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            productList = new ArrayList<AddressModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                TvEmptyBox.setVisibility(View.GONE);
                                JSONArray mainArray = new JSONArray(mainObj.getString("address"));
                                if(mainArray.length()>0) {
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        AddressModel addrList = new AddressModel();
                                        addrList.setId(obj.getString("id"));
                                        addrList.setCustomername(obj.getString("customername"));
                                        addrList.setPhone(obj.getString("phone"));
                                        addrList.setHousenumber(obj.getString("housenumber"));
                                        addrList.setStreet(obj.getString("street"));
                                        addrList.setCity(obj.getString("city"));
                                        addrList.setLandmark(obj.getString("landmark"));
                                        addrList.setState(obj.getString("state"));
                                        addrList.setPincode(obj.getString("pincode"));
//                                        if(i==0){
//                                            addrList.setRbselection(true);
//                                        }else{
                                        addrList.setRbselection(false);
//                                        }
                                        productList.add(addrList);
                                    }

                                    final ShippingAddressAdapter dbradapter = new ShippingAddressAdapter(ShippingAddressActivity.this, productList);
                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();


//                                    LvProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                                        @Override
//                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                                            AddressModel model = productList.get(i); //changed it to model because viewers will confused about it
//
//                                            model.setRbselection(true);
//
//                                            productList.set(i, model);
//
//                                            if (preSelectedIndex > -1){
//
//                                                AddressModel preRecord = productList.get(preSelectedIndex);
//                                                preRecord.setRbselection(false);
//
//                                                productList.set(preSelectedIndex, preRecord);
//
//                                            }
//
//                                            preSelectedIndex = i;
//
//                                            //now update adapter so we are going to make a update method in adapter
//                                            //now declare adapter final to access in inner method
//
//                                            dbradapter.updateRecords(productList);
//                                        }
//                                    });




//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                    TvEmptyBox.setVisibility(View.VISIBLE);
                                }

                            }else{
                                TvEmptyBox.setVisibility(View.VISIBLE);
                                //   SimpleToast.error(CartItemsActivity.this, AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.getshippingaddress);
                params.put("userid", SessionSave.getsession(AppConstants.userid,ShippingAddressActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }//getShippingAddress


    @Override
    protected void onResume() {
        super.onResume();
        getShippingAddress();
            AppHelper.checkInternetAvailability(ShippingAddressActivity.this);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Same request code for all payment APIs.
        Log.d(TAG, "ReqCode : " + CFPaymentService.REQ_CODE);
        Log.d(TAG, "API Response : ");
        //Prints all extras. Replace with app logic.
        if (data != null) {
            Bundle bundle = data.getExtras();
            String status = null;
            if (bundle != null)

                status =  bundle.getString("txStatus");
            Log.i("Staus:--> ",""+status);
                for (String key : bundle.keySet()) {
                    if (bundle.getString(key) != null) {
                        Log.d(TAG, key + " : " + bundle.getString(key));
                    }
                }
                if(status.equalsIgnoreCase("SUCCESS")){
                    updateOrderStatus();

                }else{
                    AppHelper.showSnackBar(ShippingAddressActivity.this,LLMain,status );
//                    SimpleToast.error(ShippingAddressActivity.this,status);
                }
        }
    }

    private void updateOrderStatus() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ShippingAddressActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Address:", "success! response: " + response.toString());
                        try {

                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                if(mainObj.has("message")){
                                    AppHelper.showSnackBar(ShippingAddressActivity.this,LLMain,mainObj.getString("message") );
//                                    SimpleToast.ok(ShippingAddressActivity.this,mainObj.getString("message"));
                                    if(mDialog.isShowing()) { mDialog.dismiss(); }
                                    finish();
                                }
                            }else{
                                AppHelper.showSnackBar(ShippingAddressActivity.this,LLMain,mainObj.getString("message") );
//                                SimpleToast.ok(ShippingAddressActivity.this,mainObj.getString("message"));
//                                if(mDialog.isShowing()){mDialog.dismiss();}
//                                finish();
                            }

                        }catch (Exception e){e.printStackTrace();}


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.online_orderstatus);
                params.put("temporderid", temporderid );
                params.put("userid", SessionSave.getsession(AppConstants.userid,ShippingAddressActivity.this));

                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
