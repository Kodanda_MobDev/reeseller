package com.prominere.reseller.activity.dashboard.account;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.LoginActivity;
import com.prominere.reseller.adapter.MyOrdersAdapter;
import com.prominere.reseller.adapter.ResellerOrdersAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.OrderModel;
import com.prominere.reseller.model.ResellerOrderModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.hashTagView.HashtagView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyOrdersActivity extends AppCompatActivity {

    private TextView StsAll,StsShipped,StsPending,StsCompleted,StsCancelled,TvEmptyView,StsReturngoods;
    private RequestQueue mQueue;
    private ArrayList<ResellerOrderModel> ordersList,PendingFilterList,DeliveredFilterList,ReturnFilterList,CancelledFilterList,ShippedFilterList;
    private RecyclerView RVOrders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_orders_activity);

        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);

        setTitle(getResources().getString(R.string.myOrders));


        RVOrders = (RecyclerView)findViewById(R.id.RVOrders);
        TvEmptyView = (TextView)findViewById(R.id.tvnoRecordFound);
        StsAll = (TextView)findViewById(R.id.stsAll);
        StsPending = (TextView)findViewById(R.id.stsPending);
        StsShipped = (TextView)findViewById(R.id.stsShipped);
        StsCompleted = (TextView)findViewById(R.id.stsCompleted);
        StsCancelled = (TextView)findViewById(R.id.stsCancelled);
        StsReturngoods = (TextView)findViewById(R.id.stsReturngoods);

        mQueue = Volley.newRequestQueue(MyOrdersActivity.this);
        getOrdrsList();


        changeBgColor(StsAll,StsPending,StsShipped,StsCompleted,StsCancelled,StsReturngoods);
        StsAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBgColor(StsAll,StsPending,StsShipped,StsCompleted,StsCancelled,StsReturngoods);
                setfilter(ordersList);
            }
        });
        StsPending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBgColor(StsPending,StsShipped,StsCompleted,StsCancelled,StsReturngoods,StsAll);
                setfilter(PendingFilterList);      }
        });
        StsShipped.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBgColor(StsShipped,StsCompleted,StsCancelled,StsReturngoods,StsAll,StsPending);
                setfilter(ShippedFilterList);      }
        });
      //  StsCompleted.setVisibility(View.GONE);// setOnClick(Compl,PendingFilterList);
        StsCancelled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBgColor(StsCancelled,StsReturngoods,StsAll,StsPending,StsShipped,StsCompleted);
                setfilter(CancelledFilterList);      }
        });
        StsReturngoods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBgColor(StsReturngoods,StsAll,StsPending,StsShipped,StsCompleted,StsCancelled);
                setfilter(ReturnFilterList);  }
        });
        StsCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBgColor(StsCompleted,StsAll,StsPending,StsShipped,StsReturngoods,StsCancelled);
                setfilter(DeliveredFilterList);  }
        });



    }

    private void getOrdrsList() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MyOrdersActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                                ordersList = new ArrayList<ResellerOrderModel>();
                                PendingFilterList = new ArrayList<ResellerOrderModel>();
                                ShippedFilterList = new ArrayList<ResellerOrderModel>();
                                DeliveredFilterList = new ArrayList<ResellerOrderModel>();
                                CancelledFilterList = new ArrayList<ResellerOrderModel>();
                                ReturnFilterList = new ArrayList<ResellerOrderModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){

                                JSONArray mainArray = new JSONArray(mainObj.getString("orders"));
                                if(mainArray.length()>0){
                                    TvEmptyView.setVisibility(View.GONE); RVOrders.setVisibility(View.VISIBLE);
                                    for(int i=0;i<mainArray.length();i++){
                                        JSONObject obj = mainArray.getJSONObject(i);

                                        ResellerOrderModel model = new ResellerOrderModel();

                                        model.setId(obj.getString("id"));
                                        model.setOrderid(obj.getString("orderid"));
                                        model.setProductname(obj.getString("productname"));
                                        model.setQuantity(obj.getString("quantity"));
                                        model.setProductprice(obj.getString("productprice"));
                                        model.setMargin(obj.getString("margin"));
                                        model.setDateadded(obj.getString("dateadded"));
                                        model.setProductimage(obj.getString("productimage"));
                                        model.setPrice(obj.getString("price"));
                                        model.setOrderprice(obj.getString("orderprice"));
                                        model.setCustomername(obj.getString("customername"));
                                        model.setStatus(obj.getString("status"));


                                        switch (obj.getString("status").toLowerCase()){
                                            case "pending": PendingFilterList.add(model); break;
                                            case "shipped": ShippedFilterList.add(model); break;
                                            case "delivered": DeliveredFilterList.add(model); break;
                                            case "completed": DeliveredFilterList.add(model); break;
                                            case "cancelled": CancelledFilterList.add(model); break;
                                            case "returned": ReturnFilterList.add(model); break;
                                            default:    break;
                                        }

                                        ordersList.add(model);
                                    }

                                    ResellerOrdersAdapter dbradapter = new ResellerOrdersAdapter(MyOrdersActivity.this, ordersList);

                                    RVOrders.setLayoutManager(new LinearLayoutManager(MyOrdersActivity.this) );

                                    RVOrders.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();

                                }else{
                                    TvEmptyView.setText(mainObj.getString("message"));
                                    TvEmptyView.setVisibility(View.VISIBLE);  RVOrders.setVisibility(View.GONE);
                                }
                            }else{
                                TvEmptyView.setText(mainObj.getString("message"));
                                TvEmptyView.setVisibility(View.VISIBLE);  RVOrders.setVisibility(View.GONE);
//                                SimpleToast.error(SubCategoryActivity.this,"Failed to execute Api");
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.allorders);
                params.put("userid", SessionSave.getsession(AppConstants.userid,MyOrdersActivity.this));
                params.put("status", "0");
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }


    private void changeBgColor(TextView tv1, TextView tv2, TextView tv3, TextView tv4, TextView tv5, TextView tv6) {
        tv1.setBackground(getResources().getDrawable(R.drawable.appcolor_draw_edittext_bg));     tv1.setTextColor(getResources().getColor(R.color.white));
        tv2.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg));     tv2.setTextColor(getResources().getColor(R.color.black));
        tv3.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg));     tv3.setTextColor(getResources().getColor(R.color.black));
        tv4.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg));     tv4.setTextColor(getResources().getColor(R.color.black));
        tv5.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg));     tv5.setTextColor(getResources().getColor(R.color.black));
        tv6.setBackground(getResources().getDrawable(R.drawable.draw_edittext_bg));     tv6.setTextColor(getResources().getColor(R.color.black));
    }
    private void setfilter(ArrayList<ResellerOrderModel> list) {
        RVOrders.setAdapter(null);
        if(list.size()>0){
            TvEmptyView.setVisibility(View.GONE);
            ResellerOrdersAdapter dbradapter = new ResellerOrdersAdapter(MyOrdersActivity.this, list);

            LinearLayoutManager llm = new LinearLayoutManager(MyOrdersActivity.this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            RVOrders.setLayoutManager(llm);
//            RVOrders.setHasFixedSize(true);
            RVOrders.setAdapter(dbradapter);
//            RVOrders.setLayoutManager(new LinearLayoutManager(getActivity()) );
            dbradapter.notifyDataSetChanged();
        }else{
            TvEmptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(MyOrdersActivity.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

}
