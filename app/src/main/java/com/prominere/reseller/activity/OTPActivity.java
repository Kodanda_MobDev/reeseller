package com.prominere.reseller.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.reseller.HelpActivity;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.util.PinEntryEditText;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OTPActivity extends AppCompatActivity {

    private String otp,userid,phone,type,psw;
    private PinEntryEditText EtOTP;
    private RequestQueue queue;
    private TextView TvMsg;
    private LinearLayout LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        queue = Volley.newRequestQueue(this);
        otp = getIntent().getStringExtra("otp");
        userid = getIntent().getStringExtra("userid");
        type = getIntent().getStringExtra("type");
        phone = getIntent().getStringExtra("phone");
        psw = getIntent().getStringExtra("password");

    //    overridePendingTransition(R.anim.down_from_top, R.anim.down_from_top);
        AppHelper.setStatusBarColor(this, R.color.statusBarColor);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("OTP");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);

         LLMain = (LinearLayout)findViewById(R.id.llMain);
         TvMsg = (TextView)findViewById(R.id.tvMsg);  TvMsg.setText("OTP has been sent to your Email ID and "+phone);
         EtOTP = (PinEntryEditText)findViewById(R.id.txt_pin_entry);
        Button BtnSubmit = (Button)findViewById(R.id.bb_OTPClient);
        BtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(EtOTP.getText().toString().isEmpty()){
                    AppHelper.showSnackBar(OTPActivity.this,LLMain,"Please Enter OTP" );
//                    SimpleToast.error(OTPActivity.this,"Please Enter OTP");
                }else{
                    ValidateOTPByVolley();
                }


//                Intent in = new Intent(OTPActivity.this,MainActivity.class);
//                startActivity(in);
            }
        });
    }//onCreate

    private void ValidateOTPByVolley() {

        AlertDialog.Builder builder = new AlertDialog.Builder(OTPActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){

//                                if(type.equalsIgnoreCase("1")){
//                                    SessionSave.saveSession(AppConstants.usertype,"Vendor",OTPActivity.this);
//                                }else{
//                                    SessionSave.saveSession(AppConstants.usertype,"Reseller",OTPActivity.this);
//                                }
//                                SessionSave.saveSession(AppConstants.userid,mainObj.getString("userid"),OTPActivity.this);
//                                SessionSave.saveSession(AppConstants.phone,phone,OTPActivity.this);
//                                SessionSave.saveSession(AppConstants.password,psw,OTPActivity.this);
                                AppHelper.showSnackBar(OTPActivity.this,LLMain,mainObj.getString("message") );
//                                SimpleToast.ok(OTPActivity.this,mainObj.getString("message"));
                                Intent in = new Intent(OTPActivity.this,LoginActivity.class);
                                startActivity(in);
                                finish();
                            }else{
                                AppHelper.showSnackBar(OTPActivity.this,LLMain,mainObj.getString("message") );
//                                SimpleToast.error(OTPActivity.this,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(OTPActivity.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.register);
                params.put("type",type);
                params.put("otp",EtOTP.getText().toString());
                params.put("userid",userid);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });
        queue.add(sr);



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent in = new Intent(OTPActivity.this, RegistrationActivity.class);
                startActivity(in);
                finish();
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void reSendOTPByVolley(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(OTPActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){ //success! response: {"status":"success","otp":"2689"}
                                AppHelper.showSnackBar(OTPActivity.this,LLMain,"OTP is set to your mobile number" );
//                                SimpleToast.ok(OTPActivity.this,"OTP is set to your mobile number");
//                                Intent in = new Intent(LoginActivity.this,OTPActivity.class);
//                                startActivity(in);
//                                finish();
                            }else{
                                AppHelper.showSnackBar(OTPActivity.this,LLMain,mainObj.getString("message") );
//                                SimpleToast.error(OTPActivity.this,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(OTPActivity.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.get_otp);
                params.put("phone",phone);
                params.put("password",psw);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        queue.add(sr);

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(OTPActivity.this);
    }

}
