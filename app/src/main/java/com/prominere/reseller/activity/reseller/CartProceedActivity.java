package com.prominere.reseller.activity.reseller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.AppHelper;

public class CartProceedActivity extends AppCompatActivity {

    public static Boolean isRunning = false;
    private String CashCollection,TotalPrice,ProductCharges,ShippingCharges, FirstOrderrVal,GSTCharges ;
    private RequestQueue mQueue;
    private LinearLayout LLMain;
    private RadioGroup PaymentSelRG;
    private TextView ProceedBtn,TvShippingCharges,TvFirstOrderrVal,TvCODCharges,TvProductCharges,TvTTotalPrice,TvTotalPrice ;
    private TextView TvCashToCollect;
    public static  CartProceedActivity ctx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_proceed_activity);

        ctx =  this;


        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        setTitle("Select Payment Method");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
        mQueue = Volley.newRequestQueue(CartProceedActivity.this);

        ProductCharges = getIntent().getStringExtra("ProductCharges");
        ShippingCharges = getIntent().getStringExtra("ShippingCharges");
        FirstOrderrVal = getIntent().getStringExtra("FirstOrderrVal");
        GSTCharges = getIntent().getStringExtra("GSTCharges");
        TotalPrice = getIntent().getStringExtra("TotalPrice");
        CashCollection = getIntent().getStringExtra("CashCollection");

        LLMain = (LinearLayout)findViewById(R.id.llMain);

        ProceedBtn = (TextView)findViewById(R.id.proceedBtn);
        TvShippingCharges = (TextView)findViewById(R.id.tvShippingCharges);
        TvFirstOrderrVal = (TextView)findViewById(R.id.tvFirstOrderrVal);
        TvCODCharges = (TextView)findViewById(R.id.tvCODCharges);
        TvProductCharges = (TextView)findViewById(R.id.tvProductCharges);
        TvTotalPrice = (TextView)findViewById(R.id.tvTotalSum);
        TvTTotalPrice = (TextView)findViewById(R.id.tvTotalPrice);
        TvCashToCollect = (TextView)findViewById(R.id.tvCashToCollect);
        PaymentSelRG = (RadioGroup)findViewById(R.id.paymentRadioGroup);  PaymentSelRG.check(R.id.rbCOD);

        Log.i("CashCollection: --> ",CashCollection);
        Log.i("TotalPrice: --> ",TotalPrice);
        Log.i("TotalPrice: --> ",TotalPrice.substring(1,TotalPrice.length()));

        TvShippingCharges.setText(ShippingCharges);


        TvCODCharges.setText(GSTCharges);
        TvProductCharges.setText(ProductCharges);
        TvTotalPrice.setText(TotalPrice);
        TvTTotalPrice.setText(TotalPrice);
        TvCashToCollect.setText("₹"+CashCollection+"0");

if(CashCollection.equalsIgnoreCase("0.0")){
    TvFirstOrderrVal.setText("₹0.00");
}else{
    Double  fo = Double.parseDouble(CashCollection) - Double.parseDouble(TotalPrice.substring(1,TotalPrice.length()));
    TvFirstOrderrVal.setText("₹"+fo);
}


        ProceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                        if(i == updList.size()-1){
                String paymentMethod;
                String rbSel = ((RadioButton)findViewById(PaymentSelRG.getCheckedRadioButtonId())).getText().toString();
                if(rbSel.toUpperCase().equalsIgnoreCase("ONLINE")){
                    paymentMethod = "online";
                }else if(rbSel.toUpperCase().equalsIgnoreCase("WALLET")){
                    paymentMethod = "wallet";
                }
                else{
                    paymentMethod = "cash";
                }
//                            if(cartMAmt == false) {
                Intent in = new Intent(CartProceedActivity.this, ShippingAddressActivity.class);
                in.putExtra("paymentMethod", paymentMethod);
                startActivity(in);
//                            }
//                        }
            }
        });


    }//onCreate











    @Override
    protected void onStart() {
        super.onStart();
        isRunning = true;
        Log.i("onStart:--> ","isRunning: "+isRunning);
    }

    @Override
    protected void onStop() {
        super.onStop();
        isRunning = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Log.i("onResume:--> ","Bfr isRunning: "+isRunning);
//        if(isRunning == false){
//            Log.i("onResume:--> ","isRunning:1 "+isRunning);
//            finish();
//        }else{
//            Log.i("onResume:--> ","isRunning:2 "+isRunning);
            AppHelper.checkInternetAvailability(CartProceedActivity.this);
//        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
