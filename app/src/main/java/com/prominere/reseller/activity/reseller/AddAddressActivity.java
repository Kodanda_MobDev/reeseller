package com.prominere.reseller.activity.reseller;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.activity.dashboard.DBProductsActivity;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.helper.validations.ValidationDTO;
import com.prominere.reseller.helper.validations.ValidationHelper;
import com.prominere.reseller.helper.validations.ValidationUtils;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddAddressActivity extends AppCompatActivity {

    private RequestQueue mQueue;
    private EditText EtCustName,EtPhoneNumber,EtHomeNumber, EtStreetColony,EtCity, EtLandMark,EtState, EtPincode;
    private TextView TvSaveAddress;
    private String addressId="";
    private LinearLayout LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_address_activity);


        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        setTitle("Add Shipping Address");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);

         mQueue = Volley.newRequestQueue(AddAddressActivity.this);

        Intent intent = getIntent();
        if(intent.hasExtra(AppConstants.addressId)) {
              addressId = intent.getStringExtra(AppConstants.addressId);
            setValues(addressId);

        } else {
            // If extra was not passed then this block is executed because hasExtra() method would return false on not getting extra.
        }

      inIt();

    }//onCreate

    private void setValues(final String aId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(AddAddressActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Address:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                JSONObject dataObj = new JSONObject(mainObj.getString("data"));
                                EtCustName.setText(dataObj.getString("customername"));
                                EtPhoneNumber.setText(dataObj.getString("phone"));
                                EtHomeNumber.setText(dataObj.getString("housenumber"));
                                EtStreetColony.setText(dataObj.getString("street"));
                                EtCity.setText(dataObj.getString("city"));
                                EtLandMark.setText(dataObj.getString("landmark"));
                                EtState.setText(dataObj.getString("state"));
                                EtPincode.setText(dataObj.getString("pincode"));

                            }else{
                                AppHelper.showSnackBar(AddAddressActivity.this,LLMain,"edit_shippingaddress getting fails.");
//                                SimpleToast.error(AddAddressActivity.this,"edit_shippingaddress getting fails.");
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(AddAddressActivity.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.edit_shippingaddress);
                params.put("id", aId);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    private void inIt() {
    LLMain = (LinearLayout)findViewById(R.id.llMain);
    EtCustName = (EditText)findViewById(R.id.etCustName);
    EtPhoneNumber = (EditText)findViewById(R.id.et_PhoneNumber);
    EtHomeNumber = (EditText)findViewById(R.id.et_HomeNumb);
    EtStreetColony = (EditText)findViewById(R.id.et_StreetColony);
    EtCity = (EditText)findViewById(R.id.et_City);
    EtLandMark = (EditText)findViewById(R.id.et_Landmark);
    EtState = (EditText)findViewById(R.id.et_State);
    EtPincode = (EditText)findViewById(R.id.et_pincode);
    TvSaveAddress = (TextView)findViewById(R.id.tvSaveAddress);

    TvSaveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validations();
            }
        });

    }//inIt

    private void validations() {
        try{
            ValidationHelper helper=new ValidationHelper();
            String[] strIds = getResources().getStringArray(R.array.addAddress_ids_array);
            String[] strErrMsgs = getResources().getStringArray(R.array.addAddress_errors_array);
            String[] strCompTypeArr = getResources().getStringArray(R.array.addAddress_comptypes_array);
            ArrayList<ValidationDTO> aList = new ArrayList<ValidationDTO>();

            int iPos = 0;
            for(String strCompType:strCompTypeArr){
                ValidationDTO valDTO=new ValidationDTO();
                valDTO.setComponentType(strCompType);
                valDTO.setComponentID(ValidationUtils.getIdResourceByName(AddAddressActivity.this,strIds[iPos]));
                valDTO.setErrorMessage(strErrMsgs[iPos]);
                aList.add(valDTO);
                iPos++;
            }
            boolean isValidData = helper.validateData(AddAddressActivity.this.getBaseContext(), aList,getWindow().getDecorView() );
            if (!isValidData) {
                return;
            }else{
                if(addressId.isEmpty()){//Add new Address
                    addAddressByVolley();
                }else{//Update Address
                    updateAddressByVolley();
                }


            }
        }catch (Exception e){     e.printStackTrace();    }
    }//Validations

    private void addAddressByVolley() {

        AlertDialog.Builder builder = new AlertDialog.Builder(AddAddressActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Address:", "success! response: " + response.toString());
                        try {

                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                AppHelper.showSnackBar(AddAddressActivity.this,LLMain,mainObj.getString("message"));

//                                SimpleToast.ok(AddAddressActivity.this,mainObj.getString("message"));
                                finish();
                            }else{
                                AppHelper.showSnackBar(AddAddressActivity.this,LLMain,mainObj.getString("message"));

//                                SimpleToast.error(AddAddressActivity.this,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

//                        try {
//                            JSONObject mainObj = new JSONObject(response);
//                            ArrayList<CartModel> productList = new ArrayList<CartModel>();
//                            if(mainObj.getString("status").equalsIgnoreCase("success")){
//                                TvEmptyBox.setVisibility(View.GONE);
//                                JSONArray mainArray = new JSONArray(mainObj.getString("list"));
//                                if(mainArray.length()>0) {
//                                    for (int i = 0; i < mainArray.length(); i++) {
//                                        JSONObject obj = mainArray.getJSONObject(i);
//                                        CartModel catList = new CartModel();
//                                        catList.setId(obj.getString("id"));
//                                        catList.setTitle(obj.getString("title"));
//                                        catList.setPrice(obj.getString("price"));
//                                        catList.setImage(obj.getString("image"));
//                                        catList.setCategory(obj.getString("category"));
//                                        catList.setQuantity(obj.getString("quantity"));
//                                        catList.setAttribute(obj.getString("attribute"));
//
//                                        productList.add(catList);
//                                    }
//
//                                    CartItemsAdapter dbradapter = new CartItemsAdapter(CartItemsActivity.this, productList);
//                                    LvProducts.setAdapter(dbradapter);
//                                    dbradapter.notifyDataSetChanged();
////                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
//                                }else{
//                                    TvEmptyBox.setVisibility(View.VISIBLE);
//                                }
//
//                            }else{
//                                TvEmptyBox.setVisibility(View.VISIBLE);
//                                //   SimpleToast.error(CartItemsActivity.this, AppConstants.apiIssueMsg);
//                            }
//
//                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(AddAddressActivity.this, AppConstants.noInternetConnection  );
                        }
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.add_shippingaddress);
                params.put("userid", SessionSave.getsession(AppConstants.userid,AddAddressActivity.this));
                params.put("customername",EtCustName.getText().toString());
                params.put("phone",EtPhoneNumber.getText().toString());
                params.put("housenumber",EtHomeNumber.getText().toString());
                params.put("street",EtStreetColony.getText().toString());
                params.put("city",EtCity.getText().toString());
                params.put("landmark",EtLandMark.getText().toString());
                params.put("state",EtState.getText().toString());
                params.put("pincode",EtPincode.getText().toString());
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
//                params.put("Content-Type","application/x-www-form-urlencoded");
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }
    private void updateAddressByVolley() {

        AlertDialog.Builder builder = new AlertDialog.Builder(AddAddressActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Address:", "success! response: " + response.toString());
                        try {

                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                AppHelper.showSnackBar(AddAddressActivity.this,LLMain,mainObj.getString("message"));

//                                SimpleToast.ok(AddAddressActivity.this,mainObj.getString("message"));
                                finish();
                            }else{
                                AppHelper.showSnackBar(AddAddressActivity.this,LLMain,mainObj.getString("message"));

//                                SimpleToast.error(AddAddressActivity.this,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(AddAddressActivity.this, AppConstants.noInternetConnection  );
                        }
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.update_shippingaddress);
                params.put("userid", SessionSave.getsession(AppConstants.userid,AddAddressActivity.this));
                params.put("customername",EtCustName.getText().toString());
                params.put("phone",EtPhoneNumber.getText().toString());
                params.put("housenumber",EtHomeNumber.getText().toString());
                params.put("street",EtStreetColony.getText().toString());
                params.put("city",EtCity.getText().toString());
                params.put("landmark",EtLandMark.getText().toString());
                params.put("state",EtState.getText().toString());
                params.put("pincode",EtPincode.getText().toString());
                params.put("id",addressId);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(AddAddressActivity.this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
