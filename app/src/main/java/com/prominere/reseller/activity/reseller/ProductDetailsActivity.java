package com.prominere.reseller.activity.reseller;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.CompoundButtonCompat;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.prominere.reseller.R;
import com.prominere.reseller.adapter.PreviewAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.badgecount.MenuItemBadge;
import com.prominere.reseller.util.numberPicker.NumberPicker;
import com.prominere.reseller.util.share.AndroidShare;
import com.prominere.reseller.util.share.AndroidSharePlatform;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ProductDetailsActivity extends AppCompatActivity  {

    private static final int PERMISSION_REQUEST_CODE = 200 ;
    private String pId;
    private RequestQueue mQueue;
    private TextView TvLoveSymbol,TvPincode,TvDiscountPrice,TvFeatures,TvDiscPercentage,TvWhatsAppShare,TvPrice,TvPolicy,TvOtherShare,TvDesc,TvCategory,TvTitle;
    private Typeface materialdrawerFont,fontAwesomeFont;
    private ImageView Img1,Img2,Img3;
    private TextView TvAddToCart;
    private NumberPicker NumbPicker;
    private RadioGroup radioGroup;
    private RadioButton sizeRadioButton;
    private String shareWhatsAppContent;
    private MenuItem menuItemCart;
    private List<String> listOfImgs;
    private List<Bitmap> downloadShare;
    private LinearLayout LLCheckBoxes,LLShareWhatsApp,LLChat,LLShare,LLDownload;
    private StringBuilder seleAttr;
    private static String[] PERMISSIONS_STORAGE = {
            READ_EXTERNAL_STORAGE,
            WRITE_EXTERNAL_STORAGE
    };
    private static final int REQUEST_EXTERNAL_STORAGE = 2;
    private int downloadFileCount=0;
    private ArrayList<Uri> downloadFileList;
    private ProgressBar PBar;
    private boolean isWishList;
    private LinearLayout LLMain;
    private TextView TvProductsCount;
    private LinearLayout LLImgs;
    private Dialog previewDialog;
    private ImageView[] ivArrayDotsPager;
    private GetXMLTask task;
    private ArrayList<Long> checkDulplicateUris;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details_activity);

        fontAwesomeFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        materialdrawerFont = Typeface.createFromAsset(getAssets(), "materialdrawerfont.ttf");

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
//        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_from_right);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
        mQueue = Volley.newRequestQueue(ProductDetailsActivity.this);

        pId = getIntent().getStringExtra(AppConstants.productid);
//        String pTitle = getIntent().getStringExtra(AppConstants.producttitle);
        setTitle("");

        TvLoveSymbol = (TextView)findViewById(R.id.tvLoveSymb);  TvLoveSymbol.setTypeface(fontAwesomeFont);
        radioGroup = (RadioGroup) findViewById(R.id.radioSizes);     radioGroup.check(R.id.rbMedium);

        LLImgs = (LinearLayout)findViewById(R.id.llImgs);
        LLMain = (LinearLayout)findViewById(R.id.llMain);
        TvProductsCount = (TextView)findViewById(R.id.tvCount);
        LLCheckBoxes = (LinearLayout)findViewById(R.id.llCheckBoxes);
        LLDownload = (LinearLayout)findViewById(R.id.llDownload);
        LLShare = (LinearLayout)findViewById(R.id.llShare);
        LLChat = (LinearLayout)findViewById(R.id.llChat);
        LLShareWhatsApp = (LinearLayout)findViewById(R.id.llShareWhatsApp);
        NumbPicker =(NumberPicker)findViewById(R.id.numbPicker);
        TvAddToCart =(TextView)findViewById(R.id.tvAddToCart);
        TvCategory =(TextView)findViewById(R.id.tvCategory);
        TvTitle =(TextView)findViewById(R.id.tvItemTitle);
        TvPrice =(TextView)findViewById(R.id.tvPrice);
        TvWhatsAppShare =(TextView)findViewById(R.id.tvWhatsUpShare);
        TvDiscPercentage =(TextView)findViewById(R.id.tvDiscPercentage);
        TvDiscountPrice =(TextView)findViewById(R.id.tvDiscountPrice);
        TvPincode =(TextView)findViewById(R.id.tvPincode);
        Img1 =(ImageView)findViewById(R.id.img1);
        Img2 =(ImageView)findViewById(R.id.img2);
        Img3 =(ImageView)findViewById(R.id.img3);
        TvFeatures =(TextView)findViewById(R.id.tvFeatures);
        TvDesc =(TextView)findViewById(R.id.tvDescription);
        TvPolicy =(TextView)findViewById(R.id.tvPolicy);        TvPolicy.setTypeface(materialdrawerFont);
        TvOtherShare =(TextView)findViewById(R.id.tvOtherShare);
        PBar =(ProgressBar)findViewById(R.id.progressBar);

        getProductDetails();

        if (!checkPermission()) {
//            openActivity();
        } else {
            if (checkPermission()) {
                requestPermissionAndContinue();
            } else {
//                openActivity();
            }
        }



        TvLoveSymbol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Before:-->",""+isWishList);
                if(isWishList){

                    isWishList = false;
                    TvLoveSymbol.setText(getResources().getString(R.string.fai_lovewb));
                    AddToWidhList(0);
                }else{
                    isWishList = true;
                    TvLoveSymbol.setText(getResources().getString(R.string.fai_love));
                    AddToWidhList(1);
                }

//
            }
        });
        TvAddToCart.setOnClickListener(new View.OnClickListener() {
           

            @Override
            public void onClick(View view) {
                if(NumbPicker.getValue() == 0){
                    AppHelper.showSnackBar(ProductDetailsActivity.this,LLMain,"Please Select Number of Items");
//                    SimpleToast.ok(ProductDetailsActivity.this,"Please Select Number of Items");
                }else{

                    seleAttr = new StringBuilder("");

                    final int childCount = LLCheckBoxes.getChildCount();
                     for (int i = 0; i < childCount; i++) {
                        View element = LLCheckBoxes.getChildAt(i);
                        // CheckBox
                        if (element instanceof CheckBox) {
                            CheckBox checkBox = (CheckBox)element;
                            appendAttributes(checkBox);
                        }
                    }

Log.i("seleAttr:-->",""+seleAttr);
                if(seleAttr.toString().isEmpty()){
                    AppHelper.showSnackBar(ProductDetailsActivity.this,LLMain,"Please Select Size");
//                    SimpleToast.error(ProductDetailsActivity.this,"Please Select Size");
                }else{
                    AddToCart();
                     }

                }

            }
        });
//        TvWhatsAppShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                shareProduct(2);
//            }
//        });
//        TvOtherShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                shareProduct(1);
//            }
//        });

        LLChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                openWhatsAppChat();
                String link = ("https://wa.me/+917676227187");
                PackageManager packageManager = getPackageManager();
                Intent i = new Intent(Intent.ACTION_VIEW);

                try {
                    i.setPackage("com.whatsapp");
                    i.setData(Uri.parse(link));
                    if (i.resolveActivity(packageManager) != null) {
                        startActivity(i);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        LLDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppHelper.isStoragePermissionGranted(ProductDetailsActivity.this)) {

                    for(int i=0;i<listOfImgs.size();i++) {
                        String img = listOfImgs.get(i);
                        Log.i("img:-->" + i, " " + img);
                        if (img != null && !img.isEmpty()) {
//                            downloadImagesToStorage(img,i+1);
                            AppHelper.downloadFileToStorage(ProductDetailsActivity.this,img,i+1,listOfImgs.size());
                        }
                    }
                } else {
                    ActivityCompat.requestPermissions(   ProductDetailsActivity.this,     PERMISSIONS_STORAGE,  REQUEST_EXTERNAL_STORAGE  );
                }
            }
        });
        LLShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareProduct(1);
//                downloadShare = new ArrayList<Bitmap>();
//                downloadFileCount = 0;
//                downloadFileList = new ArrayList<Uri>();
//                AppHelper.shreImgsCountSize = listOfImgs.size();
//                AppHelper.shreImgsCount =  0 ;
//                AppHelper.shreImgsDownloadFileList = new ArrayList<Uri>();
//                String[] strArray = new String[listOfImgs.size()];
//                for(int i=0;i<listOfImgs.size();i++){
//                    String url =  listOfImgs.get(i);
//                    Log.i("URLS:--> ",url);
//                    strArray[i] = url ;
//              //      AppHelper.downloadFile(2,url,ProductDetailsActivity.this,i+1,listOfImgs.size(),TvTitle.getText().toString());
//
//                    downloadImages(url,1,downloadFileCount);
//                }
//                task = new GetXMLTask(ProductDetailsActivity.this);
//                task.execute(strArray);
            }
        });
        LLShareWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareProduct(2);
//                downloadShare = new ArrayList<Bitmap>();
//                    downloadFileCount = 0;
//                    downloadFileList = new ArrayList<Uri>();
//                    AppHelper.shreImgsCountSize = listOfImgs.size();
//                    AppHelper.shreImgsCount =  0 ;
//                    AppHelper.shreImgsDownloadFileList = new ArrayList<Uri>();
//                    for(int i=0;i<listOfImgs.size();i++){
//                        String url =  listOfImgs.get(i);
//
//                        Log.i("URLS:--> ",url);
//                        downloadImages(url,2,downloadFileCount);
//            //            AppHelper.downloadFile(3,url,ProductDetailsActivity.this,i+1,listOfImgs.size(),TvTitle.getText().toString());
//                    }
            }
        });
        LLImgs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previewImgsPoUp();
            }
        });


    }//onCreate


    private void previewImgsPoUp() {
        View preView = View.inflate(ProductDetailsActivity.this,R.layout.imgs_preview_popup, null);
//		this.mDialog = new Dialog(BookingConfirmAct.this, R.style.NewDialog);
        this.previewDialog = new Dialog(ProductDetailsActivity.this);
        this.previewDialog.setContentView(preView);
        this.previewDialog.setCancelable(true);
        this.previewDialog.show();
        Window window = this.previewDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
//        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent_background));
        wlp.windowAnimations = R.anim.zoom_in_enter;
        window.setAttributes(wlp);
        final ViewPager ImgViewPager = (ViewPager) this.previewDialog.findViewById(R.id.viewpager);
        ImageView LeftImgBtm = (ImageView) this.previewDialog.findViewById(R.id.left_nav);

        LinearLayout LLPagerDots = (LinearLayout) this.previewDialog.findViewById(R.id.pager_dots);
        ImageView RightImgBtn = (ImageView) this.previewDialog.findViewById(R.id.right_nav);



        setupPagerIndidcatorDots(LLPagerDots);


        ivArrayDotsPager[0].setImageResource(R.drawable.page_indicator_selected);

        List<String>  imgsList = new ArrayList<String>();

        for(int i=0;i<listOfImgs.size();i++){
            String url =  listOfImgs.get(i);
            Log.i("URLS:--> ",url);
            imgsList.add(url);
        }
        PreviewAdapter adapter = new PreviewAdapter(ProductDetailsActivity.this,imgsList);
        ImgViewPager.setAdapter(adapter);
// Images left navigation
        LeftImgBtm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = ImgViewPager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    ImgViewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    ImgViewPager.setCurrentItem(tab);
                }
            }
        });
 // Images right navigatin
        RightImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = ImgViewPager.getCurrentItem();
                tab++;
                ImgViewPager.setCurrentItem(tab);
            }
        });


        ImgViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < ivArrayDotsPager.length; i++) {
                    ivArrayDotsPager[i].setImageResource(R.drawable.page_indicator_unselected);
                }
                ivArrayDotsPager[position].setImageResource(R.drawable.page_indicator_selected);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private void setupPagerIndidcatorDots(LinearLayout llPagerDots) {

        ivArrayDotsPager = new ImageView[listOfImgs.size()];
        for (int i = 0; i < ivArrayDotsPager.length; i++) {
            ivArrayDotsPager[i] = new ImageView(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(5, 0, 5, 0);
            ivArrayDotsPager[i].setLayoutParams(params);
            ivArrayDotsPager[i].setImageResource(R.drawable.page_indicator_unselected);
            //ivArrayDotsPager[i].setAlpha(0.4f);
            ivArrayDotsPager[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setAlpha(1);
                }
            });
            llPagerDots.addView(ivArrayDotsPager[i]);
            llPagerDots.bringToFront();
        }
    }

    private void downloadImagesToStorage(final String ImageUrl, final int i) {
//        PBar.setVisibility(View.VISIBLE);
        Picasso.with(ProductDetailsActivity.this)
                .load(ImageUrl)
                .into(new Target() {
                          @Override
                          public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                              try {
                                  Log.i("onBitmapLoaded:-->",ImageUrl);

                                  String root = Environment.getExternalStorageDirectory().getPath();
                                  File myDir1 = new File(root + "/Reeseller");

                                  if (!myDir1.exists()) {
                                      myDir1.mkdirs();
                                  }
//                                  File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()+"/brouchure.pdf");
                                  StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                                          .detectLeakedClosableObjects()
                                          .penaltyLog()
                                          .penaltyDeath()
                                          .build());




                                  String name = new Date().getTime()+ ".jpg";
                                  File myDir = new File(myDir1, name);
                                  FileOutputStream out = new FileOutputStream(myDir);
//                                  FileOutputStream out = new FileOutputStream(new File(myDir, name));

                                  bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);

                                  PBar.setVisibility(View.GONE);


                                  out.flush();
                                  out.close();
                                  Log.i(" NAvigate:  i"+i, "  ListOfImgs: " +listOfImgs.size());

                                  if(i == listOfImgs.size()){
//                                      SimpleToast.ok(ProductDetailsActivity.this,"Downloaded successfully");
                            AppHelper.showAlertDowloadFolderDialog(ProductDetailsActivity.this);
                                   //   new generatePictureStyleNotification(ProductDetailsActivity.this,TvTitle.getText().toString(), "",ImageUrl,i).execute();

                                  }




                              } catch(Exception e){
                                  Log.i("Exception :",e.getMessage());
                                  // some action
                              }
                          }

                          @Override
                          public void onBitmapFailed(Drawable errorDrawable) {
                              PBar.setVisibility(View.GONE);
                          }

                          @Override
                          public void onPrepareLoad(Drawable placeHolderDrawable) {
                          }
                      }
                );

    }

    public void downloadFile(String uRl) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/Reeseller");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Demo")
                .setDescription("Something useful. No, really.")
                .setDestinationInExternalPublicDir("/AnhsirkDasarpFiles", "fileName.jpg");

        mgr.enqueue(request);

        // Open Download Manager to view File progress
//        Toast.makeText(getActivity(), "Downloading...",Toast.LENGTH_LONG).show();
//        startActivity(Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));

    }


    private void openFile(File url) {

        try {
            Log.i("Open Fie",url.toString());
            Uri uri = Uri.fromFile(url);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/x-wav");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") ||
                    url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(ProductDetailsActivity.this, "No application found which can open the file", Toast.LENGTH_SHORT).show();
//            Log.i("Error==>","No application found which can open the file");
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateBadgeCount();
        AppHelper.checkInternetAvailability(ProductDetailsActivity.this);

    }

    private void shareProduct(final int i) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                downloadShare = new ArrayList<Bitmap>();
                                if(i == 2){
                                    downloadFileCount = 0;
                                    downloadFileList = new ArrayList<Uri>();
                                    checkDulplicateUris = new ArrayList<Long>();

                                    for(int i=0;i<listOfImgs.size();i++){
                                       String url =  listOfImgs.get(i);
                                       Log.i("URLS:--> ",url);
                                        downloadImagesByDownloadManager(url,2,downloadFileCount);
                                    }

                                }else{
                                    downloadFileCount = 0;
                                    downloadFileList = new ArrayList<Uri>();
                                    checkDulplicateUris = new ArrayList<Long>();

                                    for(int i=0;i<listOfImgs.size();i++){
                                        String url =  listOfImgs.get(i);
                                        Log.i("URLS:--> ",url);
                                        downloadImagesByDownloadManager(url,1,downloadFileCount);
//                                        downloadImages(url,1,downloadFileCount);
                                    }
                                }
                            }else{
                                AppHelper.showSnackBar(ProductDetailsActivity.this,LLMain,mainObj.getString("message"));
//                                SimpleToast.error(ProductDetailsActivity.this,mainObj.getString("message"));
                            }

//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {

//                                }
//                            },6000);


                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(ProductDetailsActivity.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.share_product);
                params.put("userid", SessionSave.getsession(AppConstants.userid,ProductDetailsActivity.this));
                params.put("productid",pId);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }
    private void downloadImages(String ImageUrl, final int cond,final int fCount) {



//        AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
//        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
//        final AlertDialog alertDialog = builder.show();
        final ProgressDialog progressBar = new ProgressDialog(ProductDetailsActivity.this);
        progressBar.setCancelable(true);
        progressBar.setMessage("File Downloading ...");
        progressBar.show();




        Glide.with(ProductDetailsActivity.this)
                .asBitmap()
                .load(ImageUrl)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull final Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                        progressBar.dismiss();
                        downloadShare.add(bitmap);
                        try {
                            String root = Environment.getExternalStorageDirectory().getPath();
                            File myDir = new File(root + "/Reeseller");

                            if (!myDir.exists()) {
                                myDir.mkdirs();
                            }

                            final String name = (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date())) + ".jpg";

                            myDir = new File(myDir, name);
                            FileOutputStream out = new FileOutputStream(myDir);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);

                            out.flush();
                            out.close();

//                            final String name = UUID.randomUUID().toString();
//                            File myDir = new File(Environment.getExternalStorageDirectory() + "/Reeseller");
//                            final File file = new File(myDir, name + ".png");
//                            try {
//                                if (!myDir.exists()) {
//                                    myDir.mkdir();
//                                }
//                                file.createNewFile();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }



                            Log.i("ListOfIms: "+listOfImgs.size() ," DownlaodPaths:"+downloadShare.size());


                            final File finalMyDir = myDir;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    String imgBitmapPath = null;
                                    try {
                                        imgBitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), finalMyDir.getAbsolutePath(),name , "Reeseller");
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                        Log.i("Exc:--> ",""+e.getMessage());
                                    }
//                                          String imgBitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(),bitmap,"title",null);
                                    Uri imgBitmapUri = Uri.parse(imgBitmapPath);
                                    downloadFileCount++;
                                    downloadFileList.add(imgBitmapUri);

                                    Log.i("fCount:-->",""+downloadFileCount+"  "+listOfImgs.size() );

                                    if(downloadFileCount == listOfImgs.size() ){


//                                        AndroidShare.Builder shareBuilder = new AndroidShare
//                                                .Builder(ProductDetailsActivity.this)
//
//                                                .setContent(TvTitle.getText().toString())
//                                                .setTitle(" Title: "+TvTitle.getText().toString());
//                                        shareBuilder.setImageUris(downloadFileList);
//                                        if(cond == 2){
//                                            shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
//                                        }
//                                        shareBuilder.build().share();


                                        if(cond == 2){
                                            AndroidShare.Builder shareBuilder = new AndroidShare
                                                    .Builder(ProductDetailsActivity.this)
                                                    .setContent(TvTitle.getText().toString())
                                                    .setTitle(" Title: "+TvTitle.getText().toString());
                                            shareBuilder.setImageUris(downloadFileList);
                                                shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
                                            shareBuilder.build().share();
                                        }else{
                                            // Use package name which we want to check
                                            boolean isAppInstalled = AppHelper.checkAppInstalledOrNot(ProductDetailsActivity.this,"com.instagram.android");

                                            if(isAppInstalled) {
                                                //This intent will help you to launch if the package is already installed

                                                openBottomShareView(TvTitle.getText().toString(),downloadFileList);
                                                Log.i("CheckINST:","Application is already installed.");
                                            } else {
                                                AndroidShare.Builder shareBuilder = new AndroidShare
                                                        .Builder(ProductDetailsActivity.this)

//                                                        .setContent(TvTitle.getText().toString())
                                                        .setTitle(" Title: "+TvTitle.getText().toString());
                                                    shareBuilder.setImageUris(downloadFileList);
                                                    shareBuilder.build().share();
                                               Log.i("CheckINST:","Application is not currently installed.");
                                            }
                                        }

                                    }
                                }
                            });



                        } catch(Exception e){
                            // some action
                            Log.i("EXC:--->",e.getMessage());
                           e.printStackTrace();
                        }
//                        if(alertDialog.isShowing()){ alertDialog.dismiss(); }
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                        Log.i("onBitmapLoaded:-->","onLoadCleared");
                    }
                });


//        Picasso.with(ProductDetailsActivity.this)
//                .load(ImageUrl)
//                .into(new Target() {
//                          @Override
//                          public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
//                              downloadShare.add(bitmap);
//                              try {
//                                  String root = Environment.getExternalStorageDirectory().toString();
//                                  File myDir = new File(root + "/Reeseller");
//
//                                  if (!myDir.exists()) {
//                                      myDir.mkdirs();
//                                  }
//
//                                  final String name = (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date())) + ".jpg";
//
//                                  myDir = new File(myDir, name);
//                                  FileOutputStream out = new FileOutputStream(myDir);
//                                  bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
//
////                                  SimpleToast.ok(activity,"Downloaded successfully");
//
//
//
//                                  out.flush();
//                                  out.close();
//
//                                  if(alertDialog.isShowing()){ alertDialog.dismiss(); }
//                                  Log.i("ListOfIms: "+listOfImgs.size() ," DownlaodPaths:"+downloadShare.size());
//
//
//                                  final File finalMyDir = myDir;
//                                  runOnUiThread(new Runnable() {
//                                      @Override
//                                      public void run() {
//
//                                        String imgBitmapPath = null;
//                                          try {
//                                              imgBitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), finalMyDir.getAbsolutePath(),name , "Reeseller");
//                                          } catch (FileNotFoundException e) {
//                                              e.printStackTrace();
//                                              Log.i("Exc:--> ",""+e.getMessage());
//                                          }
////                                          String imgBitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(),bitmap,"title",null);
//                                          Uri imgBitmapUri = Uri.parse(imgBitmapPath);
//                                          downloadFileCount++;
//                                          downloadFileList.add(imgBitmapUri);
//
//                                          Log.i("fCount:-->",""+downloadFileCount+"  "+listOfImgs.size() );
//
//                                          if(downloadFileCount == listOfImgs.size() ){
//
//
//                                              AndroidShare.Builder shareBuilder = new AndroidShare
//                                                      .Builder(ProductDetailsActivity.this)
//
//                                                      .setContent(TvTitle.getText().toString())
//                                                      .setTitle(" Title: "+TvTitle.getText().toString());
//                                                  shareBuilder.setImageUris(downloadFileList);
//                                              if(cond == 2){
//                                                  shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
//                                              }
//                                                  shareBuilder.build().share();
//
//                                          }
//                                      }
//                                  });
//
//                              } catch(Exception e){
//                                  // some action
//                              }
//                          }
//
//                          @Override
//                          public void onBitmapFailed(Drawable errorDrawable) {
//                              if(alertDialog.isShowing()){ alertDialog.dismiss(); }
//                          }
//
//                          @Override
//                          public void onPrepareLoad(Drawable placeHolderDrawable) {
//                              if(alertDialog.isShowing()){ alertDialog.dismiss(); }
//                          }
//                      }
//                );

    }
    private void downloadImagesByDownloadManager(String ImageUrl, final int condition,final int fCount) {



//        AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
//        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
//        final AlertDialog alertDialog = builder.show();
        final ProgressDialog progressBar = new ProgressDialog(ProductDetailsActivity.this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Downloading ...");
        progressBar.show();

        final DownloadManager downloadManager = (DownloadManager) ((Activity) ProductDetailsActivity.this).getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(ImageUrl));
        String fname = new Date().getTime() + ".jpg";
        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE).setAllowedOverMetered(true)
                .setAllowedOverRoaming(true).setTitle("Reeseller - " + "Downloading " + ImageUrl).
                setVisibleInDownloadsUi(true)
                .setDestinationInExternalPublicDir("Reeseller" + "/", fname);

        BroadcastReceiver onComplete = new BroadcastReceiver() {

            public void onReceive(Context ctxt, Intent intent) {
                progressBar.dismiss();
                //Download complete

                final long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                if (downloadId == 0) return;

//                Log.i("URI:-->",""+dURI.getPath());
                if(checkDulplicateUris.contains(downloadId)){
                }else{
                    downloadFileCount++;
                    checkDulplicateUris.add(downloadId);
                    Uri dURI = downloadManager.getUriForDownloadedFile(downloadId);
                    downloadFileList.add(dURI);

                    Log.i("BFR Share:--> ",""+downloadFileCount+" = "+listOfImgs.size() );
                    if(downloadFileCount == listOfImgs.size() ){
                        Log.i("Share:--> ",""+downloadFileCount+" = "+listOfImgs.size() );
                        AndroidShare.Builder shareBuilder = new AndroidShare
                                .Builder(ProductDetailsActivity.this)
                            .setContent(TvTitle.getText().toString())
                            .setTitle(" Title: "+TvTitle.getText().toString());
                        shareBuilder.setImageUris(downloadFileList);
                        if(condition == 2){
                            shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
                        }
                        shareBuilder.build().share();
                    }

                }

//                final Cursor cursor = downloadManager.query(
//                        new DownloadManager.Query().setFilterById(downloadId));
//
//                if (cursor.moveToFirst()) {
//                    final String downloadedTo = cursor.getString(
//                            cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
//                    Log.d("TAG", "The file has been downloaded to: " + downloadedTo);
//
//                }




            }
        };
        downloadManager.enqueue(request);
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));


//        Glide.with(ProductDetailsActivity.this)
//                .asBitmap()
//                .load(ImageUrl)
//                .into(new CustomTarget<Bitmap>() {
//                    @Override
//                    public void onResourceReady(@NonNull final Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
//                        progressBar.dismiss();
//                        downloadShare.add(bitmap);
//                        try {
//                            String root = Environment.getExternalStorageDirectory().getPath();
//                            File myDir = new File(root + "/Reeseller");
//
//                            if (!myDir.exists()) {
//                                myDir.mkdirs();
//                            }
//
//                            final String name = (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date())) + ".jpg";
//
//                            myDir = new File(myDir, name);
//                            FileOutputStream out = new FileOutputStream(myDir);
//                            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
//
//                            out.flush();
//                            out.close();
//
////                            final String name = UUID.randomUUID().toString();
////                            File myDir = new File(Environment.getExternalStorageDirectory() + "/Reeseller");
////                            final File file = new File(myDir, name + ".png");
////                            try {
////                                if (!myDir.exists()) {
////                                    myDir.mkdir();
////                                }
////                                file.createNewFile();
////                            } catch (IOException e) {
////                                e.printStackTrace();
////                            }
//
//
//
//                            Log.i("ListOfIms: "+listOfImgs.size() ," DownlaodPaths:"+downloadShare.size());
//
//
//                            final File finalMyDir = myDir;
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//
//                                    String imgBitmapPath = null;
//                                    try {
//                                        imgBitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), finalMyDir.getAbsolutePath(),name , "Reeseller");
//                                    } catch (FileNotFoundException e) {
//                                        e.printStackTrace();
//                                        Log.i("Exc:--> ",""+e.getMessage());
//                                    }
////                                          String imgBitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(),bitmap,"title",null);
//                                    Uri imgBitmapUri = Uri.parse(imgBitmapPath);
//                                    downloadFileCount++;
//                                    downloadFileList.add(imgBitmapUri);
//
//                                    Log.i("fCount:-->",""+downloadFileCount+"  "+listOfImgs.size() );
//
//                                    if(downloadFileCount == listOfImgs.size() ){
//
//
////                                        AndroidShare.Builder shareBuilder = new AndroidShare
////                                                .Builder(ProductDetailsActivity.this)
////
////                                                .setContent(TvTitle.getText().toString())
////                                                .setTitle(" Title: "+TvTitle.getText().toString());
////                                        shareBuilder.setImageUris(downloadFileList);
////                                        if(cond == 2){
////                                            shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
////                                        }
////                                        shareBuilder.build().share();
//
//
//                                        if(cond == 2){
//                                            AndroidShare.Builder shareBuilder = new AndroidShare
//                                                    .Builder(ProductDetailsActivity.this)
//                                                    .setContent(TvTitle.getText().toString())
//                                                    .setTitle(" Title: "+TvTitle.getText().toString());
//                                            shareBuilder.setImageUris(downloadFileList);
//                                                shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
//                                            shareBuilder.build().share();
//                                        }else{
//                                            // Use package name which we want to check
//                                            boolean isAppInstalled = AppHelper.checkAppInstalledOrNot(ProductDetailsActivity.this,"com.instagram.android");
//
//                                            if(isAppInstalled) {
//                                                //This intent will help you to launch if the package is already installed
//
//                                                openBottomShareView(TvTitle.getText().toString(),downloadFileList);
//                                                Log.i("CheckINST:","Application is already installed.");
//                                            } else {
//                                                AndroidShare.Builder shareBuilder = new AndroidShare
//                                                        .Builder(ProductDetailsActivity.this)
//
////                                                        .setContent(TvTitle.getText().toString())
//                                                        .setTitle(" Title: "+TvTitle.getText().toString());
//                                                    shareBuilder.setImageUris(downloadFileList);
//                                                    shareBuilder.build().share();
//                                               Log.i("CheckINST:","Application is not currently installed.");
//                                            }
//                                        }
//
//                                    }
//                                }
//                            });
//
//
//
//                        } catch(Exception e){
//                            // some action
//                            Log.i("EXC:--->",e.getMessage());
//                           e.printStackTrace();
//                        }
////                        if(alertDialog.isShowing()){ alertDialog.dismiss(); }
//                    }
//
//                    @Override
//                    public void onLoadCleared(@Nullable Drawable placeholder) {
//                        Log.i("onBitmapLoaded:-->","onLoadCleared");
//                    }
//                });


//        Picasso.with(ProductDetailsActivity.this)
//                .load(ImageUrl)
//                .into(new Target() {
//                          @Override
//                          public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
//                              downloadShare.add(bitmap);
//                              try {
//                                  String root = Environment.getExternalStorageDirectory().toString();
//                                  File myDir = new File(root + "/Reeseller");
//
//                                  if (!myDir.exists()) {
//                                      myDir.mkdirs();
//                                  }
//
//                                  final String name = (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date())) + ".jpg";
//
//                                  myDir = new File(myDir, name);
//                                  FileOutputStream out = new FileOutputStream(myDir);
//                                  bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
//
////                                  SimpleToast.ok(activity,"Downloaded successfully");
//
//
//
//                                  out.flush();
//                                  out.close();
//
//                                  if(alertDialog.isShowing()){ alertDialog.dismiss(); }
//                                  Log.i("ListOfIms: "+listOfImgs.size() ," DownlaodPaths:"+downloadShare.size());
//
//
//                                  final File finalMyDir = myDir;
//                                  runOnUiThread(new Runnable() {
//                                      @Override
//                                      public void run() {
//
//                                        String imgBitmapPath = null;
//                                          try {
//                                              imgBitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), finalMyDir.getAbsolutePath(),name , "Reeseller");
//                                          } catch (FileNotFoundException e) {
//                                              e.printStackTrace();
//                                              Log.i("Exc:--> ",""+e.getMessage());
//                                          }
////                                          String imgBitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(),bitmap,"title",null);
//                                          Uri imgBitmapUri = Uri.parse(imgBitmapPath);
//                                          downloadFileCount++;
//                                          downloadFileList.add(imgBitmapUri);
//
//                                          Log.i("fCount:-->",""+downloadFileCount+"  "+listOfImgs.size() );
//
//                                          if(downloadFileCount == listOfImgs.size() ){
//
//
//                                              AndroidShare.Builder shareBuilder = new AndroidShare
//                                                      .Builder(ProductDetailsActivity.this)
//
//                                                      .setContent(TvTitle.getText().toString())
//                                                      .setTitle(" Title: "+TvTitle.getText().toString());
//                                                  shareBuilder.setImageUris(downloadFileList);
//                                              if(cond == 2){
//                                                  shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
//                                              }
//                                                  shareBuilder.build().share();
//
//                                          }
//                                      }
//                                  });
//
//                              } catch(Exception e){
//                                  // some action
//                              }
//                          }
//
//                          @Override
//                          public void onBitmapFailed(Drawable errorDrawable) {
//                              if(alertDialog.isShowing()){ alertDialog.dismiss(); }
//                          }
//
//                          @Override
//                          public void onPrepareLoad(Drawable placeHolderDrawable) {
//                              if(alertDialog.isShowing()){ alertDialog.dismiss(); }
//                          }
//                      }
//                );

    }

    private void openBottomShareView(final String title, final ArrayList<Uri> imgBitmapUri) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(ProductDetailsActivity.this);
        final View parentView = getLayoutInflater().inflate(R.layout.share_instagram_otherview,null);
        bottomSheetDialog.setContentView(parentView);
        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View)parentView.getParent());
        bottomSheetBehavior.setPeekHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,1000,getResources().getDisplayMetrics() ));

// change the state of the bottom sheet
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetDialog.show();



        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        TextView TvShareTitle = (TextView) parentView.findViewById(R.id.tvTitle);   TvShareTitle.setText(title);
        final LinearLayout LLInstagram = (LinearLayout) parentView.findViewById(R.id.llInstagram);
        final LinearLayout LLInstagramStories = (LinearLayout) parentView.findViewById(R.id.llInstagramStories); LLInstagramStories.setVisibility(View.GONE);
        final LinearLayout LLMore = (LinearLayout) parentView.findViewById(R.id.llMore);

        final List<String> headinglists = new ArrayList<String>();
        for(int i=0;i<imgBitmapUri.size();i++){
            headinglists.add(title);
        }

        LLInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLInstagram);
                bottomSheetDialog.dismiss();
//                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//                shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
//                shareIntent.setType("image/*");
//                shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imgBitmapUri); // set uri
//
//                shareIntent.setPackage("com.instagram.android");
//                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                startActivity(shareIntent);


                Intent feedIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                feedIntent.setType("image/*");
//                feedIntent.putExtra(Intent.EXTRA_STREAM, imgBitmapUri.get(0));
//                feedIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imgBitmapUri);
//                feedIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, (ArrayList<? extends Parcelable>) imgBitmapUri);
                feedIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, (ArrayList<? extends Parcelable>) imgBitmapUri);
//                feedIntent.putExtra(Intent.EXTRA_TEXT, title);
                feedIntent.setPackage("com.instagram.android");

                Intent storiesIntent = new Intent("com.instagram.share.ADD_TO_STORY");
                storiesIntent.setDataAndType(imgBitmapUri.get(0),  "jpg");
                storiesIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                storiesIntent.setPackage("com.instagram.android");
                grantUriPermission(
                        "com.instagram.android", imgBitmapUri.get(0), Intent.FLAG_GRANT_READ_URI_PERMISSION);

                Intent chooserIntent = Intent.createChooser(feedIntent, title);
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {storiesIntent});
                startActivity(chooserIntent);


//                /// Method 1 : Optimize
//                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//                shareIntent.setType("image/*"); // set mime type
//                shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imgBitmapUri);
//                shareIntent.setPackage("com.instagram.android");
//                startActivity(shareIntent);


                // Intent for action_send
//		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//		shareIntent.setType("image/*"); // set mime type
//		shareIntent.putExtra(Intent.EXTRA_STREAM,uri); // set uri
//
//		//following logic is to avoide option menu, If you remove following logic then android will display list of application which support image/* mime type
//		PackageManager pm = getPackageManager();
//		List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
//		for (final ResolveInfo app : activityList) {
//		    if ((app.activityInfo.name).contains("instagram")) { // search for instagram from app list
//		        final ActivityInfo activity = app.activityInfo;
//		        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
//		        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//		        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |             Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
//		        shareIntent.setComponent(name); // set component
//		        startActivity(shareIntent);
//		        break;
//		   }
//		}
            }
        });
        LLInstagramStories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLInstagramStories);
                bottomSheetDialog.dismiss();

//                Intent storiesIntent = new Intent("com.instagram.share.ADD_TO_STORY");
//                storiesIntent.setDataAndType(imgBitmapUri, "image/*");
//                storiesIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                storiesIntent.setPackage("com.instagram.android");
//                grantUriPermission("com.instagram.android", imgBitmapUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                startActivity(storiesIntent);
            }
        });
        LLMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(LLMore);
                bottomSheetDialog.dismiss();
                AndroidShare.Builder shareBuilder = new AndroidShare
                        .Builder(ProductDetailsActivity.this)
                        .setContent(title)
                        .setTitle(title);
//                            shareBuilder.setImageUris(downloadFileList);
                shareBuilder.setImageUris(imgBitmapUri);
                shareBuilder.build().share();

            }
        });


    }//bottomSheet



    private void AddToCart() {



        AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("AddToCart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("AddToCart:", "success! response: " + response.toString());
                       try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                AppHelper.showSnackBar(ProductDetailsActivity.this,LLMain,"Item is added to cart");
//                                SimpleToast.ok(ProductDetailsActivity.this,"Item is added to cart");
                                updateBadgeCount();
                            }else{
                                AppHelper.showSnackBar(ProductDetailsActivity.this,LLMain,mainObj.getString("message"));
//                                SimpleToast.ok(ProductDetailsActivity.this,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(ProductDetailsActivity.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.addtocart);
                params.put("productid",pId);
                params.put("userid", SessionSave.getsession(AppConstants.userid,ProductDetailsActivity.this));
                params.put("quantity",""+NumbPicker.getValue());
                params.put("attribute",""+seleAttr );
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }
    private void AddToWidhList(final int cond) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// {"status":"success","message":"Product added to wishlist"}
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("AddToWishList:", "success! response: " + response.toString());
                       try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                if(cond == 1) {
                                    TvLoveSymbol.setText(getResources().getString(R.string.fai_love));
                                }else{
                                    TvLoveSymbol.setText(getResources().getString(R.string.fai_lovewb));
                                }

                                AppHelper.showSnackBar(ProductDetailsActivity.this,LLMain,mainObj.getString("message"));
//                                SimpleToast.ok(ProductDetailsActivity.this,mainObj.getString("message"));
                            }else{
                                AppHelper.showSnackBar(ProductDetailsActivity.this,LLMain,mainObj.getString("message"));
//                                SimpleToast.ok(ProductDetailsActivity.this,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(ProductDetailsActivity.this, AppConstants.noInternetConnection  );
                        }
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                if(cond == 1) {
                    params.put("action", ApiHelper.addtowishlist);
                }else{
                    params.put("action",ApiHelper.removefromwishlist);
                }
                params.put("productid",pId);
                params.put("userid", SessionSave.getsession(AppConstants.userid,ProductDetailsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }

    private void getProductDetails() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Product:", "success! response: " + response.toString());
                       //  //{"status":"success","data":[{"id":"7","title":"Darmavaram silk","price":"250","discountprice":"300","image":"http:\/\/sampletemplates.net.in\/reseller\/assets\/image\/black_db_products\/19238085321570849839.jpg","category":null,"features":"Soft and Compfort","description":"Homemade sari","attributes":"X,L","wishliststatus":0}]}
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            JSONArray mainArray = new JSONArray(mainObj.getString("data"));
                            if(mainArray.length()>0){
                                JSONObject obj = mainArray.getJSONObject(0);
                                TvCategory.setText(obj.getString("category"));
                                TvTitle.setText(obj.getString("title"));
                                TvPrice.setText("₹"+obj.getString("discountprice")+".00");
                                TvDiscountPrice.setText("₹"+obj.getString("price")+".00");    TvDiscountPrice.setPaintFlags(TvDiscountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                                Double discPrice = Double.parseDouble(obj.getString("price")) - Double.parseDouble(obj.getString("discountprice"));

                                Double discperc = (discPrice/Double.parseDouble(obj.getString("price")))*100;
                                DecimalFormat df = new DecimalFormat("#.##");
                                String dx=df.format(discperc);
                                discperc=Double.valueOf(dx);
                                TvDiscPercentage.setText(""+(int) Math.round(discperc)+"% Off");
                                String url = obj.getString("image");
                                listOfImgs = AppHelper.getImageURL(url);
                                if(listOfImgs.size()>0){
                                    for(int i=0;i<listOfImgs.size();i++){
                                        Log.i("Images:--> "+i," "+listOfImgs.get(i));
                                        switch (i){
                                            case 0:    loadImages(listOfImgs.get(i),Img1);  break;
                                            case 1:    loadImages(listOfImgs.get(i),Img2);  break;
                                            case 2:    loadImages(listOfImgs.get(i),Img3);  break;
                                        }
                                    }
                                }

                                if(listOfImgs.size()>2){
                                    TvProductsCount.setVisibility(View.VISIBLE);
                                    int count = listOfImgs.size()-1;
                                    TvProductsCount.setText("+"+count);
                                }else{
                                    TvProductsCount.setVisibility(View.GONE);
                                }

                                TvFeatures.setText(obj.getString("features"));
                                TvDesc.setText(obj.getString("description"));
                                TvPincode.setText(obj.getString("hsncode"));

                                setAttributesToViews(obj.getString("attributes"));

                                 shareWhatsAppContent = "Product Details: \n"+obj.getString("title")+"\n"+"Price:"+"₹"+obj.getString("price")+"\n"+"Discount price:"+"₹"+discperc;


                                String wishList = obj.getString("wishliststatus");
                                if(wishList.equalsIgnoreCase("0")){
                                    isWishList = false;
                                    TvLoveSymbol.setText(getResources().getString(R.string.fai_lovewb));
                                }else{
                                    isWishList = true;
                                    TvLoveSymbol.setText(getResources().getString(R.string.fai_love));
                                }


                            }else{
                                AppHelper.showSnackBar(ProductDetailsActivity.this,LLMain,"Empty List Getting.");
//                                SimpleToast.error(ProductDetailsActivity.this,"Empty List Getting.");
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(ProductDetailsActivity.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.productdetail);
                params.put("productid",pId);
                params.put("userid", SessionSave.getsession(AppConstants.userid,ProductDetailsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);



    }

    private void loadImages(String url, ImageView img) {
        if(!url.isEmpty()) {
//            Picasso.with(mContext).load(singleItem.getImage()).into(holder.itemImage);

            AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
            builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
            final AlertDialog alertDialog = builder.show();

            Picasso.with(ProductDetailsActivity.this)
                    .load(url)
                    .into(img, new Callback() {
                        @Override
                        public void onSuccess() {
                            if(alertDialog.isShowing()){alertDialog.dismiss();}
//                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            if(alertDialog.isShowing()){alertDialog.dismiss();}
                        }
                    });

        }

    }

    private void updateBadgeCount() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);


                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                int notifBadgeNumber = Integer.parseInt(mainObj.getString("cartcount"));
                                if(notifBadgeNumber>0){
                                    MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(notifBadgeNumber);
                                }else{
                                    MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(0);
                                }
                            }else{
                                MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(0);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(ProductDetailsActivity.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",AppConstants.cartcount);
                params.put("userid",SessionSave.getsession(AppConstants.userid,ProductDetailsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(AppConstants.volleyTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });

        mQueue.add(sr);



    }//LogInByVolley


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.wish_wallet_cart_menu, menu);

        menuItemCart = menu.findItem(R.id.miok);
        MenuItemBadge.update(this, menuItemCart, new MenuItemBadge.Builder()
                .iconDrawable(ContextCompat.getDrawable(this, R.drawable.ic_cart))
                .iconTintColor(Color.WHITE)
                .textBackgroundColor(Color.parseColor("#36B100"))
                .textColor(Color.WHITE));
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
//            case R.id.miSearch:
//                Intent sin = new Intent(ProductDetailsActivity.this,SearchActivity.class);
//                startActivity(sin);
//                break;
            case R.id.miCompose:
                Intent in = new Intent(ProductDetailsActivity.this,WishListItemsActivity.class);
                startActivity(in);
                finish();
                break;
            case R.id.miWallet:
                Intent in_w = new Intent(ProductDetailsActivity.this, ResellerWalletActivity.class);
                in_w.putExtra(AppConstants.wallet_transactions,ApiHelper.transactions_reseller);
                startActivity(in_w);
                finish();
                break;
            case R.id.miok:
                if(new  CartItemsActivity().isRunning == false) {
                    Intent min = new Intent(ProductDetailsActivity.this, CartItemsActivity.class);
                    startActivity(min);
                    finish();
                }
                break;
//            case R.id.miADD:
//                Intent addin = new Intent(ProductDetailsActivity.this, AddProductActivity.class);
//                startActivity(addin);
//                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void setAttributesToViews(String att) {




        LinearLayout layout = (LinearLayout) findViewById(R.id.llCheckBoxes);
//        layout.removeAllViews();


//        String[] arrOfStr = att.split("\"");
        String[] arrOfStr = att.split(",");

//            List<String>  attlist = new ArrayList<String>();
        Set<String> attlist = new HashSet<String>();
        for (String a : arrOfStr)
            attlist.add(a.trim());
        attlist.remove("[");
        attlist.remove("]");
        attlist.remove("[]");
        attlist.remove(",");
        attlist.remove("");


        List<String> aList = new ArrayList<String>(attlist);

        for(int i=0;i<aList.size();i++){
            final CheckBox cb = new CheckBox(this);
            cb.setText(aList.get(i));
//            cb.setTextSize(27);
            cb.setTextColor(Color.rgb(150, 190, 200));
            cb.setTypeface(Typeface.MONOSPACE);
            if (Build.VERSION.SDK_INT < 21) {
                CompoundButtonCompat.setButtonTintList(cb, ColorStateList.valueOf(getResources().getColor(R.color.appColor)));//Use android.support.v4.widget.CompoundButtonCompat when necessary else
            } else {
                cb.setButtonTintList(ColorStateList.valueOf(getResources().getColor(R.color.appColor)));//setButtonTintList is accessible directly on API>19
            }
            cb.setTextAppearance(ProductDetailsActivity.this,R.style.ToggleButton);
//            cb.setBackground(getResources().getDrawable(R.drawable.toggle_background));
            LLCheckBoxes.addView(cb);
//            cb.setOnCheckedChangeListener(this);
            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b == true ) {
                        final int childCount = LLCheckBoxes.getChildCount();
                        for (int i = 0; i < childCount; i++) {
                            View element = LLCheckBoxes.getChildAt(i);
                            // CheckBox
                            if (element instanceof CheckBox) {
                                CheckBox checkBox = (CheckBox) element;
                                if (checkBox.getText().toString().equalsIgnoreCase(cb.getText().toString())) {
                                    checkBox.setChecked(true);

                                } else {
                                    checkBox.setChecked(false);
                                }
                            }
                        }
                    }

                }
            });

        }//for




//        for(String s : attlist) {
//            Log.i("AttrLt:--> ", s);
//            shoHideAttributes(s.trim());
//        }
    }


    private void appendAttributes(CheckBox rb) {
        if(rb.isChecked()){
            seleAttr.append(","+rb.getText().toString().trim());
//            seleAttr =","+ seleAttr.concat(rb.getText().toString());
        }

    }


    private void openWhatsAppChat() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView( getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                String link = (mainObj.getString("whatsapplink"));

//                                Intent i = new Intent(Intent.ACTION_SENDTO, Uri.parse("content://com.android.contacts/data/" + c.getString(0)));
                                PackageManager packageManager = getPackageManager();
                                Intent i = new Intent(Intent.ACTION_VIEW);

                                try {
//                                    String url = "https://api.whatsapp.com/send?phone="+ phone +"&text=" + URLEncoder.encode(message, "UTF-8");
                                    i.setPackage("com.whatsapp");
                                    i.setData(Uri.parse(link));
                                    if (i.resolveActivity(packageManager) != null) {
                                        startActivity(i);
                                    }
                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                            }else{
                                //    Toast.makeText(LoginActivity.this,"Invalid credentials",Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(ProductDetailsActivity.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",AppConstants.adminwhatsapp);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(AppConstants.volleyTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });

        mQueue.add(sr);



    }//LogInByVolley

    public class generatePictureStyleNotification extends AsyncTask<String, Void, Bitmap> {

        private Context mContext;
        private String title, message, imageUrl;
        private  int i;

        public generatePictureStyleNotification(Context context, String title, String message, String imageUrl,int i) {
            super();
            this.mContext = context;
            this.title = title;
            this.message = message;
            this.imageUrl = imageUrl;
            this.i = i;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            try {
                URL url = new URL(this.imageUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
Log.i("OnPost:"," "+title+" "+result.toString());
//            Intent intent = new Intent(mContext, MyOpenableActivity.class);
            Intent intent = new Intent();
            intent.putExtra("key", "value");
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 100, intent, PendingIntent.FLAG_ONE_SHOT);

            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notif = new Notification.Builder(mContext)
                    .setContentIntent(pendingIntent)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(result)
                    .setStyle(new Notification.BigPictureStyle().bigPicture(result))
                    .build();
            notif.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(i, notif);
        }
    }

    private boolean checkPermission() {

        return ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ;
    }
    private void requestPermissionAndContinue() {
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle(getString(R.string.app_name));
                alertBuilder.setMessage("storage permission is nencessary");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(ProductDetailsActivity.this, new String[]{WRITE_EXTERNAL_STORAGE
                                , READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();
                Log.e("", "permission denied, show dialog");
            } else {
                ActivityCompat.requestPermissions(ProductDetailsActivity.this, new String[]{WRITE_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            }
        } else {
//            openActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
//                    openActivity();
                } else {
                    finish();
                }

            } else {
                finish();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public class RowItem {

        private Bitmap bitmapImage;

        public RowItem(Bitmap bitmapImage) {
            this.bitmapImage = bitmapImage;
        }

        public Bitmap getBitmapImage() {
            return bitmapImage;
        }

        public void setBitmapImage(Bitmap bitmapImage) {
            this.bitmapImage = bitmapImage;
        }
    }

    private class GetXMLTask extends AsyncTask<String, Integer, List<RowItem>> {
        ProgressDialog progressDialog;
        private Activity context;
        List<RowItem> rowItems;
        int noOfURLs;

        public GetXMLTask(Activity context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Downloading...");
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setIcon(R.drawable.logo);
            progressDialog.setCancelable(true);
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    // TODO Auto-generated method stub
                    Log.d("GettingCancelled","onCancel(DialogInterface dialog)");
                    task.cancel(true);
                    SimpleToast.error(ProductDetailsActivity.this,"Cancelled");
                }
            });
            progressDialog.show();
        }


        @Override
        protected List<RowItem> doInBackground(String... urls) {
            noOfURLs = urls.length;
            rowItems = new ArrayList<RowItem>();
            Bitmap map = null;
            for (String url : urls) {
                //taking 1 url at a time and downloading
                map = downloadImage(url);
                //after downloading the bitmap is added to rowitems
                rowItems.add(new RowItem(map));
                if (isCancelled()==true){
                    Log.d("GettingCancelled","isCancelled");
                    break;
                }
            }
            return rowItems;
        }

        private Bitmap downloadImage(String urlString) {

            int count = 0;
            Bitmap bitmap = null;

            URL url;
            InputStream inputStream = null;
            BufferedOutputStream outputStream = null;

            try {
                url = new URL(urlString);
                URLConnection connection = url.openConnection();
                int lenghtOfFile = connection.getContentLength();

                inputStream = new BufferedInputStream(url.openStream());
                ByteArrayOutputStream dataStream = new ByteArrayOutputStream();

                outputStream = new BufferedOutputStream(dataStream);

                byte data[] = new byte[512];
                long total = 0;

                while ((count = inputStream.read(data)) != -1) {
                    total += count;
                    Log.d("Downloading","total "+total+" Count "+count);
                    publishProgress((int) ((total * 100) / lenghtOfFile));
                    outputStream.write(data, 0, count);
                }
                outputStream.flush();

                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inSampleSize = 1;

                byte[] bytes = dataStream.toByteArray();
                bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, bmOptions);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                AppHelper.close(inputStream);
                AppHelper.close(outputStream);
            }
            return bitmap;
        }

        protected void onProgressUpdate(Integer... progress) {
            progressDialog.setProgress(progress[0]);
            if (rowItems != null) {
                progressDialog.setMessage("Loading " + (rowItems.size() + 1) + "/" + noOfURLs);
                Log.d("DownloadingFile", "Loading " + (rowItems.size() + 1) + "/" + noOfURLs);

            }
        }

        @Override
        protected void onPostExecute(List<RowItem> rowItems) {
            //passing the row items to the custom listView adapter
            List<Uri> imgsUris = new ArrayList<Uri>();
            downloadFileList = new ArrayList<Uri>();
            for(int i=0;i<rowItems.size();i++){
                RowItem mdl = rowItems.get(i);
                Log.i("URIS: ",""+mdl.getBitmapImage().toString());

                Uri uri = getImageUri(ProductDetailsActivity.this, mdl.getBitmapImage());
                downloadFileList.add(uri);
            }

            AndroidShare.Builder shareBuilder = new AndroidShare
                    .Builder(ProductDetailsActivity.this)
                    .setContent(TvTitle.getText().toString())
                    .setTitle(" Title: "+TvTitle.getText().toString());
            shareBuilder.setImageUris(downloadFileList);
            shareBuilder.setPlatform(AndroidSharePlatform.WHATSAPP);
            shareBuilder.build().share();


            progressDialog.dismiss();
        }
    }



//    @Override
//    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Log.i("AS:-->",""+compoundButton.getFontFeatureSettings());
//            Log.i("TN:-->",""+compoundButton.getTransitionName());
//        }
//Log.i("PO:-->",""+compoundButton.getPrivateImeOptions());
//Log.i("ID:-->",""+compoundButton.getId());
//
//        SimpleToast.ok(ProductDetailsActivity.this,"QAdasdf");
//    }

    public static Uri getImageUri(Activity inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

}
