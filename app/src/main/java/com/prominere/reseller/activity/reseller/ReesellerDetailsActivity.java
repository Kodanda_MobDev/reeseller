package com.prominere.reseller.activity.reseller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.helper.validations.ValidationDTO;
import com.prominere.reseller.helper.validations.ValidationHelper;
import com.prominere.reseller.helper.validations.ValidationUtils;
import com.prominere.reseller.util.CircleImageView;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.VolleyMultipartRequest;
import com.prominere.reseller.util.cropview.CropImage;
import com.prominere.reseller.util.cropview.CropImageView;
import com.prominere.reseller.util.simpleToast.SimpleToast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ReesellerDetailsActivity extends AppCompatActivity {

    private RelativeLayout RLImgs;
    private CircleImageView ImgProfilePic;
    private  EditText EtName,EtEmail,EtPhone,EtCity, EtState,EtGSTNumber,EtPincode,EtAddrs1,EtAddrs2;
    private RequestQueue mQueue;
    private Button BtnUpdate;
    private String uId;
    private String gst,vendorImgUrl;
    private static final int CAMPERMREQUEST = 112;
    private LinearLayout LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reeseller_details_activity);

        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
//        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_from_right);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        setTitle("Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);

        mQueue = Volley.newRequestQueue(ReesellerDetailsActivity.this);


        LLMain = (LinearLayout)findViewById(R.id.llMain);
        RLImgs = (RelativeLayout)findViewById(R.id.rlImgs);
        ImgProfilePic = (CircleImageView)findViewById(R.id.imgProfilePic);
        EtName = (EditText)findViewById(R.id.etname);
        EtEmail = (EditText)findViewById(R.id.etEmail);
        EtPhone = (EditText)findViewById(R.id.etPhone);
        EtCity = (EditText)findViewById(R.id.etCity);
        EtState = (EditText)findViewById(R.id.etstate);
        EtAddrs1 = (EditText)findViewById(R.id.etAddrs1);
        EtAddrs2 = (EditText)findViewById(R.id.etAddrs2);
        EtPincode = (EditText)findViewById(R.id.etPinCode);
        EtGSTNumber = (EditText)findViewById(R.id.etGSTNumber);
        BtnUpdate = (Button)findViewById(R.id.update);

        getResellerDetails();


        isGrantExternalRW(ReesellerDetailsActivity.this);

        BtnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(BtnUpdate);

                validations();


            }
        });


        RLImgs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= 23) {
                    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    if (!hasPermissions(ReesellerDetailsActivity.this, PERMISSIONS)) {
                        ActivityCompat.requestPermissions(ReesellerDetailsActivity.this, PERMISSIONS, CAMPERMREQUEST );
                    } else {
                        CropImage.activity()
                                .setGuidelines(CropImageView.Guidelines.ON)
                                .setActivityTitle("")
                                .setCropShape(CropImageView.CropShape.RECTANGLE)
                                .setCropMenuCropButtonTitle("Done")
                                .setRequestedSize(400, 400)
                                .setCropMenuCropButtonIcon(R.drawable.ic_done)
                                .start(ReesellerDetailsActivity.this);
                    }
                }else{
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setActivityTitle("")
                            .setCropShape(CropImageView.CropShape.RECTANGLE)
                            .setCropMenuCropButtonTitle("Done")
                            .setRequestedSize(400, 400)
                            .setCropMenuCropButtonIcon(R.drawable.ic_done)
                            .start(ReesellerDetailsActivity.this);
                }


            }
        });

    }//onCreate

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMPERMREQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //do here
                    if (Build.VERSION.SDK_INT >= 23) {
                        String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        if (!hasPermissions(ReesellerDetailsActivity.this, PERMISSIONS)) {
                            ActivityCompat.requestPermissions(ReesellerDetailsActivity.this, PERMISSIONS, CAMPERMREQUEST );
                        } else {
                            CropImage.activity()
                                    .setGuidelines(CropImageView.Guidelines.ON)
                                    .setActivityTitle("")
                                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                                    .setCropMenuCropButtonTitle("Done")
                                    .setRequestedSize(400, 400)
                                    .setCropMenuCropButtonIcon(R.drawable.ic_done)
                                    .start(ReesellerDetailsActivity.this);

                        }
                    }else{
                        CropImage.activity()
                                .setGuidelines(CropImageView.Guidelines.ON)
                                .setActivityTitle("")
                                .setCropShape(CropImageView.CropShape.RECTANGLE)
                                .setCropMenuCropButtonTitle("Done")
                                .setRequestedSize(400, 400)
                                .setCropMenuCropButtonIcon(R.drawable.ic_done)
                                .start(ReesellerDetailsActivity.this);

                    }
                } else {
                    Toast.makeText(ReesellerDetailsActivity.this, "The app was not allowed to read your store.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void validations() {
        try{
            ValidationHelper helper=new ValidationHelper();
            String[] strIds = getResources().getStringArray(R.array.resellerDetails_ids_array);
            String[] strErrMsgs = getResources().getStringArray(R.array.resellerDetails_errors_array);
            String[] strCompTypeArr = getResources().getStringArray(R.array.resellerDetails_comptypes_array);
            ArrayList<ValidationDTO> aList = new ArrayList<ValidationDTO>();

            int iPos = 0;
            for(String strCompType:strCompTypeArr){
                ValidationDTO valDTO=new ValidationDTO();
                valDTO.setComponentType(strCompType);
                valDTO.setComponentID(ValidationUtils.getIdResourceByName(ReesellerDetailsActivity.this,strIds[iPos]));
                valDTO.setErrorMessage(strErrMsgs[iPos]);
                aList.add(valDTO);
                iPos++;
            }
            boolean isValidData = helper.validateData(ReesellerDetailsActivity.this, aList,getWindow().getDecorView() );
            if (!isValidData) {
                return;
            }else{

//                //    updateVendorDetails();
//                if ( (vendorImgUrl.isEmpty() && filePath.isEmpty() ) || ImgProfilePic.getDrawable() == null ) {
//                    Toast.makeText(getActivity(), "Image not selected!", Toast.LENGTH_LONG).show();
//                } else {
                uploadBitmap();
//                }

            }

        }catch (Exception e){     e.printStackTrace();    }

    }

    private void uploadBitmap() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ReesellerDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        Log.i("Api:--> ", ApiHelper.appDomain);
        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, ApiHelper.appDomain,
                new Response.Listener<NetworkResponse>() {


                    @Override
                    public void onResponse(NetworkResponse response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}


                        try {

                            Log.i("Response:-->",""+new String(response.data));
                            String  resp = new String(response.data);

                            if( resp.contains("</div>")){
                                String[] sp = resp.split("</div>");

                                JSONObject obj = new JSONObject(sp[1]);
                                String sts = obj.getString("status");
                                if(sts.equalsIgnoreCase("success")){
                                    AppHelper.showSnackBar(ReesellerDetailsActivity.this,LLMain,obj.getString("message"));
//                                    SimpleToast.ok(ReesellerDetailsActivity.this,obj.getString("message"));
                                getResellerDetails();

                                }else{
                                    AppHelper.showSnackBar(ReesellerDetailsActivity.this,LLMain,"Not Updated.Please try again later.");

//                                    SimpleToast.error(ReesellerDetailsActivity.this,"Not Updated.Please try again later.");
                                }
                            }else{
                                JSONObject obj = new JSONObject(resp);
                                String sts = obj.getString("status");
                                if(sts.equalsIgnoreCase("success")){
                                    AppHelper.showSnackBar(ReesellerDetailsActivity.this,LLMain,obj.getString("message"));
//                                    SimpleToast.ok(ReesellerDetailsActivity.this,obj.getString("message"));
                                getResellerDetails();
                                }else{
                                    AppHelper.showSnackBar(ReesellerDetailsActivity.this,LLMain,"Not Updated.Please try again later." );

//                                    SimpleToast.error( ReesellerDetailsActivity.this,"Not Updated.Please try again later.");
                                }
                            }
                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.i("Response:-->",""+error.getMessage());
                        Toast.makeText( ReesellerDetailsActivity.this , error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("action", ApiHelper.updateprofile_reseller);
                params.put("userid", uId);
                params.put("name", EtName.getText().toString());
                params.put("email", EtEmail.getText().toString());
                params.put("city", EtCity.getText().toString());
                params.put("state", EtState.getText().toString());
                params.put("pincode", EtPincode.getText().toString());
                params.put("address1", EtAddrs1.getText().toString());
                params.put("address2", EtAddrs2.getText().toString());
                params.put("gst_pan", EtGSTNumber.getText().toString());
                Log.i("PARAMS:",params.toString());

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();

                if ( vendorImgUrl.isEmpty()||vendorImgUrl.equalsIgnoreCase("null")  ){

                }else{
                    if(ImgProfilePic.getDrawable() != null){
                        BitmapDrawable drawable = (BitmapDrawable) ImgProfilePic.getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
//BitmapFactory.decodeFile( easyPicker.getImagesPath().get(0))

                        params.put("userfile", new DataPart(imagename+".png", getFileDataFromDrawable(  bitmap )));

                        Log.i("PARAMS:",params.toString());

                    }
                }

                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        //adding the request to volley
        Volley.newRequestQueue(ReesellerDetailsActivity.this).add(volleyMultipartRequest);
    }
    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }



    private void getResellerDetails() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ReesellerDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                JSONArray mainArray = new JSONArray(mainObj.getString("data"));
                                JSONObject obj = mainArray.getJSONObject(0);
                                uId = obj.getString("id");
                                EtName.setText(obj.getString("name"));
                                EtEmail.setText(obj.getString("email"));
                                EtPhone.setText(obj.getString("phone"));
                                EtCity.setText(obj.getString("city"));
                                EtState.setText(obj.getString("state"));
                                EtPincode.setText(obj.getString("pincode"));
                                EtAddrs1.setText(obj.getString("address1"));
                                EtAddrs2.setText(obj.getString("address2"));
                                EtGSTNumber.setText(obj.getString("gst"));
                                vendorImgUrl = obj.getString("image");
                                gst = obj.getString("gst");

                                SessionSave.saveSession(AppConstants.image,vendorImgUrl,ReesellerDetailsActivity.this);
                                SessionSave.saveSession(AppConstants.name,obj.getString("name"),ReesellerDetailsActivity.this);
                                SessionSave.saveSession(AppConstants.phone,obj.getString("phone"),ReesellerDetailsActivity.this);

                                if( !(vendorImgUrl.isEmpty()||vendorImgUrl.equalsIgnoreCase("null") ) ){
                                    loadImage(obj.getString("image"));
                                }


                            }else{
                                Toast.makeText(ReesellerDetailsActivity.this,"Failed to get details", Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.getprofile_reseller);
                params.put("userid", SessionSave.getsession(AppConstants.userid,ReesellerDetailsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }//getResellerDetails




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                if (data != null) {
                    try {

                        Uri contentURI = result.getUri();

                        try {

                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                            // imageView.setImageBitmap(bitmap);
                            ByteArrayOutputStream bao = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);

                            ImgProfilePic.setImageBitmap(bitmap);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
     } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }

    }//onActivityResult
    private void loadImage(String image) {

        if (!image.isEmpty()){


            AlertDialog.Builder builder = new AlertDialog.Builder(ReesellerDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
            builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
            final AlertDialog alertDialog = builder.show();

            Picasso.with(ReesellerDetailsActivity.this)
                    .load(  image )
                    .into(ImgProfilePic, new Callback() {
                        @Override
                        public void onSuccess() {
                            if(alertDialog.isShowing()){ alertDialog.dismiss(); }
//                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            if(alertDialog.isShowing()){ alertDialog.dismiss(); }
                        }
                    });


//            Glide.with(getActivity()).load(image ).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().centerCrop().dontTransform())
//                    .into( ImgProfilePic );

        }else{
            ImgProfilePic.setImageDrawable(getResources().getDrawable(R.drawable.no_profile_photo));
        }
    }
    public static boolean isGrantExternalRW(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (context.checkSelfPermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

            ((Activity)context).requestPermissions(new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            },1);

            return false;
        }

        return true;
    }



    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(ReesellerDetailsActivity.this);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                break;
        }
        return true;
    }


}
