package com.prominere.reseller.activity.dashboard.account;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.activity.reseller.SearchActivity;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class OrderDetailsActivity extends AppCompatActivity {

    private TextView TvBuyerName,TvOrderDate,TvOrderId,TvStatus,TvShippingToHandle,TvAdminAmt,TvQuantity,TvGSTAmt,TvVendorAmt,TvShippingCharges,TvMarginAmt,TvOrderAmt,TvSubTotal,TvItemTotal,TvBuyerEmail,TvItemPrice,TvBuildAddrDetails,TvItemName,TvPaymentMethodDetails;
    private String orederID;
    private RequestQueue mQueue;
    private Button BtnReturn,BtnCancel,BtnSendEmail,BtnChangeStatus;
    private View localView,localView1;
    private Dialog mDialog,mDialog1;
    private LinearLayout LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_details_activity);


        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        setTitle("Order Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
        mQueue = Volley.newRequestQueue(OrderDetailsActivity.this);


        orederID = getIntent().getStringExtra(AppConstants.orderid);

        LLMain = (LinearLayout)findViewById(R.id.llMain);
        TvOrderId = (TextView)findViewById(R.id.tvOrderID);
        TvStatus = (TextView)findViewById(R.id.tvStatus);
        TvOrderDate = (TextView)findViewById(R.id.tvDate);
        TvBuyerName = (TextView)findViewById(R.id.tvBuyerName);
        TvBuyerEmail = (TextView)findViewById(R.id.tvBuyerEmail);
        TvBuildAddrDetails = (TextView)findViewById(R.id.tvBuildingAddressDetails);
        TvPaymentMethodDetails = (TextView)findViewById(R.id.tvPaymentMethodDetails);
        TvItemName = (TextView)findViewById(R.id.tvItemName);
        TvItemPrice = (TextView)findViewById(R.id.tvItemPrice);
        TvItemTotal = (TextView)findViewById(R.id.tvItemTotal);
        TvSubTotal = (TextView)findViewById(R.id.tvSubTotal);
        TvShippingToHandle = (TextView)findViewById(R.id.tvShippingToHandle);
        TvMarginAmt = (TextView)findViewById(R.id.tvMarginAmt);
        TvOrderAmt = (TextView)findViewById(R.id.tvOrderAmt);
        TvVendorAmt = (TextView)findViewById(R.id.tvVendorAmt);
        TvShippingCharges = (TextView)findViewById(R.id.tvShippingCharges);
        TvGSTAmt = (TextView)findViewById(R.id.tvGSTAmt);
        TvAdminAmt = (TextView)findViewById(R.id.tvAdminAmt);
        TvQuantity = (TextView) findViewById(R.id.tvQuantity);
        BtnChangeStatus = (Button) findViewById(R.id.btnChangeStatus);
        BtnSendEmail = (Button) findViewById(R.id.btnSendEmail);
        BtnCancel = (Button) findViewById(R.id.btnCancel);
        BtnReturn = (Button) findViewById(R.id.btnReturn);

        TextView TvbuyerInfo = (TextView)findViewById(R.id.tvBuyerInfo);
        TextView TvBuildingAddress = (TextView)findViewById(R.id.tvBuildingAddress);
        TextView TvPaymentMethod = (TextView)findViewById(R.id.tvPaymentMethod);
        TextView TvOrdDetails = (TextView)findViewById(R.id.tvOrdDetails);

        AppHelper.setUnderLineForTextview(TvbuyerInfo,"Buyer info :");
        AppHelper.setUnderLineForTextview(TvBuildingAddress,"Building Address :");
        AppHelper.setUnderLineForTextview(TvPaymentMethod,"Payment Method :");
        AppHelper.setUnderLineForTextview(TvOrdDetails,"Order Details :");

        getOrderDetails();

        BtnChangeStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(BtnChangeStatus);
                showStatusPopup();
            }
        });
        BtnSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(BtnSendEmail);
                showEmailSendPopup();
            }
        });
        BtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(BtnCancel);
                ChangeStaus(3);
            }
        });
        BtnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(BtnReturn);
                ChangeStaus(4);
            }
        });

        if(SessionSave.getsession(AppConstants.usertype,OrderDetailsActivity.this).equalsIgnoreCase("Vendor")){
            BtnChangeStatus.setVisibility(View.VISIBLE);
            BtnCancel.setVisibility(View.GONE);   BtnReturn.setVisibility(View.GONE);
        }else{
            BtnChangeStatus.setVisibility(View.GONE);
            BtnCancel.setVisibility(View.VISIBLE);   BtnReturn.setVisibility(View.GONE);
        }

    }//onCreate

    private void showStatusPopup() {

        localView = View.inflate(OrderDetailsActivity.this, R.layout.update_order_status, null);
        localView.startAnimation(AnimationUtils.loadAnimation(OrderDetailsActivity.this, R.anim.zoom_in_enter));
        this.mDialog = new Dialog(OrderDetailsActivity.this, R.style.NewDialog);
        this.mDialog.setContentView(localView);
        this.mDialog.setCancelable(true);
        this.mDialog.show();

        Window window = this.mDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER | Gravity.CENTER;
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent_background));
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.dimAmount = 0.0f;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.windowAnimations = R.anim.slide_move;

        window.setAttributes(wlp);
        window.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        List<String> stsList = new ArrayList<String>();
        stsList.add("Pending");
        stsList.add("Shipped");
        stsList.add("Delivered");
        stsList.add("Cancelled");
//        stsList.add("Return");

       final Spinner SpStatus = (Spinner) this.mDialog.findViewById(R.id.spStatus);
       final Button BtnUpdate = (Button) this.mDialog.findViewById(R.id.btnStatusUpdate);
       final ImageView ImgClose = (ImageView) this.mDialog.findViewById(R.id.closeDialog);


        ImgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDialog.isShowing()){ mDialog.dismiss(); }
            }
        });
        SpStatus.setAdapter(new ArrayAdapter<String>(OrderDetailsActivity.this,android.R.layout.simple_spinner_dropdown_item,stsList));


        BtnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.setDelayToView(BtnUpdate);
                switch (SpStatus.getSelectedItem().toString()){
                    case "Pending":    ChangeStaus(0); break;
                    case "Shipped":    ChangeStaus(1); break;
                    case "Delivered":    ChangeStaus(2); break;
                    case "Cancelled":    ChangeStaus(3); break;
                    case "Return":    ChangeStaus(4); break;
                }
            }
        });

    }
    private void showEmailSendPopup() {

        localView1 = View.inflate(OrderDetailsActivity.this, R.layout.send_email_popup, null);
        localView1.startAnimation(AnimationUtils.loadAnimation(OrderDetailsActivity.this, R.anim.zoom_in_enter));
        this.mDialog1 = new Dialog(OrderDetailsActivity.this, R.style.NewDialog);
        this.mDialog1.setContentView(localView1);
        this.mDialog1.setCancelable(true);
        this.mDialog1.show();

        Window window = this.mDialog1.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER | Gravity.CENTER;
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent_background));
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.dimAmount = 0.0f;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.windowAnimations = R.anim.slide_move;

        window.setAttributes(wlp);
        window.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

       final EditText EtEmail = (EditText) this.mDialog1.findViewById(R.id.etEmail);
       final Button BtnUpdate = (Button) this.mDialog1.findViewById(R.id.btnStatusUpdate);
       final ImageView ImgClose = (ImageView) this.mDialog1.findViewById(R.id.closeDialog);


        ImgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDialog1.isShowing()){ mDialog1.dismiss(); }
            }
        });


        BtnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if((!EtEmail.getText().toString().trim().isEmpty()) && (AppHelper.emailValidator(EtEmail.getText().toString().trim() )) ) {
                        sendEmail(EtEmail.getText().toString());
                }
                else{
                    AppHelper.setDelayToView(BtnUpdate);
                    EtEmail.setError("InValid Email Address.");
//                    SimpleToast.error(OrderDetailsActivity.this,"InValid Email Address.");
                    AppHelper.showSnackBar(OrderDetailsActivity.this,LLMain,"InValid Email Address.");
                }


            }
        });

    }

    private void ChangeStaus(final int stsId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Product:", "success! response: " + response.toString());
                        try {//{"status":"success","message":"Successfully changed the status"}
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")) {
//                                SimpleToast.ok(OrderDetailsActivity.this,mainObj.getString("message"));
                                AppHelper.showSnackBar(OrderDetailsActivity.this,LLMain,"Successfully change the status");

//                                SimpleToast.ok(OrderDetailsActivity.this,"Successfully change the status");
                                Intent in = new Intent(OrderDetailsActivity.this, OrderDetailsActivity.class);
                                in.putExtra(AppConstants.orderid,""+orederID);
                                startActivity(in);
                                if(mDialog.isShowing()){mDialog.dismiss();}
                                finish();
                            }else {
                                Log.i("OrderDetaisl:--> ","Getting Empty");
                            }


                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.orderdetail_changestatus);
                params.put("id",orederID);
                params.put("status",""+stsId);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }
    private void sendEmail(final String strEmail) {

        AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("SendEmail:", "success! response: " + response.toString());
                        try {//{"status":"success","message":"Successfully changed the status"}
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")) {
//                                SimpleToast.ok(OrderDetailsActivity.this,mainObj.getString("message"));
                                AppHelper.showSnackBar(OrderDetailsActivity.this,LLMain,mainObj.getString("message"));


                                if(mDialog1.isShowing()){mDialog1.dismiss();}
                                finish();
                            }else {
                                Log.i("OrderDetaisl:--> ",mainObj.getString("status"));
//                                SimpleToast.error(OrderDetailsActivity.this,mainObj.getString("message"));
                                AppHelper.showSnackBar(OrderDetailsActivity.this,LLMain,mainObj.getString("message"));

                            }


                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.orderdetail_sentmail);
                params.put("orderid",orederID);
                params.put("email",""+strEmail);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    private void getOrderDetails() {

        AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Product:", "success! response: " + response.toString());
                        //  //{"status":"success","data":[{"id":"7","title":"Darmavaram silk","price":"250","discountprice":"300","image":"http:\/\/sampletemplates.net.in\/reseller\/assets\/image\/black_db_products\/19238085321570849839.jpg","category":null,"features":"Soft and Compfort","description":"Homemade sari","attributes":"X,L","wishliststatus":0}]}
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){

                                    JSONObject obj = new JSONObject(mainObj.getString("data"));

                                    TvOrderId.setText("Order ID # "+obj.getString("orderid"));
                                    TvOrderDate.setText(obj.getString("orderdate"));
                                    TvBuyerName.setText(obj.getString("buyer"));
                                    TvBuyerEmail.setText(obj.getString("buyeremail"));
                                    TvStatus.setText(obj.getString("status"));
                                    TvPaymentMethodDetails.setText(obj.getString("paymentmethod"));
                                    TvQuantity.setText(obj.getString("quantity"));
                                    String iNM = "<b>"+"Item name : "+"</b>"+obj.getString("productname");
                                    TvItemName.setText(Html.fromHtml(iNM));

//                                    setRateandText(TvShippingToHandle,"Shipping to handling :",obj.getString("shipping"));
//                                    setRateandText(TvItemTotal,"Total : ",obj.getString("totalprice"));
                                    setRateandText(TvItemTotal,"Total : ",obj.getString("subtotal"));
                                    setRateandText(TvSubTotal,"Sub total : ",obj.getString("subtotal"));
                                    setRateandText(TvItemPrice,"Price : ",obj.getString("price"));

                                    setRateandText(TvVendorAmt,"Vendor amount : ",obj.getString("adminamount"));
                                    setRateandText(TvShippingCharges,"Shipping charges : ",obj.getString("shipping"));
                                    setRateandText(TvGSTAmt,"Gst amount : ",obj.getString("gst"));
//                                    setRateandText(TvAdminAmt,"Reeseller amount : ",obj.getString("vendoramount"));
                                TvAdminAmt.setVisibility(View.GONE);
                                if(SessionSave.getsession(AppConstants.usertype,OrderDetailsActivity.this).equalsIgnoreCase("Vendor")) {
                                    TvOrderAmt.setText(  Html.fromHtml(  "<b>"+"Total order amount :"+"</b>"+" ₹" +  String.format("%.2f", ( Double.parseDouble(obj.getString("totalprice")) -  Double.parseDouble(obj.getString("shipping"))  ))    ) );
//                                    setRateandText(TvOrderAmt,"Total order amount : ",obj.getString("totalprice"));
                                    TvMarginAmt.setVisibility(View.GONE);
                                }else{
                                    TvMarginAmt.setVisibility(View.VISIBLE);
                                    setRateandText(TvMarginAmt,"Margin amount : ", (obj.getString("vendoramount")) );

                                    TvOrderAmt.setText(  Html.fromHtml(  "<b>"+"Total order amount :"+"</b>"+" ₹" +  String.format("%.2f", ( Double.parseDouble(obj.getString("totalprice"))+ Double.parseDouble(obj.getString("vendoramount"))  ))    ) );
                                }

                                    String address = obj.getString("customer") + "\n" + obj.getString("phone") + "\n" + obj.getString("housenumber") + "\n" + obj.getString("street") + "\n" + obj.getString("city") + "," + obj.getString("state") + "," + obj.getString("pincode") + "\n" + "Landmark:" + obj.getString("landmark");
                                    TvBuildAddrDetails.setText(address);


       if(SessionSave.getsession(AppConstants.usertype,OrderDetailsActivity.this).equalsIgnoreCase("Vendor")){

       }else{
           switch (obj.getString("status").toLowerCase()){
               case "pending": BtnCancel.setVisibility(View.VISIBLE); BtnReturn.setVisibility(View.GONE);  break;
               case "shipped": BtnCancel.setVisibility(View.VISIBLE);BtnReturn.setVisibility(View.GONE);  break;
               case "delivered":BtnCancel.setVisibility(View.GONE); BtnReturn.setVisibility(View.GONE);  break;
               case "completed":BtnCancel.setVisibility(View.GONE); BtnReturn.setVisibility(View.GONE);  break;
               case "cancelled": BtnCancel.setVisibility(View.GONE); BtnReturn.setVisibility(View.GONE);   break;
               case "returned": BtnCancel.setVisibility(View.GONE); BtnReturn.setVisibility(View.GONE); break;
               default:    break;
           }

       }

                            }else{
                                Log.i("OrderDetaisl:--> ","Getting Empty");
                            }


                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.orderdetail);
                params.put("id",orederID);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    private void setRateandText(TextView tv,String s, String amt) {
        if(amt.isEmpty()||amt.equalsIgnoreCase("null")){
            tv.setText(Html.fromHtml("<b>"+s+"</b>"+"₹0") );
        }else{
            tv.setText(Html.fromHtml("<b>"+s+"</b> ₹"+amt+".00") );
        }

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_search, menu);
//        return true;
//    }

    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(OrderDetailsActivity.this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.miSearch:
                Intent sin = new Intent(OrderDetailsActivity.this, SearchActivity.class);
                startActivity(sin);
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
