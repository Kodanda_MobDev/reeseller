package com.prominere.reseller.activity.reseller;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.adapter.MyCatalogsAdapter;
import com.prominere.reseller.adapter.WishListItemsAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.ProductModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.badgecount.MenuItemBadge;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WishListItemsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private RequestQueue mQueue;
    private ListView LvProducts;
    private MenuItem menuItemCart;
    private LinearLayout LLEmptyView;
    private Button BtnContinueShopping;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wish_list_items_activity);



        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
//        overridePendingTransition(R.anim.enter_from_right, R.anim.enter_from_right);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
        setTitle("");
        mQueue = Volley.newRequestQueue(WishListItemsActivity.this);


        LvProducts = (ListView)findViewById(R.id.lvProducts);
        LLEmptyView = (LinearLayout)findViewById(R.id.llEmptyView);
        BtnContinueShopping = (Button)findViewById(R.id.btnContinueShop);

        BtnContinueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
//        getProducts();
        getWishlistProduct();

        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_green_dark,android.R.color.holo_orange_dark, android.R.color.holo_blue_dark);

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
//                getProducts();
                getWishlistProduct();
            }
        });


    }//omCreate
    @Override
    public void onRefresh() {
//        getProducts();
        getWishlistProduct();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateBadgeCount();
        AppHelper.checkInternetAvailability(WishListItemsActivity.this);

    }


    private void getWishlistProduct() {

        AlertDialog.Builder builder = new AlertDialog.Builder(WishListItemsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            ArrayList<ProductModel> productList = new ArrayList<ProductModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                LLEmptyView.setVisibility(View.GONE);
                                JSONArray mainArray = new JSONArray(mainObj.getString("products"));
                                if(mainArray.length()>0) {
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        ProductModel catList = new ProductModel();
                                        catList.setId(obj.getString("id"));
                                        catList.setTitle(obj.getString("title"));
                                        catList.setPrice(obj.getString("price"));
                                        catList.setDiscountprice(obj.getString("discountprice"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setCategory(obj.getString("category"));
                                        catList.setWishliststatus(obj.getString("wishliststatus"));

                                        productList.add(catList);
                                    }

                                    MyCatalogsAdapter dbradapter = new MyCatalogsAdapter(0,WishListItemsActivity.this, productList);
                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                    LLEmptyView.setVisibility(View.VISIBLE);
                                }

                            }else{
                                LLEmptyView.setVisibility(View.VISIBLE);
//                                SimpleToast.error(WishListItemsActivity.this,AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeRefreshLayout.setRefreshing(false);
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.wishlist);
                params.put("userid", SessionSave.getsession(AppConstants.userid,WishListItemsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey, ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }



    private void getProducts() {

        AlertDialog.Builder builder = new AlertDialog.Builder(WishListItemsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Category Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            ArrayList<ProductModel> productList = new ArrayList<ProductModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                LLEmptyView.setVisibility(View.GONE);
                                JSONArray mainArray = new JSONArray(mainObj.getString("products"));
                                if(mainArray.length()>0) {
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);
                                        ProductModel catList = new ProductModel();
                                        catList.setId(obj.getString("id"));
                                        catList.setTitle(obj.getString("title"));
                                        catList.setPrice(obj.getString("price"));
                                        catList.setDiscountprice(obj.getString("discountprice"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setCategory(obj.getString("category"));
                                        catList.setWishliststatus(obj.getString("wishliststatus"));

                                        productList.add(catList);
                                    }

                                    WishListItemsAdapter dbradapter = new WishListItemsAdapter(WishListItemsActivity.this, productList);
                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                    LLEmptyView.setVisibility(View.VISIBLE);
                                }

                            }else{
                                LLEmptyView.setVisibility(View.VISIBLE);
//                                SimpleToast.error(WishListItemsActivity.this,AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}
                        swipeRefreshLayout.setRefreshing(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.wishlist);
                params.put("userid", SessionSave.getsession(AppConstants.userid,WishListItemsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }

    private void updateBadgeCount() {

        AlertDialog.Builder builder = new AlertDialog.Builder(WishListItemsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);


                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                int notifBadgeNumber = Integer.parseInt(mainObj.getString("cartcount"));
                                if(notifBadgeNumber>0){
                                    MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(notifBadgeNumber);
                                }else{
                                    MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(0);
                                }
                            }else{
                                MenuItemBadge.getBadgeTextView(menuItemCart).setBadgeCount(0);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",AppConstants.cartcount);
                params.put("userid",SessionSave.getsession(AppConstants.userid,WishListItemsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(AppConstants.volleyTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//        sr.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
//            @Override
//            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
//            @Override
//            public void retry(VolleyError error) throws VolleyError {       }
//        });

        mQueue.add(sr);



    }//LogInByVolley



    @Override
        public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_wallet_cart_menu, menu);


        menuItemCart = menu.findItem(R.id.miok);
        MenuItemBadge.update(this, menuItemCart, new MenuItemBadge.Builder()
                .iconDrawable(ContextCompat.getDrawable(this, R.drawable.ic_cart))
                .iconTintColor(Color.WHITE)
                .textBackgroundColor(Color.parseColor("#36B100"))
                .textColor(Color.WHITE));
        
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.miSearch:
                Intent sin = new Intent(WishListItemsActivity.this,SearchActivity.class);
                startActivity(sin);
                finish();
                break;
            case R.id.miWallet:
                Intent win = new Intent(WishListItemsActivity.this,ResellerWalletActivity.class);
                win.putExtra(AppConstants.wallet_transactions,ApiHelper.transactions_reseller);
                startActivity(win);
                finish();
                break;
            case R.id.miok:
                if(new  CartItemsActivity().isRunning == false) {
                    Intent min = new Intent(WishListItemsActivity.this, CartItemsActivity.class);
                    startActivity(min);
                    finish();
                }
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



}
