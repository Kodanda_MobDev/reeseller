package com.prominere.reseller.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordActivity extends AppCompatActivity {

    private TextView TvConfirmPswShow,TvPswShow;
    private EditText EtphNo,EtPassword,EtConfirmPassword;
    private boolean hideSeek = false;
    private boolean chideSeek = false;
    private Button BtnSubmit;
    private String phNo;
    private Typeface fontAwesomeFont;
    private RequestQueue queue;
    private LinearLayout LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password_activity);

        phNo = getIntent().getStringExtra(AppConstants.phone);

        fontAwesomeFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");

        Toolbar toolbar = (Toolbar) findViewById(R.id.resetpsw_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Forgot Password");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
        AppHelper.setStatusBarColor(this, R.color.statusBarColor);

        queue = Volley.newRequestQueue(this);


        LLMain = (LinearLayout)findViewById(R.id.llMain);
        EtphNo = (EditText)findViewById(R.id.et_phNo);      EtphNo.setText(phNo);
        EtPassword = (EditText)findViewById(R.id.etPassword);
        TvPswShow = (TextView)findViewById(R.id.tvPswShow);      TvPswShow.setTypeface(fontAwesomeFont);
        EtConfirmPassword = (EditText)findViewById(R.id.etConfirmPassword);
        TvConfirmPswShow = (TextView)findViewById(R.id.tvConfirmPswShow);  TvConfirmPswShow.setTypeface(fontAwesomeFont);
        BtnSubmit = (Button)findViewById(R.id.btnSubmit);
        TvPswShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hideSeek) {// show password
                    TvPswShow.setTextColor(getResources().getColor(R.color.black));
                    TvPswShow.setText(R.string.fai_remove_eye);
                    EtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    hideSeek = false;

                } else { // hide password
                    TvPswShow.setTextColor(getResources().getColor(R.color.black));
                    TvPswShow.setText(R.string.fai_eye);
                    EtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    hideSeek=true;
                }
            }
        });
        TvConfirmPswShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chideSeek) {// show password
                    TvConfirmPswShow.setTextColor(getResources().getColor(R.color.black));
                    TvConfirmPswShow.setText(R.string.fai_remove_eye);
                    EtConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    chideSeek = false;

                } else { // hide password
                    TvConfirmPswShow.setTextColor(getResources().getColor(R.color.black));
                    TvConfirmPswShow.setText(R.string.fai_eye);
                    EtConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    chideSeek=true;
                }
            }
        });
        BtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!EtPassword.getText().toString().isEmpty()){
                    if(EtPassword.getText().toString().equalsIgnoreCase(EtConfirmPassword.getText().toString())){
                        resetPasword(EtphNo.getText().toString(),EtPassword.getText().toString());
                    }else{
                        AppHelper.showSnackBar(ResetPasswordActivity.this,LLMain,"Passwords are not matching" );
//                        SimpleToast.error(ResetPasswordActivity.this,"Passwords are not matching");
                    }
                }else{
                    AppHelper.showSnackBar(ResetPasswordActivity.this,LLMain,"Please Enter Password" );
//                    SimpleToast.error(ResetPasswordActivity.this,"Please Enter Password");
                }
            }
        });




    }//onCreate

    private void resetPasword(final String phno, final String psw) {
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(ResetPasswordActivity.this, R.style.AppCompatAlertDialogStyle);
            builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
            final AlertDialog alertDialog = builder.show();
            String url = ApiHelper.appDomain;
            Log.i("Api:-->",url);
            StringRequest sr = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(alertDialog.isShowing()){alertDialog.dismiss();}
                            Log.e("HttpClient", "success! response: " + response.toString());
                            //       if(mProgressDialog.isShowing()){mProgressDialog.dismiss();}
                            try {
                                JSONObject mainObj = new JSONObject(response);

                                if(mainObj.getString("status").equalsIgnoreCase("success")){
//                                    SimpleToast.ok(ResetPasswordActivity.this,mainObj.getString("message"));
//                                    SimpleToast.ok(ResetPasswordActivity.this,"Password updated successfully Please Login");
                                    AppHelper.showSnackBar(ResetPasswordActivity.this,LLMain,"Password updated successfully Please Login" );

                                    Intent in = new Intent(ResetPasswordActivity.this,LoginActivity.class);
                                    startActivity(in);
                                    finish();
                                }else{
                                    AppHelper.showSnackBar(ResetPasswordActivity.this,LLMain,"Failed to reset the password" );
//                                        SimpleToast.error(ResetPasswordActivity.this,"Failed to reset the password");
                                   }

                            }catch (Exception e){e.printStackTrace();}

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(alertDialog.isShowing()){alertDialog.dismiss();}
                            //      if(mProgressDialog.isShowing()){mProgressDialog.dismiss();}
                            Log.e("HttpClient", "error: " + error.toString());
                        }
                    })
            {
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("action",ApiHelper.resetpwd);
                    params.put("phone",phno);
                    params.put("password",psw);
                    Log.i("Ob:--> ",params.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                    return params;
                }
            };
            sr.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
                @Override
                public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
                @Override
                public void retry(VolleyError error) throws VolleyError {       }
            });

            queue.add(sr);



        }
        
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(ResetPasswordActivity.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(ResetPasswordActivity.this,ForgotPasswordActivity.class);
        in.putExtra(AppConstants.phone,phNo);
        startActivity(in);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent in = new Intent(ResetPasswordActivity.this,ForgotPasswordActivity.class);
                in.putExtra(AppConstants.phone,phNo);
                startActivity(in);
                finish();
                break;
        }
        return true;
    }

}
