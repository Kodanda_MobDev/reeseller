package com.prominere.reseller.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.prominere.reseller.R;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.util.PinEntryEditText;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends AppCompatActivity {
        EditText EtPhNo;
    private Button BtnSend;
    private RequestQueue queue;
    private Dialog mDialog;
    private LinearLayout LLMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.forgotpsw_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Forgot Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);
        AppHelper.setStatusBarColor(this, R.color.statusBarColor);


        LLMain = (LinearLayout)findViewById(R.id.llMain);
        EtPhNo = (EditText)findViewById(R.id.et_phNo);
        BtnSend = (Button)findViewById(R.id.btnSend);

        queue = Volley.newRequestQueue(this);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            String phNo = getIntent().getStringExtra(AppConstants.phone);
            EtPhNo.setText(phNo);
        }

        BtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(EtPhNo.getText().toString().isEmpty()){
                    AppHelper.showSnackBar(ForgotPasswordActivity.this,LLMain, "Enter Phone Number");
//                    SimpleToast.error(ForgotPasswordActivity.this,"Enter Phone Number");
                }else{
                    callForgotPassword();
                }

            }
        });

    }

    private void callForgotPassword() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPasswordActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();


        String url = ApiHelper.appDomain;
        Log.i("Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("forgotPSW:", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){

                            OTPpopUp(EtPhNo.getText().toString(),mainObj.getString("otp"));
                            }else{
                                AppHelper.showSnackBar(ForgotPasswordActivity.this,LLMain, "Invalid number.Please try again later.");
//                              SimpleToast.error(ForgotPasswordActivity.this,"Invalid number.Please try again later.");
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(ForgotPasswordActivity.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.forgotpwd);
                params.put("phone",EtPhNo.getText().toString());
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        queue.add(sr);



    }

    private void OTPpopUp(final String phNo, final String msgOTP ) {
        View localView = View.inflate(ForgotPasswordActivity.this, R.layout.forgotpsw_otp_activity, null);
        localView.startAnimation(AnimationUtils.loadAnimation(ForgotPasswordActivity.this, R.anim.zoom_in_enter));
        this.mDialog = new Dialog(ForgotPasswordActivity.this, R.style.NewDialog);
        this.mDialog.setContentView(localView);
        this.mDialog.setCancelable(true);
        this.mDialog.show();

        Window window = this.mDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER | Gravity.CENTER;
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent_background));
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.dimAmount = 0.0f;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.windowAnimations = R.anim.slide_move;

        window.setAttributes(wlp);
        window.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);


        final PinEntryEditText EtOTP = (PinEntryEditText) this.mDialog.findViewById(R.id.txt_pin_entry);
        ImageView imgClose = (ImageView) this.mDialog.findViewById(R.id.closeDialog);
        TextView TvResend = (TextView) this.mDialog.findViewById(R.id.tvResend);
        Button BtnSubmit = (Button) this.mDialog.findViewById(R.id.bb_OTPClient);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        BtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!EtOTP.getText().toString().equalsIgnoreCase("") ){
                    try {
                        if(msgOTP.equalsIgnoreCase(EtOTP.getText().toString())){
                            if(mDialog.isShowing()){mDialog.dismiss();}
                            Intent in = new Intent(ForgotPasswordActivity.this,ResetPasswordActivity.class);
                            in.putExtra(AppConstants.phone,phNo);
                            startActivity(in);
                            finish();

                        }else{
                            AppHelper.showSnackBar(ForgotPasswordActivity.this,LLMain, "Invalid OTP");
//                            SimpleToast.error(ForgotPasswordActivity.this,"Invalid OTP");
                        }
             //           checkOTPAsyncTask runner = new  checkOTPAsyncTask(EtOTP.getText().toString(),uName);  runner.execute();
                    }catch (Exception e){e.printStackTrace();}
                }else{
                    AppHelper.showSnackBar(ForgotPasswordActivity.this,LLMain, "OTP is not Valid");
//                    SimpleToast.error(ForgotPasswordActivity.this,"OTP is not Valid.");
                }
            }
        });
        TvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CallResendByVolley(phNo);
            }
        });

    }//popUp

    private void CallResendByVolley(final String phNo) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPasswordActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();


        String url = ApiHelper.appDomain;
        Log.i("Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("forgotPSW:", "success! response: " + response.toString());
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        try {
                            JSONObject mainObj = new JSONObject(response);

                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                            }else{
                                AppHelper.showSnackBar(ForgotPasswordActivity.this,LLMain, mainObj.getString("message"));
//                                SimpleToast.error(ForgotPasswordActivity.this,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(ForgotPasswordActivity.this, AppConstants.noInternetConnection  );
                        }

                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.resend_forgototp);
                params.put("phone",phNo);
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        queue.add(sr);



    }
    @Override
    protected void onResume() {
        super.onResume();
        AppHelper.checkInternetAvailability(ForgotPasswordActivity.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(ForgotPasswordActivity.this,LoginActivity.class);
        startActivity(in);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent in = new Intent(ForgotPasswordActivity.this,LoginActivity.class);
                startActivity(in);
                finish();
                break;
        }
        return true;
    }

}
