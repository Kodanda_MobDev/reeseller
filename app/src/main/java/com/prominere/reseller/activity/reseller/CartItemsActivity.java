package com.prominere.reseller.activity.reseller;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prominere.reseller.R;
import com.prominere.reseller.adapter.CartAdapter;
import com.prominere.reseller.helper.ApiHelper;
import com.prominere.reseller.helper.AppConstants;
import com.prominere.reseller.helper.AppHelper;
import com.prominere.reseller.model.CartModel;
import com.prominere.reseller.util.SessionSave;
import com.prominere.reseller.util.simpleToast.SimpleToast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CartItemsActivity extends AppCompatActivity {
    public static Boolean isRunning = false;
    private RequestQueue mQueue;
    private RecyclerView LvProducts;
    public static TextView TvCODCharges,TvFirstOrderrVal,TvShippingCharges,ProceedBtn,TvTotalPrice,TvProductCharges;
    private RadioGroup PaymentSelRG;
    private Button BtnContinueShop;
    private LinearLayout LLLoadData,LLEmptyView;
    private CartAdapter dbradapter;
    public static boolean cartMAmt = false;
    private LinearLayout LLMain;
    public static  CartItemsActivity ctx;
    private ArrayList<CartModel> productList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_items_activity);

        ctx =  this;

        AppHelper.setStatusBarColor(this,R.color.statusBarColor);
//        AppHelper.setupHideleyboard(this.getWindow().getDecorView(),MainNavigationActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.nav_toolbar);
        setSupportActionBar(toolbar);
        setTitle("Cart");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_icon);

        mQueue = Volley.newRequestQueue(CartItemsActivity.this);

        LLMain = (LinearLayout)findViewById(R.id.llMain);
        LvProducts = (RecyclerView)findViewById(R.id.lvProducts);
        ProceedBtn = (TextView)findViewById(R.id.proceedBtn);
        TvShippingCharges = (TextView)findViewById(R.id.tvShippingCharges);
        TvFirstOrderrVal = (TextView)findViewById(R.id.tvFirstOrderrVal);
        TvCODCharges = (TextView)findViewById(R.id.tvCODCharges);
        TvProductCharges = (TextView)findViewById(R.id.tvProductCharges);
        TvTotalPrice = (TextView)findViewById(R.id.tvTotalSum);
        LLLoadData = (LinearLayout)findViewById(R.id.llLoadData);
        LLEmptyView = (LinearLayout)findViewById(R.id.llEmptyView);
        BtnContinueShop = (Button)findViewById(R.id.btnContinueShop);
        PaymentSelRG = (RadioGroup)findViewById(R.id.paymentRadioGroup);  PaymentSelRG.check(R.id.rbCOD);

        LvProducts.setLayoutManager(new LinearLayoutManager(CartItemsActivity.this, LinearLayoutManager.VERTICAL, false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        ProceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              boolean condCheck = false; int tCount = 0; int fCunt = 0;
                for(int i=0;i<productList.size();i++){
                    CartModel mdl = productList.get(i);
                    if(mdl.getCtcType() == true){
                        condCheck = true; tCount++;
                    }else{
                        condCheck = false;  fCunt++;
                    }

                }
                if(condCheck == true && tCount ==productList.size()){
                    cartProceed();
                }
                if( condCheck == false && fCunt == productList.size() ){
                    cartProceed();
                }
                if(condCheck == true && tCount !=productList.size()){
                    SimpleToast.error(CartItemsActivity.this,"Please add cash collection for other products ");
                }
                if(condCheck == false && fCunt !=productList.size()){
                    SimpleToast.error(CartItemsActivity.this,"Please add cash collection for other products ");
                }


            }
        });

        BtnContinueShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainNavigationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        getProducts();


    }

    private void cartProceed() {
        if( cartMAmt == false ){

            if(LvProducts.getChildCount()>0){
//                    cartMAmt = false;
                Double cashCollection = 0.0;
                ArrayList<CartModel> updList = dbradapter.getcartLsit();
                for(int i=0;i<updList.size();i++) {
                    CartModel mdl = updList.get(i);
                    cashCollection = cashCollection + mdl.getCashToCollectMoney();

                    Log.i("Cart: "+mdl.getId()," "+mdl.getTitle()+"    Money:"+mdl.getCashToCollectMoney() );
                    if(mdl.getCashToCollectMoney()>0) {
                        cartMarginUpdate(i, updList.size(), mdl.getId(), mdl.getCashToCollectMoney(),cashCollection);
                    }
                }

                Intent cIn = new Intent(CartItemsActivity.this,CartProceedActivity.class);
                cIn.putExtra("ProductCharges",TvProductCharges.getText().toString());
                cIn.putExtra("ShippingCharges",TvShippingCharges.getText().toString());
                cIn.putExtra("FirstOrderrVal",TvFirstOrderrVal.getText().toString());
                cIn.putExtra("GSTCharges",TvCODCharges.getText().toString());
                cIn.putExtra("TotalPrice",TvTotalPrice.getText().toString());
                cIn.putExtra("CashCollection",""+cashCollection);
                startActivity(cIn);


            }else{
                AppHelper.showSnackBar(CartItemsActivity.this,LLMain,"Cart is empty");
//                    SimpleToast.ok(CartItemsActivity.this,"Cart is empty");
            }

        }else{
            AppHelper.showSnackBar(CartItemsActivity.this,LLMain,"Amount should be lesser than product price");
        }
    }


    private void getProducts() {

        AlertDialog.Builder builder = new AlertDialog.Builder(CartItemsActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Category:", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            productList = new ArrayList<CartModel>();
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                LLEmptyView.setVisibility(View.GONE);  LLLoadData.setVisibility(View.VISIBLE); ProceedBtn.setVisibility(View.VISIBLE);
                                JSONArray mainArray = new JSONArray(mainObj.getString("list"));
                                if(mainArray.length()>0) {
                                    int total = 0; int shipping_charge = 0;  int codCharges = 0; int foVal = 0; int productCharges = 0;int sc = 0; int gst = 0;
                                    for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject obj = mainArray.getJSONObject(i);

                                        CartModel catList = new CartModel();

                                        catList.setId(obj.getString("id"));
                                        catList.setTitle(obj.getString("title"));
                                        catList.setPrice(obj.getString("price"));
                                        catList.setImage(obj.getString("image"));
                                        catList.setCategory(obj.getString("category"));
                                        catList.setQuantity(obj.getString("quantity"));
                                        catList.setAttribute(obj.getString("attribute"));
                                        catList.setAvail_attributes(obj.getString("avail_attributes"));
                                        catList.setProductprice(obj.getString("productprice"));
                                        catList.setMargin(Integer.parseInt(obj.getString("margin")));
                                        catList.setShipping_charge(Integer.parseInt(obj.getString("shipping_charge")));
                                        catList.setGst(Integer.parseInt(obj.getString("gst")));
                                        catList.setCtcType(false);
                                        catList.setCashToCollectMoney(0.0);

                                            int productPrice =   Integer.parseInt(obj.getString("quantity"))*Integer.parseInt(obj.getString("productprice"));
                                            int sch =   Integer.parseInt(obj.getString("quantity"))*Integer.parseInt(obj.getString("shipping_charge"));
                                            int gst1 =   Integer.parseInt(obj.getString("quantity"))*Integer.parseInt(obj.getString("gst"));

                                        productCharges = productCharges+ productPrice;
                                        sc = sc+ sch;
                                        gst = gst+ gst1;

//                                        total = total+(  productPrice + (Integer.parseInt(obj.getString("shipping_charge") )+Integer.parseInt(obj.getString("gst")) +Integer.parseInt(obj.getString("margin")   )) );
                                        total = total+(  productPrice + sch + gst1 );
                                        shipping_charge = shipping_charge+( Integer.parseInt(obj.getString("shipping_charge") ) );
                                        codCharges = codCharges+( Integer.parseInt(obj.getString("gst") ) );
                                        foVal = foVal+( Integer.parseInt(obj.getString("margin") ) );
                                        productList.add(catList);
                                    }

                                    Log.d("totalcash", String.valueOf(total));
                                    TvProductCharges.setText("₹"+productCharges+".00");
                                    TvShippingCharges.setText("₹"+sc+".00");
                                    TvFirstOrderrVal.setText("₹"+foVal+".00");
                                    TvCODCharges.setText("₹"+codCharges+".00");
                                    TvTotalPrice.setText("₹"+total+".00");

//                                    CartItemsAdapter dbradapter = new CartItemsAdapter(CartItemsActivity.this, productList);
                                     dbradapter = new CartAdapter(CartItemsActivity.this, productList);

                                    LvProducts.setLayoutManager(new LinearLayoutManager(CartItemsActivity.this));

                                    LvProducts.setAdapter(dbradapter);
                                    dbradapter.notifyDataSetChanged();
//                                AppCompatAlertDialogStyleHelper.setListViewHeightBasedOnChildren(LvDB);
                                }else{
                                    LLEmptyView.setVisibility(View.VISIBLE);  LLLoadData.setVisibility(View.GONE); ProceedBtn.setVisibility(View.GONE);
                                }

                            }else{
                                LLEmptyView.setVisibility(View.VISIBLE);  LLLoadData.setVisibility(View.GONE);  ProceedBtn.setVisibility(View.GONE);
                                //   SimpleToast.error(CartItemsActivity.this, AppConstants.apiIssueMsg);
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(CartItemsActivity.this, AppConstants.noInternetConnection  );
                        }
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.cartlist);
                params.put("userid", SessionSave.getsession(AppConstants.userid,CartItemsActivity.this));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }
    private void cartMarginUpdate(final int index,final int listSize,final String cartId, final Double money, final Double totalCashCollection) {

//        AlertDialog.Builder builder = new AlertDialog.Builder(CartItemsActivity.this, R.style.AppCompatAlertDialogStyle);
//        builder.setView(getLayoutInflater().inflate(R.layout.waiting_dialog,null)).setCancelable(false);
//        final AlertDialog alertDialog = builder.show();

        String url = ApiHelper.appDomain;
        Log.i("Cart Api:-->",url);
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("Cart :", "success! response: " + response.toString());
                        try {
                            JSONObject mainObj = new JSONObject(response);
                            if(mainObj.getString("status").equalsIgnoreCase("success")){
                                if(index == listSize-1){
//                                        String paymentMethod;
//                                        String rbSel = ((RadioButton)findViewById(PaymentSelRG.getCheckedRadioButtonId())).getText().toString();
//                                        if(rbSel.toUpperCase().equalsIgnoreCase("ONLINE")){
//                                            paymentMethod = "online";
//                                        }else{
//                                            paymentMethod = "cash";
//                                        }

//                                    if(cartMAmt != true) {
//                                        Intent cIn = new Intent(CartItemsActivity.this,CartProceedActivity.class);
//                                        cIn.putExtra("ProductCharges",TvProductCharges.getText().toString());
//                                        cIn.putExtra("ShippingCharges",TvShippingCharges.getText().toString());
//                                        cIn.putExtra("FirstOrderrVal",TvFirstOrderrVal.getText().toString());
//                                        cIn.putExtra("GSTCharges",TvCODCharges.getText().toString());
//                                        cIn.putExtra("TotalPrice",TvTotalPrice.getText().toString());
//                                        cIn.putExtra("CashCollection",""+totalCashCollection);
//                                        startActivity(cIn);
//
//                                    }else{
//                                        AppHelper.showSnackBar(CartItemsActivity.this,LLMain,"Cash to collect from customer is lesser than product amount. ");
//                                    }

                                }
                            }else{
                                AppHelper.showSnackBar(CartItemsActivity.this,LLMain,mainObj.getString("message"));
                            }

                        }catch (Exception e){e.printStackTrace();}

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NoConnectionError){
                            AppHelper.notFoundExceptionDialog(CartItemsActivity.this, AppConstants.noInternetConnection  );
                        }
//                        if(alertDialog.isShowing()){alertDialog.dismiss();}
                        Log.e("HttpClient", "error: " + error.toString());
//                        if(money == 0){
//                            cartMAmt = true;
//                            SimpleToast.error(CartItemsActivity.this, "Cash to collect from customer is empty.");
//                        }
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("action",ApiHelper.cart_marginupdate);
                params.put("cartid",cartId );
                params.put("userid", SessionSave.getsession(AppConstants.userid,CartItemsActivity.this));
                params.put("margin", String.valueOf(money));
                Log.i("Ob:--> ",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put(ApiHelper.appDomainHeaderKey,ApiHelper.appDomainHeaderValue);
                return params;
            }
        };
        sr.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {  return AppConstants.volleyTimeOut;  }
            @Override
            public int getCurrentRetryCount() { return AppConstants.volleyTimeOut;  }
            @Override
            public void retry(VolleyError error) throws VolleyError {       }
        });

        mQueue.add(sr);

    }


    @Override
    protected void onStart() {
        super.onStart();
        isRunning = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isRunning = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if(isRunning == false){
//            finish();
//        }else{
            AppHelper.checkInternetAvailability(CartItemsActivity.this);
//        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
