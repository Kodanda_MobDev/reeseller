package com.prominere.reseller.model;

public class ResellerCatModel {
   int id;
   String cname;
   String slug;
   String status;
   String image;
   String productcount;
   String mainsubcategory_count;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProductcount() {
        return productcount;
    }

    public void setProductcount(String productcount) {
        this.productcount = productcount;
    }

    public String getMainsubcategory_count() {
        return mainsubcategory_count;
    }

    public void setMainsubcategory_count(String mainsubcategory_count) {
        this.mainsubcategory_count = mainsubcategory_count;
    }
}
