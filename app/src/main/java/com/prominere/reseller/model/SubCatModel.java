package com.prominere.reseller.model;

import androidx.annotation.NonNull;

public class SubCatModel {
    String id;
    String subname;
    String attributes;
    String catId;
    String catName;
    String mainSubCatId;
    String mainSubCatName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getMainSubCatId() {
        return mainSubCatId;
    }

    public void setMainSubCatId(String mainSubCatId) {
        this.mainSubCatId = mainSubCatId;
    }

    public String getMainSubCatName() {
        return mainSubCatName;
    }

    public void setMainSubCatName(String mainSubCatName) {
        this.mainSubCatName = mainSubCatName;
    }

    @NonNull
    @Override
    public String toString() {
        return subname;
    }

}
