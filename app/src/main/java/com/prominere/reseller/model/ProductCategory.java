package com.prominere.reseller.model;

import org.json.JSONArray;

public class ProductCategory {
    String id;
    String cname;
    String slug;
    String status;
    String image;
    JSONArray subcategories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public JSONArray getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(JSONArray subcategories) {
        this.subcategories = subcategories;
    }
}
