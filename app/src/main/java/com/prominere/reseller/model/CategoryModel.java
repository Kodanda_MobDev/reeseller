package com.prominere.reseller.model;

import androidx.annotation.NonNull;

public class CategoryModel {
    String id;
    String cname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }


    @NonNull
    @Override
    public String toString() {
        return cname;
    }

}
