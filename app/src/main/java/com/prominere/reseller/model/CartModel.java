package com.prominere.reseller.model;

public class CartModel   {
    String id;
    String title;
    String price;
    String image;
    String category;
    String quantity;
    String attribute;
    String avail_attributes;
    String productprice;
    int margin;
    int shipping_charge;
    int gst;
    Double cashToCollectMoney;
    boolean ctcType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getAvail_attributes() {
        return avail_attributes;
    }
    public void setAvail_attributes(String avail_attributes) {
        this.avail_attributes = avail_attributes;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }

    public Double getCashToCollectMoney() {
        return cashToCollectMoney;
    }

    public void setCashToCollectMoney(Double cashToCollectMoney) {
        this.cashToCollectMoney = cashToCollectMoney;
    }

    public int getMargin() {
        return margin;
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }

    public int getShipping_charge() {
        return shipping_charge;
    }

    public void setShipping_charge(int shipping_charge) {
        this.shipping_charge = shipping_charge;
    }

    public int getGst() {
        return gst;
    }

    public void setGst(int gst) {
        this.gst = gst;
    }


    public boolean getCtcType() {
        return ctcType;
    }

    public void setCtcType(boolean ctcType) {
        this.ctcType = ctcType;
    }

    @Override
    public String toString() {
        return "CartModel{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", price='" + price + '\'' +
                ", image='" + image + '\'' +
                ", category='" + category + '\'' +
                ", quantity='" + quantity + '\'' +
                ", attribute='" + attribute + '\'' +
                '}';
    }
}
