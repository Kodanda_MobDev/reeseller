package com.prominere.reseller.model;

public class ProductModel {
   String id;
   String title;
   String price;
   String discountprice;
   String image;
   String category;
   String wishliststatus;

    public ProductModel( ) {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscountprice() {
        return discountprice;
    }

    public void setDiscountprice(String discountprice) {
        this.discountprice = discountprice;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getWishliststatus() {
        return wishliststatus;
    }

    public void setWishliststatus(String wishliststatus) {
        this.wishliststatus = wishliststatus;
    }
}
