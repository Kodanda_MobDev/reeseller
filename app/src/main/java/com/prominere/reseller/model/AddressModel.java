package com.prominere.reseller.model;

public class AddressModel {
    String id;
    String customername;
    String phone;
    String housenumber;
    String street;
    String city;
    String landmark;
    String state;
    String pincode;
    Boolean rbselection;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public Boolean getRbselection() {
        return rbselection;
    }

    public void setRbselection(Boolean rbselection) {
        this.rbselection = rbselection;
    }

    @Override
    public String toString() {
        return "AddressModel{" +
                "id='" + id + '\'' +
                ", customername='" + customername + '\'' +
                ", phone='" + phone + '\'' +
                ", pincode='" + pincode + '\'' +
                ", rbselection=" + rbselection +
                '}';
    }
}
