package com.prominere.reseller.model;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DBIconsView {
    LinearLayout linearLayout;
    ImageView imageView;
    TextView textView;
    TextView heaadbgColor;

    public DBIconsView(LinearLayout ll, ImageView iMg, TextView tv,TextView heaadbgColor) {
        this.linearLayout = ll;
        this.imageView = iMg;
        this.textView = tv;
        this.heaadbgColor = heaadbgColor;
    }

    public LinearLayout getLinearLayout() {
        return linearLayout;
    }

    public void setLinearLayout(LinearLayout linearLayout) {
        this.linearLayout = linearLayout;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public TextView getTextView() {
        return textView;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }

    public TextView getHeaadbgColor() {
        return heaadbgColor;
    }

    public void setHeaadbgColor(TextView heaadbgColor) {
        this.heaadbgColor = heaadbgColor;
    }
}
