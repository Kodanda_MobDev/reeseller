package com.prominere.reseller.model;

public class ProductSubCategory {
    String id;
    String cname;
    String slug;
    String category;
    String attr;
    String status;
    String image;
    String productcount;
    String subcategorycount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAttr() {
        return attr;
    }

    public void setAttr(String attr) {
        this.attr = attr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProductcount() {
        return productcount;
    }

    public void setProductcount(String productcount) {
        this.productcount = productcount;
    }

    public String getSubcategorycount() {
        return subcategorycount;
    }

    public void setSubcategorycount(String subcategorycount) {
        this.subcategorycount = subcategorycount;
    }

    @Override
    public String toString() {
        return "ProductSubCategory{" +
                "id='" + id + '\'' +
                ", cname='" + cname + '\'' +
                ", slug='" + slug + '\'' +
                ", status='" + status + '\'' +
                ", image='" + image + '\'' +
                ", productcount='" + productcount + '\'' +
                '}';
    }
}
