package com.prominere.reseller.model;

import androidx.annotation.NonNull;

import org.json.JSONArray;

public class MainSubCatModel {
    String catName;
    String catId;
    String mainSubCatId;
    String mainSubCatname;
    JSONArray mainsubname;

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getMainSubCatId() {
        return mainSubCatId;
    }

    public void setMainSubCatId(String mainSubCatId) {
        this.mainSubCatId = mainSubCatId;
    }

    public String getMainSubCatname() {
        return mainSubCatname;
    }

    public void setMainSubCatname(String mainSubCatname) {
        this.mainSubCatname = mainSubCatname;
    }

    public JSONArray getMainsubname() {
        return mainsubname;
    }

    public void setMainsubname(JSONArray mainsubname) {
        this.mainsubname = mainsubname;
    }
    @NonNull
    @Override
    public String toString() {
        return mainSubCatname;
    }
}
