package com.prominere.reseller.model;

public class CatSelectionModel {


    boolean isSelected;
    String catName;
    String catId;

    //now create constructor and getter setter method using shortcut like command+n for mac & Alt+Insert for window.


    public CatSelectionModel(boolean isSelected, String catName, String catId) {
        this.isSelected = isSelected;
        this.catName = catName;
        this.catId = catId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    @Override
    public String toString() {
        return "CatSelectionModel{" +
                "isSelected=" + isSelected +
                ", catName='" + catName + '\'' +
                '}';
    }
}
